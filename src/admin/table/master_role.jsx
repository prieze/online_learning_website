import React, { useState, Fragment, Component } from 'react';
import ReactDOM from 'react-dom';
import axios from "axios";
import {Table, Space} from 'antd';
import Navigation from "../../component/Navigation/Navigation";
import { Button } from "react-bootstrap";
import Post from "./../../component/Post/Post";

// import role from './../data';

const {Column} = Table;

// function Roles() {
//     const [page, setPage] = useState(1);
//     const [roles, setRoles] = useState([]);
//     const [isLoading, setLoading] = useState(true);
// }

// const loadMore = () => {
//     setPage(page + 1);
// }

// useEffect(() => {
//     fetch(
//         `https://api-video-solman.herokuapp.com/api/read/roles`,
//         {
//             method
//         }
//     )
// })

// const Roles = () => {
class Roles extends Component {
// export default class Roles extends Component {
    // const [hasError, setErrors] = useState(false);
    // const [roles, setRoles] = useState({});

    // state = {
    //     // hasError: false,
    //     // roles: {}
    //     post: []
    // }
    // componentDidMount(){
    //     const token = localStorage.getItem('token');
    //     const limit = 5;
    //     const offset = 0;
    //     const search = "";
    //     fetch(`https://api-video-solman.herokuapp.com/api/read/roles?limit=${limit}&offset=${offset}&search=${search}`, {
    //         method: 'GET',
    //         headers: {
    //                 "Authorization": `Bearer ${token}`,
    //                 "Content-Type": "application/json"
    //             },
    //         })
    //             .then(response => response.json())
    //             .then(json => {
    //                 this.setState({
    //                     post:json.rows
    //                 })
    //             })
    //         .catch(error => console.log('error', error));
    // }
    state = {
        post: []
    }

    componentDidMount(){
    const token = localStorage.getItem('token');
    const limit = 5;
    const offset = 0;
    const search = "";
    fetch(`/api/users?limit=${limit}&offset=${offset}&search=${search}`, {
        method: 'GET',
        headers: {
                "Authorization": `Bearer ${token}`,
                "Content-Type": "application/json"
            },
        })
            .then(response => response.json())
            .then(json => {
                this.setState({
                    post:json.rows
                })
            })
        .catch(error => console.log('error', error));
    }
    // componentDidMount() {
    // useEffect(() =>
    //     fetch("https://api-video-solman.herokuapp.com/api/read/roles")
    //         .then(res => res.json())
    //         .then(res => this.setState({ roles : res }))
    //         .catch(() => this.setState({ hasError: true }))
    // );
    // useEffect (() => {
    //     async function fetchData(){
    //         const res = await fetch("https://api-video-solman.herokuapp.com/api/read/roles");
    //         res
    //             .json()
    //             .then(res => setRoles(res))
    //             .catch(err => setErrors(err));
    //     }

    //     fetchData();
    // })
    // }
    render(){
    return (
        // <table>
        //     <thead>
        //         <tr>
        //             <th>Roles ID</th>
        //             <th>Role Name</th>
        //         </tr>
        //     </thead>
        //     <tbody>
        //         <tr key={roles.id}>
        //             {/* <td>{JSON.stringify(roles.id)}</td> */}
        //             <td>{roles.id}</td>
        //             <td>{roles.role_name}</td>
        //             <td>
        //                 {/* <button onClick={() => props.editRole(id, role)}>Edit</button>
        //                 <button onClick={() => props.deleteRole(id)}>Delete</button> */}
        //             </td>
        //         </tr>
        //         {/* { props.roles.length > 0 ? (
        //             props.roles.map(role => {
        //                 const {id, name} = role;
        //                 return (
        //                     <tr key={id}>
        //                         <td>{id}</td>
        //                         <td>{name}</td>
        //                         <td>
        //                             <button onClick={() => props.editRole(id, role)}>Edit</button>
        //                             <button onClick={() => props.deleteRole(id)}>Delete</button>
        //                         </td>
        //                     </tr>
        //                 )
        //             })
        //         ) : (
        //             <tr>
        //                 <td colSpan={3}>No Roles found</td>
        //             </tr>
        //         )
        //         } */}
        //     </tbody>
        // </table>
        // <Table pagination={{pageSize: '5'}} dataSource={role} size="middle" >
        //     <Column title="Role ID" key="id" dataIndex="id" />
        //     <Column title="Role Name" key="name" dataIndex="name" />
        //     <Column title="" key="action" render={(text, record) => (
        //         <Space size="middle">
        //             <a>Edit</a>
        //             <a>Delete</a>
        //         </Space>
        //     )} />
        // </Table>

        <Fragment>
            <Navigation history={this.props.history}/>
            <div className="container bg">
                <Fragment>
                    <div className="col md-3">
                        <h5 className="mt-0">User</h5>
                        <Button>+ Add User</Button>
                    </div>
                </Fragment>
            </div>

            {
                this.state.post.map(post => {
                    return <Post key={post.id} first-name={post.firstname} email={post.email}> remove={this.handleRemove}</Post>
                })
            }
        </Fragment>
        )
    }
    
}

export default Roles;