import React from "react";
import { Card, Form, Input, Button, Table } from "antd";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import initialState from './data';
import Roles from './table/master_role';
import role from './data';

// export default function add_role(){
class add_role extends React.Component{
    handleEdit(index){
        const { items } = this.state;

        this.setState({
            mode: "edit",
            formItem: items[index]
        });
    }
    render(){

        return(
            <div className="site-card-border-less-wrapper" style={{ padding: 24, minHeight: 380 }}>
                <Card title="Add Role" bordered={false} style={{ width: 500, margin: '0 auto' }}>
                    <Form name="basic" initialValues={{ remember: true }}>
                        <Form.Item label="Roles ID" name="id" rules={[{ required: true, message: 'Please input your Roles ID!' }]}>
                        </Form.Item>
                            <Input />

                        <Form.Item label="Roles Name" name="name" rules={[{ required: true, message: 'Please input your Role Name!' }]}>
                        </Form.Item>
                            <Input />

                        <Form.Item>
                            <br />
                            <Button type="primary" htmlType="submit" style={{ left: '30%', backgroundColor: '#ffffff', color: '#000000' }}>
                                {/* Cancel */}
                                <Link to="/role">Cancel</Link>
                            </Button>
                            <Button htmlType="submit" style={{ left: '50%', backgroundColor: '#0050b3', color: '#ffffff' }}>
                                Save
                            </Button>
                        </Form.Item>
                    </Form>
                    {/* <Table columns={} pagination={{pageSize: '5'}} dataSource={initialState} size="middle" /> */}
                    {/* <table>
                        
                    </table> */}
                    {/* <Tables /> */}
                    {/* <Roles roles={role} /> */}
                    <Roles />
                </Card>
            </div>
        )
        }
}

export default add_role;