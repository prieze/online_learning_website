import React, {useState} from 'react';
import { Layout, Avatar, Menu, Row, Col } from 'antd';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import './App.css';
// import Mobile from './toppng.com-available-on-google-play-png-app-store-play-store-569x377.png'
// import Role from "./admin/table/master_role";
import Roles from './Roles';
// import Add_role from "./admin/add_role";
import Add_role from './add_role';
import Dashboard from "./Dashboard";
import Android from './../components/image/playstore.png';
import Apple from './../components/image/AppStore.png';
import instagram from './../components/image/instagram.png';
import twitter from './../components/image/twitter.png';
import facebook from './../components/image/facebook.png';
// import Test from "./components/image/test.jpg";

import { DownOutlined } from '@ant-design/icons';
import Title from 'antd/lib/typography/Title';
import Logo from './../gambar/logo.png';
// import Logo from './logo.svg';
// import logo from ''
// import { Row, Col, Divider } from 'antd';
const { Header, Footer, Content } = Layout;
// const router = [
//   {
//     path: '/',
//     exact: true,
//     main() => 
//   }
// ]

function App() {
  return (
    <Router>
      <div className="App">
      <Layout style={{minHeight: '100vh'}}>
      <Header style={{ position: 'fixed', zIndex: 1, width: '100%' }}>
        {/* <svg width="152" height="27" viewBox="0 0 152 27" fill="none" xmlns="http://www.w3.org/2000/svg" className="judul">
            <path d="M10.816 10.864C7.968 10.064 6.784 9.296 6.784 7.792C6.784 6.288 8.064 5.232 10.112 5.232C12.416 5.232 13.984 6.256 14.848 8.304L19.136 5.968C17.696 2.512 14.528 0.559999 10.336 0.559999C7.936 0.559999 5.856 1.264 4.16 2.704C2.464 4.112 1.6 5.904 1.6 8.112C1.6 9.968 2.24 11.504 3.52 12.752C4.672 13.904 6.72 14.928 9.632 15.792C11.488 16.336 12.736 16.848 13.376 17.296C14.048 17.712 14.368 18.288 14.368 19.024C14.368 20.56 12.768 21.68 10.56 21.68C8.032 21.68 6.08 20.368 5.088 18.192L0.608 20.496C2.112 24.112 5.696 26.384 10.4 26.384C13.28 26.384 15.52 25.68 17.152 24.24C18.784 22.8 19.584 20.912 19.584 18.64C19.584 14.64 16.8 12.56 10.816 10.864ZM26.987 0.943998V26H43.467V21.168H32.107V15.76H42.219V10.896H32.107V5.808H43.403V0.943998H26.987ZM56.5578 26V14.096L65.5818 26H72.2698L62.0618 12.912L71.6298 0.943998H65.1658L56.5578 11.856V0.943998H51.4058V26H56.5578ZM92.9202 0.943998V16.08C92.9202 19.312 91.1282 21.36 87.9602 21.36C85.1122 21.36 83.0962 19.504 83.0962 15.952L83.1282 0.943998H77.9122V16.08C77.9122 22.384 81.2402 26.352 87.9282 26.352C94.5842 26.352 98.0402 22.448 98.0402 15.888V0.943998H92.9202ZM106.806 0.943998V26H122.678V21.072H111.958V0.943998H106.806ZM131.59 26L133.446 20.944H144.55L146.438 26H151.782L142.182 0.943998H135.942L126.374 26H131.59ZM139.014 5.968L142.79 16.144H135.238L139.014 5.968Z" fill="white"/>
        </svg>
        <Avatar style={{float: 'right',marginTop: '1%'}} size="large" src='./test.jpg' />
        <svg width="17" height="9" viewBox="0 0 17 9" fill="none" xmlns="http://www.w3.org/2000/svg" className="drop">
            <path d="M0 0.999844L8.5 9L17 0.999844L15.9377 0L8.5 7.00032L1.06231 0L0 0.999844Z" fill="white"/>
        </svg> */}
            <a className="logo" href="#">
                <img className="img-responsive logo" src={Logo} alt="" />
            </a>
            <Menu theme="dark" mode="horizontal" defaultSelectedKeys={['1']} style={{float: 'right'}}>
                <Menu.Item key="1">
                <Link to="/" />Dashboard
                </Menu.Item>
                <Menu.Item key="2">Users</Menu.Item>
                <Menu.Item key="3">
                  <Link to="/role" />Roles
                </Menu.Item>
                <Menu.Item key="4">Category</Menu.Item>
            </Menu>
      </Header>
      <Content className="site-layout" style={{ padding: '0 50px', marginTop: 64 }}>
        {/* A <Switch> looks through its children <Route>s and
          renders the first one that matches the current URL. */}
        <Switch>
          <Route path="/role/add">
            <Add_role />
          </Route>
          <Route path="/role">
            <Roles />
          </Route>
          <Route path="/users">
            {/* <Users /> */}
          </Route>
          <Route path="/">
            <Dashboard />
          </Route>
        </Switch>
      </Content>
      <Footer style={{background: '#001529'}}>
      <Row justify="start">
        <Col span={6}>
          <Menu theme="dark" mode="vertical">
            <Menu.Item className="custom" key="1"><Title level={2} style={{color: 'white'}}>Company</Title></Menu.Item>
            {/* <Menu.Item key="2">Users</Menu.Item>
            <Menu.Item key="3">Roles</Menu.Item>
            <Menu.Item key="4">Category</Menu.Item> */}
            <Menu.Item className="custom" key="2">About</Menu.Item>
            <Menu.Item className="custom" key="3">Help and Support</Menu.Item>
            <Menu.Item className="custom" key="4">Contact</Menu.Item>
            <Menu.Item className="custom" key="5">Become a teacher</Menu.Item>
            <span>&copy;	2020 Sekula. All Right Reserved</span>
          </Menu>
        </Col>
        <Col span={6}>
          <Menu theme="dark" mode="vertical">
            <Menu.Item className="custom" key="1"><Title level={2} style={{color: 'white'}}>Community</Title></Menu.Item>
            <Menu.Item className="custom" key="2">Free Clases</Menu.Item>
            <Menu.Item className="custom" key="3">Scholarships</Menu.Item>
          </Menu>
        </Col>
        <Col span={6}>
          <Menu theme="dark" mode="vertical">
            <Menu.Item className="custom" key="1"><Title level={2} style={{color: 'white'}}>Mobile</Title></Menu.Item>
              <br /><img src={Android} alt="logo" /> <br />
              <img src={Apple} alt="logo" />
          </Menu>
        </Col>
        <Col span={6}>
        <Menu theme="dark" mode="vertical">
            <Menu.Item className="custom" key="1"><Title level={2} style={{color: 'white'}}>Get Connected</Title></Menu.Item>
              <img src={instagram} alt="logo" className="gambar" />
              <img src={twitter} alt="logo" className="gambar" />
              <img src={facebook} alt="logo" className="gambar" />
          </Menu>
        </Col>
      </Row>
      </Footer>
    </Layout>
      </div>
    </Router>
  );
}

export default App;
