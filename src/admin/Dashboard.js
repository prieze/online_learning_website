import React from "react";
import { Row, Col, Card } from "antd";
// import { Layout, Avatar, Menu } from 'antd';
// import { TeamOutlined, StarOutlined } from '@ant-design/icons';
import Test from "./../components/image/test.jpg";
import { TeamOutlined, StarOutlined } from '@ant-design/icons';
// const { Header, Footer, Content } = Layout;
// const { Meta } = Card;
// import './Dashboard.css';

export default function Role() {
    return(
        <div className="site-layout-background" style={{ padding: 24, minHeight: 380 }}>
        <br />
          <div>
            <b style={{left: '25%'}}><h2>Active Course Category</h2></b>
            <Row gutter={16}>
              <Col span={8}>
                <Card bordered={false}>
                  <img src={Test} style={{float: 'left',paddingRight: '10%'}} />
                  <b><h3>Business</h3></b>
                  <p>50 new User &gt;</p>
                </Card>
              </Col>
              <Col span={8}>
                <Card bordered={false}>
                  <img src={Test} style={{float: 'left',paddingRight: '10%'}} />
                  <b><h3>Development</h3></b>
                  <p>50 new User &gt;</p>
                </Card>
              </Col>
              <Col span={8}>
                <Card bordered={false}>
                  <img src={Test} style={{float: 'left',paddingRight: '10%'}} />
                  <b><h3>IT & Software</h3></b>
                  <p>50 new User &gt;</p>
                </Card>
              </Col>
            </Row>&nbsp;
            <Row gutter={16}>
              <Col span={8}>
                <Card bordered={false}>
                  <img src={Test} style={{float: 'left',paddingRight: '10%'}} />
                  <b><h3>Marketing</h3></b>
                  <p>50 new User &gt;</p>
                </Card>
              </Col>
              <Col span={8}>
                <Card bordered={false}>
                  <img src={Test} style={{float: 'left',paddingRight: '10%'}} />
                  <b><h3>Design</h3></b>
                  <p>50 new User &gt;</p>
                </Card>
              </Col>
              <Col span={8}>
                <Card bordered={false}>
                  <img src={Test} style={{float: 'left',paddingRight: '10%'}} />
                  <b><h3>Personal Development</h3></b>
                  <p>50 new User &gt;</p>
                </Card>
              </Col>
            </Row>
          </div>
          <br /><br /><br />
          <div>
            <b style={{left: '25%'}}><h2>Top Course </h2></b>
            <Row gutter={16}>
              <Col span={8}>
                <Card style={{ width: 350 }} cover={ <img alt="example" src="https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"/>}>
                  <div className="ant-card-meta-title" style={{fontSize: "152%"}}>Business Analisys Fundamental</div>
                  <p>
                    <b><TeamOutlined />16k </b><b style={{marginLeft: '20%'}}><StarOutlined />4.7 (11.332)</b>
                    <button style={{backgroundColor: '#00FF84',borderRadius: '5px',float: 'right',marginRight: '11%'}}>view</button>
                  </p>
                </Card>
              </Col>
              <Col span={8}>
                <Card style={{ width: 350 }} cover={ <img alt="example" src="https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"/>}>
                  <div className="ant-card-meta-title" style={{fontSize: "152%"}}>Business Analisys Fundamental</div>
                  <p>
                    <b><TeamOutlined />16k </b><b style={{marginLeft: '20%'}}><StarOutlined />4.7 (11.332)</b>
                    <button style={{backgroundColor: '#00FF84',borderRadius: '5px',float: 'right',marginRight: '11%'}}>view</button>
                  </p>
                </Card>
              </Col>
              <Col span={8}>
                <Card style={{ width: 350 }} cover={ <img alt="example" src="https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"/>}>npm install react-router-dom
                  <div className="ant-card-meta-title" style={{fontSize: "152%"}}>Business Analisys Fundamental</div>
                  <p>
                    <b><TeamOutlined />16k </b><b style={{marginLeft: '20%'}}><StarOutlined />4.7 (11.332)</b>
                    <button style={{backgroundColor: '#00FF84',borderRadius: '5px',float: 'right',marginRight: '11%'}}>view</button>
                  </p>
                </Card>
              </Col>
            </Row>
          </div>
        </div>
    )
}