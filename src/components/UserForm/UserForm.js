import React, { Component, Fragment } from 'react';
// import axios from "axios";
import '../../style/user.css'
import Navigation from "../../component/Navigation/Navigation"
import { Container, Row, Col, Card, Form, Button, Image } from "react-bootstrap";
import { Link } from 'react-router-dom';
import { message } from 'antd';
import { checkRole } from '../../component/Utils';
import Footer from "../../component/Footer/Footer"
import Moment from 'moment';
import { Formik } from 'formik';
import * as yup from 'yup';

class UserForm extends Component {
  // Menerima Props
  // is_edit
  // is_add
  // is_detail
	state = {
			post: {},
      id: 0,
      textState: ''
	}
	formRef = React.createRef();
	isLowerToday = ((tgl) => {
		const date = new Date();
		date.setDate(date.getDate() - 1);
		console.log(date);
		if (new Date(tgl) < date) {
			return true
		} else {
			return false
		}
	})
	schema = yup.object({
    first_name: yup.string().required('First Name is required'),
    last_name: yup.string().required('Last Name is required'),
    email: yup.string().email().required('Email is required'),
		mobile_number: yup.string()
			.matches(/^08/, "Mobile number must start with 08")
			.matches(/^[0-9]+$/, "Mobile Number must be only digits")
			.min(10, 'Mobile Number is not Valid')
			.max(13,'Mobile Number is not Valid').required('Mobile Number is required'),
		gender: yup.string().nullable()
			.test('malefemale', 'Gender is Required', val => (val === 'male' || val === 'female')),
    birthday: yup.date().nullable().required().test('nottoday',
			'BirthDay is Not Valid', val => this.isLowerToday(val)),
    school: yup.string().nullable(),
    last_degree: yup.string().nullable(),
    job_title: yup.string().nullable(),
    employment_type: yup.string().nullable(),
    company: yup.string().nullable(),
  });

  editSchema = yup.object({
    password: yup.string().min(4).max(30),
    compass: yup.string().when("password", {
      is: val => val && val.length > 0,
      then: yup.string()
        .oneOf([yup.ref("password")], "Both passwords need to be the same")
        .required()
    }),
    roleId: yup.number().nullable()
			.test('roleid', 'Role is Required', val => (val === 1 || val === 2)),
  })

  addSchema = yup.object({
    password: yup.string().min(4).max(30).required(),
    compass: yup.string().when("password", {
      is: val => val && val.length > 0,
      then: yup.string()
        .oneOf([yup.ref("password")], "Both passwords need to be the same")
        .required()
    }),
    roleId: yup.number().nullable()
			.test('roleid', 'Role is Required', val => (val === 1 || val === 2)),
  })

  checkProps() {
    let text = 'Edit User'
    if (this.props.is_edit) {
      text = 'Edit User'
      this.schema = this.schema.concat(this.editSchema)
    }
    if (this.props.is_add) {
      text = 'Add User'
      this.schema = this.schema.concat(this.addSchema)
    }
    if (this.props.is_detail) {text = 'Detail User'}
    console.log(this.schema);
    this.setState({
      textState: text
    })
  }

	componentDidMount() {
		console.log(this.props);
    const token = localStorage.getItem('token');
    this.checkProps();
    // const id = this.props.match.params.id;
    const id = this.props.id
		this.setState({
			post: {},
			id: id,
    });
    if (id) {
      fetch(`${process.env.REACT_APP_BASEURL}/api/user/${id}`, {
        method: 'GET',
        headers: {
            "Authorization": `Bearer ${token}`,
            "Content-Type": "application/json"
        },
      })
      .then(response => response.json())
      .then(json => {
        if (json.sukses) {
          const a = {
            ...json.data,
            roleId: (json.data.roles && json.data.roles[0] === 'admin') ? 1 : 2,
            password: '',
            compass: ''
          }
          this.setState({
            post: a
          })
          if (this.formRef && this.formRef.current){
            this.formRef.current.setValues(a)
          }
        } else {
          console.log('Failed');
          console.log(json.msg);
        }
        console.log(json.data);
      })
      .catch(error => console.log('error', error));
    }
	}

	toBase64 = file => new Promise((resolve, reject) => {
		const reader = new FileReader();
		reader.readAsDataURL(file);
		reader.onload = () => resolve(reader.result);
		reader.onerror = error => reject(error);
	});

	updateFiles(file) {
		console.log(file);
		this.toBase64(file).then(f => {
			f = f.split(';base64,');
			if (f.length > 1) {
				f = f[1]
			} else {
				f = f[0]
			}
			this.setState({
				fileSelected: {
					file_name: file.name,
					file_size: file.size,
					file_type: file.type,
					file: f
				}
			})
		}).catch(err => {
			console.log(err);
		})
	}

	updateuser() {
		this.formRef.current.validateForm().then( () => {
			console.log(this.formRef.current.values);
      if (this.formRef.current.isValid) {
				console.log('valid');
				const val = this.formRef.current.values;
				const token = localStorage.getItem('token');
				const id = this.props.id;
				const body = {
          email: val.email,
					first_name: val.first_name,
					last_name: val.last_name,
					mobile_number: val.mobile_number,
					gender: val.gender,
					roleId: val.roleId
        }
        if (id) {body.id = id}
				if (this.state.fileSelected) { body.cover = this.state.fileSelected}
				if (val.birthday) { body.birthday = val.birthday}
				if (val.school) { body.school = val.school}
				if (val.last_degree) { body.last_degree = val.last_degree}
				if (val.job_title) { body.job_title = val.job_title}
				if (val.employment_type) { body.employment_type = val.employment_type}
				if (val.company) { body.company = val.company}
        if (val.password) { body.password = val.password}
        let url = `${process.env.REACT_APP_BASEURL}/api/user`
        let method = 'POST'
        if (this.props.is_edit) {
          url += `/${id}`;
          method = 'PUT'
        }
				fetch(url, {
					method,
					body: JSON.stringify(body),
					headers: {
						"Authorization": `Bearer ${token}`,
						"Content-Type": "application/json"
					},
				})
					.then((response) => {
						response.json()
							.then(async(result) => {
								if (result.sukses) {
									console.warn("result", result);
									const a = {
										...result.data,
										roleId: (result.data.roles && result.data.roles[0] === 'admin') ? 1 : 2,
										password: '',
										compass: ''
									}
									await this.setState({
										post: a,
										fileSelected: null,
									})
									this.formRef.current.setValues(a)
									message.success("Successfully !", 1)
								}else{
									console.log("Failed");
									message.error(result.msg, 1);
								}
							})
					})
					.catch(error => console.log('error', error));
			} else {
				console.log('tidak valid')
			}
		})
	}

	render() {
		if (!checkRole('admin').sukses) {
				return (
						<div>Tidak Bisa Mengakses</div>
				)
		}
		const { post, id } = this.state;
		return (
      <>
				<Container fluid style={{ width: "100%", minHeight: "800px", padding: "50px", backgroundColor: "#F1F1F1" }}>                   
					<p style={{ marginLeft: '10%', color: '#000000' }}>
            <Link to={`/User`} style={{ color: '#000000' }}>
              Users
            </Link> / <strong>{this.state.textState}</strong>
          </p>
					<Card style={{ width: '80%', margin: 'auto' }}>
						<p style={{ fontSize: '20px', marginTop: '3%', marginLeft: '3%', color: '#3722D3' }}>{this.state.textState}</p>
						<hr style={{ border: '1px solid #3722D3', width: '95%', margin: 'auto', marginBottom: '5%'}} />
						<Card.Body style={{ padding: '2rem' }}>
							<Formik
									innerRef={this.formRef} 
									validationSchema={this.schema}
									validateOnChange={false}
									validateOnBlur={false}
									onSubmit={console.log}
									initialValues={post}
							>
								{({
									handleSubmit,
									values,
									setFieldValue,
									errors,
								}) => (
									<Form noValidate onSubmit={handleSubmit}>
										<Row>
											<Col xs={12} lg={6} >
												<Form.Group style={{paddingBottom: "23px"}}>
													<Form.Label>Photo</Form.Label>
													<Row>
														<Col>
															<Image src={post.gambarr?.url} roundedCircle style={{ width: '100px', height: '100px' }} />
                            </Col>
                            {!this.props.is_detail &&
                              <Col>
                                <Col>
                                  <Form.Control 
                                    type="file"
                                    id="file"
                                    placeholder="+ Edit Photo"
                                    className="btn btn-light  btn-add-cat"
                                    onChange={e => {
                                      this.updateFiles(e.target.files[0]);
                                    }}
                                  />
                                </Col>
                              </Col>
                            }
													</Row>
												</Form.Group>
												<Form.Group>
													<Form.Label>First Name : </Form.Label>
                          <Form.Control style={{ backgroundColor: "white" }} type="text" name="first_name"
                            disabled={this.props.is_detail}
														onChange={(e) => {
															setFieldValue('first_name', e.target.value)
														}}
														isInvalid={ !!errors.first_name}
														value={values.first_name}
													/>
													<Form.Control.Feedback type="invalid">{errors.first_name}</Form.Control.Feedback>
												</Form.Group>
												<Form.Group>
													<Form.Label>Last Name : </Form.Label>
                          <Form.Control style={{ backgroundColor: "white" }} type="text" name="last_name"
                            disabled={this.props.is_detail}
														onChange={(e) => {
															setFieldValue('last_name', e.target.value)
														}}
														isInvalid={ !!errors.last_name}
														value={values.last_name} 
													/>
													<Form.Control.Feedback type="invalid">{errors.last_name}</Form.Control.Feedback>
												</Form.Group>
												<Form.Group>
													<Form.Label>Email</Form.Label>
                          <Form.Control style={{ backgroundColor: "white" }} type="text" name="email"
                            disabled={!this.props.is_add}
														onChange={(e) => {
															setFieldValue('email', e.target.value)
														}}
														isInvalid={ !!errors.email}
														value={values.email}
													/>
													<Form.Control.Feedback type="invalid">{errors.email}</Form.Control.Feedback>
												</Form.Group>
												<Form.Group>
													<Form.Label>Mobile Number : </Form.Label>
                          <Form.Control style={{ backgroundColor: "white" }} type="number" name="mobile_number"
                            disabled={this.props.is_detail}
														onChange={(e) => {
															setFieldValue('mobile_number', e.target.value)
														}}
														isInvalid={ !!errors.mobile_number}
														value={values.mobile_number}
													/>
													<Form.Control.Feedback type="invalid">{errors.mobile_number}</Form.Control.Feedback>
												</Form.Group>
                        {!this.props.is_detail &&
                        <>
												<Form.Group>
													<Form.Label>Password</Form.Label>
                          <Form.Control type="password"  name="password"
														onChange={(e) => {
															setFieldValue('password', e.target.value)
														}}
														isInvalid={ !!errors.password}
														value={values.password}
													/>
													<Form.Control.Feedback type="invalid">{errors.password}</Form.Control.Feedback>
												</Form.Group>

												<Form.Group>
													<Form.Label>Confirm Password</Form.Label>
                          <Form.Control type="password" name="compass"
														onChange={(e) => {
															setFieldValue('compass', e.target.value)
														}}
														isInvalid={ !!errors.compass}
														value={values.compass}
													/>
													<Form.Control.Feedback type="invalid">{errors.compass}</Form.Control.Feedback>
												</Form.Group>
                        </>
                        }
												<Form.Group>
													<Form.Label>Roles User *</Form.Label>
                          <Form.Control as="select" style={{ backgroundColor: "white" }} name="roleId"
                            disabled={this.props.is_detail}
														isInvalid={!!errors.roleId}
														feedback={errors.roleId}
														value={values.roleId}
														onChange={(e) => {
															setFieldValue('roleId', e.target.value)
														}}
													>
                            <option value="0">Please Select</option>
														<option value="2">User</option>
														<option value="1">Admin</option>
													</Form.Control>
													<Form.Control.Feedback type="invalid">{errors.roleId}</Form.Control.Feedback>
												</Form.Group>
											</Col>
											<Col xs={12} lg={6} >
												<Form.Group>
													<Form.Label>Gender </Form.Label>
                          <Form.Control as="select" style={{ backgroundColor: "white" }} name="gender"
                            disabled={this.props.is_detail}
														isInvalid={!!errors.gender}
														feedback={errors.gender}
														value={values.gender}
														onChange={(e) => {
															setFieldValue('gender', e.target.value)
														}}
													>
														<option value="">Please Select</option>
														<option value="male">Male</option>
														<option value="female">Female</option>
													</Form.Control>
													<Form.Control.Feedback type="invalid">{errors.gender}</Form.Control.Feedback>
												</Form.Group>
											
												<Form.Group>
													<Form.Label>Birthday </Form.Label>
													<Row>
														<Col>
															<Form.Control style={{ backgroundColor: "white" }} type="date" name="birthday"
                                disabled={this.props.is_detail}
                                value={Moment(values.birthday).format('YYYY-MM-DD')}
																isInvalid={!!errors.birthday}
																feedback={errors.birthday}
																// value={values.birthday}
																onChange={(e) => {
																	setFieldValue('birthday', e.target.value)
																}}
															/>
															<Form.Control.Feedback type="invalid">{errors.birthday}</Form.Control.Feedback>
														</Col>
													</Row>
												</Form.Group>
												<Form.Group>
													<Form.Label>School </Form.Label>
													<Form.Control style={{ backgroundColor: "white" }} name="school"
                            disabled={this.props.is_detail}
                            onChange={(e) => {
															setFieldValue('school', e.target.value)
														}}
														isInvalid={ !!errors.school}
														value={values.school}
													/>
													<Form.Control.Feedback type="invalid">{errors.school}</Form.Control.Feedback>
												</Form.Group>
												<Form.Group>
													<Form.Label>Last Degree</Form.Label>
													<Form.Control style={{ backgroundColor: "white" }} name="last_degree"
                            disabled={this.props.is_detail}
                            onChange={(e) => {
															setFieldValue('last_degree', e.target.value)
														}}
														isInvalid={ !!errors.last_degree}
														value={values.last_degree}
													/>
													<Form.Control.Feedback type="invalid">{errors.last_degree}</Form.Control.Feedback>
												</Form.Group>
												<Form.Group>
													<Form.Label>Job Title </Form.Label>
													<Form.Control style={{ backgroundColor: "white" }} name="job_title"
                            disabled={this.props.is_detail}
                            onChange={(e) => {
															setFieldValue('job_title', e.target.value)
														}}
														isInvalid={ !!errors.job_title}
														value={values.job_title}
													/>
													<Form.Control.Feedback type="invalid">{errors.job_title}</Form.Control.Feedback>
												</Form.Group>
												<Form.Group>
													<Form.Label>Employment Type </Form.Label>
													<Form.Control style={{ backgroundColor: "white" }} name="employment_type"
                            disabled={this.props.is_detail}
                            onChange={(e) => {
															setFieldValue('employment_type', e.target.value)
														}}
														isInvalid={ !!errors.employment_type}
														value={values.employment_type}
													/>
													<Form.Control.Feedback type="invalid">{errors.employment_type}</Form.Control.Feedback>
												</Form.Group>
												<Form.Group>
													<Form.Label>Company </Form.Label>
													<Form.Control style={{ backgroundColor: "white" }} name="company"
                            disabled={this.props.is_detail}
                            onChange={(e) => {
															setFieldValue('company', e.target.value)
														}}
														isInvalid={ !!errors.company}
														value={values.company}
													/>
													<Form.Control.Feedback type="invalid">{errors.company}</Form.Control.Feedback>
												</Form.Group>
												<Form.Group>
													<Link to={`/User`} style={{ marginRight: '5%' }} className="btn btn-outline-secondary">Back</Link>
                          {this.props.is_edit &&
													  <Button className="button-grape" onClick={() => { this.updateuser() }}>Update</Button>
                          }
                          {this.props.is_add &&
													  <Button className="button-grape" onClick={() => { this.updateuser() }}>Save</Button>
                          }
												</Form.Group>
											</Col>
										</Row>
									</Form>
								)}
							</Formik>
						</Card.Body>
					</Card>
				</Container>
				<Footer />
        </>
		);

	}
}

export default UserForm;