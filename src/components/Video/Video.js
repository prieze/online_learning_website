import React from "react";

const Video = (props =>{
    // MENERIMA PROPS: {
    //   width,
    //   height,
    //   src,
    //   canSeek   DISABLE SEEK
    //   courseId
    //   videoId
    // }
    let supposedCurrentTime;
    const saveHistoryCourses = (async() => {
      if (props.courseId && props.videoId) {
      const token = localStorage.getItem("token");
      await fetch(`${process.env.REACT_APP_BASEURL}/api/history/video`, {
          method: 'POST',
          headers: {
              "Authorization": `Bearer ${token}`,
              "Content-Type": "application/json"
          },
          body: JSON.stringify({
              course_id: props.courseId,
              video_id: props.videoId
          })
      })
        .then((response) => response.json())
        .then((json) => {
          // this.setState({
          //   course: json.rows,
          //   loading: false
          // });
          console.log("json" + json);
          // console.log(this.state.course);
        })
        .catch((error) => console.log("error", error));
      }
    })

    const onEnded = (async (e) => {
      e.persist();
      const video = e.target;
      console.log('video ended');
      await saveHistoryCourses();
      if (props.videoDonePlaying) {
        props.videoDonePlaying();
      }
    })
    const onTimeUpdate = (e) => {
      e.persist();
      const video = e.target;
      if (!video.seeking) {
        supposedCurrentTime = video.currentTime;
      }
    }
    const onSeek = (e) => {
      e.persist();
      const video = e.target;
      const delta = video.currentTime - supposedCurrentTime;
      // if (Math.abs(delta) > 0.01 && !props.canSeek) {
      if (delta > 0.01 && !props.canSeek) {
        video.currentTime = supposedCurrentTime;
      }
    }
    return(
      <video controls autoPlay controlsList="nodownload"
        width={props.width}
        height={props.height} 
        muted={false}
        src={props.src}
        onEnded={(e)=>{onEnded(e)}}
        onSeeking={(e)=>{onSeek(e)}}
        onTimeUpdate={(e)=>{onTimeUpdate(e)}}
      />
    );
  }
)

export default Video;