import React from 'react';

import './SideDrawer.css';

const sideDrawer = () => {
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const [active1, setActive1] = React.useState(true);
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const [active2, setActive2] = React.useState(false);
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const [active3, setActive3] = React.useState(false);
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const [active4, setActive4] = React.useState(false);
    return(
        <nav className="side-drawer">
            <ul>
                <li>
                    <div className={active1 ? "active" : null}>
                        <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg" className="vektor1">
                        <path d="M1 10H7C7.26522 10 7.51957 9.89464 7.70711 9.70711C7.89464 9.51957 8 9.26522 8 9V1C8 0.734784 7.89464 0.48043 7.70711 0.292893C7.51957 0.105357 7.26522 0 7 0H1C0.734784 0 0.48043 0.105357 0.292893 0.292893C0.105357 0.48043 0 0.734784 0 1V9C0 9.26522 0.105357 9.51957 0.292893 9.70711C0.48043 9.89464 0.734784 10 1 10ZM0 17C0 17.2652 0.105357 17.5196 0.292893 17.7071C0.48043 17.8946 0.734784 18 1 18H7C7.26522 18 7.51957 17.8946 7.70711 17.7071C7.89464 17.5196 8 17.2652 8 17V13C8 12.7348 7.89464 12.4804 7.70711 12.2929C7.51957 12.1054 7.26522 12 7 12H1C0.734784 12 0.48043 12.1054 0.292893 12.2929C0.105357 12.4804 0 12.7348 0 13V17ZM10 17C10 17.2652 10.1054 17.5196 10.2929 17.7071C10.4804 17.8946 10.7348 18 11 18H17C17.2652 18 17.5196 17.8946 17.7071 17.7071C17.8946 17.5196 18 17.2652 18 17V10C18 9.73478 17.8946 9.48043 17.7071 9.29289C17.5196 9.10536 17.2652 9 17 9H11C10.7348 9 10.4804 9.10536 10.2929 9.29289C10.1054 9.48043 10 9.73478 10 10V17ZM11 7H17C17.2652 7 17.5196 6.89464 17.7071 6.70711C17.8946 6.51957 18 6.26522 18 6V1C18 0.734784 17.8946 0.48043 17.7071 0.292893C17.5196 0.105357 17.2652 0 17 0H11C10.7348 0 10.4804 0.105357 10.2929 0.292893C10.1054 0.48043 10 0.734784 10 1V6C10 6.26522 10.1054 6.51957 10.2929 6.70711C10.4804 6.89464 10.7348 7 11 7Z" fill="white"/>
                        </svg>
                        <a href="#" onClick={()=>{setActive1(!active1);setActive2(false);setActive3(false);setActive4(false)}} style={{'marginLeft' : '3.5%'}}>Dashboard</a>
                    </div>
                </li>
                <br></br>
                <li>
                    <div className={active2 ? "active" : null}>
                        <svg width="18" height="19" viewBox="0 0 18 19" fill="none" xmlns="http://www.w3.org/2000/svg" className="vektor2">
                        <path d="M4.5 4.5C4.5 6.981 6.519 9 9 9C11.481 9 13.5 6.981 13.5 4.5C13.5 2.019 11.481 0 9 0C6.519 0 4.5 2.019 4.5 4.5ZM17 19H18V18C18 14.141 14.859 11 11 11H7C3.14 11 0 14.141 0 18V19H17Z" fill="white"/>
                        </svg>&nbsp;
                        <a href="#" onClick={()=>{setActive1(false);setActive2(!active2);setActive3(false);setActive4(false)}}>Users</a>
                    </div>
                </li>
                <br></br>
                <li>
                    <div className={active3 ? "active" : null}>
                        <svg width="20" height="16" viewBox="0 0 20 16" fill="none" xmlns="http://www.w3.org/2000/svg" className="vektor3">
                        <path d="M7 7.99994C7.79113 7.99994 8.56448 7.76535 9.22228 7.32582C9.88008 6.88629 10.3928 6.26158 10.6955 5.53067C10.9983 4.79977 11.0775 3.9955 10.9231 3.21958C10.7688 2.44366 10.3878 1.73092 9.82843 1.17151C9.26902 0.612104 8.55628 0.231141 7.78036 0.0767996C7.00444 -0.0775412 6.20017 0.00167209 5.46927 0.304423C4.73836 0.607173 4.11365 1.11986 3.67412 1.77766C3.2346 2.43546 3 3.20882 3 3.99994C3 5.06081 3.42143 6.07822 4.17157 6.82837C4.92172 7.57851 5.93913 7.99994 7 7.99994ZM10 9.99994C10.0023 9.66592 10.0894 9.33795 10.2531 9.04682C10.1031 9.03119 9.95625 8.99994 9.8 8.99994H9.27812C8.56383 9.32934 7.78659 9.49993 7 9.49993C6.21341 9.49993 5.43617 9.32934 4.72188 8.99994H4.2C3.08617 9.00019 2.01802 9.44277 1.23043 10.2304C0.442827 11.018 0.00024854 12.0861 0 13.1999L0 14.4999C0 14.8978 0.158035 15.2793 0.43934 15.5606C0.720644 15.8419 1.10218 15.9999 1.5 15.9999H10.2781C10.0984 15.6972 10.0024 15.352 10 14.9999V9.99994ZM19 8.99994H18V6.49994C18 5.8369 17.7366 5.20101 17.2678 4.73217C16.7989 4.26333 16.163 3.99994 15.5 3.99994C14.837 3.99994 14.2011 4.26333 13.7322 4.73217C13.2634 5.20101 13 5.8369 13 6.49994V8.99994H12C11.7348 8.99994 11.4804 9.1053 11.2929 9.29283C11.1054 9.48037 11 9.73472 11 9.99994V14.9999C11 15.2652 11.1054 15.5195 11.2929 15.707C11.4804 15.8946 11.7348 15.9999 12 15.9999H19C19.2652 15.9999 19.5196 15.8946 19.7071 15.707C19.8946 15.5195 20 15.2652 20 14.9999V9.99994C20 9.73472 19.8946 9.48037 19.7071 9.29283C19.5196 9.1053 19.2652 8.99994 19 8.99994ZM15.5 13.4999C15.3022 13.4999 15.1089 13.4413 14.9444 13.3314C14.78 13.2215 14.6518 13.0654 14.5761 12.8826C14.5004 12.6999 14.4806 12.4988 14.5192 12.3049C14.5578 12.1109 14.653 11.9327 14.7929 11.7928C14.9327 11.653 15.1109 11.5577 15.3049 11.5192C15.4989 11.4806 15.7 11.5004 15.8827 11.5761C16.0654 11.6517 16.2216 11.7799 16.3315 11.9444C16.4414 12.1088 16.5 12.3022 16.5 12.4999C16.5 12.7652 16.3946 13.0195 16.2071 13.207C16.0196 13.3946 15.7652 13.4999 15.5 13.4999ZM16.5 8.99994H14.5V6.49994C14.5 6.23472 14.6054 5.98037 14.7929 5.79283C14.9804 5.6053 15.2348 5.49994 15.5 5.49994C15.7652 5.49994 16.0196 5.6053 16.2071 5.79283C16.3946 5.98037 16.5 6.23472 16.5 6.49994V8.99994Z" fill="white"/>
                        </svg>&nbsp;
                        <a href="#" onClick={()=>{setActive1(false);setActive2(false);setActive3(!active3);setActive4(false)}}>Roles</a>
                    </div>
                </li>
                <br></br>
                <li>
                    <div className={active4 ? "active" : null}>
                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" className="vektor4">
                            <path d="M12 2L6.5 11H17.5L12 2Z" fill="white"/>
                            <path d="M17.5 22C19.9853 22 22 19.9853 22 17.5C22 15.0147 19.9853 13 17.5 13C15.0147 13 13 15.0147 13 17.5C13 19.9853 15.0147 22 17.5 22Z" fill="white"/>
                            <path d="M3 13.5H11V21.5H3V13.5Z" fill="white"/>
                        </svg>
                        <a href="#" onClick={()=>{setActive1(false);setActive2(false);setActive3(false);setActive4(!active4)}} style={{'marginLeft' : '3.5%'}}>Category</a>
                    </div>
                </li>
            </ul>
        </nav>
    );
};

export default sideDrawer;