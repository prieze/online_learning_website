import React from "react";
import {perPage} from '../../component/Utils';
import './paginate.css';

const Paginate = (props =>{
  const totalPage = Math.floor( props.count / perPage ) + 1
  let canNavFirst = false;
  let canNavLast = false;
  let canNavPrev = false;
  let canNavPrev2 = false;
  let canNavNext = false;
  let canNavNext2 = false;
  let prev = 1;
  let prev2 = 1;
  let next = 1;
  let next2 = 1;
  const pageNow = parseInt(props.pageNow);
  console.log(`totalPage: ${totalPage}`);
  if (totalPage === 1) {
    return null;
  } else {
    if (pageNow > 1) {
      canNavFirst = true;
      canNavPrev = true;
      prev = (pageNow - 1);
      if (prev - 1 >= 1) {
        canNavPrev2 = true;
        prev2 = prev - 1;
      }
    }
    if (pageNow < totalPage) {
      canNavLast = true;
      canNavNext = true;
      prev = (pageNow - 1);
      if (prev - 1 >= 1) {
        canNavPrev2 = true;
        prev2 = prev - 1;
      }
      next = (pageNow + 1);
      if (next + 1 <= totalPage) {
        canNavNext2 = true;
        next2 = next + 1;
      }
    }
    return(
      <div className={props.className}>
        {canNavFirst &&
          <button className="btn" onClick={() => props.changePage(1)}>&lt;&lt;</button>
        }
        
        {canNavPrev &&
          <button className="btn" onClick={() => props.changePage(prev)}>&lt;</button>
        }
        {canNavPrev2 &&
          <button className="btn" onClick={() => props.changePage(prev2)}><u>{prev2}</u></button>
        }
        {canNavPrev &&
          <button className="btn" onClick={() => props.changePage(prev)}><u>{prev}</u></button>
        }
        <button className="btn">{pageNow}</button>
        {canNavNext &&
          <button className="btn" onClick={() => props.changePage(next)}><u>{next}</u></button>
        }
        {canNavNext2 &&
          <button className="btn" onClick={() => props.changePage(next2)}><u>{next2}</u></button>
        }
        {canNavNext &&
          <button className="btn" onClick={() => props.changePage(next)}>&gt;</button>
        }
        {canNavLast &&
          <button className="btn" onClick={() => props.changePage(totalPage)}>&gt;&gt;</button>
        }
      </div>
    );
  }
})

export default Paginate;