import React from 'react';

import DrawerToogleButton from '../Sidebar/DraweToggleButton';
import './Toolbar.css';

const toolbar = props =>(
    <header className="toolbar">
        <nav className="toolbar__navigation">
            <div>
                <DrawerToogleButton />
            </div>
            <div className="toolbar__logo"><a href="/">SEKULA</a></div>
            <div className="spacer" />
            <div className="toolbar_navigation-items">
                <ul>
                    <li>
                        <img src="https://static.republika.co.id/uploads/images/inpicture_slide/foto-profil-_170704110636-890.jpg" className="picture"></img>
                    </li>
                    <li>
                        <svg width="17" height="9" viewBox="0 0 17 9" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M0 0.999844L8.5 9L17 0.999844L15.9377 0L8.5 7.00032L1.06231 0L0 0.999844Z" fill="white"/>
                        </svg>
                        {/* <select>
                            <option>Test</option>
                        </select> */}
                    </li>
                </ul>
            </div>
        </nav>
    </header>
);

export default toolbar;