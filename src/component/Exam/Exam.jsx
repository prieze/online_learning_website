import React, { Component } from 'react';
import Navigation from '../Navigation/Navigation';
import { Container, CardGroup, Card, Row, Col, ProgressBar, Button, Form } from 'react-bootstrap';
import Footer from '../Footer/Footer';
import { Link } from 'react-router-dom';
import { message } from 'antd';

class Exam extends Component{

	constructor(props) {
		super(props);
		console.log(props);

		this.state = {
            id: '',
			course: [],
			soal: [],
			soal_index: 0,
			soal_skr: {},
			jawaban: [],
			percent: 0
		};

	}

	componentDidMount(){
		console.log(this.props);
		const jawaban = [];
		if (this.props.location.master_soals) {
			this.props.location.master_soals.map(a => {
				jawaban.push('')
			})
			this.setState({
				soal: this.props.location.master_soals,
				soal_index: 0,
				soal_skr: this.props.location.master_soals[0],
				jawaban,
			})
		}
	}

	nextButton() {
		console.log(this.state);
		let index = this.state.soal_index + 1;
		if (index >= this.state.soal.length) {
			index = 0
		};
		this.setState({
			soal_index: index,
			soal_skr: this.state.soal[index]
		})
	}

	prevButton() {
		let index = this.state.soal_index - 1;
		if (index < 0) {
			index = this.state.soal.length - 1;
		};
		this.setState({
			soal_index: index,
			soal_skr: this.state.soal[index]
		})
	}

	clickJawaban = async(jawab) => {
		const jawaban = this.state.jawaban.map((a, index) => {
			if (index === this.state.soal_index) {
				a = jawab;
			}
			return a;
		});
		console.log(jawaban);
		await this.setState({
			jawaban
		})
		console.log(this.state.jawaban);
		this.calcPercentJawaban();
	}

	calcPercentJawaban = () => {
		let count = 0;
		this.state.jawaban.map(a => {
			if (a !== '') {
				count += 1
			}
		})
		const percent = count / this.state.jawaban.length * 100;
		this.setState({
			percent
		})
    }
    
    submitExam() {
        // console.log(this.props.location.master_soals[1].ordering);

        const soal = this.props.location.master_soals.map((a, index) => {
            return {
                ordering: a.ordering,
                answer: this.state.jawaban[index]
            }
        })

        console.log(soal);

        const token = localStorage.getItem('token');
        fetch(`${process.env.REACT_APP_BASEURL}/api/submit/exam`, {
            method: "POST",
            body: JSON.stringify({
                id: this.props.location.course_user,
                jawaban: soal
            }),
            headers: {
                "Authorization": `Bearer ${token}`,
                "Content-Type": "application/json"
            },
        }).then((response) => {
            response.json()
                .then((result) => {
                    console.warn("result", result);
                    this.setState({id: this.props.location.course_user})
                    console.log(this.state.id);
                    if (result.sukses) {
                        this.props.history.push({
							pathname: '/Result',
							state: result,
							course: this.props.location.courseId
						})
                        console.log(result);
                    } else {
                        message.success(result.msg, 1)
                    }
                })
        })
    }
	
	render(){
		
		return(
			<>
			<Navigation/>
			<Container fluid style={{ width: "100%", minHeight: "800px", paddingTop: "30px" }}>
				<Row style={{ width: '80%', margin: 'auto' }}>
					<Col>
						<Link to={`/ViewCourses/${this.props.location?.courseId}`} style={{ fontFamily: 'Gordita-bold', color: '#000000' }}><i className="fa fa-chevron-left"></i> Back</Link>
					</Col>
				</Row>

				<Row style={{ width: '80%', margin: 'auto', marginTop: '2%' }}>
					<Col>
						<Card>
							<Card.Body>
								<Row>
									<Col><Card.Text>Question {this.state.soal_index + 1}/{this.state.soal?.length}</Card.Text></Col>
									<Col lg={10}><ProgressBar now={this.state.percent}/></Col>
								</Row>
							</Card.Body>
						</Card>
					</Col>
				</Row>

				<Row style={{ width: '80%', margin: 'auto', marginTop: '2%' }}>
					<Col>
						<CardGroup>
							<Card style={{ overflow: 'auto', height: '300px', border: 'none', boxShadow: 'none', backgroundColor: '#F9F6F6' }}>
								<Card.Body style={{ padding: '2rem' }}>
									<p style={{ fontFamily: 'Gordita-bold' }}>{this.state.soal_skr.question}</p>
								</Card.Body>
							</Card>
							<Card style={{ overflow: 'auto', height: '300px', border: 'none', boxShadow: 'none', backgroundColor: '#D9D1FF' }}>
                                <Card.Body style={{ padding: '2rem' }}>
									<Form>
										<Form.Group controlId="formGridAddress1">
											<Card style={{ marginTop: '3%' }}>
												<Card.Body>
                                                    <Form.Check name="jawaban" type="radio" label={this.state.soal_skr.a}
                                                        onClick={(e) => {this.clickJawaban('a')}}
                                                        checked={this.state.jawaban[this.state.soal_index] === 'a' ? 'on': null}
                                                    />
												</Card.Body>
											</Card>
											<Card style={{ marginTop: '3%' }}>
												<Card.Body>
                                                    <Form.Check name="jawaban" type="radio" label={this.state.soal_skr.b}
                                                        onClick={(e) => {this.clickJawaban('b')}}
                                                        checked={this.state.jawaban[this.state.soal_index] === 'b' ? 'on': null}
                                                    />
												</Card.Body>
											</Card>
											<Card style={{ marginTop: '3%' }}>
												<Card.Body>
                                                    <Form.Check name="jawaban" type="radio" label={this.state.soal_skr.c}
                                                        onClick={(e) => {this.clickJawaban('c')}}
                                                        checked={this.state.jawaban[this.state.soal_index] === 'c' ? 'on': null}
                                                    />
												</Card.Body>
											</Card>
											<Card style={{ marginTop: '3%' }}>
												<Card.Body>
                                                    <Form.Check name="jawaban" type="radio" label={this.state.soal_skr.d}
                                                        onClick={(e) => {this.clickJawaban('d')}}
                                                        checked={this.state.jawaban[this.state.soal_index] === 'd' ? 'on': null}
                                                    />
												</Card.Body>
											</Card>
										</Form.Group>
									</Form>
								</Card.Body>
							</Card>
						</CardGroup>
					</Col>
				</Row>

				<Row>
					<Col lg={{ span: 6, offset: 6 }}>
						<Row style={{ width: '55%', margin: 'auto', marginTop: '2%' }}>
							<Col xs={6} lg={2}><Button onClick={ () => {this.prevButton()}} variant="light" style={{ borderRadius: '20px', border: '2px solid #D6D6D6' }}><i style={{ color: '#838383' }} className="fa fa-chevron-left"></i></Button></Col>
							<Col xs={6} lg={2}><Button onClick={ () => {this.nextButton()}} variant="light" style={{ borderRadius: '20px', border: '2px solid #D6D6D6' }}><i style={{ color: '#838383' }} className="fa fa-chevron-right"></i></Button></Col>
							{
								(this.state.soal_index === this.state.soal.length - 1) 
								? <Col xs={12} lg={8}><p onClick={() => { this.submitExam() }} style={{ cursor: 'pointer' }} >Get Result</p></Col> 
								:null
							}
						</Row>
					</Col>
				</Row>

			</Container>
			<Footer/>
			</>
		)
	}

}

export default Exam;