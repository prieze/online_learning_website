import React, { Component } from 'react';
import { Container, Row, Col, Image, Button } from 'react-bootstrap';
import Navigation from '../Navigation/Navigation';
import Danger from '../../gambar/danger.png';
import Success from '../../gambar/success.png';
import { Link } from 'react-router-dom';

class Result extends Component{

    constructor(props) {
		super(props);
		console.log(props);

		this.state = {
            percent: 0,
            kkm: 0
		};

	}

    componentDidMount() {
        console.log(this.props);
        this.calcScore();
        // this.calcPass();
    }

    calcScore = () => {
        const soal = this.props.location.state;
		const percent = soal.data.score / soal.data.total_soal * 100;
		this.setState({
			percent
		})
    }

    // calcPass = () => {
    //     const soal = this.props.location.state;
    //     console.log(soal);
    //     const kkm = soal.data.passing_grade * 10;
    //     this.setState({
    //         kkm
    //     })
    // }



    render(){
        return(
            <>
            <Navigation/>
            <Container fluid style={{ width: "100%", minHeight: "590px", paddingTop: "5%", backgroundColor: '#F4F4F4' }}>
                
                <Row className="justify-content-center">
                    <Col lg={6}>
                        {
                            (this.state.percent >= this.props.location.state.data?.passing_grade)
                            ?<><Image style={{ width: '100px', margin: 'auto' }} src={Success} rounded /><h3 style={{ fontFamily: 'Gordita-bold', textAlign: 'center', marginTop: '4%' }}>Result :</h3><p style={{ marginTop: '4%', textAlign: 'center' }}>Your score : <b style={{ color: '#54d691' }}>{this.state.percent}%</b>, minimum score to pass in: <b style={{ color: '#54d691' }}>{this.props.location.state.data?.passing_grade}%</b>. Congratulation! You Passed!</p><Button onClick={() => {window.open(`${this.props.location.state?.data?.cert?.file?.url}`)}} style={{ marginLeft: '36%' }} className="button-grape">Download Certificate</Button></>
                            :<><Image style={{ width: '100px', margin: 'auto' }} src={Danger} rounded /><h3 style={{ fontFamily: 'Gordita-bold', textAlign: 'center', marginTop: '4%' }}>Result :</h3><p style={{ marginTop: '4%', textAlign: 'center' }}>Your score : <b style={{ color: '#F42C00' }}>{this.state.percent}%</b>, minimum score to pass in: <b style={{ color: '#F42C00' }}>{this.props.location.state.data?.passing_grade}%</b>. Sorry You Failed</p><Link to={{ pathname: `/ViewCourses/${this.props.location.course}` }}><Button style={{ marginLeft: '40%' }} variant="danger">Restart Exam</Button></Link></>
                        }

                    </Col>
                </Row>
            
            </Container>
            </>
        )
    }
    
}

export default Result;