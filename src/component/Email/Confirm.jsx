import React, { Component } from 'react';
import Navigation from "../Navigation/Navigation";
import { Container, Row, Col, Card, Button } from 'react-bootstrap';
import Footer from '../Footer/Footer';
import './Confirm.css'

class Confirm extends Component{
    render(){
        return(
            <div>
                <Navigation history={this.props.history}/>
                <Container fluid
                     style={{
                        width: "100%",
                        minHeight: "800px",
                        paddingTop: "200px",
                        backgroundColor: "#F1F1F1"
                      }}
                >
                <Row>
                    <Col>
                        <Card style={{ width: '20rem', height: '150px', borderRadius: '10px', fontFamily: 'Gordita' }}>
                            <Card.Title className="text-center">Konfirmasi Email Anda</Card.Title>
                            <Button className="button-grape">Konfirmasi</Button>
                        </Card>
                    </Col>
                </Row>
                </Container>
                <Footer/>
            </div>
        )
    }
}

export default Confirm;