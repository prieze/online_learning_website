import React from 'react';
import { Col, Card, Image, CardColumns } from 'react-bootstrap';
import 'font-awesome/css/font-awesome.min.css';
import './Category.css'
import { useHistory } from "react-router-dom";

const List = (props) => {
    const history = useHistory();

    const handleClick = ((url) => {
        history.push(url);
    })
    return (
        <Col xs={12} sm={6} md={3} lg={2}>
            <CardColumns>
                <Card style={{ width: '10rem', height: '150px', borderRadius: '10px', border: '0.2px solid #DFDFFF' }}>
                    <i style={{ color: '#020288', marginLeft: '88%'}} className="fa fa-pencil pencil" aria-hidden="true" onClick={() => props.edit(props.id)}></i>
                    <Card.Img style={{ cursor: 'pointer', width: '70%', height: '80px', marginLeft: '15%' }} src={props.file} rounded onClick={() => {handleClick(`/Course/${props.id}`)}} />
                    <Card.Body>
                        <Card.Title style={{ cursor: 'pointer', marginTop: '5px' }} className="list-category" onClick={() => {handleClick(`/Course/${props.id}`)}}>{props.category}</Card.Title>
                    </Card.Body>
                </Card>
            </CardColumns>
        </Col>
    )
}

export default List;