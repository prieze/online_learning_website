import React, { Component } from 'react';
import Navigation from '../Navigation/Navigation'
import './Category.css'
import List from './List'
import Plus from '../../gambar/plus.png'
import Picture from '../../gambar/init.png'
import { checkRole } from '../Utils'
import { Popconfirm, message } from 'antd';
import _ from "lodash";
import Footer from "../Footer/Footer"
import { Container, Row, Col, Card, Image, CardColumns, Button, Modal, Form, InputGroup } from 'react-bootstrap';


class Category extends Component {
    constructor(props) {
        super(props)
        this.state = {
            list: [],
            modalAddIsOpen: false,
            modalEditIsOpen: false,
            createcategory: false,
            category_name: null,
            file: null,
            url: "base64 url",
            updatecategory: false,
            loading: false,
            store: null,
            edited_list: {},
            fileSelected: {},
            image: ""
        }
    }

    onOpenAddModal = () => {
        this.setState({ modalAddIsOpen: true });
    };

    onOpenEditModal = async (e) => {
        console.log(e);
        await this.setState({
            modalEditIsOpen: true,
            edited_list: e,
            updatecategory: false,
        });
        console.log(this.state.edited_list.id);
    };

    onCloseAddModal = () => {
        this.setState({ modalAddIsOpen: false });
    };

    onCloseEditModal = () => {
        this.setState({ modalEditIsOpen: false });
    }

    componentDidMount() {
        // if (checkRole('admin').sukses) {
        this.listCategory();
        // }
    }

    onFileChange = (event) => {
        // Update the state
        this.setState({ image: event.target.files[0] });
      };

    listCategory() {
        const token = localStorage.getItem('token');
        this.setState({ loading: true });
        fetch(`${process.env.REACT_APP_BASEURL}/api/read/categories?offset=0&search=`, {
            method: 'GET',
            headers: {
                "Authorization": `Bearer ${token}`,
                "Content-Type": "application/json"
            },
        })
            .then(response => response.json())
            .then(json => {
                this.setState({
                    list: json.rows,
                    loading: false,
                })
                console.log(this.state.list);
            })
            .catch(error => console.log('error', error));
    }

    toBase64 = file => new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => resolve(reader.result);
        reader.onerror = error => reject(error);
    });

    createcategory(e) {
        if (!e) { return };
        this.toBase64(e).then(f => {
            f = f.split(';base64,');
            if (f.length > 1) {
                f = f[1]
            } else {
                f = f[0]
            }
            const token = localStorage.getItem('token');
            console.log(f);
            fetch(`http://149.129.255.54/api/create/category`, {
                method: "POST",
                body: JSON.stringify({
                    category_name: this.state.category_name,
                    cover_photo: this.state.url,
                    cover: {
                        file: f,
                        file_name: e.name,
                        file_size: e.size,
                        file_type: e.type
                    }
                }),
                headers: {
                    "Authorization": `Bearer ${token}`,
                    "Content-Type": "application/json"
                },
            }).then((response) => {
                response.json()
                    .then((result) => {
                        if (result.sukses === true) {
                            console.warn("result", result);
                            console.log(result)
                            console.log(result.files.url);
                            this.setState({ url: result.files.url });
                            localStorage.setItem('createcategory', JSON.stringify({
                                createcategory: true,
                            }))
                            this.listCategory()
                            this.onCloseAddModal()
                            message.success("Successfully !", 1)
                        } else {
                            message.error(result.msg, 1)
                        }
                    })
            })
        })
    }

    updateFiles(file) {
        console.log(file);
        this.toBase64(file).then(f => {
            f = f.split(';base64,');
            if (f.length > 1) {
                f = f[1]
            } else {
                f = f[0]
            }
            this.setState({
                fileSelected: {
                    file_name: file.name,
                    file_size: file.size,
                    file_type: file.type,
                    file: f
                }
            })
        }).catch(err =>{
            console.log(err);
        })
    }

    updatecategory(id) {
        const token = localStorage.getItem('token');
        console.log(this.state.edited_list.id)
        fetch(`${process.env.REACT_APP_BASEURL}/api/update/category/${id}`, {
            method: 'PUT',
            body: JSON.stringify({
                category_name: this.state.category_name ? this.state.category_name : this.state.edited_list.category_name,
                cover_photo: this.state.url,
                cover: this.state.fileSelected
            }),
            headers: {
                "Authorization": `Bearer ${token}`,
                "Content-Type": "application/json"
            },
        })
            .then((response) => {
                response.json()
                    .then((result) => {
                        console.warn("result", result);
                       if(result.sukses){
                        localStorage.setItem('updatecategory', JSON.stringify({
                            updatecategory: true,
                        }))
                        this.onCloseEditModal();
                        this.listCategory()
                        message.success("Successfully !", 1)
                       }else{
                        message.error(result.msg, 1)
                       }
                    })
            })
            .catch(error => console.log('error', error));
    }


    handleRemoveCat(id) {
        console.log('handle remove')
        console.log(id);
        const token = localStorage.getItem('token');
        fetch(`http://149.129.255.54/api/delete/category/${id}`, {
            method: 'DELETE',
            headers: {
                "Authorization": `Bearer ${token}`,
                "Content-Type": "application/json"
            },
        })
            .then(response => response.json())
            .then(json => {
                if (json && json.sukses) {
                    this.listCategory();
                    message.success("Successfully !", 1)
                } else {
                    message.error(json.msg, 1)
                }
                console.log(json);
                this.onCloseEditModal();
            })

            .catch(error => console.log('error', error));
    }

    onChange = (event) => {
        
        event.persist();
        if (!this.debouncedFn) {
            this.debouncedFn = _.debounce(() => {
                this.listCategory();
            }, 1000);
        }
        this.debouncedFn();
    }

    render() {
        const { modalAddIsOpen, modalEditIsOpen, list, id } = this.state;
        return (
            <div>
                <Navigation history={this.props.history}/>
                <Container fluid
                    style={{
                        width: "100%",
                        minHeight: "800px",
                        padding: "50px",
                        backgroundColor: "#F1F1F1"
                    }}
                >
                    <Row style={{ width: '85%', margin: 'auto' }}>
                        {checkRole('admin').sukses &&
                            <Col xs={12} sm={6} md={3} lg={2}>
                                <CardColumns>
                                    <Card onClick={this.onOpenAddModal} style={{ width: '10rem', height: '150px', borderRadius: '10px', background: '#DFDFFF', border: '0.2px solid #DFDFFF' }}>
                                        <Card.Img style={{ width: '30%', margin: 'auto', marginTop: '28%' }} src={Plus} roundedCircle />
                                        <Card.Body>
                                            <Card.Title className="list-category">Add Categories</Card.Title>
                                        </Card.Body>
                                    </Card>
                                </CardColumns>
                            </Col>
                        }
                        {
                            this.state.list.map(list => {
                                return <List
                                    key={list.id}
                                    // category={list}
                                    // edit={(id) => this.updatecategory(id)}
                                    id={list.id}
                                    category={list.category_name}
                                    file={(list && list.file && list.file.url) ? list.file.url : null}
                                    edit={() => this.onOpenEditModal(list)}
                                    redir={() => this.props.history.push(`/Course/${list.id}`)}
                                />

                            })
                        }
                    </Row>
                </Container>
                <Footer />

                <Modal size="sm" show={modalAddIsOpen} onHide={this.onCloseAddModal} animation={false}>
                    <Modal.Header closeButton>
                        <Modal.Title style={{ fontFamily: 'Gordita-bold', fontSize: '18px' }}>Add Categories</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Row className="justify-content-center">
                            <Col lg={6}><Image src={Picture} rounded /></Col>
                            <Col lg={12}>
                                <Form.Group>
                                    <Form.Control type="file" id="file" name="file" placeholder="+ Add Cover Photo" className="btn btn-light  btn-add-cat" onChange={this.onFileChange} />
                                </Form.Group>
                                <Form.Group>
                                    <Form.Control type="text" placeholder="Categories Name" onChange={(event) => { this.setState({ category_name: event.target.value }) }} />
                                </Form.Group>
                            </Col>
                        </Row>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button className="button-grape" onClick={() => { this.createcategory(this.state.image) }}>Create</Button>
                    </Modal.Footer>
                </Modal>

                <Modal size="sm" show={modalEditIsOpen} onHide={this.onCloseEditModal} animation={false}>
                    <Modal.Header closeButton>
                        <Modal.Title style={{ fontFamily: 'Gordita-bold', fontSize: '18px' }}>Edit Categories</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Row className="justify-content-center">
                            <Col lg={12}><Image src={this.state?.edited_list?.file?.url} rounded style={{ width: '265px' }} /></Col>
                            <Col lg={12}>

                                <Form.Group>
                                    <Form.Control type="file" id="file" placeholder="+ Add Cover Photo" className="btn btn-light  btn-add-cat" onChange={e => {
                                        this.updateFiles(e.target.files[0]);
                                    }} />
                                </Form.Group>
                                <Form.Group>
                                    <label>Category Name: {this.state.edited_list.category_name} </label>
                                    <Form.Control type="text" onChange={(event) => { this.setState({ category_name: event.target.value }) }} />
                                </Form.Group>
                            </Col>
                        </Row>
                    </Modal.Body>
                    <Modal.Footer>
                        <Popconfirm
                                title="Remove Category ?"
                                onConfirm={() => { this.handleRemoveCat(this.state.edited_list.id) }}
                                okText="Yes"
                                cancelText="No"
                            >
                                <Button variant="danger"><i className="fa fa-trash-o" aria-hidden="true"></i></Button>
                        </Popconfirm>
                        <Button variant="success" onClick={() => { this.updatecategory(this.state.edited_list.id) }}><i className="fa fa-check" aria-hidden="true"></i></Button>
                    </Modal.Footer>
                </Modal>
            </div>
        )
    }
}

export default Category;