import { addToChart, addToChartArray } from './api/cart.js';
const perPage = 5;
const checkRole = (role) => {
  const userData = localStorage.getItem('userData');
  if (userData) {
    const roles = JSON.parse(userData).roles;
    // console.log(roles);
    if (roles) {
      if (roles.includes(role)) {
        return {
          sukses: true
        }
      } else {
        return {
          sukses: false,
          msg: 'Roles tidak mencukupi'
        }
      }
    } else {
      return {
        sukses: false,
        msg: 'Tidak ada Roles'
      }
    }
  } else {
    return {
      sukses: false,
      msg: 'Not Login'
    };
  }
}

const pageToLimitOffset = (page => {
  // const perPage = 5
  const limit = perPage;
  console.log(`page: ${page}`);
  const offset = (page - 1) * perPage;
  return {
    limit,
    offset
  }
})

const countMinutetoSecond = (s => {
  const [a, b] = s.split(':');
  return (parseInt(a)*60) + parseInt(b)
})

const countMinuteFromSecond = (s => {
  let a = Math.floor(s / 60)
  let b = s % 60;
  if (a < 10) { a = `0${a}`};
  if (b < 10) { b = `0${b}`};
  return `${a}:${b}`;
})

const sumDurationSubTopic = (sub => {
  let total = 0;
  if(sub) {
    sub.map(x => {
      if (x.duration) {
        const a = countMinutetoSecond(x.duration)
        if (a) {
          total += a;
        }
      }
    })
  }
  if (total) {
    return countMinuteFromSecond(total)
  } else {
    return '0:0'
  }
})

export {
  checkRole,
  pageToLimitOffset,
  perPage,
  countMinutetoSecond,
  countMinuteFromSecond,
  sumDurationSubTopic,
  addToChart,
  addToChartArray
}