
const addToChart = ( (id) => {
  return new Promise((resolve,reject) => {
    const token = localStorage.getItem('token');
    fetch(`${process.env.REACT_APP_BASEURL}/api/cart`, {
        method: "POST",
        body: JSON.stringify({course_id: id}),
        headers: {
            "Authorization": `Bearer ${token}`,
            "Content-Type": "application/json"
        },
    }).then((response) => {
        response.json()
            .then((result) => {
                console.warn("result", result);
                if (result) {
                    resolve(result.data)
                } else {
                    reject(result.msg)
                }
            })
    })
  })
})

const addToChartArray = ((ids) => {
  const promises = [];
  for (const a of ids) {
    promises.push(addToChart(a));
  }
  Promise.all(promises).then(result => {
    console.log(result);
  })
})

export {
  addToChart,
  addToChartArray
}