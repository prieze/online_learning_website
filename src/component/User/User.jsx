import React, { Component, Fragment } from 'react';
// import axios from "axios";
import '../../style/user.css'
import Post from './Post'
import Navigation from "../Navigation/Navigation"
import Footer from "../Footer/Footer"
import { Link } from 'react-router-dom'
import { Button, Container, Col, Row, Table, Form } from "react-bootstrap";
// import { BrowserRouter as Link } from "react-router-dom";
import PropTypes from 'prop-types';
import _ from "lodash";
import { message } from 'antd';
import Paginate from '../../components/Paginate/paginate';
import Moment from 'moment';
import { checkRole, pageToLimitOffset } from '../Utils'

class user extends Component {

    state = {
        post: [],
        file: null,
        url: "base64 url",
        search: '',
        loading: false,
        pageNow: 1,
        count: 0
    }

    handleRemove(id) {

        console.log('handle remove')
        console.log(id);
        const token = localStorage.getItem('token');
        fetch(`${process.env.REACT_APP_BASEURL}/api/user/${id}`, {
            method: 'DELETE',
            headers: {
                "Authorization": `Bearer ${token}`,
                "Content-Type": "application/json"
            },
        })
            .then(response => response.json())
            .then(json => {
                if (json.sukses) {
                    this.listTable(1);
                }
                message.success("Successfully !", 1)
            })

            .catch(error => console.log('error', error));
    }

    componentDidMount() {
        const page = this.props.match.params.page ? this.props.match.params.page : 1;
        if (checkRole('admin').sukses) {
            this.listTable(page);
        }
    }

    listTable(page) {
        const token = localStorage.getItem('token');
        // const limit = 5;
        // const offset = 0;
        const { limit, offset } = pageToLimitOffset(page);
        console.log(`limit: ${limit}`);
        console.log(`offset: ${offset}`);
        // this.state({

        // })
        const search = this.state.search;
        this.setState({ loading: true });
        fetch(`${process.env.REACT_APP_BASEURL}/api/users?limit=${limit}&offset=${offset}&search=${search}`, {
            method: 'GET',
            headers: {
                "Authorization": `Bearer ${token}`,
                "Content-Type": "application/json"
            },
        })
            .then(response => response.json())
            .then(json => {
                this.setState({
                    post: json.rows,
                    loading: false,
                    pageNow: page,
                    count: json.count
                })
            })
            .catch(error => console.log('error', error));
    }

    onChange = (event) => {
        /* signal to React not to nullify the event object */
        event.persist();
        if (!this.debouncedFn) {
            this.debouncedFn = _.debounce(() => {
                //  let searchString = event.target.value;
                this.setState({ search: event.target.value });
                this.listTable(1);
            }, 1000);
        }
        this.debouncedFn();
    }

    ChangePage(page) {
        console.log(`Change page to: ${page}`)
        this.listTable(page);
    }

    render() {
        if (!checkRole('admin').sukses) {
            return (
                <div>Tidak Bisa Mengakses</div>
            )
        }
        const { post } = this.state;
        return (
            <Fragment>
                <Navigation history={this.props.history}/>
                <Container fluid
                    style={{
                        width: "100%",
                        minHeight: "800px",
                        padding: "50px",
                        backgroundColor: "#F1F1F1"
                    }}
                >
                    <Fragment>
                        <Row className="justify-content-center">
                            <Col lg="6"><h3>User</h3></Col>
                        </Row>
                        <Row className="justify-content-md-center">
                            <Col xs={8} md={{ span: 5, offset: 2 }} lg={5}>
                                <Form.Control 
                                    type="text"
                                    id="search"
                                    placeholder="Search"
                                    style={{ borderRadius: '30px' }}
                                    onChange={this.onChange} debounce="1000" />
                            </Col>
                            {/* <Col xs={2} md={2}><Button className="button-grape">Export to Excel</Button></Col> */}
                            <Col xs={4} md={3}>
                                <Link to={`/Create`}><Button className="button-grape" >Add User</Button></Link>
                            </Col>
                        </Row>
                        <br></br>
                    </Fragment>
                    <Row className="justify-content-md-center">
                        <Col xs={12} md={12} lg={6}>
                            <Table responsive hover style={{ background: "#F9F9F9" }}>
                                {
                                    this.state.post.map(post => {
                                        return <Post
                                            id={post.id}
                                            key={post.id}
                                            first_name={post.first_name}
                                            email={post.email}
                                            createdAt={Moment(post.createdAt).format('YYYY-MM-DD')}
                                            file={(post && post.gambarr && post.gambarr.url) ? post.gambarr.url : null}
                                            remove={(id) => this.handleRemove(id)}></Post>
                                    })
                                }
                            </Table>
                        </Col>
                    </Row>
                    <Paginate className="paginate-user" pageNow={this.state.pageNow} count={this.state.count}
                        changePage={(page) => this.ChangePage(page)}>
                    </Paginate>
                </Container>
                <Footer />
            </Fragment>
        );
    }
}

export default user;