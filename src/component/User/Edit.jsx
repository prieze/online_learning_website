import React, { Component, Fragment } from 'react';
// import axios from "axios";
import '../../style/user.css'
import Navigation from "../Navigation/Navigation"
import 'react-toastify/dist/ReactToastify.css';
import UserForm from '../../components/UserForm/UserForm';

class UserEdit extends Component {

	componentDidMount() {
		console.log(this.props);
	}

	render() {
		return (
			<div>
				<Navigation history={this.props.history}/>
				<UserForm
					is_edit
					id={this.props.match.params.id}
				/>
			</div>
		);

	}
}

export default UserEdit;