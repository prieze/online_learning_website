import React from 'react';
import 'font-awesome/css/font-awesome.min.css';
import { Image } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { Popconfirm } from 'antd';

const Post = (props) => {
    return (
        <tr>
            <td>
                <tr><Image style={{ width: '50px', height: '50px' }} src={props.file} roundedCircle />
                {/* <Image src="https://placeimg.com/100/100/any" roundedCircle /> */}</tr>
            </td>
            <th width="200px">
                <tr><Link to={`/Detail/${props.id}`} style={{ color: "black" }}>{props.first_name}</Link></tr>
                <tr>{props.email}</tr>
            </th>
            <th width="50px">{props.createdAt}</th>
            <th width="50px">
                <Link to={`/Admin/User/${props.id}`}><i className="fa fa-pencil-square-o" aria-hidden="true"></i></Link>
            </th>
            <th width="50px">
                <Popconfirm
                        title="Remove User ?"
                        onConfirm={() => { props.remove(props.id) }}
                        okText="Yes"
                        cancelText="No"
                    >
                        <i style={{ cursor: 'pointer', }} className="fa fa-trash" aria-hidden="true"></i>
                </Popconfirm>
               </th>
        </tr>
    )
}

export default Post;