import React, { Component } from 'react';
import Navigation from "../Navigation/Navigation"
import UserForm from '../../components/UserForm/UserForm';

class DetailUser extends Component {
    
    render() {
		return (
			<div>
				<Navigation history={this.props.history}/>
				<UserForm
					is_detail
					id={this.props.match.params.id}
				/>
			</div>
		);
	}
}
export default DetailUser;