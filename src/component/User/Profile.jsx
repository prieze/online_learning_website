import React, { Component, Fragment } from 'react';
import { Container, Row, Col, Card, Form, Button, Image } from "react-bootstrap";
import { checkRole } from '../Utils';
import Navigation from "../Navigation/Navigation"
import { Link } from 'react-router-dom';
import { message } from 'antd';
import Footer from "../Footer/Footer"
import Moment from 'moment';
import { Formik } from 'formik';
import * as yup from 'yup';

class Profile extends Component {
	constructor(){
		super();
		let user = JSON.parse(localStorage.getItem('userData'));
		this.state = {
			post: user,
			fileSelected: false
		};
	}
	formRef = React.createRef();
	isLowerToday = ((tgl) => {
		const date = new Date();
		date.setDate(date.getDate() - 1);
		console.log(date);
		if (new Date(tgl) < date) {
			return true
		} else {
			return false
		}
	})
  schema = yup.object({
    first_name: yup.string().required('First Name is required'),
    last_name: yup.string().required('Last Name is required'),
    email: yup.string().email().required('Email is required'),
		mobile_number: yup.string()
			.matches(/^08/, "Mobile number must start with 08")
			.matches(/^[0-9]+$/, "Mobile Number must be only digits")
			.min(10, 'Mobile Number is not Valid')
			.max(13,'Mobile Number is not Valid').required('Mobile Number is required'),
    // password: yup.string().min(4).max(30),
    // compass: yup.string().oneOf([yup.ref('password'), null], 'Passwords must match'),
    // gambar: yup.string(),
		gender: yup.string().nullable()
			.test('malefemale', 'Gender is Required', val => (val === 'male' || val === 'female')),
    birthday: yup.date().nullable().required().test('nottoday',
			'BirthDay is Not Valid', val => this.isLowerToday(val)),
    school: yup.string().nullable(),
    last_degree: yup.string().nullable(),
    job_title: yup.string().nullable(),
    employment_type: yup.string().nullable(),
    company: yup.string().nullable(),
    // cover: yup.string().required(),
    // rolesId: yup.number().required().test('role', 'Must be Admin or User', val => (val === 0 || val === 1)),
	});

	componentDidMount = () => {
		const token = localStorage.getItem('token');
		console.log(this.props)
		const id = this.props.match.params.id;
	}

	toBase64 = file => new Promise((resolve, reject) => {
		const reader = new FileReader();
		reader.readAsDataURL(file);
		reader.onload = () => resolve(reader.result);
		reader.onerror = error => reject(error);
	});

	updateFiles(file) {
		console.log(file);
		this.toBase64(file).then(f => {
			f = f.split(';base64,');
			if (f.length > 1) {
				f = f[1]
			} else {
				f = f[0]
			}
			this.setState({
				fileSelected: {
					file_name: file.name,
					file_size: file.size,
					file_type: file.type,
					file: f
				}
			})
			console.log(this.state);
		}).catch(err => {
			console.log(err);
		})
	}

	updateadmin() {
		this.formRef.current.validateForm().then( () => {
			console.log(this.formRef.current.values);
      if (this.formRef.current.isValid) {
				console.log('valid');
				const val = this.formRef.current.values;
				const token = localStorage.getItem('token');
				const id = this.props.match.params.id;
				const body = {
					id,
					first_name: val.first_name,
					last_name: val.last_name,
					mobile_number: val.mobile_number,
					gender: val.gender,
				}
				if (this.state.fileSelected) { body.cover = this.state.fileSelected}
				if (val.birthday) { body.birthday = val.birthday}
				if (val.school) { body.school = val.school}
				if (val.last_degree) { body.last_degree = val.last_degree}
				if (val.job_title) { body.job_title = val.job_title}
				if (val.employment_type) { body.employment_type = val.employment_type}
				if (val.company) { body.company = val.company}
				fetch(`${process.env.REACT_APP_BASEURL}/api/user/profile`, {
					method: 'POST',
					body: JSON.stringify(body),
					headers: {
						"Authorization": `Bearer ${token}`,
						"Content-Type": "application/json"
					},
				})
					.then((response) => {
						response.json()
							.then(async(result) => {
								if (result.sukses) {
									console.warn("result", result);
									await localStorage.setItem('userData', JSON.stringify(result.data));
									await this.setState({
										post: result.data,
										fileSelected: null,
									})
									message.success("Successfully !", 1);
								}else{
									console.log("Failed");
									message.error(result.msg, 1);
								}
							})
					})
					.catch(error => console.log('error', error));
			} else {
				console.log('tidak valid')
			}
		})
	}

	changeUser(event) {
		event.persist();
		this.setState(prevState => ({
			post: {
				...prevState.post,
				first_name: event.target.value
			}
		}))
	}
	handleChange = ({ target }) => {
		this.setState({ [target.name]: target.value });
	 };
	render() {
		const { post, id, user } = this.state;
		return (
			<Fragment>
				<Navigation history={this.props.history}/>
					<Container fluid style={{ width: "100%", minHeight: "800px", padding: "50px", backgroundColor: "#F1F1F1" }}>
						{checkRole('admin').sukses ? (
							<>
								<p style={{ marginLeft: '10%', color: '#000000' }}><Link to={`/User`} style={{ color: '#000000' }}>Users</Link> / <strong>User Profile</strong></p>
							</>
						): (
							<>
								<p style={{ marginLeft: '10%', color: '#000000' }}><Link to={`/`} style={{ color: '#000000' }}>Dashboard</Link> / <strong>User Profile</strong></p>
							</>
						)}
						<Card style={{ width: '80%', margin: 'auto' }}>
							<p style={{ fontSize: '20px', marginTop: '3%', marginLeft: '3%', color: '#3722D3' }}>User Profile</p>
							<hr style={{ border: '1px solid #3722D3', width: '95%', margin: 'auto', marginBottom: '5%'}} />
							<Card.Body style={{ padding: '2rem' }}>
								<Formik
									innerRef={this.formRef} 
									validationSchema={this.schema}
									validateOnChange={false}
									validateOnBlur={false}
									onSubmit={console.log}
									initialValues={post}
								>
									{({
										handleSubmit,
										values,
										setFieldValue,
										errors,
									}) => (
										<Form noValidate onSubmit={handleSubmit}>
											<Row>
												<Col xs={12} lg={6} >
													<Form.Group>
														<Form.Label>Photo</Form.Label>
														<Row>
															<Col>
																<Image src={post.gambarr?.url} roundedCircle style={{ width: '100px', height: '100px' }} /></Col>
															<Col>
															<Col>
																<Form.Control 
																	type="file"
																	id="file"
																	placeholder="+ Edit Photo"
																	className="btn btn-light  btn-add-cat"
																	onChange={e => {
																		this.updateFiles(e.target.files[0]);
																	}}
																/>
															</Col>
															</Col>
														</Row>
													</Form.Group>
													<Form.Group>
														<Form.Label>First Name : </Form.Label>
														<Form.Control style={{ backgroundColor: "white" }} type="text" name="first_name"
															onChange={(e) => {
																setFieldValue('first_name', e.target.value)
															}}
															isInvalid={ !!errors.first_name}
															value={values.first_name}
														/>
														<Form.Control.Feedback type="invalid">{errors.first_name}</Form.Control.Feedback>
													</Form.Group>
													<Form.Group>
														<Form.Label>Last Name : </Form.Label>
														<Form.Control style={{ backgroundColor: "white" }} type="text" name="last_name"
															onChange={(e) => {
																setFieldValue('last_name', e.target.value)
															}}
															isInvalid={ !!errors.last_name}
															value={values.last_name} 
														/>
														<Form.Control.Feedback type="invalid">{errors.last_name}</Form.Control.Feedback>
													</Form.Group>
													<Form.Group>
														<Form.Label>Email</Form.Label>
														<Form.Control style={{ backgroundColor: "white" }} type="email" disabled
															value={values.email} 
														/>
													</Form.Group>
													<Form.Group>
														<Form.Label>Mobile Number : </Form.Label>
														<Form.Control style={{ backgroundColor: "white" }} type="number" name="mobile_number"
															onChange={(e) => {
																setFieldValue('mobile_number', e.target.value)
															}}
															isInvalid={ !!errors.mobile_number}
															value={values.mobile_number}
														/>
														<Form.Control.Feedback type="invalid">{errors.mobile_number}</Form.Control.Feedback>
													</Form.Group>
													{/* <Form.Group>
													{checkRole('admin').sukses ? (
														<>
															<Form.Label>Roles User </Form.Label>
															<Form.Control style={{ backgroundColor: "white" }} disabled />
														</>
														): null}
													</Form.Group> */}
														<Form.Group>
														<Form.Label>Gender </Form.Label>
														<Form.Control as="select" style={{ backgroundColor: "white" }} name="gender"
															isInvalid={!!errors.gender}
															feedback={errors.gender}
															value={values.gender}
															onChange={(e) => {
																setFieldValue('gender', e.target.value)
															}}
														>
															<option value="">Please Select</option>
															<option value="male">Male</option>
															<option value="female">Female</option>
														</Form.Control>
														<Form.Control.Feedback type="invalid">{errors.gender}</Form.Control.Feedback>
													</Form.Group>
												</Col>
												<Col xs={12} lg={6} >
													<Form.Group>
														<Form.Label>Birthday </Form.Label>
														<Row>
															<Col>
																<Form.Control style={{ backgroundColor: "white" }} type="date" name="birthday"
																	value={Moment(values.birthday).format('YYYY-MM-DD')}
																	isInvalid={!!errors.birthday}
																	feedback={errors.birthday}
																	// value={values.birthday}
																	onChange={(e) => {
																		setFieldValue('birthday', e.target.value)
																	}}
																/>
																<Form.Control.Feedback type="invalid">{errors.birthday}</Form.Control.Feedback>
															</Col>
															{/* <Col>
																<Form.Control style={{ backgroundColor: "white" }} />
															</Col>
															<Col>
																<Form.Control style={{ backgroundColor: "white" }} />
															</Col> */}
														</Row>
													</Form.Group>
													<Form.Group>
														<Form.Label>School </Form.Label>
														<Form.Control style={{ backgroundColor: "white" }} name="school"
															onChange={(e) => {
																setFieldValue('school', e.target.value)
															}}
															isInvalid={ !!errors.school}
															value={values.school}
														/>
														<Form.Control.Feedback type="invalid">{errors.school}</Form.Control.Feedback>
													</Form.Group>
													<Form.Group>
														<Form.Label>Last Degree</Form.Label>
														<Form.Control style={{ backgroundColor: "white" }} name="last_degree"
															onChange={(e) => {
																setFieldValue('last_degree', e.target.value)
															}}
															isInvalid={ !!errors.last_degree}
															value={values.last_degree}
														/>
														<Form.Control.Feedback type="invalid">{errors.last_degree}</Form.Control.Feedback>
													</Form.Group>
													<Form.Group>
														<Form.Label>Job Title </Form.Label>
														<Form.Control style={{ backgroundColor: "white" }} name="job_title"
															onChange={(e) => {
																setFieldValue('job_title', e.target.value)
															}}
															isInvalid={ !!errors.job_title}
															value={values.job_title}
														/>
														<Form.Control.Feedback type="invalid">{errors.job_title}</Form.Control.Feedback>
													</Form.Group>
													<Form.Group>
														<Form.Label>Employment Type </Form.Label>
														<Form.Control style={{ backgroundColor: "white" }} name="employment_type"
															onChange={(e) => {
																setFieldValue('employment_type', e.target.value)
															}}
															isInvalid={ !!errors.employment_type}
															value={values.employment_type}
														/>
														<Form.Control.Feedback type="invalid">{errors.employment_type}</Form.Control.Feedback>
													</Form.Group>
													<Form.Group>
														<Form.Label>Company </Form.Label>
														<Form.Control style={{ backgroundColor: "white" }} name="company"
															onChange={(e) => {
																setFieldValue('company', e.target.value)
															}}
															isInvalid={ !!errors.company}
															value={values.company}
														/>
														<Form.Control.Feedback type="invalid">{errors.company}</Form.Control.Feedback>
													</Form.Group>
													<Form.Group>
														{checkRole('admin').sukses ? (
															<Link to={`/User`} style={{ marginRight: '5%' }} className="btn btn-outline-secondary">Back</Link>
														):
															<Link to={`/`} style={{ marginRight: '5%' }} className="btn btn-outline-secondary">Back</Link>
														}
														<Button className="button-grape" onClick={() => { this.updateadmin() }}>Update</Button>
													</Form.Group>
												</Col>
											</Row>
										</Form>
									)}
								</Formik>
							</Card.Body>
						</Card>
					</Container>
				<Footer />
			</Fragment >
		)
	}
}
export default Profile;