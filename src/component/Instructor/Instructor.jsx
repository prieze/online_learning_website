import React, { Component, Fragment } from 'react';
// import axios from "axios";
import '../../style/user.css'
import Navigation from "../Navigation/Navigation"
import Footer from "../Footer/Footer"
import { Button, Container, Col, Row, Table, Form, Image } from "react-bootstrap";
import { BrowserRouter as Link } from "react-router-dom";
import PropTypes from 'prop-types';
import _ from "lodash";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Paginate from '../../components/Paginate/paginate';
import Moment from 'moment';
import CreateInstructor from './CreateInstructor';
import { checkRole, pageToLimitOffset } from '../Utils'

class Instructor extends Component {

    state = {
        post: [],
        file: null,
        url: "base64 url",
        search: '',
        loading: false,
        pageNow: 1,
        count: 0
    }

    handleRemove(id) {

        console.log('handle remove')
        console.log(id);
        const token = localStorage.getItem('token');
        fetch(`${process.env.REACT_APP_BASEURL}/api/delete/instructor/${id}`, {
            method: 'DELETE',
            headers: {
                "Authorization": `Bearer ${token}`,
                "Content-Type": "application/json"
            },
        })
            .then(response => response.json())
            .then(json => {
                if (json.sukses) {
                    this.listTable(1);
                }
                toast.success("Successfully !")
            })

            .catch(error => console.log('error', error));
    }

    componentDidMount() {
        const page = this.props.match.params.page ? this.props.match.params.page : 1;
        if (checkRole('admin').sukses) {
            this.listTable(page);
        }
    }

    listTable(page) {
        const token = localStorage.getItem('token');
        // const limit = 5;
        // const offset = 0;
        const { limit, offset } = pageToLimitOffset(page);
        console.log(`limit: ${limit}`);
        console.log(`offset: ${offset}`);
        // this.state({

        // })
        const search = this.state.search;
        this.setState({ loading: true });
        fetch(`${process.env.REACT_APP_BASEURL}/api/read/instructors?limit=${limit}&offset=${offset}&search=${search}`, {
            method: 'GET',
            headers: {
                "Authorization": `Bearer ${token}`,
                "Content-Type": "application/json"
            },
        })
            .then(response => response.json())
            .then(json => {
                this.setState({
                    post: json.rows,
                    loading: false,
                    pageNow: page,
                    count: json.count
                })
            })
            .catch(error => console.log('error', error));
    }

    onChange = (event) => {
        /* signal to React not to nullify the event object */
        event.persist();
        if (!this.debouncedFn) {
            this.debouncedFn = _.debounce(() => {
                //  let searchString = event.target.value;
                this.setState({ search: event.target.value });
                this.listTable(1);
            }, 1000);
        }
        this.debouncedFn();
    }

    ChangePage(page) {
        console.log(`Change page to: ${page}`)
        this.listTable(page);
    }

    render() {
        if (!checkRole('admin').sukses) {
            return (
                <div>Tidak Bisa Mengakses</div>
            )
        }
        const { post } = this.state;
        return (
            <Fragment>
                <Navigation history={this.props.history}/>
                <ToastContainer />
                <Container fluid
                    style={{
                        width: "100%",
                        minHeight: "800px",
                        padding: "50px",
                        backgroundColor: "#F1F1F1"
                    }}
                >
                    <Fragment>
                        <Row style={{ width: '83%', margin: 'auto', fontFamily: 'gordita-bold' }}>
                            <Col lg="6"><h3>Instructor</h3></Col>
                        </Row>
                        <br/>
                        <Row style={{ width: '83%', margin: 'auto' }}>
                            <Col lg={5} >
                                <Form.Control 
                                    type="text"
                                    id="search"
                                    placeholder="Search"
                                    style={{ borderRadius: '30px' }}
                                    onChange={this.onChange} debounce="1000" />
                            </Col>
                            <Col style={{ marginLeft: '350px' }}>
                                {/* <Link to={'/CreateInstructor'}><Button className="button-grape" >Add New Instructor</Button></Link> */}
                                <CreateInstructor/>
                            </Col>
                        </Row>
                        <br></br>
                    </Fragment>
                    <Row style={{ width: '83%', margin: 'auto' }}>
                        <Col>
                            <Table responsive hover style={{ background: "#F9F9F9" }}>
                                {
                                    this.state.post.map(post => {
                                        return <tr>
                                        <td md={2} style={{ marginLeft: '150px' }}>
                                            <tr><Image style={{ width: '50px', height: '50px' }} src={post.cover?.url} roundedCircle />
                                            {/* <Image src="https://placeimg.com/100/100/any" roundedCircle /> */}</tr>
                                        </td>
                                        <th md={3}>
                                            <tr><Link to={`/Detail/${post.id}`} style={{ color: "black" }}>{post.instructor_name}</Link></tr>
                                            <tr>{post.email}</tr>
                                        </th>
                                        <th md={3}>{Moment(post.createdAt).format("DD MMMM YYYY")}</th>
                                        {/* <th md={2}>
                                            <Link to={`/Admin/User/${post.id}`}><i className="fa fa-pencil-square-o" aria-hidden="true"></i></Link>
                                        </th>
                                        <th md={2}><i className="fa fa-trash" aria-hidden="true" onClick={() => {
                                            if (
                                                window.confirm(
                                                    `Are You sure ?`
                                                )
                                            ) {
                                                this.handleRemove(post.id);
                                            }
                                        }}
                                        ></i></th> */}
                                    </tr>
                                    })
                                }
                            </Table>
                        </Col>
                    </Row>
                    <Paginate className="paginate-user" pageNow={this.state.pageNow} count={this.state.count}
                        changePage={(page) => this.ChangePage(page)}>
                    </Paginate>
                </Container>
                <Footer />
            </Fragment>
        );
    }
}

export default Instructor;