import React, { Component } from "react";
import ModalInstructor from "./ModalInstructor";
import {
  Row,
  Col,
  Card,
  Button,
} from "react-bootstrap";

class CreateInstructor extends Component{
  state = {
    instructors:[],
    instructor:{},
    id: null,
    file: null,
    fileList: [],
    photo: null,
    fileListPhoto: [],
    photoResult: null,
    cvResult: null,
    choosing: false,
    instructor_dropdown: [],
    modalAddIsOpen: false
  };
  formRef = React.createRef();
  onOpenAddModal = () => {
    this.setState({ 
      modalAddIsOpen: true,
      instructor:{},
      file: null,
      photo: null,
      fileList: [],
      fileListPhoto: [],
      photoResult: null,
      cvresult: null,
      choosing: false,
      instructor_dropdown: []
    });
    if (this.formRef && this.formRef.current) {
      this.formRef.current.resetFields();
    }
  };
  render(){
    return(
      <>
        <Row>
          <Col>
                <Button
                  onClick={this.onOpenAddModal}
                  className="button-grape btn-sm"
                  style={{ width: "200px" }}
                >
                  Add New Instructor
                </Button>
          </Col>
        </Row>
        <ModalInstructor
          formRef={this.formRef}
          modalAddIsOpen={this.state.modalAddIsOpen}
          onCloseAddModal={() => {this.setState({modalAddIsOpen: false})}}
        //   addInstructor={(data) => {this.props.addInstructor(data)}}
        />
      </>
    )
  }
}

export default CreateInstructor;