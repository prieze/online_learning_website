import React, { Component } from "react";
import Navigation from "../Navigation/Navigation";
import { Link } from "react-router-dom";
import "../Course/Course.css";
import Footer from "../Footer/Footer";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import _ from "lodash";
import {
  Container,
  Row,
  Col,
  Card,
  Navbar,
  Button,
  Modal,
  ProgressBar,
  // Form,
  InputGroup
} from "react-bootstrap";
// import { Select,  } from 'antd';
import { Upload, Button as ButtonAnt, Select, notification } from 'antd';
import { Form, Input } from 'antd';
import { FormInstance } from 'antd/lib/form';
import { UploadOutlined } from '@ant-design/icons';
// import socketIOClient from "socket.io-client";
// import UploadVideo from '../Upload/uploadVideo';
import { CheckOutlined, CloseOutlined } from '@ant-design/icons';
import {fetchInstructorById, fetchInstructorSearch} from '../Course/api'
const { Option } = Select;

class ModalInstructor extends Component{
  state = {
    instructors:[],
    instructor:{},
    id: null,
    file: null,
    fileList: [],
    photo: null,
    fileListPhoto: [],
    photoResult: null,
    cvResult: null,
    choosing: false,
    instructor_dropdown: [],
    modalAddIsOpen: false
  };
  formRef = this.props.formRef;

  componentDidUpdate(prevProps) {
    if (this.props.instructors && this.formRef && this.formRef.current) {
      this.formRef.current.setFieldsValue({
        id: this.props.instructors.id,
        instructor_name: this.props.instructors.instructor_name,
        job_title: this.props.instructors.job_title,
        biography: this.props.instructors.biography,
        uploadPhoto: this.props.instructors.cover,
        uploadCV: this.props.instructors.cv,
      });
    }
  }


  onCloseAddModal = async () => {
    this.setState({
      id: null,
      file: null,
      fileList: [],
      photo: null,
      fileListPhoto: [],
      photoResult: null,
      cvResult: null,
    })
    this.props.onCloseAddModal();
    // this.setState({ modalAddIsOpen: false });
  };

  onSaveModal = async () => {
    console.log(this.state);
    console.log(this.state.file);
    console.log(this.state.photo);
    this.formRef.current
      .validateFields()
      .then(values => {
        if (this.props.is_edit) {
          this.editInstructor().then((hasil) => {
            console.log(hasil);
            if (hasil.data) {
              this.props.onCloseAddModal();
              this.formRef.current.resetFields();
            //   this.props.addInstructor({
            //     ...hasil.data,
            //     key: hasil.data.id
            //   });
            } else {
              console.log('ada yg error');
            }
          })
        } else {
          if (!this.formRef.current.getFieldValue('id')) {
            this.saveInstructor().then((hasil) => {
              console.log(hasil);
              this.props.onCloseAddModal();
              this.formRef.current.resetFields();
            //   this.props.addInstructor({
            //     ...hasil.data,
            //     key: hasil.data.id
            //   });
            })
          } else {
            this.props.onCloseAddModal();
            this.formRef.current.resetFields();
            if (this.state.instructor && this.state.instructor.id) {
            //   this.props.addInstructor({
            //     ...this.state.instructor,
            //     key: this.state.instructor.id
            //   })
            } else {
            //   this.props.addInstructor({
            //     id: this.state.id,
            //     key: this.state.id,
            //     instructor_name: this.state.instructor_name,
            //     job_title: this.state.job_title,
            //     biography: this.state.biography,
            //     files: this.state.files
            //   });
            }
          }
        }
      })
      .catch(info => {
        console.log('Validate Failed:', info);
      });
  }

  toBase64 = file => new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
  });

  processFile(f) {
    f = f.split(';base64,');
    if (f.length > 1) {
      f = f[1]
    } else {
      f = f[0]
    }
    return f
  }

  editInstructor() {
    return new Promise(async(resolve, reject) => {
      let f, g;
      if (this.state.file) {
        f = await this.toBase64(this.state.file)
        f = this.processFile(f)
      }
      if (this.state.photo) {
        g = await this.toBase64(this.state.photo)
        g = this.processFile(g)
      }
      const token = localStorage.getItem('token');
      const body = {
        instructor_name: this.formRef.current.getFieldValue('instructor_name'),
        job_title: this.formRef.current.getFieldValue('job_title'),
        biography: this.formRef.current.getFieldValue('biography'),
      }
      if (f && this.state.file) {
        body.cv = {
          file: f,
          file_name: this.state.file.name,
          file_size: this.state.file.size,
          file_type: this.state.file.type
        }
      }
      if (g && this.state.photo) {
        body.cover = {
          file: g,
          file_name: this.state.photo.name,
          file_size: this.state.photo.size,
          file_type: this.state.photo.type
        }
      }
      fetch(`${process.env.REACT_APP_BASEURL}/api/update/instructor/${this.props.instructors.id}`, {
        method: "PUT",
        body: JSON.stringify(body),
        headers: {
            "Authorization": `Bearer ${token}`,
            "Content-Type": "application/json"
        },
      }).then((response) => {
        response.json()
        .then((result) => {
          console.log(result)
          resolve(result)
        })
      })
    })
  }

  saveInstructor() {
    return new Promise((resolve, reject) => {
      if (!this.state.file) {reject()};
      if (!this.state.photo) {reject()};
      this.toBase64(this.state.file).then(f => {
        f = this.processFile(f)
        this.toBase64(this.state.photo).then(g => {
          g = this.processFile(g)
          const token = localStorage.getItem('token');
          const body = {
            instructor_name: this.formRef.current.getFieldValue('instructor_name'),
            job_title: this.formRef.current.getFieldValue('job_title'),
            biography: this.formRef.current.getFieldValue('biography'),
            cv: {
              file: f,
              file_name: this.state.file.name,
              file_size: this.state.file.size,
              file_type: this.state.file.type
            },
            cover: {
              file: g,
              file_name: this.state.photo.name,
              file_size: this.state.photo.size,
              file_type: this.state.photo.type
            }
          }
          fetch(`${process.env.REACT_APP_BASEURL}/api/create/instructor`, {
            method: "POST",
            body: JSON.stringify(body),
            headers: {
                "Authorization": `Bearer ${token}`,
                "Content-Type": "application/json"
            },
          }).then((response) => {
            response.json()
            .then((result) => {
              console.log(result)
              resolve(result)
            })
          })
        })
      })
    })
  }

  fetchInstructor = () => {
    fetchInstructorSearch(this.formRef.current.getFieldValue('instructor_name')).then((hasil) => {
      this.setState({
        instructor_dropdown: hasil.rows,
        loading: false,
      });
    })
  }

  handleSearch = async(value) => {
    console.log('handleSearch')
    if (value) {
      this.setState({ 
        photoResult: null,
        cvResult: null,
        choosing: false
      })
      this.formRef.current.setFieldsValue({
        instructor_name: value,
        job_title: null,
        biography: null,
        uploadPhoto: null,
        uploadCV: null,
      })
    }
    if (!this.debouncedFn) {
      this.debouncedFn = _.debounce(() => {
        if (value && this.formRef.current) {
          this.fetchInstructor(this.formRef.current.getFieldValue('instructor_name'));
          console.log(`instructor_name: ${this.formRef.current.getFieldValue('instructor_name')}`);
        } else {
          this.setState({ instructor_dropdown: [] });
        }
      }, 1000);
    }
    this.debouncedFn();
  };

  handleChange = (value, options) => {
    console.log(value);
    console.log(options);
    const result = this.state.instructor_dropdown.filter(a => a.id === parseInt(value))
    console.log(result);
    if (result && result.length === 1) {
      fetchInstructorById(result[0].id).then(hasil => {
        console.log(`hasilFetch: `,hasil);
        this.setState({
          instructor: hasil.data,
          photoResult: hasil.data.cover.url,
          cvResult: hasil.data.cv.url,
          choosing: true
        })
      }).catch(err => {
        console.log(err)
      }).finally(() => {
        this.formRef.current.setFieldsValue({
          id: result[0].id,
          instructor_name: result[0].instructor_name,
          job_title: result[0].job_title,
          biography: result[0].biography,
          uploadPhoto: result[0].cover,
          uploadCV: result[0].cv,
        });
      })
    } else {
      // this.setState({
      //   instructor_name: options.children
      // })
      this.formRef.current.setFieldsValue({
        instructor_name: options.children,
        choosing: false
      })
    }
  };

  propsUpload = {
    onRemove: file => {
      this.setState({
        fileList: [],
        file: null
      })
    },
    beforeUpload: file => {
      this.setState({
        fileList: [file],
        file
      })
      return false;
    },
    fileList: this.state.fileList,
  }

  propsPhoto = {
    onRemove: file => {
      this.setState({
        fileListPhoto: [],
        photo: null
      })
    },
    beforeUpload: file => {
      this.setState({
        fileListPhoto: [file],
        photo: file
      })
      return false;
    },
    fileList: this.state.fileListPhoto,
  }

  onFinish = values => {
    console.log('Success:', values);
  };

  onFinishFailed = errorInfo => {
    console.log('Failed:', errorInfo);
  };

  render(){
    const options = this.state.instructor_dropdown?.map(d => <Option key={d.id}>{d.instructor_name}</Option>);
    return(
      <>
        <Modal show={this.props.modalAddIsOpen} onHide={this.onCloseAddModal}>
          <Modal.Header bsPrefix closeButton >
              <Modal.Title>Add New Instructor</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form
              name="basic"
              ref={this.formRef}
              initialValues={{
                instructor_name: null
              }}
              onFinish={()=> {this.onFinish()}}
              onFinishFailed={()=> {this.onFinishFailed()}}
            >
              <Form.Item
                name="instructor_name"
                rules={[{ required: true, message: 'Please input Instructor Name!' }]}
              > 
              <Input type="text" placeholder="Instructor Name"/>
              </Form.Item>
              <Form.Item
                name="job_title"
                rules={[{ required: true, message: 'Please input Job Title!' }]}
              >
                <Input type="text" placeholder="Job Title"/>
              </Form.Item>
              <Form.Item
                name="biography"
                rules={[{ required: true, message: 'Please input Biography!' }]}
              >
                <Input.TextArea rows="4" placeholder="Biography" 
                  value={this.state.biography} />
              </Form.Item>
              <Form.Item
                name="uploadPhoto"
                rules={[{ required: true, message: 'Please Upload Photo!' }]}
              >
                <Upload {...this.propsPhoto}>
                  <ButtonAnt type="primary" block>
                    <UploadOutlined /> Upload Photo
                  </ButtonAnt>
                </Upload>
              </Form.Item>
              <Form.Item
                name="uploadCV"
                rules={[{ required: true, message: 'Please Upload CV!' }]}
              >
                <Upload {...this.propsUpload}>
                  <Button style={{width: "100%"}}>
                    <UploadOutlined /> Upload CV
                  </Button>
                </Upload>
              </Form.Item>
            </Form>
          </Modal.Body>
        <Modal.Footer>
        { this.props.is_edit
            ? <>
                <ButtonAnt icon={<CloseOutlined />}
                  style={{backgroundColor: "#FF2E00", color: "white", width: "45px"}}
                  onClick={() => { this.props.onCloseAddModal() }}>
                </ButtonAnt>
                <ButtonAnt icon={<CheckOutlined />}
                  style={{backgroundColor: "#00C242", color: "white", width: "45px"}}
                  onClick={() => { this.onSaveModal() }}>
                </ButtonAnt>
              </>
            : 
              <ButtonAnt htmlType="submit" className="button-grape" 
                onClick={() => { this.onSaveModal() }}>{this.state.choosing ? 'Add' : 'Create'}
              </ButtonAnt>
          }
        </Modal.Footer>
        </Modal>
      </>
    )
  }
}

export default ModalInstructor;