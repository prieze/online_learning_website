import React, { Component} from 'react';
import { Link, Redirect } from 'react-router-dom';
import { Row, Col, Container, Image } from "react-bootstrap";
import '../Footer/Footer.css'
import 'font-awesome/css/font-awesome.min.css';

class Footer extends Component {

    constructor(props) {
        super(props)
        this.state = {
            redirect: false
        }
    }

    Appstore = 'https://galilagi-media.oss-ap-southeast-5.aliyuncs.com/assets/appstore.png';
    PlayStore = 'https://galilagi-media.oss-ap-southeast-5.aliyuncs.com/assets/playstore.png';

    changeURL(url) {
        if (this.props && this.props.history) {
            this.props.history.push(url)
        }
        this.setState({ redirect: url });
    }

    render() {
        const { redirect } = this.state;

        if (redirect) {
            this.state.redirect = false;
            return <Redirect to={redirect} />;
        }

        return (
            <Container fluid style={{ backgroundColor:  "#002333", padding: '2rem' }}>
            <Row style={{ width: '90%', margin: 'auto', lineHeight: '1', marginLeft: '12%' }}>
                <Col xs={12} sm={6} md={4} lg={4}>
                    <h5 className="footer-h" >Company</h5>
                    <ul>
                        <li><Link to="/Dashboard" className="footer-list">About</Link></li>
                        <li><Link to="/Dashboard" className="footer-list">Help and Support</Link></li>
                        <li><Link to="/Dashboard" className="footer-list">Contact</Link></li>
                        <li><Link to="/Dashboard" className="footer-list">Become a Partnership</Link></li>
                        <li><p style={{ fontSize: '12px', marginTop: '10%' }}>&copy; 2020 Galilagi. All Rights Reserved</p></li>
                    </ul>
                </Col>
                <Col xs={12} sm={6} md={4} lg={4}>
                    <h5 className="footer-h" >Community</h5>
                    <ul>
                        <li><Link to="/Dashboard" className="footer-list">Free Classes</Link></li>
                        <li><Link to="/Dashboard" className="footer-list">Scholarships</Link></li>
                        <li style={{ cursor: 'pointer' }} className="footer-list" onClick={() => { this.changeURL('/Verification') }} >Certificate Verification</li>
                    </ul>
                </Col>
                <Col xs={12} sm={6} md={4} lg={4}>
                    <h5 className="footer-h" >Mobile</h5>
                    <ul>
                        <li><Link to="/Dashboard"><Image className="store" src={this.PlayStore} /></Link></li>
                        {/* <li><Link to="/Dashboard"><Image className="store" src={this.Appstore} /></Link></li> */}
                    </ul>
                </Col>
                {/* <Col xs={12} sm={6} md={3} lg={3}>
                    <h5 className="footer-h" >Get Connected</h5>
                    <br/>
                    <a style={{ marginLeft: '14%' }} >
                        <i className="fa fa-instagram fa-lg mr-md-4 mr-4"> </i>
                        <i className="fa fa-twitter fa-lg white-text mr-md-4 mr-4"> </i>
                        <i className="fa fa-facebook fa-lg white-text mr-md-4 mr-4"> </i>
                    </a>
                </Col> */}
                </Row>
            </Container>
        )
    }
}

export default Footer;