import React, { Component } from 'react';
import { Container, Row, Col, Button, Card, Accordion, ProgressBar, Form } from 'react-bootstrap';
import Footer from '../Footer/Footer';
import Navigation from '../Navigation/Navigation';
import ListInstructor from './ListInstructor';
import ListTopics from './ListTopics';
import ListReviews from '../Reviews/ListReviews';   
import _ from "lodash";
import { Collapse, Rate, message } from 'antd';
import Moment from 'moment';
const { Panel } = Collapse;

class UserDetailWebinar extends Component{

    constructor(props) {
        super(props);

        this.state = {
         webinar: [],
         reviews: [],
         star: [],
         search: '',
         index: 0,
         hasil: 0,
         jumlah: [5, 4, 3, 2, 1]
        };

    }

    componentDidMount(){
        this.WebinarbyId();
        this.listReviews();
    }

    WebinarbyId(){
        const token = localStorage.getItem('token');
        const id = this.props.match.params.id;
        fetch(`${process.env.REACT_APP_BASEURL}/api/webinar/${id}`, {
            method: "GET",
            headers: {
                "Authorization": `Bearer ${token}`,
                "Content-Type": "application/json"
            },
        }).then((response) => {
            response.json()
                .then((result) => {
                    this.setState({
                        webinar: result.rows,
                    });
                    console.log("WEBINAR",this.state.webinar)
                })
        })
    }

    addToChart(){
        const token = localStorage.getItem('token');
        const id = this.props.match.params.id;
        fetch(`${process.env.REACT_APP_BASEURL}/api/cart`, {
            method: "POST",
            body: JSON.stringify({webinar_id: this.state.webinar.id}),
            headers: {
                "Authorization": `Bearer ${token}`,
                "Content-Type": "application/json"
            },
        }).then((response) => {
            response.json()
                .then((result) => {
                    console.warn("result", result);
                    if (result.sukses === true) {
                        this.props.history.push(`/UserDetailWebinar/${id}`)
                        message.success("Berhasil ditambahkan", 1)
                    } else {
                        message.error(result.msg)
                    }
                })
        })
    }

    listReviews(){
        const id = this.props.match.params.id;
        const search = this.state.search;
        const token = localStorage.getItem('token');
        const next = this.state.index;
        fetch(`${process.env.REACT_APP_BASEURL}/api/rating/webinar/${id}?limit=2&offset=${next}&search=${search}`, {
            method: "GET",
            headers: {
                "Authorization": `Bearer ${token}`,
                "Content-Type": "application/json"
            },
        }).then((response) => {
            response.json()
                .then((result) => {
                    this.setState({
                        reviews: result.rows,
                        star: result.star,
                        avg: result.avg,
                        hasil: parseFloat(result.avg.average).toFixed(1)
                    });
console.log(this.state);
                })
        })
    }

    loadMore = async(index) => {
        const next = this.state.index + index;
        await this.setState({
            index: next
        });
        console.log(next);
        console.log(this.state.index);
        this.listReviews();
    }

    onChange = (event) => {
        /* signal to React not to nullify the event object */
        console.log('onChange triggered');
        event.persist();

        if (!this.debouncedFn) {
            this.debouncedFn = _.debounce(() => {
                //  let searchString = event.target.value;
                this.setState({ search: event.target.value });
                this.listReviews(1);
            }, 1000);
        }
        this.debouncedFn();
    }

    render(){

        return(
            <>
            <Navigation/>
            <Container fluid style={{ padding: '30px', width:'100%', backgroundColor: '#f1f1f1' }}>
                <Row style={{ width: "85%", margin: 'auto' }}>
                    <Col lg={8}>
                        <p style={{ fontFamily: "Gordita-Bold", fontSize: "18px" }}>{this.state.webinar.webinar_name}</p>
                        <p style={{  fontSize: "14px" }}>{this.state.webinar.description}</p>
                        <Row style={{ width: '80%' }}>
                            <Col>
                                <p style={{  fontSize: "15px" }}><i style={{ color: '#002333' }} className="fa fa-calendar"></i> {Moment(this.state.webinar.from_date).format( 'dddd, DD MMMM YYYY')}</p>
                                <p style={{  fontSize: "15px" }}><i style={{ color: '#002333' }} className="fa fa-clock-o"></i> {Moment(this.state.webinar.from_date).format( 'H:mm')} - {Moment(this.state.webinar.end_date).format( 'H:mm')} WIB</p>
                            </Col>
                            <Col>
                                <p style={{  fontSize: "15px" }}><i style={{ color: '#002333' }} className="fa fa-map-marker"></i> {this.state.webinar.location}</p>
                                <p style={{  fontSize: "15px" }}><i style={{ color: '#002333' }} className="fa fa-ticket"></i> {this.state.webinar.capacity} Participans</p>
                            </Col>
                        </Row>
                    </Col>
                    <Col >
                        <Card>
                            <Card.Body style={{ padding: '1rem' }}>
                            <video controls width="100%" height="auto" src={this.state.webinar?.video?.url}/>
                            {/* {
                                this.state.course?.courses_user?.length > 0 */}
                                {/* ? <Link to={`/ViewCourses/${this.state.course.id}`}><Button className="button-grape" block style={{ fontFamily: "Gordita-Bold" }} >See Courses</Button></Link> */}
                                <Button variant="danger" block style={{ fontFamily: "Gordita-Bold" }} onClick = { () => { this.addToChart() }} >Add to Chart</Button>

                            {/* } */}
                                <Button disabled variant="outline-secondary" block style={{ fontFamily: "Gordita-Bold", marginTop: "5%" }}>Buy now IDR </Button>
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>

                <Row style={{ width: "85%", margin: 'auto' }}>
                    <Col>
                        <p style={{ fontFamily: "Gordita-Bold", fontSize: "24px", color: "#020288", }}>Instructor</p>
                        <ListInstructor data={this.state.webinar.instructors}/>
                    </Col>
                </Row>

                <Row style={{ width: "85%", margin: 'auto', marginTop: '5%' }}>
                    <Col>
                        <p style={{ fontFamily: "Gordita-Bold", fontSize: "24px", color: "#020288", }}>Material</p>
                        <Card style={{ color: "white", background: "rgba(58, 44, 125, 0.34)", borderRadius: '0', boxShadow: 'none' }} >
                            <Card.Body>
                                <Row className="justify-content-center" style={{ color: '#020202' }}>
                                    <Col>Topic</Col>                                    
                                </Row>
                            </Card.Body>
                        </Card>
                        <Accordion defaultActiveKey="0">
                        {
                            this.state.webinar.webinar_topics?.map((list, index) => {
                                return <ListTopics
                                    id={list.id}
                                    key={list.id}
                                    topic={list}
                                    index={index}
                                    sub_topic={(list && list.webinar_topics && list.webinar_topics.sub_topic_name) ? list.webinar_topics.sub_topic_name : null}></ListTopics>
                            })
                        }
                        </Accordion>
                    </Col>
                </Row>

                <Row style={{ width: "85%", margin: 'auto', marginTop: '5%' }}>
                    <Col>
                        <Card style={{ background: "rgba(86, 103, 255, 0.13)", borderRadius: '0', boxShadow: 'none' }} >
                            <Card.Body>
                                <Row className="justify-content-center" style={{fontSize: "24px", color: '#020288', fontFamily: 'Gordita-bold' }}>
                                    <Col>Term & Condition</Col>
                                </Row>
                            </Card.Body>
                        </Card>
                        <Card style={{ background: "#FFFFFF", borderRadius: '0', boxShadow: 'none' }} >
                            <Card.Body>
                                <Row className="justify-content-center">
                                    <Col>{this.state.webinar.term_condition}</Col>                                    
                                </Row>
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>

                <Row style={{ width: "85%", margin: 'auto', marginTop: '5%' }}>
                        <Col>
                            <p style={{ fontFamily: 'Gordita-bold', color: '#392C7E', fontSize: '18px' }}>How students rated this class</p>
                            <Card style={{ backgroundColor: '#F9F8F8', boxShadow: 'none', border: '0' }}>
                                <Card.Body>
                                    <Row className="justify-content-center">
                                        <Col style={{ textAlign: 'center' }} lg={4}>
                                            <p style={{ fontFamily: "Gordita-Bold", fontSize: "19px", color: "#020288", marginTop: "13%" }}>{this.state.hasil}</p>
                                            <Rate disabled defaultValue={5} />
                                            <p style={{ color: '#020288' }}>Course Rating</p>
                                            <p style={{ color: '#020288', lineHeight: '0.5' }}>(Based on {this.state.avg?.count_review} Reviews)</p>
                                        </Col>
                                        <Col lg={8}>
                                        <div style={{ paddingTop: "4%" }}>
                                                {   this.state.jumlah.map(a => {
                                                    return  <Row>
                                                    <Col xs={1} sm={1} md={1} lg={1} style={{ color: '#020288' }} ></Col>
                                                    <Col lg={9}style={{ marginLeft: '-6%', marginTop: '4px' }}><ProgressBar now={this.state.star.filter(z => z.star === a).map(e => {return e.count})} variant="warning" /><br/></Col>
                                                    <Col xs={2} sm={2} md={2} lg={2} style={{ marginLeft: '-3%', color: '#020288' }} >{a} <i className="fa fa-star" style={{ color: '#FF8A00' }} /></Col>
                                                </Row>
                                                })
                                                }
                                            </div>
                                        </Col>
                                    </Row>
                                </Card.Body>
                            </Card>
                        </Col>
                    </Row>
                    
                    <Row style={{ width: "85%", margin: 'auto', marginTop: '5%' }}>
                        <Col>
                            <p style={{ fontFamily: "Gordita-Bold", fontSize: "24px", color: "#020288" }}>Reviews</p>
                            <Form >
                                <Form.Group>
                                    <Row>
                                        <Col lg={6}>
                                            <Form.Control type="text" placeholder="Search reviews" onChange={this.onChange} debounce="1000" style={{ backgroundColor: "#F1F1F1" }} />
                                            <span className="icon"><i ></i></span>
                                        </Col>
                                        <Col lg={6}>
                                            <Form.Control as="select" style={{ backgroundColor: "#F1F1F1" }} onChange={(e) => {this.dropRate(e.target.value)}} >
                                                <option value="0">All Ratings</option>
                                                {
                                                    this.state.star.map((list) => {
                                                        return <option value={list.star} >Bintang {list.star}</option>
                                                    })
                                                }
                                            </Form.Control>
                                        </Col>
                                    </Row>
                                </Form.Group>
                            </Form>
                        </Col>
                    </Row>

                    <Row style={{ width: "85%", margin: 'auto' }}>
                        <Col>
                        {
                            this.state.reviews.map((list) => {
                                return <ListReviews id={list.id} key={list.id} list={list} />
                            })
                        }
                        </Col>
                    </Row>
                    
                    <Row style={{ width: "18%", margin: 'auto', marginTop: '3%' }}>
                        <Button onClick={ () => { this.loadMore(2) } } className="button-grape" block >Load more reviews</Button>
                    </Row>

            </Container>
            <Footer/>
            </>
        )

    }

}

export default UserDetailWebinar;