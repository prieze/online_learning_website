import React, { Component } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import Navigation from '../Navigation/Navigation';
import Footer from '../Footer/Footer';
import { Tabs, message } from 'antd';
import WaitingReviews from './WaitingReviews';
import MyReviews from './MyReviews';
const { TabPane } = Tabs;

class WebinarReviews extends Component{

    state = {
        webinar:[],
        list: []
    }

    componentDidMount(){
      this.myReviews();
        this.waitingReviews();
    }

    waitingReviews() {

        const token = localStorage.getItem("token");
        this.setState({ loading: true });
        fetch(`${process.env.REACT_APP_BASEURL}/api/wait/rating/webinar`, {
          method: "GET",
          headers: {
            Authorization: `Bearer ${token}`,
            "Content-Type": "application/json",
          },
        })
          .then((response) => response.json())
          .then((json) => {
            this.setState({
              webinar: json.data,
            });
          })
          .catch((error) => console.log("error", error));
    }

    myReviews() {

        const token = localStorage.getItem("token");
        this.setState({ loading: true });
        fetch(`${process.env.REACT_APP_BASEURL}/api/myrating`, {
          method: "GET",
          headers: {
            Authorization: `Bearer ${token}`,
            "Content-Type": "application/json",
          },
        })
          .then((response) => response.json())
          .then((json) => {
            console.log(json);
            this.setState({
              list: json.data,
            });
            console.log(this.state);
          })
          .catch((error) => console.log("error", error));
    }

    render(){
        return(
            <>
            <Navigation />
            <Container fluid style={{ width: '100%', backgroundColor: '#F1F1F1', paddingBottom: '30px' }}>
                <Row className="justify-content-center">
                    <Col md={10}><p style={{ color: '#6831B0', fontSize: '18px', fontFamily: 'Gordita-bold', marginTop: '5%' }}>My Webinar</p></Col>
                </Row>

                <Row style={{ width: '85%', margin: 'auto' }}>
                    <Col>
                        <Tabs defaultActiveKey="1">
                            <TabPane tab="Waiting for Review" key="1">
                            {
                                this.state.webinar.map((a) => {
                                    return <WaitingReviews id={a.id} key={a.id} webinar={a} state={ this.state } ></WaitingReviews>
                                })
                            }
                            </TabPane>
                            <TabPane tab="My Reviews"  key="2">
                            {
                                this.state.list.map((a) => {
                                  return <MyReviews id={a.id} key={a.id} webinar={a} state={ this.state } ></MyReviews>
                                })
                            }
                            </TabPane>
                        </Tabs> 
                    </Col>
                </Row>


            </Container>
            <Footer />
            </>
        )
    }

}

export default WebinarReviews;