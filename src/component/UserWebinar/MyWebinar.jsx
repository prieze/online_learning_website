import React from 'react';
import { Component } from 'react';
import { Col, Container, Row, Navbar, Form, Nav } from 'react-bootstrap';
import Footer from '../Footer/Footer';
import Navigation from '../Navigation/Navigation';
import ListWebinar from './ListWebinar';
import Webinars from './Webinars';
import { Tabs } from 'antd';
import _ from "lodash";
const { TabPane } = Tabs;

class MyWebinar extends Component{

    constructor(props) {
        super(props);

        this.state = {
         webinar: [],
         search: ''
        };

    }

    componentDidMount(){
        this.WebinarAll();
    }

    WebinarAll(){
        const token = localStorage.getItem('token');
        const search = this.state.search;
        fetch(`${process.env.REACT_APP_BASEURL}/api/webinar?limit=&offset=&search=${search}`, {
            method: "GET",
            headers: {
                "Authorization": `Bearer ${token}`,
                "Content-Type": "application/json"
            },
        }).then((response) => {
            response.json()
                .then((result) => {
                    this.setState({
                        webinar: result.rows,
                    });
                    // console.log("WEBINAR",this.state.webinar)
                })
        })
    }

    onChange = (event) => {
        console.log('onChange triggered');
        event.persist();
  
        if (!this.debouncedFn) {
            this.debouncedFn = _.debounce(() => {
                this.setState({ search: event.target.value });
                this.WebinarAll(1);
            }, 1000);
        }
        this.debouncedFn();
    }

    changeURL(url) {
        // this.setState({ redirect: url });
        this.props.history.push(url);
    }

    render(){
        return(
            <>
            <Navigation/>
            <Container fluid style={{ width: '100%', padding: '50px', backgroundColor: '#F1F1F1' }}>
                <Row style={{ width: '85%', margin: 'auto' }}>
                    <Col>
                        <Navbar expand="lg" style={{ background: "rgba(58, 44, 125, 0.34)", marginBottom: "3%" }}>
                        <Navbar.Brand style={{ color: "#FFFFFF", fontFamily: "Gordita-bold" }}>Webinar</Navbar.Brand>
                            <Navbar.Toggle aria-controls="basic-navbar-nav" />
                            <Navbar.Collapse id="basic-navbar-nav">
                            <Nav className="mr-auto" />
                                <Form inline>
                                    <Form.Control type="text" id="search" placeholder="Search" style={{ borderRadius: "30px" }} onChange={this.onChange} debounce="1000" />
                                </Form>
                            </Navbar.Collapse>
                        </Navbar>
                    </Col>
                </Row>
                <Row style={{ width: '85%', margin: 'auto' }}>
                    <Col>
                        <Tabs>
                            <TabPane tab="All Webinar"  key="1">
                                <Row>
                                {
                                    this.state.webinar.map((webinar) => {
                                    return <Col style={{ paddingBottom: "5%" }} xs={12} sm={6} md={4} lg={3} xl={3} >
                                        <ListWebinar key={webinar.id} id={webinar.id} webinar={webinar} history={(url) => { this.changeURL(url); }} />
                                    </Col>
                                    })
                                }
                                </Row>
                            </TabPane>
                            <TabPane tab="My Webinar"  key="2">
                                <Webinars/>
                            </TabPane>
                        </Tabs> 
                    </Col>
                </Row>
            </Container>
            <Footer/>
            </>
        )
    }

}

export default MyWebinar;