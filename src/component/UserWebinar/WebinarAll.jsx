import React, { Component } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import Footer from '../Footer/Footer';
import Navigation from '../Navigation/Navigation';
import ListWebinar from './ListWebinar';

class WebinarAll extends Component{

    constructor(props) {
        super(props);

        this.state = {
         webinar: []
        };
        console.log(props);
    }

    componentDidMount(){
        this.WebinarAll();
    }

    WebinarAll(){
        const token = localStorage.getItem('token');
        fetch(`${process.env.REACT_APP_BASEURL}/api/webinar?limit=&offset=&search=`, {
            method: "GET",
            headers: {
                "Authorization": `Bearer ${token}`,
                "Content-Type": "application/json"
            },
        }).then((response) => {
            response.json()
                .then((result) => {
                    // this.setState({
                    //     webinar: result.rows,
                    // });
                    // console.log("WEBINAR",this.state.webinar)
                    const temp = [];
                    const discount = result.rows.map((disc)=>{
                        const price = parseInt(disc.price);
                        const dic = parseInt(disc.discount);
                        const jum = (dic/100)*price;
                        const tot = price-parseInt(jum);
                        temp.push({
                            ...disc,
                            hargadisc: tot
                        })
                        return tot
                      });
                    // this.state.webinar.forEach(function(jum, index) {
                    // jum.hargadisc = discount[index]
                    // console.log(typeof(jum));
                    // console.log(typeof(discount));
                    // console.log(index);
                    // })
                    this.setState({
                        webinar: temp
                    })
                    console.log(`webinarall`, this.state);
                })
        })
    }

    changeURL(url) {
        this.props.history.push(url);
    }

    render(){
        return(
            <>
            <Navigation/>
            <Container fluid style={{ width: '100%', backgroundColor: '#f1f1f1', padding: '35px' }}>
                <Row style={{ width: "90%", margin: "auto" }}>
                  {
                    this.state.webinar?.map((webinar) => {
                      return <Col style={{ paddingBottom: "5%" }} xs={12} sm={6} md={4} lg={3} xl={3} >
                        <ListWebinar key={webinar.id} id={webinar.id} webinar={webinar} disc={webinar.hargadisc} history={(url) => { this.changeURL(url); }} />
                      </Col>
                    })
                  }
                </Row>
            </Container>
            <Footer/>
            </>
        )
    }

}

export default WebinarAll;