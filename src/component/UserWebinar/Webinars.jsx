import React, { Component } from 'react';
import { Row, Col } from 'react-bootstrap';
import WebinarPaid from './WebinarPaid';
import _ from "lodash";

class Webinars extends Component{

    constructor(props) {
        super(props);

        this.state = {
            webinars: [],
            search: ''
        };

    }

    componentDidMount(){
        this.listWebinar();
    }
    
    listWebinar(status){
        const token = localStorage.getItem('token');
        const search = this.state.search;
        fetch(`${process.env.REACT_APP_BASEURL}/api/webinars/user?limit=&offset=&search=${search}`, {
            method: "GET",
            headers: {
                "Authorization": `Bearer ${token}`,
                "Content-Type": "application/json"
            },
        }).then((response) => {
            response.json()
                .then((result) => {
                    this.setState({
                        webinars: result.rows,
                    });
                    console.log(this.state.webinars)
                })
        })
    }

    onChange = (event) => {
        console.log('onChange triggered');
        event.persist();
  
        if (!this.debouncedFn) {
            this.debouncedFn = _.debounce(() => {
                this.setState({ search: event.target.value });
                this.listWebinar(1);
            }, 1000);
        }
        this.debouncedFn();
    }


    render(){

        const style={
            cursor: 'pointer'
        }

        return(
            <>
            <Row  >
                <Col lg={3}>
                    <p style={style} onClick={ () => { this.statusTransaction('') } } >All</p>
                    <p style={style} onClick={ () => { this.statusTransaction('waiting_payment') } } >Sedang Berlangsung</p>
                    <p style={style} onClick={ () => { this.statusTransaction('waiting_confirmation') } }>Akan Datang</p>
                    <p style={style} onClick={ () => { this.statusTransaction('paid') } }>Sudah Berakhir</p>
                </Col>
                <Col>
                    {
                        this.state.webinars.map((list) => {
                            return <WebinarPaid key={list.id} id={list.id} list={list} />
                        })
                    }
                </Col>
            </Row>
            </>
        )
    }

}

export default Webinars;