import React from 'react';
import {useContext} from 'react';
import { Row, Col, Table, Accordion, Card, Button, useAccordionToggle, AccordionContext } from 'react-bootstrap';
import 'font-awesome/css/font-awesome.min.css';
import { useHistory } from "react-router-dom";
import {countMinuteFromSecond, countMinutetoSecond, sumDurationSubTopic } from '../Utils'

function ContextAwareToggle({ children, eventKey, callback }) {
    const currentEventKey = useContext(AccordionContext);
  
    const decoratedOnClick = useAccordionToggle(
      eventKey,
      () => callback && callback(eventKey),
    );
  
    const isCurrentEventKey = currentEventKey === eventKey;
  
    return (
        <>
        { isCurrentEventKey 
            ? <i style={{ cursor: 'pointer' }} className="fa fa-minus" aria-hidden="true" onClick={decoratedOnClick}/>
            : <i style={{ cursor: 'pointer' }} className="fa fa-plus" aria-hidden="true" onClick={decoratedOnClick}/>
        }
        </>
        
    );
  }

const ListTopics = (props) => {
    
    return (
        <Card style={{ borderRadius: '0', boxShadow: 'none' }} key={props.key}>
            <Card.Header style={{background: "white", color: "black" }}>
                <Row>
                    <Col xs={11} lg={11}>
                        {props.topic?.topic_name}
                    </Col>
                    <Col xs={1} lg={1}>
                        <ContextAwareToggle eventKey={props.index.toString()}></ContextAwareToggle>
                    </Col>
                </Row>
            </Card.Header>
            <Accordion.Collapse eventKey={props.index.toString()}>
            <Card.Body style={{background: "#E8E8E8"}}>
                {props.topic?.webinar_sub_topics?.map(sub => {
                    return (
                        <Row className="justify-content-center">
                            <Col>
                                {sub.sub_topic_name}
                            </Col>
                        </Row>
                    )
                })}
            </Card.Body>
            </Accordion.Collapse>
        </Card>
    )
}

export default ListTopics;