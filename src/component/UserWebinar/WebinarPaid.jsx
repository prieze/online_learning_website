import React from 'react';
import { Row, Col, Card } from 'react-bootstrap';
import Moment from 'moment';

const WebinarPaid = (props) => {

    return(
        <>
        <Card style={{ marginTop: '1%', borderRadius: '12px', boxShadow: 'none' }}>
            <Row>   
                <Col xs={12} sm={7} md={6} lg={3}>
                    <Card.Img style={{ borderRadius: '12px 0 0 12px', height: '100%' }} src={props.list.webinar.cover?.url} />
                </Col>
                <Col>
                    <Card.Body style={{ padding: '1.5rem' }}>
                    {Moment(props.list.webinar.from_date) >= Moment() && <p className="soon" >Akan Datang</p>}
                    {Moment(props.list.webinar.from_date) === Moment() && <p className="now" >Sedang Berlangsung</p>}
                    {Moment(props.list.webinar.from_date) <= Moment() && <p className="end" >Sudah Berakhir</p>}
                    <Card.Title style={{ color: '#414141', fontFamily: 'Gordita-bold' }} >{props.list.webinar.webinar_name}</Card.Title>
                        <table>
                            <tr>
                                <td><i className="fa fa-calendar"></i></td>
                                <td>{Moment(props.list.webinar.from_date).format( 'dddd, DD MMMM YYYY')}</td>
                            </tr>
                            <tr>
                                <td><i className="fa fa-clock-o"></i></td>
                                <td>{Moment(props.list.webinar.from_date).format( 'H:mm')} - {Moment(props.list.webinar.end_date).format( 'H:mm')} WIB</td>
                            </tr>
                            <tr>
                                <td><i className="fa fa-map-marker"></i></td>
                                <td>{props.list.webinar.location}</td>
                            </tr>
                        </table>
                    </Card.Body>
                </Col>
            </Row>
        </Card>
        </>
    )
}

export default WebinarPaid;