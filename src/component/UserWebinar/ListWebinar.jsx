import React from 'react';
import { Row, Col, Card, Image, Button } from 'react-bootstrap';
import Shop from "../../gambar/shop.png";
import { useHistory } from "react-router-dom";
import { Avatar, Divider, Tooltip, message } from 'antd';
import { UserOutlined, AntDesignOutlined } from '@ant-design/icons';
import soldout from '../../gambar/soldout.png';
import Moment from 'moment';

const ListWebinar = (props) => {
    console.log(props);

    const addToChart = (async (id) => {
		const token = localStorage.getItem('token');
		fetch(`${process.env.REACT_APP_BASEURL}/api/cart`, {
			method: "POST",
			body: JSON.stringify({webinar_id: id}),
			headers: {
				"Authorization": `Bearer ${token}`,
				"Content-Type": "application/json"
			},
		}).then((response) => {
			response.json()
				.then((result) => {
					console.warn("result", result);
					if (result.sukses) {
						console.log(props);
						if(props.reloadData) {
							props.reloadData();
						}
						message.success("Berhasil ditambahkan", 1)
					} else {
						message.error(result.msg, 1)
					}
				})
		})
	})

	// const calDiskon = () => {
	// 	const harga_awal = props.webinar.price;
	// 	console.log(harga_awal);
	// }

	// console.log(calDiskon);
    
    return(
        <>
       <Card className="h-100" style={{ width: "15rem", margin: 'auto', borderRadius: "12px", border: "0.2px solid #DFDFFF", marginTop: '10%' }}>
			<Card.Img variant="top" style={{ height: '150px', borderRadius: '12px 12px 0 0', opacity: props.webinar.capacity > "0" ? '1' : '0.2' }} src={props.webinar.cover?.url}/>
			<p hidden={ props.webinar.discount > "0" ? false : true  } className="diskon">{props.webinar.discount}% Off</p>
            <Image hidden={ props.webinar.capacity > "0" ? true : false } className="soldout" src={soldout}/>
			<Card.Body>
			<Card.Title style={{ cursor: 'pointer', fontFamily: 'Gordita-bold', fontSize: '15px', color: '#002333' }} onClick={ () => { props.history(`/UserDetailWebinar/${props.webinar.id}`) }}>{props.webinar.webinar_name}</Card.Title>
            <table style={{ color: '#5E747E', fontSize: '12px' }} >
                <tr>
                    <td><i className="fa fa-calendar"></i></td>
                    <td>{Moment(props.webinar.from_date).format( 'dddd, DD MMMM YYYY')}</td>
                </tr>
                <tr>
                    <td><i className="fa fa-map-marker"></i></td>
                    <td>{props.webinar.location}</td>
                </tr>
                <tr>
                    <td><i className="fa fa-ticket"></i></td>
                    <td>{props.webinar.capacity} Participans</td>
                </tr>
            </table>
            <Avatar.Group maxCount={2} maxStyle={{ color: '#f56a00', backgroundColor: '#fde3cf' }}>
				{props.webinar.instructors.map(a => {
					return <Avatar src={a.cover?.url} />
				})}
            </Avatar.Group>
				<Row>
					<Col lg={12}>
						<Card.Text style={{ fontSize: '10px', color: '#BAB9B9' }}><s>IDR {parseInt(props.webinar.price).toLocaleString("id-ID")}</s></Card.Text>
					</Col> 
					<Col>
						<Card.Text style={{ color: '#3722D3', fontSize: '15px', fontFamily: 'Gordita-bold' }}>IDR {parseInt(props.webinar.hargadisc).toLocaleString("id-ID")}</Card.Text>
					</Col>
				</Row>
			</Card.Body>
			<Card.Footer>
				{props.webinar?.courses_user?.length > 0 
					? <Button onClick={()=> { props.history(`/ViewCourses/${props.webinar.id}`); }} size="sm" className="button-grape" style={{ borderRadius: '0', backgroundColor: 'rgba(165, 148, 255, 0.78)' }} block>See Course</Button>
					: <Image onClick={()=> { addToChart(props.webinar.id) }} style={{ cursor: 'pointer', marginBottom: '-35px', boxShadow: '4px 4px 20px rgba(0, 0, 0, 0.2)', width: '60px', marginLeft: '65%' }} className="shop" src={Shop} roundedCircle />
				}
			</Card.Footer>
		</Card>
        </>
    )
}

export default ListWebinar;