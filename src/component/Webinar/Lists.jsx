import React from 'react';
import {useContext} from 'react';
import { Row, Col, Table, Accordion, Card, Button, useAccordionToggle, AccordionContext } from 'react-bootstrap';
import 'font-awesome/css/font-awesome.min.css';
import { useHistory } from "react-router-dom";
import {countMinuteFromSecond, countMinutetoSecond, sumDurationSubTopic } from '../Utils'

function ContextAwareToggle({ children, eventKey, callback }) {
    const currentEventKey = useContext(AccordionContext);
  
    const decoratedOnClick = useAccordionToggle(
      eventKey,
      () => callback && callback(eventKey),
    );
  
    const isCurrentEventKey = currentEventKey === eventKey;
  
    return (
        <>
        { isCurrentEventKey 
            ? <i className="fa fa-minus" aria-hidden="true" onClick={decoratedOnClick}/>
            : <i className="fa fa-plus" aria-hidden="true" onClick={decoratedOnClick}/>
        }
        </>
        
    );
  }

const Lists = (props) => {
    // console.log(props.topic.webinar_sub_topics[0].sub_topic_name);
    console.log(props);
    // console.log(typeof(props));
    return (
        <Card style={{ width: "100%", margin: 'auto' }} key={props.key}>
            <Card.Header style={{background: "white",color: "black"}}>
                <Row>
                    <Col xs={4} lg={4}>
                        {/* {props.webinar_topic?.topic_name} */}
                        {props.topic?.topic_name}
                    </Col>
                    <Col xs={4} lg={4}>
                        {/* {props.webinar_topic?.webinar_sub_topics?.length} Material */}
                        {props.topic?.webinar_sub_topics?.length} Material
                    </Col>
                    
                    <Col xs={1} lg={1}>
                        { props.is_edit &&
                            <i className="fa fa-pencil pencil" aria-hidden="true" onClick={() => props.edit(props.webinar_topic)}></i>
                        }
                        {/* <i className="fa fa-pencil pencil" aria-hidden="true" onClick={() => console.log(typeof props.edit)}></i> */}
                    </Col>
                    <Col xs={1} lg={1}>
                        {/* <i className="fa fa-pencil pencil" aria-hidden="true" onClick={() => props.edit(props.id)}></i> */}
                        <ContextAwareToggle eventKey={props.index.toString()}></ContextAwareToggle>
                        {/* <Accordion.Toggle as={Button} variant="link" eventKey={props.index.toString()}>
                            <i className="fa fa-plus" aria-hidden="true"></i>
                        </Accordion.Toggle> */}
                    </Col>
                </Row>
            </Card.Header>
            <Accordion.Collapse eventKey={props.index.toString()}>
            <Card.Body style={{background: "#E8E8E8"}}>
                { 
                // console.log(props.topic.webinar_sub_topics)
                props.topic?.webinar_sub_topics?.map(sub => {
                    // console.log(sub);
                    return (
                        <Row className="justify-content-center">
                            <Col xs={4} lg={4}>
                                {sub.sub_topic_name}
                            </Col>
                            <Col xs={4} lg={4}></Col>
                            <Col xs={4} lg={4}></Col>
                        </Row>
                    )
                })
                }
            </Card.Body>
            </Accordion.Collapse>
        </Card>
    //     <Table>
    //     <Row className="justify-content-center">
    //     <Col >
    //             <th width="200px">
    //             <tr>{props.topic}</tr>
    //             <tr>{props.sub_topic}</tr>
    //         </th>
    //         <th width="50px"><i className="fa fa-plus" aria-hidden="true"></i>
    //         <i style={{ color: '#020288', marginLeft: '88%'}} className="fa fa-pencil pencil" aria-hidden="true" onClick={() => props.edit(props.id)}></i>
    //         </th>
            
    //     </Col>
    // </Row></Table>
    )
}

export default Lists;