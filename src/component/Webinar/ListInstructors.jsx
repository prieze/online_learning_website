import React, { Component } from "react";
import { Collapse, Divider } from 'antd';
import {PlusOutlined, MinusOutlined, EditOutlined} from '@ant-design/icons'
// import ModalInstructor from './ModalInstructor'
const { Panel } = Collapse;


class ListInstructors extends Component{
  state = {
    modalAddIsOpen: false,
    instructor: {}
  }
  formRef = React.createRef();
  customExpandIcon(props) {
    if (props.isActive) {
      return <MinusOutlined onClick={((e) => {
        props.expandIcon(props, e);
      })}/>
    } else {
      return <PlusOutlined onClick={((e) => {
        props.expandIcon(props, e);
      })}/>
    }
  }
  editButton = (onEdit, data) => {
    if (onEdit) {
      return <EditOutlined
        onClick={event => {
          console.log(event);
          event.stopPropagation();
          console.log('edit button clicked')
          this.onOpenAddModal(data);
        }}
      />
    } 
  };

  onOpenAddModal = (data) => {
    this.setState({ 
      modalAddIsOpen: true,
      instructor: data
    });
  };

  defaultKey = (this.props.data && this.props.data[0]) ? this.props.data[0].id : '1'

  render() {
    return (
      <>
        <Collapse 
          accordion 
          style={{ width: "99%", margin: 'auto' }}
          expandIconPosition={"right"}
          defaultActiveKey={this.defaultKey}
          expandIcon={(props) => this.customExpandIcon(props)}
        >
          {this.props.data?.map(list => {
            return <Panel header={list.instructor_name} key={list.id}
              style={{backgroundColor: "#EFEFEF"}}
              extra={this.editButton(this.props.is_edit, list)}
            >
              <p><b>Job Title</b></p>
              <p>{list.job_title}</p>
              <Divider />
              <p><b>BioGraphy</b></p>
              <p>{list.biography}</p>
            </Panel>
          })}
        </Collapse>
        {/* { this.props.is_edit &&
          <ModalInstructor
            formRef={this.formRef}
            modalAddIsOpen={this.state.modalAddIsOpen}
            onCloseAddModal={() => {this.setState({modalAddIsOpen: false})}}
            addInstructor={(data) => {this.props.addInstructor(data)}}
            is_edit={this.props.is_edit}
            instructors={this.state.instructor}
          />
        } */}
      </>
    )
  }
}

export default ListInstructors;