import React, { Component } from "react";
import Navigation from "../Navigation/Navigation";
import { Link } from "react-router-dom";
import Footer from "../Footer/Footer";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import {
  Container,
  Row,
  Col,
  Form,
  Card,
  Navbar,
  Button,
  Modal,
  ProgressBar
} from "react-bootstrap";
import UploadVideo from '../Upload/uploadVideo';
import { Formik } from 'formik';
import * as yup from 'yup';

class AddTopics extends Component{
  state = {
    webinar_topics:[],
    topic_name: '',
    webinar_sub_topics: [],
    modalAddIsOpen: false,
    is_edit: false,
  };
  formRef = React.createRef();
  schema = yup.object({
      topic_name: yup.string().required('Topic Name is Required'),
      webinar_sub_topics: yup.array().of(
        yup.object().shape({
          sub_topic_name: yup.string().required('Sub Topic Name is Required'),
        })
      ).required('Sub Topic is Required'),
  });

  onOpenAddModal = (data) => {
    if (data) {
      const video = [];
      const is_edit = data.topic_name ? true : false;
      this.setState({ 
        modalAddIsOpen: true,
        topic_name: data.topic_name,
        webinar_sub_topics: data.sub_topics,
        is_edit,
      });
    } else {
      this.setState({ 
        modalAddIsOpen: true,
        topic_name: '',
        webinar_sub_topics: [],
        is_edit: false,
      });
    }
  };

  addtopic(values){
    console.log('bisa klik');
    console.log("values : " + JSON.stringify(values));
    console.log("state : " + JSON.stringify(this.state));
    this.props.addTopic({
      topic_name: values.topic_name,
      webinar_sub_topics: values.webinar_sub_topic
    });
    this.onCloseAddModal();
  }
  onCloseAddModal = values => {
      console.log(values);
      this.setState({ modalAddIsOpen: false });
  };

  onSaveModalTopic = () => {
      if (this.formRef.current.isValid) {
        console.log('valid');
        console.log(this.formRef.current.values);
        this.props.addTopic({
          topic_name: this.formRef.current.values.topic_name,
          webinar_sub_topics: this.formRef.current.values.webinar_sub_topics
        });
        this.setState({
          modalAddIsOpen: false,
          topic_name: '',
          webinar_sub_topics: []
        });
        console.log(this.state);
      }
  }


  add_subtopic_array = (e) => {
    e.persist();
    console.log(this.formRef.current.values);
    let newSub;
    const sub = {
      sub_topic_name: ''
    }
    if (this.formRef.current.values.webinar_sub_topics && this.formRef.current.values.webinar_sub_topics.length > 0) {
      newSub = [
        ...this.formRef.current.values.webinar_sub_topics,
        sub
      ]
    } else {
      newSub = [
        sub
      ]
    }
    this.formRef.current.setFieldValue('webinar_sub_topics', newSub);
    console.log(this.formRef.current.values.webinar_sub_topics);
  }

  render(){
    return(
      <>
        <Card>
            <Card.Body style={{ padding: '2rem' }}>
            <Button size="sm" block onClick={this.onOpenAddModal} className="button-grape" >
                Add New Topic
            </Button>
            </Card.Body>
        </Card>
        
        <Modal show={this.state.modalAddIsOpen} onHide={this.onCloseAddModal} size='lg'>
          <Modal.Header bsPrefix closeButton />
          <Modal.Body>
            <Formik
              innerRef={this.formRef}
              validationSchema={this.schema}
              validateOnChange={false}
              validateOnBlur={false}
              onSubmit={() => this.onSaveModalTopic()}
              initialValues={{
                topic_name: this.state.topic_name,
                webinar_sub_topics: this.state.webinar_sub_topics
              }}
            >
              {({
                    handleSubmit,
                    handleChange,
                    values,
                    setFieldValue,
                    errors,
                }) => (
            <Form noValidate onSubmit={handleSubmit}>
            <Form.Group>
              <Form.Label style={{ fontFamily: "Gordita-bold" }}>
                Topic Name*
              </Form.Label>
              <Form.Control type="text"
                // onChange={(event) => { this.setState({ topic_name: event.target.value }) }} 
                placeholder="Topic Name"
                name="topic_name"
                value={values.topic_name}
                onChange={handleChange}
                // value={this.state.topic_name}
                isInvalid={!!errors.topic_name}
              />
                <Form.Control.Feedback type="invalid">
                  {errors.topic_name}
                </Form.Control.Feedback>
            </Form.Group>
              <Form.Group>
                <Button className="button-grape btn-sm"
                  style={{ width: "30%" }}
                  onClick={(e) => this.add_subtopic_array(e)}
                  >
                    + Add Sub Topic
                </Button>
                  {values.webinar_sub_topics?.map((sub_topic, index) => (
                  <Form.Row className="justify-content-center" key={index}>
                    <Form.Group as={Col}>
                      <Form.Label style={{ fontFamily: "Gordita-bold" }}>
                        Sub Topic Name*
                      </Form.Label>
                      <Form.Control
                        type="text"
                        name={`sub_topic_name${index+1}`}
                        onChange={(event) => {
                          event.persist();
                          setFieldValue(`webinar_sub_topics.[${index}].sub_topic_name`, event.target.value)
                        }}
                        isInvalid={errors.webinar_sub_topics?.[index]?.sub_topic_name}
                        value={values.webinar_sub_topics?.[index]?.sub_topic_name}
                      />
                    <Form.Control.Feedback type="invalid">{errors.sub_topics?.[index]?.sub_topic_name}</Form.Control.Feedback>
                    </Form.Group>
                  </Form.Row>
                  ))}
              </Form.Group>
              {this.state.currentUploadState
                ? <Button disabled className="button-grape">Uploading ...</Button>
                : this.state.is_edit
                  ? <Button className="button-grape"
                      onClick={() => { this.onSaveModalTopic() }}
                  >Edit</Button>
                  : <Button className="button-grape"
                      onClick={() => { this.onSaveModalTopic() }}
                  >Create</Button>
              }
                </Form>
                )}
              </Formik>
          </Modal.Body>
        <Modal.Footer>

        </Modal.Footer>
        </Modal>
      </>
    )
  }
}

export default AddTopics;