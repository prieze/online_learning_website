import React, { Component } from "react";
import Navigation from "../Navigation/Navigation";
import { Link } from "react-router-dom";
// import "../Course/Course.css";
import Footer from "../Footer/Footer";
import { Container, Row,Col, Form, Card, Navbar, Button, Modal, Accordion, ProgressBar, Image} from "react-bootstrap";
import socketIOClient from "socket.io-client";
import UploadVideo from '../Upload/uploadVideo';
import AddTopic from './AddTopics';
import List from './Lists';
import AddInstructor from './AddInstructors';
import ListInstructor from './ListInstructors';
import AddExam from '../Course/AddExam'
import { DatePicker, TimePicker, Space, message, Popconfirm } from 'antd';
import 'font-awesome/css/font-awesome.min.css';
import moment from 'moment';
import { fetchWebinarById, updateWebinar } from "./api";

class AddWebinar extends Component {
  SelectedFile;
  reader = new FileReader();
  socket;
  state = {
    list: [],
    webinar_topics: [],
    instructors: [],
    webinar_name: null,
    cover_photo: null,
    description: null,
    discount: 0,
    price: null,
    status: 1,
    webinarId: null,
    file: null,
    url: "base64 url",
    videoid: "",
    videoId: "",
    duration: 0,
    topic_name_edit: "",
    sub_topic_name_edit: "",
    duration_edit: "",
    image: "",
    modalAddIsOpen: false,
    selectedVideo: {},
    uploadState: {}, //Upload State dari Component untuk merubah data dari page ini
    edittopicModal: false,
    editinsModal: false,
    incIdTopic: 1,
    is_edit: false,
    is_new: true,
    toggle: 'Add'
  };
  
  componentDidMount = async() => {
    const id = this.props.match.params.id;
    console.log(`id: ${id}`);
    console.log(`is_edit: ${this.props.is_edit}`);
    console.log(this.state);
    if (id && this.props.is_edit) {
      const webinar = await fetchWebinarById(id);
      const webinars = {
        ...webinar
      }
      await this.setState((state)=> {
        return {
          ...state,
          ...webinars,
          is_edit: id,
          toggle: 'Edit',
        }
      })
      // if (webinars === 0 || webinars === null) {
      //   message.error("No Data");
      //   this.props.history.push("/Webinar");
      // }
      console.log(this.state.coverId);
      console.log(this.state.videoId);
      // console.log(webinar);
      console.log(webinars);
    }else{
      console.log(`gak masuk ke if`);
    }
  }

  // onChange = async (value, dateString) => {
  //   await this.setState({ date: dateString });
  //   console.log(this.state.date);
  //   console.log(this.state.date[0]);
  //   console.log(typeof this.state.date[1]);
  //   console.log(value);
  //   console.log(dateString);
  //   console.log(typeof dateString);
  // }

  onStartChange = async(value, dateString) =>{
    await this.setState({ from_date: dateString });
    console.log(this.state);
  }

  onEndChange = async(value, dateString) =>{
    await this.setState({ to_date: dateString });
    console.log(this.state);
  }

  setAddInstructorsState(states) {
    const instructors = this.state.instructors.concat(states);
    this.setState({instructors})
    console.log(instructors);
  }

  setParentState(states) {
    this.setState(
      { uploadState: states }
    )
  }

  onFileChange = (event) => {
    // Update the state
    this.setState({ image: event.target.files[0] });
    console.log(this.state);
  };

  setAddTopicState = async(states) => {
    if (states.id) {
      const hasil = this.state.webinar_topics.map(a => {
        if (a.id === states.id) {
          a = states;
        }
        return a;
      })
      await this.setState({
        webinar_topics: hasil
      })
    }
    const webinar_topics = this.state.webinar_topics.concat(states);
    await this.setState({ webinar_topics })
    console.log(this.state.webinar_topics);
    console.log("state variabel fungsi => ", states);
  }

  onOpenEditTopicModal(states) {
    console.log(states);
    this.addTopicRef.current.onOpenAddModal(states);
  }

  toBase64 = (file) =>
    new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = (error) => reject(error);
    });

    findf = (f) => {
      f = f.split(";base64,");
      if (f.length > 1) {
        f = f[1];
      } else {
        f = f[0];
      }
      return f;
    }


    Createwebinar(){
      console.log(this.state);
      // console.log(this.state.uploadState.socketUploadDataReturn.data.id);
      if (!this.state.image) {
        message.error("image can not be empty")
        return;
      }
      this.toBase64(this.state.image).then((f) => {
        f = this.findf(f)
        console.log(this.state);
        const token = localStorage.getItem("token");
        console.log(this.state);
        const from_date = moment(this.state.from_date).format('YYYY-MM-DD');
        const to_date = moment(this.state.to_date).format('YYYY-MM-DD');
        const id = this.state.instructors.map(i => i.id);
        console.log(this.state);
        // const topics = this.state.topics.map(a => {
        //   a.webinar_sub_topics.map(b => {
        //     delete b.video
        //   })
        //   delete a.id;
        //   return a;
        // })
      fetch(
        `${process.env.REACT_APP_BASEURL}/api/webinar`,
        {
          method: "POST",
          body: JSON.stringify({
            webinar_name: this.state.webinar_name,
            description: this.state.description,
            price: this.state.price,
            discount: this.state.discount,
            from_date: from_date,
            to_date: to_date,
            location: this.state.location,
            capacity: this.state.capacity,
            is_new: this.state.is_new,
            webinar_topics: this.state.webinar_topics,
            instructors: this.state.instructors.map(i => i.id),
            video_id: this.state.uploadState.socketUploadDataReturn.data.id,
            term_condition: this.state.term_condition,
            status: "available",
            cover: {
              file: f,
              file_name: this.state.image.name,
              file_size: this.state.image.size,
              file_type: this.state.image.type,
            }
          }),
          headers: {
            Authorization: `Bearer ${token}`,
            "Content-Type": "application/json",
          },
        }
      ).then((response) => {
        response.json().then((result) => {
          if (result.sukses) {
            console.log(result);
            this.props.history.push("/Webinar");
            message.success("Successfully !", 1);
          } else {
            message.error(result.msg, 1);
          }
        });
      });
      });
    }

    validasi() {
      let hasil = true;
      if (this.state.errors){
        const keys = Object.keys(this.state.errors)
        keys.map(a => {
          if (a && this.state.errors[a] && this.state.errors[a] !== '') {
            hasil = false;
          }
        })
      }
      console.log('Validasi', hasil);
      return hasil
    }

    updateWebinar = async() =>{
      console.log(this.state);
      console.log(this.state.term_condition);
      if (!this.validasi()) { return ;}
      const topics = this.state.webinar_topics.map(a => {
        a.webinar_sub_topics.map(b => {
          delete b.video
          delete b.id
          delete b.topicId
        })
        delete a.id;
        delete a.courseId;
        return a;
      })
      const body = {
        term_condition: this.state.term_condition,
        webinar_name: this.state.webinar_name,
        description: this.state.description,
        price: this.state.price,
        discount: this.state.discount,
        from_date: this.state.from_date,
        to_date: this.state.to_date,
        location: this.state.location,
        capacity: this.state.capacity,
        is_new: this.state.is_new,
        webinar_topics: topics,
        instructors: this.state.instructors.map(i => i.id),
        status: "available",
      }
      if (this.state.image) {
        let f = await this.toBase64(this.state.image);
        f = this.findf(f)
        body.cover = {
          file: f,
          file_name: this.state.image.name,
          file_size: this.state.image.size,
          file_type: this.state.image.type,
        }
      } else{
        console.log("masuk else");
        body.coverId = this.state.coverId
      }
      // else {
      //   console.log("masuk elseif");
      //   let f = await this.toBase64(this.state.image);
      //   f = this.findf(f)
      //   body.cover = {
      //     file: f,
      //     file_name: this.state.image.name,
      //     file_size: this.state.image.size,
      //     file_type: this.state.image.type,
      //   }
      // }
      console.log("video_id ", this.state.videoId);
      if (this.state.uploadState && this.state.uploadState.socketUploadDataReturn &&
        this.state.uploadState.socketUploadDataReturn.data && this.state.uploadState.socketUploadDataReturn.data.id) {
          body.video_id = this.state.uploadState.socketUploadDataReturn.data.id
      }else{
        body.video_id = this.state.videoId
      } 
      // else {
      //   if (this.state.uploadState && this.state.uploadState.socketUploadDataReturn &&
      //     this.state.uploadState.socketUploadDataReturn.data && this.state.uploadState.socketUploadDataReturn.data.id) {
      //       body.video_id = this.state.uploadState.socketUploadDataReturn.data.id
      //     }
      // }
      updateWebinar( this.state.id, body ).then(hasil => {
        this.props.history.push("/Webinar");
        message.success("Webinar Updated", 1);
      }).catch(err => {
        message.error("Failed update Webinar", 1);
      });
    }

    handleRemoveWeb() {
      const id = this.props.match.params.id;
      console.log('handle remove')
      console.log(id);
      const token = localStorage.getItem('token');
      fetch(
        `${process.env.REACT_APP_BASEURL}/api/webinar/${id}`, {
          method: 'DELETE',
          headers: {
              "Authorization": `Bearer ${token}`,
              "Content-Type": "application/json"
          },
      })
          .then(response => response.json())
          .then(json => {
            console.log(json);
            if (json) {
              this.props.history.push("/Webinar");
              message.success("Successfully !", 1);
              } else {
                  message.error(json.msg, 1)
              }
          })

          .catch(error => console.log('error', error));
  }

  render() {
    const { modalAddIsOpen, list } = this.state;
    // const { RangePicker } = DatePicker;
    const dateFormat = "YYYY-MM-DD HH:mm:ss";
    return (
      <>
        <Navigation history={this.props.history}/>
        <Container
          fluid
          style={{
            width: "100%",
            padding: "50px",
            backgroundColor: "#F1F1F1",
          }}
        >          
          <Row className="justify-content-center">
            <Col xs={10} md={10}>
              <Navbar
                expand="lg" style={{ background: "rgba(58, 44, 125, 0.34)" }}>
                <Link to={`/Webinar`}>
                  <button className="btn icon-back">
                    <span style={{ color: "#FFFFFF" }} className="fa fa-chevron-left"></span>
                  </button>
                </Link>
                <Navbar.Brand style={{ color: "#FFFFFF", fontFamily: "Gordita-bold" }}>{this.state.toggle} Webinar</Navbar.Brand>
              </Navbar>
            </Col>
          </Row>

          <Row className="justify-content-center">
            <Col xs={10} md={10}>
              <h4 className="title-course">1. Webinar</h4>
            </Col>
          </Row>

          <Row>
            <Col>
              <Card style={{ width: "83%", margin: 'auto' }}>
                <Card.Body style={{ padding: '2rem' }}>
                  <Row>
                    <Col xs={12} lg={5}>
                      <Form>

                        <Form.Group>
                          <Form.Label className="form-label">Webinar Name *</Form.Label>
                          <Form.Control type="text" onChange={(event) => {
                            this.setState({
                              webinar_name: event.target.value,
                            });
                          }}
                          value={this.state.webinar_name}
                          />
                        </Form.Group>

                        <Form.Group>
                        {(this.props.match.params.id)
                          ? <Image src={this.state.cover?.url} style={{ width: '100%' }} />
                          : null 
                        }
                          <input type="file" name="file" id="file" className="inputfile" onChange={this.onFileChange} />
                          <label style={{ width: '100%' }} for="file"><i className="fa fa-upload"></i>Add Cover Photo</label>
                        </Form.Group>

                        <Form.Group>
                          <Form.Label>Webinar Description *</Form.Label>
                          <Form.Control as="textarea" rows="3" onChange={(event) => {
                            this.setState({
                              description: event.target.value,
                            });
                          }}
                          value={this.state.description}
                          />
                        </Form.Group>
                        
                        <Form.Group>
                          <Form.Label>Price *</Form.Label>
                          <Form.Control
                            type="text"
                            onChange={(event) => {
                              this.setState({ price: event.target.value });
                            }}
                            value={this.state.price}
                          />
                        </Form.Group>

                        <Form.Group>
                          <Form.Label>Discount (%)</Form.Label>
                          <Form.Control
                            type="text"
                            onChange={(event) => {
                              this.setState({ discount: event.target.value });
                            }}
                            value={this.state.discount}
                          />
                        </Form.Group>

                      </Form>
                    </Col>
                    <Col lg={2} />
                    <Col xs={12} lg={5}>
                      <Form>
                        <Form.Group>
                          {(this.props.match.params.id)
                           ? <video controls autoplay controlsList="nodownload" src={this.state.video?.url} style={{width: '100%'}} />
                           : null
                          }
                          <Card style={{ width: "100%", marginTop: '6%', backgroundColor: "#EFEFEF", boxShadow: "none", border: "none" }}>
                            {(this.props.match.params.id)
                              ?   null
                              :  <Card.Body style={{ padding: '2rem' }}>
                                  <Card.Title style={{ color: '#392C7E', textAlign: 'center', fontFamily: 'Gordita-bold', fontSize: '16px' }}>Submit a Video Demo</Card.Title>
                                  <Card.Text style={{ color: '#392C7E', textAlign: 'center', fontFamily: 'Gordita', fontSize: '11px' }}>CLICK ON THE BUTTON OR DRAG & DROP FILE HERE</Card.Text>
                                </Card.Body>
                            }
                           
                            <input type="file" id="file" placeholder="Upload File" className="form-control" onChange={e => {
                              this.setState({ selectedVideo: e.target.files[0] });
                            }} />
                            <UploadVideo
                              selectedFile={this.state.selectedVideo}
                              uploadState={(states) => this.setParentState(states)}
                            >
                            </UploadVideo>
                            {(this.state.uploadState.socketUploading && !this.state.uploadState.socketIsByte) &&
                              <div>Uploading ({this.state.uploadState.socketMBDone} / {this.state.uploadState.socketTotalSizeMB} MB) ( {this.state.uploadState.socketPercent}% )</div>
                            }
                            {(this.state.uploadState.socketUploading && this.state.uploadState.socketIsByte) &&
                              <div>Uploading ({this.state.uploadState.socketUploaded} / {this.state.uploadState.socketTotalSize} Byte) ( {this.state.uploadState.socketPercent}% )</div>
                            }
                            {this.state.uploadState.socketPercent === 100 &&
                              <div>Uploaded ({this.state.uploadState.socketTotalSize.toLocaleString()}) ( {this.state.uploadState.socketPercent}% )</div>
                            }
                            {(this.state.uploadState.socketUrl && this.state.uploadState.socketUrl !== '') &&
                              <div><a target='_blank' href={this.state.uploadState.socketUrl}>Link</a></div>
                            }
                          </Card>
                        </Form.Group>
                        <Form.Row>
                          <Form.Group as={Col} md="6">
                            <Form.Label>Start Date *</Form.Label>
                              <DatePicker
                                disabledDate={this.disabledStartDate}
                                // showTime
                                format="YYYY-MM-DD"
                                placeholder="Start"
                                // defaultValue={moment(this.state.from_date)}
                                // moment={this.state.from_date}
                                onChange={this.onStartChange}
                                // onOpenChange={this.handleStartOpenChange}
                              />
                          </Form.Group>
                          <Form.Group as={Col} md="6">
                              <Form.Label>End Date *</Form.Label>
                              <DatePicker
                                disabledDate={this.disabledEndDate}
                                // showTime
                                format="YYYY-MM-DD"
                                placeholder="End"
                                // defaultValue={moment(this.state.to_date)}
                                // moment={this.stateto_date}
                                onChange={this.onEndChange}
                                // onOpenChange={this.handleEndOpenChange}
                              />
                          </Form.Group>
                        </Form.Row>
                        <Form.Group>
                          <Form.Label>Location *</Form.Label>
                          <Form.Control type="text" onChange={(event) => {this.setState({ location: event.target.value });}} value={this.state.location} />
                        </Form.Group>
                        <Form.Group controlId="formGridAddress1">
                          <Form.Label>New Webinar ?*</Form.Label>
                          <Row>
                            <Col>
                              <Form.Check type="radio" name="groupOptions" value="true" label="Yes" checked={(this.state.is_new === true)? true: false} onChange={(event) => {this.setState({is_new: true})}} />
                            </Col>
                            <Col>
                              <Form.Check type="radio" name="groupOptions" value="false" label="No" checked={(this.state.is_new === false)? true: false} onChange={(event) => {this.setState({is_new: false})}} />
                            </Col>
                          </Row>
                        </Form.Group>
                        <Form.Row>
                          <Form.Group as={Col} md="6">
                            <Form.Label>Capacity</Form.Label>
                            <Row>
                              <Col>
                                <Form.Control onChange={(event) => {this.setState({capacity: event.target.value})}} value={this.state.capacity} />
                              </Col>
                              <Col style={{ marginLeft: '-10%', marginTop: '3%' }}>Seat</Col>
                            </Row>
                          </Form.Group>
                          {/* <Form.Group as={Col} md="6">
                              <Form.Label>Status</Form.Label>
                              <Form.Control as="select" custom onChange={(event) => {this.setState({status: event.target.value})}}>
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                              </Form.Control>
                          </Form.Group> */}
                        </Form.Row>
                      </Form>
                    </Col>
                  </Row>
                </Card.Body>
              </Card>
            </Col>
          </Row>

        <Row style={{ width: '85%', margin: 'auto' }}>
          <Col>
              <h4 className="title-course">2. Instructor</h4>
            <AddInstructor addInstructor={(states) => {this.setAddInstructorsState(states)}}/>
            <ListInstructor data={this.state.instructors}/>
          </Col>
          <Col>
            <Row className="justify-content-center">
              <Col>
                <h4 className="title-course">3. Content & Material</h4>
              </Col>
            </Row>
              <AddTopic addTopic={(states) => { this.setAddTopicState(states) }} ref={this.addTopicRef} topic={this.state.selectedTopic} />
          <Accordion defaultActiveKey="0">
          {
                this.state.webinar_topics?.map((list, index) => {
                    return <List
                        id={list.id}
                        key={list.id}
                        topic={list}
                        index={index}
                        // is_edit
                        // edit={(hasil) => this.onOpenEditTopicModal(hasil)}
                        sub_topic={(list && list.webinar_topics && list.webinar_topics.sub_topic_name) ? list.webinar_topics.sub_topic_name : null}></List>
                        // sub_topic={(list && list.topics && list.topics.sub_topic_name) ? list.topics.sub_topic_name : null}></List>
                })
            }
            </Accordion>
          </Col>
        </Row>
          

        <Row className="justify-content-center">
          <Col xs={10} md={10}>
            <h4 className="title-course">4. Term & Condition</h4>
          </Col>
        </Row>

        <Row style={{ width: '85%', margin: 'auto' }} >
          <Col lg={12} >
            <Form.Control as="textarea" rows="10" style={{ boxShadow: '4px 4px 20px rgba(0, 0, 0, 0.2)' }} value={this.state.term_condition} onChange={(event) => {this.setState({term_condition : event.target.value})}} />
          </Col>
        </Row>
        <br />
        <Row>
          <Col md={{span: 8, offset: 9}}>
            {this.state.is_edit 
                ? <>
                  <Popconfirm
                          title="Remove Webinar ?"
                          onConfirm={() => { this.handleRemoveWeb() }}
                          okText="Yes"
                          cancelText="No"
                      >
                      <Button variant="danger" >Delete</Button>
                  </Popconfirm>
                  <Button className="button-grape" onClick={() => {this.updateWebinar()}} style={{marginLeft: "2.5%"}} >Update</Button>
                  </>
                : <Button className="button-grape" onClick={() => {this.Createwebinar()}} disabled={this.state.uploadState.socketPercent=="100" ? false : true}>Save</Button>
            }
            </Col>
        </Row>

        </Container>
        <Footer />
      </>
    );
  }
}

export default AddWebinar;