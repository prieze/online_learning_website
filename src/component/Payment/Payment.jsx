import React, { Component, Fragment } from 'react';
import "./Payment.css";
import Navigation from "../Navigation/Navigation"
import Footer from "../Footer/Footer"
import { Link } from 'react-router-dom'
import { Button, Container, Col, Row, Table, Form, DropdownButton, Dropdown, Image } from "react-bootstrap";
import _ from "lodash";
import { message } from 'antd';
import Paginate from '../../components/Paginate/paginate';
import { checkRole, pageToLimitOffset } from '../Utils'
import BootstrapSwitchButton from 'bootstrap-switch-button-react'

class Payment extends Component {

    state = {
        post: [],
        file: null,
        url: null,
        loading: false,
        pageNow: 1,
        count: 0,
        enabled: '',
    }

    

    handleRemove(id) {

        console.log('handle remove')
        console.log(id);
        const token = localStorage.getItem('token');
        fetch(`${process.env.REACT_APP_BASEURL}/api/delete/payment_method/${id}`, {
            method: 'DELETE',
            headers: {
                "Authorization": `Bearer ${token}`,
                "Content-Type": "application/json"
            },
        })
            .then(response => response.json())
            .then(json => {
                if (json.sukses) {
                    this.listTable(1);
                    this.props.history.push("/Payment");
                }
                message.success("Successfully !", 1)
            })

            .catch(error => console.log('error', error));
    }

    componentDidMount() {
        const page = this.props.match.params.page ? this.props.match.params.page : 1;
        if (checkRole('admin').sukses) {
            this.listTable(page);
        }
    }

    listTable(page) {
        const token = localStorage.getItem('token');
        // const limit = 5;
        // const offset = 0;
        const { limit, offset } = pageToLimitOffset(page);
        console.log(`limit: ${limit}`);
        console.log(`offset: ${offset}`);
        // this.state({

        // })
        this.setState({ loading: true });
        fetch(`${process.env.REACT_APP_BASEURL}/api/admin/read/payment_methods?limit=${limit}&offset=${offset}&search=`, {
            method: 'GET',
            headers: {
                "Authorization": `Bearer ${token}`,
                "Content-Type": "application/json"
            },
        })
            .then(response => response.json())
            .then(json => {
                this.setState({
                    post: json.rows,
                    loading: false,
                    pageNow: page,
                    count: json.count
                })
            })
            .catch(error => console.log('error', error));
    }

    ChangePage(page) {
        console.log(`Change page to: ${page}`)
        this.listTable(page);
    }

    // handleEnableChange(e) {
    //     this.props.onEnableChange(e.target.checked);
    //   }

    enableChange(id) {
        console.log('enable disable')
        console.log(id);
        const token = localStorage.getItem('token');
        fetch(`${process.env.REACT_APP_BASEURL}/api/enabled/disabled/${id}`, {
            method: 'POST',
            headers: {
                "Authorization": `Bearer ${token}`,
                "Content-Type": "application/json"
            },
        })
            .then(response => response.json())
            .then(json => {
                if (json.sukses) {
                    this.listTable(1);
                    this.props.history.push("/Payment");
                }
                message.success("Successfully !", 1)
            })

            .catch(error => console.log('error', error));
    }

    render() {
        return (
            <Fragment>
                <Navigation history={this.props.history} />
                <Container fluid
                    style={{
                        width: "100%",
                        minHeight: "800px",
                        padding: "50px",
                        backgroundColor: "#F1F1F1"
                    }}
                >
                    <Row className="justify-content-center" style={{ width: '83%', margin: 'auto' }}>
                        <Col md={10}>
                            <h4 className="title-course">Payment Method</h4>
                        </Col>
                        <Col md={2}>
                            <Link to={`/AddPayment`}><Button className="button-grape" >Add New</Button></Link>
                        </Col>
                    </Row>
                    <br></br>

                    {
                        this.state.post.map((list) => {
                            return <Row className="justify-content-md-center" style={{ width: '83%', margin: 'auto' }}>
                                <Col>
                                    <Table style={{ background: "#F9F9F9" }}>
                                        <tr>
                                            <td width="150px">
                                                <tr><Image style={{ width: '150px', height: '50px' }} src={list.file.url} />
                                                </tr>
                                            </td>
                                            <th width="100px">{list.bank_name}</th>
                                            <th width="200px">Manual Bank Transfer</th>
                                            <th width="50px">{list.kode}</th>
                                            <th width="50px">
                                                <Dropdown >
                                                    <Dropdown.Toggle className="dropdown-payment">
                                                        Action
                                            </Dropdown.Toggle>
                                                    <Dropdown.Menu>
                                                        <Dropdown.Item><Link to={`/EditPayment/${list.id}`}>Edit</Link></Dropdown.Item>
                                                        {/* <Dropdown.Item onClick={() => {
                                                            if (
                                                                window.confirm(
                                                                    `Are You sure ?`
                                                                )
                                                            ) {
                                                                this.handleRemove(list.id)
                                                            }
                                                        }}>Delete</Dropdown.Item> */}
                                                    </Dropdown.Menu>
                                                </Dropdown>
                                            </th>
                                            <th width="100px">
                                                <label className="switch">
                                                    <input type="checkbox" 
                                                         onClick={() => {
                                                                this.enableChange(list.id)
                                                        }}
                                                         checked={list.enabled}
                                                    />
                                                    <span className="slider round"></span>
                                                </label>
                                            </th>
                                        </tr>
                                    </Table>
                                </Col>
                            </Row>
                        })}
                    <Paginate className="paginate-user" pageNow={this.state.pageNow} count={this.state.count}
                        changePage={(page) => this.ChangePage(page)}>
                    </Paginate>
                </Container>
                <Footer />
            </Fragment >
        );
    }
}

export default Payment;