import React, { Component } from "react";
import Navigation from "../Navigation/Navigation";
import { Link } from "react-router-dom";
import "./Payment.css";
import Footer from "../Footer/Footer";
import {  message } from 'antd';
import {
  Container,
  Row,
  Col,
  Form,
  Card,
  Navbar,
  Button,
  Modal,
  Accordion,
  ProgressBar
} from "react-bootstrap";
import 'font-awesome/css/font-awesome.min.css';import { Collapse, Divider } from 'antd';
import { PlusOutlined, MinusOutlined, DeleteOutlined, EditOutlined, CheckOutlined, CloseOutlined } from '@ant-design/icons'
import { findRenderedComponentWithType } from "react-dom/test-utils";
const { Panel } = Collapse;

class EditPayment extends Component {
  SelectedFile;
  reader = new FileReader();
  state = {
    list: [],
    id: 0,
    file: null,
    // bank_name: "",
    // account_name: "",
    // account_number: "",
    // kode: "",
    fileSelected: {},
    payment_informations: [],
    image: "",
    description: null,
  };
  formRef = React.createRef();

  customExpandIcon(props, id) {
    if (props.isActive) {
      return <><MinusOutlined onClick={((e) => {
        props.expandIcon(props, e);
      })}/>
      <CheckOutlined onClick={() => {
        this.editInformation(id)
      }}/>
      <CloseOutlined onClick={((e) => {
        props.expandIcon(props, e);
      })}/></>
    } else {
      return <><PlusOutlined onClick={((e) => {
        props.expandIcon(props, e);
      })}/>
      <EditOutlined onClick={((e) => {
        props.expandIcon(props, e);
      })}/></>
    }
  }

  removeButton (id) {
       return <DeleteOutlined
      onClick={() => {
        if (
            window.confirm(
                `Are You sure ?`
            )
        ) {
            this.handleRemove(id)
        }
    }}
      />
  };

  handleRemove(id) {
    console.log('handle remove')
    console.log(id);
    const token = localStorage.getItem('token');
    fetch(`${process.env.REACT_APP_BASEURL}/api/delete/payment_information/${id}`, {
        method: 'DELETE',
        headers: {
            "Authorization": `Bearer ${token}`,
            "Content-Type": "application/json"
        },
    })
    .then(response => response.json())
    .then(json => {
        if (json.sukses) {
            this.listPayment();
            this.props.history.push("/Payment");
        }
        message.success("Successfully !", 1)
    })

    .catch(error => console.log('error', error));
}

  componentDidMount() {
    this.listPayment();
  }

  listPayment() {
    const token = localStorage.getItem("token");
    console.log(this.props);
    const id = this.props.match.params.id;
    console.log(id);
    fetch(
      `${process.env.REACT_APP_BASEURL}/api/read/payment_method/${id}`,
      {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      }
    )
      .then(response => response.json())
      .then(json => {
        this.setState({
          list: json.data,
        })
        console.log(this.state.list);
        console.log(json.data);
      })
      .catch(error => console.log('error', error));
  }

  onFileChange = (event) => {
    // Update the state
    this.setState({ image: event.target.files[0] });
  };

  toBase64 = file => new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
  });

  updateFiles(file) {
    console.log(file);
    this.toBase64(file).then(f => {
      f = f.split(';base64,');
      if (f.length > 1) {
        f = f[1]
      } else {
        f = f[0]
      }
      this.setState({
        fileSelected: {
          file_name: file.name,
          file_size: file.size,
          file_type: file.type,
          file: f
        }
      })
    }).catch(err => {
      console.log(err);
    })
  }

  updatePayment() {
    const token = localStorage.getItem('token');
    console.log(this.props);
    const id = this.props.match.params.id;
    this.setState({
      id: id,
    });
    console.log(id);
    fetch(`${process.env.REACT_APP_BASEURL}/api/update/payment_method/${id}`, {
      method: 'PUT',
      body: JSON.stringify({
        bank_name: this.state.bank_name,
        account_name: this.state.account_name,
        account_number: this.state.account_number,
        kode: this.state.kode,
        cover: this.state.fileSelected
      }),
      headers: {
        "Authorization": `Bearer ${token}`,
        "Content-Type": "application/json"
      },
    })
      .then((response) => {
        response.json()
          .then((result) => {
            console.warn("result", result);
            // localStorage.setItem('updatePayment', JSON.stringify({
            //   updatePayment: true,
            // }))
            this.props.history.push("/Payment");
            message.success("Successfully !", 1)
          })
      })
      .catch(error => console.log('error', error));
  }

  addInformation(){
    const token = localStorage.getItem('token');
    console.log(this.props);
    const id = this.props.match.params.id;
    this.setState({
      id: id,
    });
    console.log(id);
    fetch(`${process.env.REACT_APP_BASEURL}/api/create/payment_information/${id}`, {
      method: 'POST',
      body: JSON.stringify({
        information_name: this.state.information_name,
        description: this.state.description,
      }),
      headers: {
        "Authorization": `Bearer ${token}`,
        "Content-Type": "application/json"
      },
    })
    .then(response => response.json())
    .then(json => {
        if (json.sukses) {
            this.listPayment();
        }
        message.success("Successfully !", 1)
    })

    .catch(error => console.log('error', error));
}

  editInformation(id) {
    const token = localStorage.getItem('token');
    console.log("Edit Information");
    console.log(id);
    fetch(`${process.env.REACT_APP_BASEURL}/api/update/payment_information/${id}`, {
      method: 'PUT',
      body: JSON.stringify({
        description: this.state.description
      }),
      headers: {
        "Authorization": `Bearer ${token}`,
        "Content-Type": "application/json"
      },
    })
      .then((response) => {
        response.json()
          .then((result) => {
            console.warn("result", result);
            message.success("Successfully !", 1)
          })
      })
      .catch(error => console.log('error', error));
  }

  render() {
    const { list } = this.state;
    return (
      <>
        <Navigation history={this.props.history} />
        <Container
          fluid
          style={{
            width: "100%",
            minHeight: "2000px",
            paddingTop: "50px",
            backgroundColor: "#F1F1F1",
          }}
        >
          <Row className="justify-content-center">
            <Col xs={10} md={10}>
              <Navbar
                expand="lg" style={{ background: "rgba(58, 44, 125, 0.34)" }}>
                <Link to={`/Payment`}>
                  <button className="btn icon-back">
                    <span style={{ color: "#FFFFFF" }} className="fa fa-chevron-left"></span>
                  </button>
                </Link>
                <Navbar.Brand style={{ color: "#FFFFFF", fontFamily: "Gordita-bold" }}>Edit Payment Method</Navbar.Brand>
              </Navbar>
            </Col>
          </Row>
          <br/><br/> 

          <Row className="justify-content-center">
            <Col xs={10} md={10}>
              <h4 className="title-course">1. Bank</h4>
            </Col>
          </Row>

          <Row>
            <Col>
              <Card style={{ width: "83%", margin: 'auto' }}>
                <Card.Body style={{ padding: '2rem' }}>
                  <Row>
                    <Col xs={12} lg={5}>
                      <Form>

                        <Form.Group>
                          <Form.Label className="form-label">Bank Name *</Form.Label>
                          <Form.Control type="text" disable
                            value={this.state.list?.bank_name}
                          />
                        </Form.Group>

                        <Form.Group>
                          <input type="file" name="file" id="file" className="inputfile" onChange={e => {
                            this.updateFiles(e.target.files[0]);
                          }} />
                          <label style={{ width: '100%' }} for="file"><i className="fa fa-upload"></i> Add Logo Bank</label>
                          {/* <div>url: {this.state.url}</div> */}
                        </Form.Group>

                      </Form>

                    </Col>
                    <Col lg={2} />
                    <Col xs={12} lg={5}>
                      <Form>
                        <Form.Group>
                          <Form.Label>Account Name</Form.Label>
                          <Form.Control
                            type="text"
                            disable
                            value={this.state.list?.account_name}
                          />
                        </Form.Group>
                        <Form.Group>
                          <Form.Label>Account Number</Form.Label>
                          <Form.Control
                            type="text"
                            disable
                            value={this.state.list?.account_number}
                          />
                        </Form.Group>
                        <Form.Group>
                          <Form.Label>Kode Bank</Form.Label>
                          <Form.Control
                            type="text"
                            disable
                            value={this.state.list?.kode}
                          />
                        </Form.Group>
                      </Form>
                    </Col>
                  </Row>
                </Card.Body>
              </Card>
            </Col>
          </Row>
          <br/><br/> 

          <Row className="justify-content-center">
            <Col xs={10} md={10}>
              <h4 className="title-course">2. Payment Information</h4>
            </Col>
          </Row>

          <Row>
            <Col>
              <Card style={{ width: "83%", margin: 'auto' }}>
                <Card.Body style={{ padding: '2rem' }}>
                  <Row>
                    <Col xs={6} lg={5}>
                      <Form>

                        <Form.Group>
                          <Form.Label className="form-label">Information Name</Form.Label>
                          <Form.Control type="text" onChange={(event) => {
                            this.setState({
                              information_name: event.target.value,
                            });
                          }}
                          />
                        </Form.Group>

                        <Form.Group>
                          <Form.Label>Description *</Form.Label>
                          <Form.Control as="textarea" rows="3" onChange={(event) => {
                            this.setState({
                              description: event.target.value,
                            });
                          }}
                          />
                        </Form.Group>
                        <Button style={{ marginLeft: '350px' }} className="button-purple"
                        onClick={() => {
                          this.addInformation();
                        }}
                        >Add</Button>

                      </Form>

                    </Col>
                    <Col lg={2} />
                    <Col xs={6} lg={5}>
                      
                {this.state.list?.payment_informations?.map(list => {
              return <Collapse
                accordion
                style={{ width: "83%", margin: 'auto' }}
                // expandIconPosition={"Right"}
                defaultActiveKey={this.defaultKey}
                expandIcon={(props, id) => this.customExpandIcon(props, list.id)}
              >
                  <Panel header={list.information_name} key={list.id}
                    style={{ backgroundColor: "#EFEFEF" }}
                    extra={this.removeButton(list.id)}
                  >
                    <p><b>Deskripsi</b></p>
                    <Form.Control type="text" onChange={(event) => {
                            this.setState({
                              description: event.target.value,
                            });
                            console.log(this.state.description);
                          }}
                          defaultValue={list.description}
                    />
                    {/* <p>{list.description}</p> */}
                  </Panel>
              </Collapse>
                })}
            </Col>

                  </Row>
                </Card.Body>
              </Card>
            </Col>
          </Row>

          <br /><br />

          <Row style={{ marginLeft: '1040px' }}>
            <>

              <Button style={{ marginLeft: '30px' }} className="button-purple"
                onClick={() => {
                  this.updatePayment();
                }}
              >Edit</Button>
            </>
          </Row>

        </Container>
        <Footer />
      </>
    );
  }
}

export default EditPayment;