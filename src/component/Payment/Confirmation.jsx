import React, { Component, Fragment } from 'react';
import "./Payment.css";
import Navigation from "../Navigation/Navigation"
import Footer from "../Footer/Footer"
import { Link } from 'react-router-dom'
import { Button, Container, Col, Row, Table, Modal,Form, DropdownButton, Dropdown, Image } from "react-bootstrap";
import _ from "lodash";
import { Popconfirm, message } from 'antd';
import 'font-awesome/css/font-awesome.min.css';
import Paginate from '../../components/Paginate/paginate';
import { checkRole, pageToLimitOffset } from '../Utils'
import BootstrapSwitchButton from 'bootstrap-switch-button-react'
import moment from 'moment';

class Confirmation extends Component {

    state = {
        post: [],
        file: null,
        url: null,
        loading: false,
        modalIsOpen: false,
        edited_list: {},
        cover: null,
    }

    onOpenModal = async (e) => {
        console.log(e);
        await this.setState({
            modalIsOpen: true,
            edited_list: e,
        });
        console.log(this.state.edited_list.id);
    };

    onCloseModal = () => {
        this.setState({ modalIsOpen: false });
    }

    handleCancel(id) {
        console.log('Handle Cancel')
        console.log(id);
        const token = localStorage.getItem('token');
        fetch(`${process.env.REACT_APP_BASEURL}/api/cancel/payment/${id}`, {
            method: 'POST',
            headers: {
                "Authorization": `Bearer ${token}`,
                "Content-Type": "application/json"
            },
        })
        .then((response) => {
            response.json()
              .then((result) => {
                console.warn("result", result);
                message.success("Successfully !")
                this.listTable();
              })
          })
          .catch(error => console.log('error', error));
    }

    confirmPayment(id) {

        console.log('Confirmation')
        console.log(id);
        const token = localStorage.getItem('token');
        fetch(`${process.env.REACT_APP_BASEURL}/api/confirm/${id}`, {
            method: 'PUT',
            headers: {
                "Authorization": `Bearer ${token}`,
                "Content-Type": "application/json"
            },
        })
            .then(response => response.json())
            .then(json => {
                if (json.sukses) {
                    message.success("Successfully !", 1)
                    this.listTable();
                }
                // else{
                //     message.error( json.msg, 1)
                // }
            })

            .catch(error => console.log('error', error));
    }

    componentDidMount() {
            this.listTable();
    }

    listTable() {
        const token = localStorage.getItem('token');
        
        fetch(`${process.env.REACT_APP_BASEURL}/api/admin/read/transactions/waiting_confirmation`, {
            method: 'GET',
            headers: {
                "Authorization": `Bearer ${token}`,
                "Content-Type": "application/json"
            },
        })
            .then(response => response.json())
            .then(json => {
                this.setState({
                    post: json.rows,
                })
            })
            .catch(error => console.log('error', error));
    }

    render() {
        const { modalIsOpen} = this.state;
        return (
            <Fragment>
                <Navigation history={this.props.history} />
                <Container fluid
                    style={{
                        width: "100%",
                        minHeight: "800px",
                        padding: "50px",
                        backgroundColor: "#F1F1F1"
                    }}
                >
                    <Row className="justify-content-center" style={{ width: '85%', margin: 'auto' }}>
                        <Col>
                            <h4 className="title-course">Konfirmasi Pembayaran</h4>
                        </Col>
                    </Row>
                    <br></br>
                    <Row className="justify-content-center" style={{ width: '85%', margin: 'auto' }}>
                        <Col>
                            <h4 className="sub-title">Data Konfirmasi Pembayaran</h4>
                        </Col>
                    </Row>
                    <br></br>

                    <Row>
                        <Col>
                            <Table style={{ background: "#F9F9F9" }} className="justify-content-md-center" style={{ width: '83%', margin: 'auto' }}>
                                <thead style={{ fontFamily: "gordita", color: '#3B2881', fontSize: '12px', background: '#E1DCFF' }}>
                                    <tr>
                                        <th>Invoice No</th>
                                        <th>Student Name</th>
                                        <th>Bank Asal</th>
                                        <th>Jumlah Pembayaran</th>
                                        <th>Tanggal Bayar</th>
                                        <th>Attachment</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                {
                                    this.state.post.map((list) => {
                                        return<>
                                            <tr className="justify-content-md-center" style={{ width: '83%', margin: 'auto', fontSize: '12px' }}>
                                                <td style={{ cursor: 'pointer' }} onClick={ () => { window.open(`/Invoice/${list.id}`) } } >{list.invoice_number}</td>
                                                <td>{list.student_name}</td>
                                                <td>{list.bank_asal}</td>
                                                <td>Rp. {parseInt(list.jumlah_pembayaran).toLocaleString("id-ID")}</td>
                                                <td>{moment(list.tanggal_bayar).format('LLLL')}</td>
                                                <td style={{ margin: 'auto' }}><i style={{ cursor: 'pointer', marginLeft: '30px' }} className="fa fa-paperclip" onClick={ () => { this.onOpenModal(list) }}></i></td>
                                                <td>
                                                    <tr>
                                                    <Popconfirm title="Confirm Payment ?" onConfirm={() => { this.confirmPayment(list.id) }} okText="Yes" cancelText="No">
                                                        <i style={{ cursor: 'pointer', marginLeft: '-2%' }} className="fa fa-check" aria-hidden="true"></i>
                                                    </Popconfirm>
                                                    <Popconfirm title="Cancel Payment ?" onConfirm={() => { this.handleCancel(list.id) }} okText="Yes" cancelText="No">
                                                        <i style={{ cursor: 'pointer', marginLeft: '15px' }} className="fa fa-times" aria-hidden="true"></i>
                                                    </Popconfirm>
                                                    </tr>
                                                </td>
                                            </tr>
                                        </>
                                    })}

                            </Table>
                        </Col>
                    </Row>

                </Container>
                <Footer />

                <Modal size="sm" show={modalIsOpen} onHide={this.onCloseModal} animation={false}>
                    <Modal.Header closeButton>
                        <Modal.Title style={{ fontFamily: 'Gordita-bold', fontSize: '15px' }}>Attachment</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Row className="justify-content-center">
                            <Col lg={12}><Image src={this.state?.edited_list?.cover?.url} rounded style={{ width: '265px' }} /></Col>
                        </Row>
                    </Modal.Body>
                </Modal>

            </Fragment >
        );
    }
}

export default Confirmation;