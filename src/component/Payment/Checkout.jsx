import React, { Component } from 'react';
import Navigation from '../Navigation/Navigation';
import { Container, Row, Col, Image } from 'react-bootstrap';
import Footer from '../Footer/Footer';
import { Card, Collapse, Form, Input, Button, Upload, message } from 'antd';
import Moment from 'moment';
const { Panel } = Collapse;

class Checkout extends Component{

    constructor(props) {
        super(props);

        this.state = {
            bank: [],
            transfer: [],
            nama: '',
            noreq: 0,
            image: ''     
        };

    }

    componentDidMount(){
        this.TransactionbyId();
    }

    TransactionbyId(){
        const id = this.props.match.params.id;
        const token = localStorage.getItem('token');
        fetch(`${process.env.REACT_APP_BASEURL}/api/user/read/transaction/${id}`, {
            method: "GET",
            headers: {
                "Authorization": `Bearer ${token}`,
                "Content-Type": "application/json"
            },
        }).then((response) => {
            response.json()
                .then((result) => {
                    console.log(result);
                    this.setState({transaction: result.data});
                    console.log(this.state.transaction);
                })
        })
        console.log(id);
    }

    onFileChange = (event) => {
        this.setState({ image: event.target.files[0] });
        console.log(this.state.image);
    };

    toBase64 = (file) =>
    new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = (error) => reject(error);
    });

    findf = (f) => {
        f = f.split(";base64,");
        if (f.length > 1) {
        f = f[1];
        } else {
        f = f[0];
        }
        return f;
    }



    Checkout(){        
        
        if (!this.state.image) {
            message.error("image can not be empty")
            return;
          }

        this.toBase64(this.state.image).then((f) => {
            f = this.findf(f)
        
            const token = localStorage.getItem('token');
            const id = this.state.transaction?.id;
            fetch(`${process.env.REACT_APP_BASEURL}/api/transfer/${id}`, {
                method: "PUT",
                body: JSON.stringify({
                    nomor_rekening: this.state.noreq,
                    nama_pemilik_rekening: this.state.nama,
                    cover: {
                        file_name: this.state.image.name,
                        file: f,
                        file_size: this.state.image.size,
                        file_type: this.state.image.type
                    }
                }),
                headers: {
                    "Authorization": `Bearer ${token}`,
                    "Content-Type": "application/json"
                },
            }).then((response) => {
                response.json()
                    .then((result) => {
                        console.log(result);
                        if(result.sukses){
                            this.setState({
                              transfer: result.data
                            });
                            message.success("berhasil", 1);
                            this.props.history.push({
                                pathname: `/MyCourses`,
							    state: this.state.transfer
                            });
                        }else{
                            message.error(result.msg, 1);
                        }
                    })
            })
        })
    }

    Cancel(){
        const token = localStorage.getItem('token');
        const id = this.props.match.params.id;

        fetch(`${process.env.REACT_APP_BASEURL}/api/cancel/payment/${id}`, {
            method: "POST",
            headers: {
                "Authorization": `Bearer ${token}`,
                "Content-Type": "application/json"
            },
        }).then((response) => {
            response.json()
                .then((result) => {
                    message.success("Transaksi Dibatalkan", 1);
                    this.props.history.push('/ShoppingChart');
                })
        })
    }

    callback = (key) => {
        console.log(key);
    }  

    render(){

        const data = [
            `1.`,
            `2.`,
            `3.`,
            `4.`
        ];

        const text = `
            A dog is a type of domesticated animal.
            Known for its loyalty and faithfulness,
            it can be found as a welcome guest in many households across the world.
            `;

        return(
            <>
            <Navigation/>
            <Container fluid style={{ width: '100%', backgroundColor: '#F1F1F1', padding: '50px' }}>
                <Row className="justify-content-center">
                    <Col lg={5}>
                        <Card title={<Image style={{ width: '90px', height: '40px' }} src={this.state.transaction?.payment?.file?.url} />} ghost>
                            <Row>
                                <Col lg={12} style={{ lineHeight: '0.5' }}>
                                   <Row>
                                       <Col lg={10}>
                                            <p style={{ fontFamily: 'Gordita-bold' }}>No. Rekening</p>
                                            <p>{this.state.transaction?.payment?.account_number}</p>
                                            <p>{this.state.transaction?.payment?.account_name}</p>
                                       </Col>
                                       <Col>
                                            <p style={{ color: '#392C7E', cursor: 'pointer' }} >Copy</p>
                                       </Col>
                                   </Row>
                                </Col>
                                <Col lg={12} style={{ lineHeight: '0.5', marginTop: '5%' }}>
                                    <p style={{ fontFamily: 'Gordita-bold' }}>Total</p>
                                    <p>IDR {this.state.transaction?.jumlah_pembayaran?.toLocaleString("id-ID")}</p>
                                </Col>
                                <Col lg={12}>
                                    <Collapse style={{ backgroundColor: '#FFFFFF' }} expandIconPosition={"right"} accordion>
                                        <Panel header="Detail" key="1">
                                            <div style={{ lineHeight: '0.5'}}>
                                                <p>Invoice No :</p>
                                                <p style={{ fontFamily: 'Gordita-bold' }}>{this.state?.transaction?.invoice_number}</p>
                                            </div>
                                            <div style={{ marginTop: '5%' }}>
                                                <p>Order Detail :</p>
                                                {
                                                    this.state.transaction?.courses.map((list) => {
                                                        return <>
                                                            <Row>
                                                                <Col><Image style={{ width: '70px', height: '50px' }} src={list.cover?.url} /></Col>
                                                                <Col style={{ marginTop: '-1%' }}><p style={{ fontSize: '12px' }} >{list.course_name}</p></Col>
                                                                <Col style={{ marginTop: '-2%'}}>
                                                                    <s style={{ color: '#A4A4A4', fontSize: '12px'}} >{list.price}</s>
                                                                    <p style={{ color: '#392C7E',fontSize: '12px' }}>{list.price}</p>
                                                                </Col>
                                                            </Row>
                                                        </>
                                                    })
                                                }
                                                {
                                                    this.state?.transaction?.webinars.map((list) => {
                                                        return <>
                                                            <Row>
                                                                <Col><Image style={{ width: '70px', height: '50px' }} src={list.cover?.url} /></Col>
                                                                <Col style={{ marginTop: '-1%' }}><p style={{ fontSize: '12px' }} >{list.webinar_name}</p></Col>
                                                                <Col style={{ marginTop: '-2%'}}>
                                                                    <s style={{ color: '#A4A4A4', fontSize: '12px'}} >{list.price}</s>
                                                                    <p style={{ color: '#392C7E',fontSize: '12px' }}>{list.price}</p>
                                                                </Col>
                                                            </Row>
                                                        </>
                                                    })
                                                }
                                            </div>
                                        </Panel>
                                    </Collapse>
                                </Col>
                                <Col lg={12} style={{ marginTop: '5%' }}>
                                    <p style={{ fontFamily: 'Gordita-bold' }}>Batas Akhir Pembayaran</p>
                                    <p>{Moment(this.state?.transaction?.createdAt).add(1, 'day').format( 'DD MMMM YYYY')}</p>
                                </Col>
                            </Row>
                        </Card><br/>

                        <Card title="Tata Cara Pembayaran" >
                            <Collapse style={{ backgroundColor: '#FFFFFF' }} expandIconPosition={"right"} accordion>
                                {
                                    this.state.bank?.payment_informations?.map((list) => {
                                        return  <Panel header={list.information_name} key={list.id}>
                                        <p>{list.description}</p>
                                    </Panel>   
                                    })
                                }                             
                            </Collapse>
                        </Card><br/>

                        <Card title="Bukti Pembayaran" >
                            <Form name="basic" initialValues={{ remember: true }}>
                                <Form.Item name="noreq" rules={[{ required: true, message: 'Please input your noreq!' }]} >
                                    <Input onChange={ (event) => { this.setState({ noreq: event.target.value }) } } placeholder="No. Rekening" />
                                </Form.Item>
                                <Form.Item name="nama" rules={[{ required: true, message: 'Please input your username!' }]} >
                                    <Input onChange={ (event) => { this.setState({ nama: event.target.value }) } } placeholder="Atas Nama" />
                                </Form.Item>
                                <Form.Item>
                                    <Input onChange={this.onFileChange} type="file" />
                                </Form.Item>
                                <Form.Item>
                                    <Button onClick={ () => { this.Checkout() } } style={{ backgroundColor: '#392C7E', border: '0' }} block type="primary" htmlType="submit">Kirim</Button>
                                    <Button onClick={ () => { this.Cancel() } } style={{ border: '1px solid #392C7E', marginTop: '2%' }} block htmlType="submit">Batalkan</Button>
                                </Form.Item>
                            </Form>
                        </Card>
                    </Col>
                </Row>
            </Container>
            <Footer/>
            </>
        )
    }

}

export default Checkout;