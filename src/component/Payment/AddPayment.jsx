import React, { Component } from "react";
import Navigation from "../Navigation/Navigation";
import { Link } from "react-router-dom";
import "./Payment.css";
import Footer from "../Footer/Footer";
import { message } from 'antd';
import {
  Container,
  Row,
  Col,
  Form,
  Card,
  Navbar,
  Button,
  Modal,
  Accordion,
  ProgressBar
} from "react-bootstrap";
import { Formik } from 'formik';
import 'font-awesome/css/font-awesome.min.css';
import 'font-awesome/css/font-awesome.min.css'; import { Collapse, Divider } from 'antd';
import { findRenderedComponentWithType } from "react-dom/test-utils";

import { PlusOutlined, MinusOutlined, DeleteOutlined, EditOutlined, CheckOutlined, CloseOutlined } from '@ant-design/icons'
const { Panel } = Collapse;

class AddPayment extends Component {
  SelectedFile;
  reader = new FileReader();
  socket;
  state = {
    list: [],
    payment_informations: [],
    bank_name: null,
    account_name: null,
    account_number: null,
    method: "Manual Bank Transfer",
    kode: null,
    status: 1,
    file: null,
    image: "",
    information_name: '',
    description: '',
    incIdTopic: 1
  };
  // formRef = this.props.formRef;
  formRef = React.createRef();

  // componentDidUpdate(prevProps) {
  //   if (this.props.instructors && this.formRef && this.formRef.current) {
  //     this.formRef.current.setFieldsValue({
  //       id: this.props.instructors.id,
  //       informatin_name: this.props.instructors.information_name,
  //       description: this.props.instructors.description,
  //     });
  //   }
  // }

  customExpandIcon(props) {
    if (props.isActive) {
      return <MinusOutlined onClick={((e) => {
        props.expandIcon(props, e);
      })}/>
    } else {
      return <PlusOutlined onClick={((e) => {
        props.expandIcon(props, e);
      })}/>
    }
  }

  componentDidMount() {
  }

  onFileChange = (event) => {
    // Update the state
    this.setState({ image: event.target.files[0] });
  };

  setParentState(states) {
    this.setState(
      { uploadState: states }
    )
    console.log(this.state.uploadState);
    console.log(this.state.uploadState?.socketUploadDataReturn?.data?.id);
  }


  setAddInformationState(states) {
    const payment_informations = this.state.payment_informations.concat(states);
    this.setState({ payment_informations })
    console.log(payment_informations);
  }


  toBase64 = (file) =>
    new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = (error) => reject(error);
    });

  AddPayment() {
    if (!this.state.image) {
      message.error("image can not be empty", 1)
      return;
    }
    this.toBase64(this.state.image).then((f) => {
      f = f.split(";base64,");
      if (f.length > 1) {
        f = f[1];
      } else {
        f = f[0];
      }
      console.log(this.state);
      const id = this.state.payment_informations.map(i => i.id);
      const token = localStorage.getItem("token");
      console.log(f);
      const payment_informations = this.state.payment_informations.map(a => {
        delete a.id;
        return a;
      })
      console.log(this.state);
      fetch(
        `${process.env.REACT_APP_BASEURL}/api/create/payment_method`,
        {
          method: "POST",
          body: JSON.stringify({
            bank_name: this.state.bank_name,
            account_name: this.state.account_name,
            account_number: this.state.account_number,
            method: "Manual Bank Transfer",
            kode: this.state.kode,
            //instructors: this.state.instructors,
            cover: {
              file: f,
              file_name: this.state.image.name,
              file_size: this.state.image.size,
              file_type: this.state.image.type,
            },
            //   payment_informations: {
            //     information_name: this.state.information_name,
            //     description: this.state.description,
            // }
            payment_informations: payment_informations,
          }),
          headers: {
            Authorization: `Bearer ${token}`,
            "Content-Type": "application/json",
          },
        }
      ).then((response) => {
        response.json().then((result) => {
          if (result.sukses) {
            console.log(result);
            // console.log(result.files.url);
            // this.setState({ url: result.files.url });
            this.props.history.push("/Payment");
            message.success("Successfully !", 1);
          } else {
            message.error(result.msg, 1);
          }
        });
      });
    });
  }
  saveFile(e) {
    if (!e) {
      return;
    }
    this.toBase64(e).then((f) => {
      // console.log(`base64: ${f}`);
      // REMOVE BEFORE ;base64,
      f = f.split(";base64,")[1];
      const token = localStorage.getItem("token");
      fetch(`${process.env.REACT_APP_BASEURL}/api/upload`, {
        method: "POST",
        body: JSON.stringify({
          file: f,
          file_name: e.name,
          file_size: e.size,
          file_type: e.type,
        }),
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      }).then((response) => {
        response.json().then((result) => {
          console.log(result);
          console.log(result.files.url);
          this.setState({ url: result.files.url });
        });
      });
    });
  }

  setAddInformationState(states) {
    if (states.id) {
      const hasil = this.state.payment_informations.map(a => {
        if (a.id === states.id) {
          a = states;
        }
        return a;
      })
      this.setState({
        payment_informations: hasil
      })
    } {
      states.id = `add${this.state.incIdTopic}`
      this.setState({ incIdTopic: parseInt(this.state.incIdTopic) + 1 })
    }
    const payment_informations = this.state.payment_informations.concat(states);
    this.setState({ payment_informations })
  }

  information = (value) => {
    console.log(this.formRef);
    console.log(value);
    this.setAddInformationState({
      id: this.state.id,
      information_name: this.formRef.current.values.information_name,
      description: this.formRef.current.values.description
    });
    this.setState({
      information_name: '',
      description: ''
    });
    
  }

  render() {
    return (
      <>
        <Navigation history={this.props.history} />
        <Container
          fluid
          style={{
            width: "100%",
            minHeight: "2000px",
            paddingTop: "50px",
            backgroundColor: "#F1F1F1",
          }}
        >
          <Row className="justify-content-center">
            <Col xs={10} md={10}>
              <Navbar
                expand="lg" style={{ background: "rgba(58, 44, 125, 0.34)" }}>
                <Link to={`/Payment`}>
                  <button className="btn icon-back">
                    <span style={{ color: "#FFFFFF" }} className="fa fa-chevron-left"></span>
                  </button>
                </Link>
                <Navbar.Brand style={{ color: "#FFFFFF", fontFamily: "Gordita-bold" }}>Add Payment Method</Navbar.Brand>
              </Navbar>
            </Col>
          </Row>
          <br /><br />

          <Row className="justify-content-center">
            <Col xs={10} md={10}>
              <h4 className="title-course">1. Bank</h4>
            </Col>
          </Row>

          <Row>
            <Col>
              <Card style={{ width: "83%", margin: 'auto' }}>
                <Card.Body style={{ padding: '2rem' }}>
                  <Row>
                    <Col xs={12} lg={5}>
                      <Form>

                        <Form.Group>
                          <Form.Label className="form-label">Bank Name *</Form.Label>
                          <Form.Control type="text" onChange={(event) => {
                            this.setState({
                              bank_name: event.target.value,
                            });
                          }}
                          />
                        </Form.Group>

                        <Form.Group>
                          <input type="file" name="file" id="file" className="inputfile" onChange={this.onFileChange} />
                          <label style={{ width: '100%' }} for="file"><i className="fa fa-upload"></i> Add Logo Bank</label>
                          {/* <div>url: {this.state.url}</div> */}
                        </Form.Group>

                      </Form>

                    </Col>
                    <Col lg={2} />
                    <Col xs={12} lg={5}>
                      <Form>
                        <Form.Group>
                          <Form.Label>Account Name</Form.Label>
                          <Form.Control
                            type="text"
                            onChange={(event) => {
                              this.setState({ account_name: event.target.value });
                            }}
                          />
                        </Form.Group>
                        <Form.Group>
                          <Form.Label>Account Number</Form.Label>
                          <Form.Control
                            type="text"
                            onChange={(event) => {
                              this.setState({ account_number: event.target.value });
                            }}
                          />
                        </Form.Group>
                        <Form.Group>
                          <Form.Label>Kode Bank</Form.Label>
                          <Form.Control
                            type="text"
                            onChange={(event) => {
                              this.setState({ kode: event.target.value });
                            }}
                          />
                        </Form.Group>
                      </Form>
                    </Col>
                  </Row>
                </Card.Body>
              </Card>
            </Col>
          </Row>
          <br /><br />

          <Row className="justify-content-center">
            <Col xs={10} md={10}>
              <h4 className="title-course">2. Payment Information</h4>
            </Col>
          </Row>

          {/* <AddInstructor addInstructor={(states) => {this.setAddInstructorsState(states)}}/>
          <ListInstructor data={this.state.instructors}/> */}

          <Row>
            <Col>
              <Card style={{ width: "83%", margin: 'auto' }}>
                <Card.Body style={{ padding: '2rem' }}>
                  <Row>
                    <Col xs={6} lg={5}>
                        <Formik
                          innerRef={this.formRef}
                          validationSchema={this.schema}
                          validateOnChange={false}
                          validateOnBlur={false}
                          onSubmit={() => this.information()}
                          initialValues={{
                            information_name: '',
                            description: ''
                          }}
                        >
                          {({
                            handleSubmit,
                            handleChange,
                            values,
                            setFieldValue,
                            errors,
                          }) => (
                      <Form onSubmit={handleSubmit}>
                        <Form.Group>
                          <Form.Label className="form-label">Information Name</Form.Label>
                          <Form.Control type="text" 
                          // onChange={(event) => {
                          //   this.setState({
                          //     information_name: event.target.value,
                          //   });
                          // }}
                          name="information_name"
                          value={values.information_name}
                          onChange={handleChange}
                          />
                        </Form.Group>

                        <Form.Group>
                          <Form.Label>Description *</Form.Label>
                          <Form.Control as="textarea" rows="3" 
                          // onChange={(event) => {
                          //   this.setState({
                          //     description: event.target.value,
                          //   });
                          // }}
                          name="description"
                          value={values.description}
                          onChange={handleChange}
                          />
                        </Form.Group>

                        <Button style={{ marginLeft: '350px' }} className="button-purple"
                          onClick={() => {
                            this.information();
                          }}
                        >Add</Button>
                      </Form>
                        )}
                        </Formik>

                    </Col>
                      <Col lg={2} />
                      <Col xs={6} lg={5}>

                        <Accordion defaultActiveKey="0">
                          {
                            this.state.payment_informations?.map((list, index) => {
                              return <Collapse
                              accordion
                              style={{ width: "83%", margin: 'auto' }}
                              expandIconPosition={"Right"}
                              defaultActiveKey={this.defaultKey}
                              expandIcon={(props) => this.customExpandIcon(props)}
                            >
                                <Panel header={list.information_name} key={list.id}
                                  style={{ backgroundColor: "#EFEFEF" }}
                                >
                                  <p><b>Deskripsi</b></p>
                                  <Form.Control type="text" onChange={(event) => {
                                          this.setState({
                                            description: event.target.value,
                                          });
                                          console.log(this.state.description);
                                        }}
                                        defaultValue={list.description}
                                  />
                                  {/* <p>{list.description}</p> */}
                                </Panel>
                            </Collapse>
                            })
                          }
                        </Accordion>
                        {/* <Collapse
                        accordion
                        style={{ width: "83%", margin: 'auto' }}
                        expandIconPosition={"right"}
                        defaultActiveKey={this.defaultKey}
                        expandIcon={(props) => this.customExpandIcon(props)}
                      >
                        {this.state.list?.payment_informations?.map(list => {
                          return <Panel header={list.information_name} key={list.id}
                            style={{ backgroundColor: "#EFEFEF" }}
                          // extra={this.editButton(this.props.is_edit, list)}
                          >
                            <p><b>Deskripsi</b></p>
                            <p>{list.description}</p>
                          </Panel>
                        })}
                      </Collapse> */}
                      </Col>

                  </Row>
                </Card.Body>
              </Card>
            </Col>
          </Row>

            <br /><br />

            <Row style={{ marginLeft: '1040px' }}>
              <>

                <Button style={{ marginLeft: '30px' }} className="button-purple" onClick={() => {
                  this.AddPayment(this.state.gambar);
                }}
                >Save</Button>
              </>
            </Row>

        </Container>
          <Footer />
      </>
    );
  }
}

export default AddPayment;