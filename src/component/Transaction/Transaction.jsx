import React, { Component } from 'react';
import { Row, Col } from 'react-bootstrap';
import ListTransaction from './ListTransaction';

class Transaction extends Component{

    constructor(props) {
        super(props);

        this.state = {
            transaction: [],
        };

    }

    componentDidMount(){
        this.listTransaction();
    }
    
    statusTransaction(status){

        this.listTransaction(status);
    }
    
    listTransaction(status){
        const token = localStorage.getItem('token');
        let url = `${process.env.REACT_APP_BASEURL}/api/user/read/transactions`;
        if(status){
            url = `${process.env.REACT_APP_BASEURL}/api/user/read/transactions/${status}`;
        }
        fetch(url, {
            method: "GET",
            headers: {
                "Authorization": `Bearer ${token}`,
                "Content-Type": "application/json"
            },
        }).then((response) => {
            response.json()
                .then((result) => {
                    console.log(result.rows);
                    this.setState({
                        transaction: result.rows,
                    });
                    // console.log(this.state.transaction)
                })
        })
    }


    render(){

        const style={
            cursor: 'pointer'
        }

        return(
            <>
            <Row style={{ width: '95%' }} >
                <Col lg={3}>
                    <p style={style} onClick={ () => { this.statusTransaction('') } } >All</p>
                    <p style={style} onClick={ () => { this.statusTransaction('waiting_payment') } } >Waiting Payment</p>
                    <p style={style} onClick={ () => { this.statusTransaction('waiting_confirmation') } }>Waiting Confirmation</p>
                    <p style={style} onClick={ () => { this.statusTransaction('paid') } }>Paid</p>
                    <p style={style} onClick={ () => { this.statusTransaction('cancel') } }>Canceled</p>
                </Col>
                <Col>
                    {
                        this.state.transaction?.map((list) => {
                            return <ListTransaction key={list.id} id={list.id} list={list} />
                        })
                    }
                </Col>
            </Row>
            </>
        )
    }

}

export default Transaction;