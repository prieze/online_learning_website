import React, { Component } from 'react';
import { Container, Row, Col, Table, Image } from 'react-bootstrap';
import ICON from '../../gambar/galilagi.png';
import Moment from 'moment';

class Invoice extends Component{

    constructor(props) {
        super(props);
        
        this.state = {
          invoice: []
        };

    }

    componentDidMount(){
        this.InvoicebyId();
    }

    InvoicebyId(){
        const id = this.props.match.params.id;
        const token = localStorage.getItem('token');
        fetch(`${process.env.REACT_APP_BASEURL}/api/user/read/transaction/${id}`, {
            method: "GET",
            headers: {
                "Authorization": `Bearer ${token}`,
                "Content-Type": "application/json"
            },
        }).then((response) => {
            response.json()
                .then((result) => {
                    const course = [];
                    const webinar = [];
                    const discount = result.data?.courses?.map((disc)=>{
                        const price = parseInt(disc.price);
                        const dic = parseInt(disc.discount);
                        const jum = (dic/100)*price;
                        const tot = price-parseInt(jum);
                        course.push({
                            ...disc,
                            hargadisc: tot,
                        })
                        return tot
                      });
                    const diskon = result.data?.webinars?.map((disc)=>{
                        const price = parseInt(disc.price);
                        const dic = parseInt(disc.discount);
                        const jum = (dic/100)*price;
                        const tot = price-parseInt(jum);
                        webinar.push({
                            ...disc,
                            hargadisc: tot,
                        })
                        return tot
                      });
                    this.setState(prevstate => ({
                        ...prevstate,
                        invoice: {
                            invoice_number: result.data.invoice_number,
                            confirmed: result.data.confirmed,
                            cover: result.data.cover,
                            jumlah_pembayaran: result.data.jumlah_pembayaran,
                            tanggal_bayar: result.data.tanggal_bayar,
                            course: course, 
                            webinar: webinar
                        } 
                    }));
                    // this.setState({invoice: result.data});
                    console.log(`temp `, webinar);
                    console.log(`temp `, course);
                    console.log(`disc `, discount);
                    console.log(`state `,this.state);
                    console.log(`result `,result);
                })
        })
    }

    render(){

        const paid = {
            borderRadius: '10px',
            backgroundColor: 'rgba(6, 228, 121, 0.16)',
            color: '#06E479',
            textAlign: 'center'
        }

        const wait = {
            borderRadius: '10px',
            backgroundColor: 'rgba(255, 212, 138)',
            color: '#ffa100',
            textAlign: 'center'
        }

        return(
            <>
            <Container fluid style={{ width: '100%', padding: '50px' }}>
                <Row className="justify-content-center" style={{ marginLeft: '21%' }} >
                    <Col lg={6}>
                        <Image src={ICON} style={{ width: '45%' }} />
                    </Col>
                </Row>
                <Row className="justify-content-center" style={{ width: '65%', margin: 'auto' }} >
                    <Col>
                        <table style={{ lineHeight: '2' }}>
                            <tr>
                                <td>Nomor Invoice </td>
                                <td>:</td>
                                <td>{this.state.invoice.invoice_number}</td>
                            </tr>
                            <tr>
                                <td>Pembayaran </td>
                                <td>:</td>
                                <td><p>Manual Bank Transfer</p></td>
                                {/* <Image src={this.state.invoice?.payment?.file?.url} style={{ width: '5%' }} /> */}
                            </tr>
                            <tr>
                                <td>Tanggal </td>
                                <td>:</td>
                                <td>{Moment(this.state.invoice.tanggal_bayar).format( 'dddd, DD MMMM YYYY')}</td>
                            </tr>
                            <tr>
                                <td>Status Pembayaran </td>
                                <td>:</td>
                                {
                                    this.state.invoice.confirmed === 2 ?<td style={wait}>Waiting Confirmation</td> :<td style={paid}>Paid</td>
                                }
                            </tr>
                        </table>
                    </Col>
                </Row>
                <Row className="justify-content-center" style={{ width: '65%', margin: 'auto', marginTop: '3%' }} >
                    <Col>
                        <Table striped>
                            <thead style={{ backgroundColor: '#392C7E', color: '#FFFFFF' }}>
                                <tr>
                                    <th>Item Description</th>
                                    <th>Price</th>
                                    <th>Discount (%)</th>
                                    <th>Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    this.state.invoice?.webinar?.map(a => {
                                        return <tr>
                                            <td>{a.webinar_name}</td>
                                            <td>{parseInt(a.price).toLocaleString("id-ID")}</td>
                                            <td>{a.discount} %</td>
                                            <td>{parseInt(a.hargadisc).toLocaleString("id-ID")}</td>
                                        </tr>
                                    })
                                }
                                {
                                    this.state.invoice?.course?.map(a => {
                                        return <tr>
                                            <td>{a.course_name}</td>
                                            <td>{parseInt(a.price).toLocaleString("id-ID")}</td>
                                            <td>{a.discount} %</td>
                                            <td>{parseInt(a.hargadisc).toLocaleString("id-ID")}</td>
                                        </tr>
                                    })
                                }
                                <tr>
                                    <td colSpan="3" style={{ fontFamily: 'Gordita-bold' }}>SUB TOTAL</td>                                    
                                    <td style={{ fontFamily: 'Gordita-bold' }}>IDR {parseInt(this.state.invoice.jumlah_pembayaran).toLocaleString("id-ID")}</td>
                                </tr>
                            </tbody>
                        </Table>
                    </Col>
                </Row>
            </Container>
            </>
        )
    }

}

export default Invoice;