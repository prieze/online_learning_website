import React from 'react';
import { Row, Col, Button } from 'react-bootstrap';
import { Card, Badge, message } from 'antd';
import { Link } from 'react-router-dom';
import Moment from 'moment';

const ListTransaction = (props) => {
    console.log(props);

    const confirm = (id) => {
        if(id === 1){
            return {
                name: "Waiting Payment",
                style: "#3e99c7"
            }
        }else if(id === 2){
            return {
                name: "Waiting Confirmation",
                style: "#f5a527"
            }
        }else if(id === 3){
            return {
                name: "Paid",
                style: "#73d428"
            }
        }else{
            return {
                name: "Canceled",
                style: "#fa1e1e"
            }
        }
    }
    
    return(
        <>
        <Badge.Ribbon color={ confirm(props.list.confirmed).style } text={ confirm(props.list.confirmed).name }>
            <Card style={{ borderRadius: '10px', boxShadow: '10px 10px 10px rgba(0, 0, 0, 0.1)' }} >
                <Row>
                    <Col>
                        <p>{props.list.invoice_number} | Invoice</p>
                        <p>Total : <b style={{ color: '#FF2E00' }}>{parseInt(props.list.jumlah_pembayaran).toLocaleString("id-ID")}</b></p>
                        <table>
                            <tr>
                                <td>Purchase date</td>
                                <td>:</td>
                                <td>{(props.list.tanggal_bayar !== null)?Moment(props.list.tanggal_bayar).format( 'dddd, DD MMMM YYYY'):Moment(props.list.createdAt).format( 'dddd, DD MMMM YYYY')}</td>
                            </tr>
                            <tr>
                                <td> Payment method</td>
                                <td>:</td>
                                <td>Transfer Manual {props.list.bank_asal}</td>    
                            </tr>
                            <tr>
                                <td> No rekening</td>
                                <td>:</td>
                                <td>{props.list.nomor_rekening} a.n {props.list.nama_pemilik_rekening}</td>    
                            </tr>
                            {(props.list.confirmed === 1)
                                ?<tr>
                                    <td>
                                        <Link to={`/Checkout/${props.list.id}`}><Button variant="light" size="sm">Lanjutkan Pembayaran</Button></Link>
                                    </td>
                                </tr>
                                :null
                            }
                            {(props.list.confirmed === 3)
                                ?<tr>
                                    <td>
                                        <Button onClick={ () => { window.open(`/Invoice/${props.list.id}`) } } variant="light" size="sm">Invoice</Button>
                                    </td>
                                </tr>
                                :null
                            }      
                        </table>
                    </Col>
                </Row>
            </Card>
        </Badge.Ribbon>
        <br/>
        </>
    )
}

export default ListTransaction;