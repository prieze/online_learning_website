import React, { Component, Fragment, } from 'react';
import { Navbar, Nav,FormGroup, Form, Button, Row, Col, Card, Image, Container, Modal } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../../style/styles.css';
import 'font-awesome/css/font-awesome.min.css';
import { Link } from 'react-router-dom';
import { BrowserRouter as Router, Redirect } from "react-router-dom";
import GoogleLogin from 'react-google-login'
import Footer from "../Footer/Footer"
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const validEmailRegex = RegExp(/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i);
const validateForm = errors => {
    let valid = true;
    Object.values(errors).forEach(val => val.length > 0 && (valid = false));
    return valid;
};


class ForgotPassword extends Component {

    constructor(props) {
        super(props)
        this.state = {
            modalLoginIsOpen: false,
            modalSignIsOpen: false,
            email: null,
            password: null,
            compass: null,
            first_name: null,
            last_name: null,
            login: false,
            register: false,
            store: null,
            message: null
        };
    }

    Logo = 'https://galilagi-media.oss-ap-southeast-5.aliyuncs.com/assets/logo.png';
    Desktop = 'https://galilagi-media.oss-ap-southeast-5.aliyuncs.com/assets/banner-main.png';
    User1 = 'https://galilagi-media.oss-ap-southeast-5.aliyuncs.com/assets/user1.png'

    onOpenLoginModal = () => {
        this.setState({ modalLoginIsOpen: true, modalSignIsOpen: false, message: null });
    };

    onOpenSignModal = () => {
        this.setState({ modalSignIsOpen: true, modalLoginIsOpen: false, message: null });
    };

    onCloseLoginModal = () => {
        this.setState({ modalLoginIsOpen: false, message: null });
    };

    onCloseSignModal = () => {
        this.setState({ modalSignIsOpen: false, message: null });
    };

    responseGoogle = (response) => {
        console.log(response);
        console.log(response.profileObj);

    }

    login() {
        fetch(`${process.env.REACT_APP_BASEURL}/api/login`, {
            method: "POST",
            body: JSON.stringify(this.state),
            headers: {
                "Content-Type": "application/json"
            },
        }).then((response) => {
            response.json()
                .then((result) => {
                    if (result.sukses === true) {
                        console.warn("result", result);
                        console.log(result.data.id);
                        localStorage.setItem('login', JSON.stringify({
                            login: true,
                            token: result.token
                        }))
                        localStorage.setItem('token', result.token);
                        localStorage.setItem('userData', JSON.stringify(result.data));
                        localStorage.setItem('id', result.data.id);
                        this.setState({ login: true });
                        this.setState({ redirect: true });
                        console.log("result" + JSON.stringify(result.data));
                        console.log(this.result);
                        console.log(this.state);
                    } else {
                        // this.setState({ message: "Username or Password Doesn't Match" })
                        toast.error(result.msg);
                    }
                })
        })
            .catch(function (error) {
                console.log(error);
            })
    }

    register() {
        console.log(this.state);
        if (this.state.compass !== this.state.password) {
            // return new Promise(resolve => {
            //     this.setState({ message: "Password Doesn't match" });
            //     setTimeout(resolve, 1000);
            // });
            toast.error("Password Doesn't match");
            // } 
            // else if (this.state.first_name === "" || this.state.first_name === null) {
            //     return new Promise(resolve => {
            //         this.setState({ message: "First Name cannot be empty" });
            //         setTimeout(resolve, 1000);
            //     });
        } else {
            fetch(`${process.env.REACT_APP_BASEURL}/api/register`, {
                method: "POST",
                body: JSON.stringify(this.state),
                headers: {
                    "Content-Type": "application/json"
                },
            }).then((response) => {
                response.json()
                    .then((result) => {
                        if (result.sukses === true) {
                            console.warn("result", result);
                            localStorage.setItem('register', JSON.stringify({
                                register: true,
                                token: result.token
                            }))
                            this.props.history.push('/Dashboard')
                        } else {
                            // return new Promise(resolve => {
                            // this.setState({ message: 'Make Sure Email and Password is Filled Out Correctly!' })
                            // setTimeout(resolve, 1000);
                            toast.error(result.msg)
                            // });
                        }
                    })
            })
        }
    }

    validate() {
        let input = this.state.input;
        let errors = {};
        let isValid = true;

        if (!input["name"]) {
            isValid = false;
            errors["name"] = "Please enter your name.";
        }

        if (!input["email"]) {
            isValid = false;
            errors["email"] = "Please enter your email Address.";
        }

        if (typeof input["email"] !== "undefined") {

            var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
            if (!pattern.test(input["email"])) {
                isValid = false;
                errors["email"] = "Please enter valid email address.";
            }
        }

        if (!input["comment"]) {
            isValid = false;
            errors["comment"] = "Please enter your comment.";
        }

        this.setState({
            errors: errors
        });

        return isValid;
    }

    componentDidMount() {
        const token = localStorage.getItem('token');
        if (token) {
            this.setState({ login: true });
            this.setState({ redirect: true });
        }
    }

     changeURL(url) {
        this.setState({ redirect: url });
    }

    render() {
        const { modalLoginIsOpen, modalSignIsOpen, redirect, errors } = this.state;

        if (redirect) {
            return <Redirect to='/' />;
        }
        return (
            <Router>
                <ToastContainer />
                <Container fluid style={{ padding: 'none' }}>
                    <Fragment>
                        <Navbar expand="lg">
                            <a className="logo" href="/"><img className="img-responsive logo" src={this.Logo} alt="" /></a>
                        <Navbar.Toggle aria-controls="basic-navbar-nav" />
                            <Navbar.Collapse id="basic-navbar-nav">
                                <Nav className="mr-auto"/>                                  
                                <Form inline>
                                    <a id="login" className="btn" onClick={this.onOpenLoginModal}>Login</a>
                                    <a id="signup" className="btn" onClick={this.onOpenSignModal}>Sign Up</a>
                                </Form>
                            </Navbar.Collapse>
                        </Navbar>
                    </Fragment>
                </Container>

                <Container fluid style={{ minHeight: "600px", paddingTop: "50px", backgroundColor: "#F1F1F1" }}>
                    <Row className="justify-content-center">
                        <Col xs={8} sm={8} md={6} lg={6} xl={6}>
                            <Card style={{ width: '100%', borderRadius: '10px' }}>
                                <Card.Body style={{ padding: '3rem' }}>
                                    <Card.Title style={{ fontFamily: 'Gordita-bold', fontSize: '18px', color: '#392C7E' }}>Find Your Account</Card.Title>
                                    <Card.Text>Please enter your email address or phone number *</Card.Text>
                                    <FormGroup>
                                        <Form.Control placeholder="Enter email address or phone number" style={{ backgroundColor: '#EDEDED' }}></Form.Control>
                                    </FormGroup>
                                    <FormGroup style={{ float: 'right' }}>
                                        <a href="/"><button style={{ marginLeft: '-5%' }} className="btn btn-outline-secondary"> Cancel</button></a>
                                        <a href="/FindAccount"><Button style={{ marginLeft: '5%' }} className="button-grape">Search</Button></a>
                                    </FormGroup>
                                </Card.Body>
                            </Card>
                        </Col>
                    </Row>
                </Container>

                <Modal
                    size="lg"
                    show={modalLoginIsOpen}
                    onHide={this.onCloseLoginModal}
                    aria-labelledby="contained-modal-title-vcenter">
                    <Modal.Header closeButton bsPrefix></Modal.Header>
                    <Modal.Body className="show-grid">
                        <Container>
                            <Row className="justify-content-center">
                                <Col xs={9} md={6}>
                                    <Row className="justify-content-center">
                                        <Col md={{ span: 9, offset: 3 }}><Image src={this.User1} rounded /></Col>
                                    </Row>
                                    <Row className="justify-content-center">
                                        <Col md={12}>
                                            <p className="text-center" style={{ fontFamily: "Gordita-Bold", fontSize: "22px" }}>Hello, Galilagi !</p>
                                        </Col>
                                    </Row>
                                    <Row className="justify-content-center">
                                        <Col md={{ span: 10, offset: 1 }}>
                                            <p text-center style={{ fontFamily: "Gordita", fontSize: "15px" }}>Login and start learning with video</p>
                                        </Col>
                                    </Row>
                                </Col>
                                <Col xs={9} md={6}>
                                    <Form.Label style={{ color: "red" }}>{this.state.message}</Form.Label>
                                    <Row className="justify-content-center">
                                        <GoogleLogin
                                            clientId="944247106925-m1vaiosjtsdsv876ctg54qdpgfcvkpht.apps.googleusercontent.com"
                                            buttonText="Login with Google"
                                            onSuccess={this.responseGoogle}
                                            onFailure={this.responseGoogle}
                                            cookiePolicy={'single_host_origin'}
                                        />
                                    </Row>
                                    <p className="text-center">or</p>
                                    <Row className="justify-content-center">
                                        <Col>
                                            <Fragment>
                                                <Form>
                                                    <Form.Group controlId="formBasicEmail">
                                                        <Form.Control type="email" placeholder="Enter email" onChange={(event) => { this.setState({ email: event.target.value }) }} required />
                                                    </Form.Group>
                                                    <Form.Group controlId="formBasicPassword">
                                                        <Form.Control type="password" placeholder="Password" onChange={(event) => { this.setState({ password: event.target.value }) }} required />
                                                    </Form.Group>
                                                    <Form.Group>
                                                        <Link>forgot password?</Link>
                                                    </Form.Group>
                                                    <Button variant="dark" onClick={() => { this.login() }} block>Log In</Button>
                                                    <Form.Group>
                                                        <br></br>
                                                        <Form.Label>Not a member yet? <a onClick={this.onOpenSignModal} className="blue-text" style={{ color: "#219ddb" }}>Sign Up</a></Form.Label>
                                                    </Form.Group>
                                                </Form>
                                            </Fragment>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Container>
                    </Modal.Body>
                </Modal>

                <Modal
                    size="lg"
                    show={modalSignIsOpen}
                    onHide={this.onCloseSignModal}
                    aria-labelledby="contained-modal-title-vcenter">
                    <Modal.Header closeButton bsPrefix></Modal.Header>
                    <Modal.Body className="show-grid">
                        <Container>
                            <Row className="justify-content-center">
                                <Col xs={9} md={6}>
                                    <Row className="justify-content-center">
                                        <Col md={12}>
                                            <p style={{ fontFamily: "Gordita-Bold", fontSize: "22px" }}>Joining Galilagi For</p>
                                            <p style={{ fontFamily: "Gordita-Bold", fontSize: "22px", width: "15%" }}>Free</p>
                                            <p style={{ width: "15%", height: "5px", marginTop: "-24px", background: "#3722D3", borderRadius: "8px" }}></p>
                                            <p style={{ fontFamily: "Gordita", fontSize: "18px" }}>
                                                Learn and improve skills across business, tech, design, and more.
                                                Taught by experts to help you workforce do whatever comes next.
                                            </p>
                                        </Col>
                                    </Row>
                                </Col>
                                <Col xs={9} md={6}>
                                    <Form.Label style={{ color: "red" }}>{this.state.message}</Form.Label>
                                    <Row className="justify-content-center">
                                        <GoogleLogin
                                            clientId="944247106925-m1vaiosjtsdsv876ctg54qdpgfcvkpht.apps.googleusercontent.com"
                                            buttonText="Login with Google"
                                            onSuccess={this.responseGoogle}
                                            onFailure={this.responseGoogle}
                                            cookiePolicy={'single_host_origin'}
                                        />
                                    </Row>
                                    <p className="text-center">or</p>
                                    <Row className="justify-content-center">
                                        <Fragment>
                                            <Form>
                                                <Form.Row className="justify-content-center">
                                                    <Form.Group as={Col}>
                                                        <Form.Control placeholder="First Name" onChange={(event) => { this.setState({ first_name: event.target.value }) }} />
                                                    </Form.Group>

                                                    <Form.Group as={Col}>
                                                        <Form.Control placeholder="Last Name" onChange={(event) => { this.setState({ last_name: event.target.value }) }} />
                                                    </Form.Group>
                                                </Form.Row>
                                                <Form.Group controlId="formGridEmail">
                                                    <Form.Control type="email" placeholder="Enter email" onChange={(event) => { this.setState({ email: event.target.value }) }} />
                                                </Form.Group>
                                                <Form.Row className="justify-content-center">
                                                    <Form.Group as={Col} controlId="formGridPassword">
                                                        <Form.Control type="password" placeholder="Password" onChange={(event) => { this.setState({ password: event.target.value }) }} />
                                                    </Form.Group>

                                                    <Form.Group as={Col} controlId="formGridPassword">
                                                        <Form.Control type="password" placeholder="Confirm" onChange={(event) => { this.setState({ compass: event.target.value }) }} />
                                                    </Form.Group>
                                                </Form.Row>
                                                <Button variant="dark" onClick={() => { this.register() }} block>Sign Up</Button>
                                                <p className="text-center">Already Registered? <a onClick={this.onOpenLoginModal} className="blue-text" style={{ color: "#219ddb" }}>Login</a></p>
                                                {/* <Form.Group controlId="formBasicEmail">
                                                    <Form.Control type="email" placeholder="Enter email" onChange={(event) => { this.setState({ email: event.target.value }) }} required />
                                                </Form.Group>
                                                <Form.Group controlId="formBasicPassword">
                                                    <Form.Control type="password" placeholder="Password" onChange={(event) => { this.setState({ password: event.target.value }) }} required />
                                                </Form.Group>
                                                <Form.Group>
                                                    <a href="#" className="blue-text">forgot password?</a>
                                                </Form.Group>
                                                <Button variant="dark" onClick={() => { this.login() }} block>Log In</Button>
                                                <Form.Group>
                                                    <br></br>
                                                    <Form.Label>Not a member yet? <a href="#" onClick={this.onOpenSignModal} className="blue-text">Sign Up</a></Form.Label>
                                                </Form.Group> */}
                                            </Form>
                                        </Fragment>

                                    </Row>
                                </Col>
                            </Row>
                        </Container>
                    </Modal.Body>
                </Modal>

            <Footer />
            </Router >
        );
    }
}

export default ForgotPassword;
