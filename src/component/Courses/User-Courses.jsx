import React, { Component, Fragment } from 'react';
import { Container, Col, Row, Card, Button, Accordion, Image, Form, Table, Nav, useAccordionToggle, AccordionContext } from 'react-bootstrap';
import Navigation from "../Navigation/Navigation";
import Footer from '../Footer/Footer';
import { Link } from 'react-router-dom';
import ProgressBar from 'react-bootstrap/ProgressBar'
import Material from './Material';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import ListInstructor from './ListInstructor';
import ListReviews from '../Reviews/ListReviews';
import _ from "lodash";
import { Rate, message } from 'antd';

class UserCourses extends Component {

    state = {
        course: [],
        reviews: [],
        star: [],
        search: '',
        index: 0,
        hasil: 0,
        jumlah: [5, 4, 3, 2, 1]
    }

    
    componentDidMount(){
        this.listReviews();
        this.show();
    }

    show() {
        const token = localStorage.getItem('token');
        const id = this.props.match.params.id;
        this.setState({
            course: [],
            id: id,
        });
        fetch(`${process.env.REACT_APP_BASEURL}/api/read/course/${id}`, {
            method: "GET",
            headers: {
                "Authorization": `Bearer ${token}`,
                "Content-Type": "application/json"
            },
        })
            .then(response => response.json())
            .then(json => {
                if (json.sukses) {
                    this.setState({
                        course: json.rows
                    })
                }
            })
            .catch(error => console.log('error', error));
    }

    listReviews(){
        const id = this.props.match.params.id;
        const search = this.state.search;
        const token = localStorage.getItem('token');
        const next = this.state.index;
        fetch(`${process.env.REACT_APP_BASEURL}/api/rating/${id}?limit=2&offset=${next}&search=${search}`, {
            method: "GET",
            headers: {
                "Authorization": `Bearer ${token}`,
                "Content-Type": "application/json"
            },
        }).then((response) => {
            response.json()
                .then((result) => {
                    this.setState({
                        reviews: result.rows,
                        star: result.star,
                        avg: result.avg,
                        hasil: parseFloat(result.avg.average).toFixed(1)
                    });
    // console.log(this.state);
                })
        })
    }

    addToChart(){
        const token = localStorage.getItem('token');
        const id = this.props.match.params.id;
        console.log(this.state.course.id);
        fetch(`${process.env.REACT_APP_BASEURL}/api/cart`, {
            method: "POST",
            body: JSON.stringify({course_id: this.state.course.id}),
            headers: {
                "Authorization": `Bearer ${token}`,
                "Content-Type": "application/json"
            },
        }).then((response) => {
            response.json()
                .then((result) => {
                    console.warn("result", result);
                    if (result.sukses === true) {
                        this.props.history.push(`/DetailCourses/${id}`)
                        message.success("Berhasil ditambahkan", 1)
                    } else {
                        message.error(result.msg)
                    }
                })
        })
    }

    loadMore = async(index) => {
        const next = this.state.index + index;
        await this.setState({
            index: next
        });
        console.log(next);
        console.log(this.state.index);
        this.listReviews();
    }

    onChange = (event) => {
        /* signal to React not to nullify the event object */
        console.log('onChange triggered');
        event.persist();

        if (!this.debouncedFn) {
            this.debouncedFn = _.debounce(() => {
                //  let searchString = event.target.value;
                this.setState({ search: event.target.value });
                this.listReviews(1);
            }, 1000);
        }
        this.debouncedFn();
    }

    render() {
        return (
            <div>
                <Navigation history={this.props.history}/>
                <Container fluid style={{ width: "100%", padding: "50px", backgroundColor: "#F1F1F1" }}>

                    <Row style={{ width: "85%", margin: 'auto' }}>
                        <Col lg={8}>
                            <p style={{ fontFamily: "Gordita-Bold", fontSize: "18px" }}>{this.state.course.course_name}</p>
                            <p style={{  fontSize: "14px" }}>{this.state.course.description}</p>
                            <Row>
                                <Col lg={2}>
                                    <p style={{  fontSize: "15px", color: "#020288" }}><span style={{ color: "#020288", fontSize: "18px" }}><i className="fa fa-clock-o"></i></span> 1h 2m</p>
                                </Col>
                                <Col lg={3}>
                                    <p style={{  fontSize: "15px", color: "#020288" }}><span style={{ color: "#020288", fontSize: "18px" }}><i className="fa fa-star"></i></span> 3.8 Ratings</p>
                                </Col>
                                <Col lg={3}>
                                    <p style={{  fontSize: "15px", color: "#020288" }}><span style={{ color: "#020288", fontSize: "18px" }}><i className="fa fa-users"></i></span> 19000 students</p>
                                </Col>
                            </Row>
                        </Col>
                        <Col >
                            <Card>
                                <Card.Body style={{ padding: '1rem' }}>
                                <video controls width="100%" height="auto" src={this.state.course?.video?.url}/>
                                {
                                    this.state.course?.courses_user?.length > 0
                                    ? <Link to={`/ViewCourses/${this.state.course.id}`}><Button className="button-grape" block style={{ fontFamily: "Gordita-Bold" }} >See Courses</Button></Link>
                                    : <Button variant="danger" block style={{ fontFamily: "Gordita-Bold" }} onClick = { () => { this.addToChart() }} >Add to Chart</Button>

                                }
                                    <Button disabled variant="outline-secondary" block style={{ fontFamily: "Gordita-Bold", marginTop: "5%" }}>Buy now IDR {this.state.course.price}</Button>
                                </Card.Body>
                            </Card>
                        </Col>
                    </Row>
                    
                    <Row style={{ width: "85%", margin: 'auto' }}>
                        <Col>
                            <p style={{ fontFamily: "Gordita-Bold", fontSize: "24px", color: "#020288", }}>Instructor</p>
                            <ListInstructor data={this.state.course.instructors}/>
                        </Col>
                    </Row>
                    
                    <Row style={{ width: "85%", margin: 'auto', marginTop: '5%' }}>
                        <Col>
                            <p style={{ fontFamily: "Gordita-Bold", fontSize: "24px", color: "#020288", }}>Material</p>
                            <Card style={{ color: "white", background: "rgba(58, 44, 125, 0.34)", borderRadius: '0', boxShadow: 'none' }} >
                                <Card.Body>
                                    <Row className="justify-content-center" style={{ color: '#020202' }}>
                                        <Col xs={4} lg={4}>Topic</Col>
                                        <Col xs={4} lg={4}>Course Material</Col>
                                        <Col xs={4} lg={4}>Duration</Col>
                                    </Row>
                                </Card.Body>
                            </Card>
                            <Accordion defaultActiveKey="0">
                            {
                                this.state.course.topics?.map((list, index) => {
                                    return <Material
                                        id={list.id}
                                        key={list.id}
                                        topic={list}
                                        index={index}
                                        sub_topic={(list && list.topics && list.topics.sub_topic_name) ? list.topics.sub_topic_name : null}></Material>
                                })
                            }
                            </Accordion>
                        </Col>
                    </Row>
                    
                    <Row style={{ width: "85%", margin: 'auto', marginTop: '5%' }}>
                        <Col>
                            <p style={{ fontFamily: 'Gordita-bold', color: '#392C7E', fontSize: '18px' }}>How students rated this class</p>
                            <Card style={{ backgroundColor: '#F9F8F8', boxShadow: 'none', border: '0' }}>
                                <Card.Body>
                                    <Row className="justify-content-center">
                                        <Col style={{ textAlign: 'center' }} lg={4}>
                                            <p style={{ fontFamily: "Gordita-Bold", fontSize: "19px", color: "#020288", marginTop: "13%" }}>{this.state.hasil}</p>
                                            <Rate disabled defaultValue={5} />
                                            <p style={{ color: '#020288' }}>Course Rating</p>
                                            <p style={{ color: '#020288', lineHeight: '0.5' }}>(Based on {this.state.avg?.count_review} Reviews)</p>
                                        </Col>
                                        <Col lg={8}>
                                            <div style={{ paddingTop: "4%" }}>
                                                {   this.state.jumlah.map(a => {
                                                    return  <Row>
                                                    <Col xs={1} sm={1} md={1} lg={1} style={{ color: '#020288' }} ></Col>
                                                    <Col lg={9}style={{ marginLeft: '-6%', marginTop: '4px' }}><ProgressBar now={this.state.star.filter(z => z.star === a).map(e => {return e.count})} variant="warning" /><br/></Col>
                                                    <Col xs={2} sm={2} md={2} lg={2} style={{ marginLeft: '-3%', color: '#020288' }} >{a} <i className="fa fa-star" style={{ color: '#FF8A00' }} /></Col>
                                                </Row>
                                                })
                                                }
                                            </div>
                                        </Col>
                                    </Row>
                                </Card.Body>
                            </Card>
                        </Col>
                    </Row>
                    
                    <Row style={{ width: "85%", margin: 'auto', marginTop: '5%' }}>
                        <Col>
                            <p style={{ fontFamily: "Gordita-Bold", fontSize: "24px", color: "#020288" }}>Reviews</p>
                            <Form >
                                <Form.Group>
                                    <Row>
                                        <Col lg={6}>
                                            <Form.Control type="text" placeholder="Search reviews" onChange={this.onChange} debounce="1000" style={{ backgroundColor: "#F1F1F1" }} />
                                            <span className="icon"><i ></i></span>
                                        </Col>
                                        <Col lg={6}>
                                            <Form.Control as="select" style={{ backgroundColor: "#F1F1F1" }} onChange={(e) => {this.dropRate(e.target.value)}} >
                                                <option value="0">All Ratings</option>
                                                {
                                                    this.state.star.map((list) => {
                                                        return <option value={list.star} >Bintang {list.star}</option>
                                                    })
                                                }
                                            </Form.Control>
                                        </Col>
                                    </Row>
                                </Form.Group>
                            </Form>
                        </Col>
                    </Row>

                    <Row style={{ width: "85%", margin: 'auto' }}>
                        <Col>
                        {
                            this.state.reviews.map((list) => {
                                return <ListReviews id={list.id} key={list.id} list={list} />
                            })
                        }
                        </Col>
                    </Row>
                    
                    <Row style={{ width: "18%", margin: 'auto', marginTop: '3%' }}>
                        <Button onClick={ () => { this.loadMore(2) } } className="button-grape" block >Load more reviews</Button>
                    </Row>
                    
                </Container>
                <Footer />
            </div >
        )
    }
}

export default UserCourses;