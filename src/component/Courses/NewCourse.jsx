import React, { Component } from 'react';
import Navigation from '../Navigation/Navigation';
import { Form, Nav, Navbar, NavDropdown, Container, Row, Col, CardDeck } from 'react-bootstrap';
import Footer from '../Footer/Footer';
import { Link } from 'react-router-dom';
import NewCourseList from './NewCourseList';
import Login from '../Login';
import _ from "lodash";

class NewCourses extends Component{

    state = {
        course: [],
        id: 0,
        category_name: '',
        categories: [],
        modalLoginIsOpen: false,
        coba: [],
        search: '',
    }

    Logo = 'https://galilagi-media.oss-ap-southeast-5.aliyuncs.com/assets/logo.png';

    onOpenLoginModal = () => {
      this.setState({ modalLoginIsOpen: true, modalSignIsOpen: false, message: null });
  };

    componentDidMount(){
      // console.log(this.props);
      //   const id = this.props.match.params.id;
      //   const cat = JSON.parse(localStorage.getItem('categories'));
      //   cat.map(a => {
      //     if (a.id === parseInt(id)) {
      //       this.setState({category_name: a.category_name})
      //     }
      //   })
      //   this.listCourse(id);
        this.listcat();
        this.Courses();
    }

    componentDidUpdate(prevProps){
        const id = this.props.match.params.id;
        const cat = JSON.parse(localStorage.getItem('categories'));
        if (prevProps.match.params.id !== id) {
          cat.map(a => {
            if (a.id === parseInt(id)) {
              this.setState({category_name: a.category_name})
            }
          })
          this.listCourse(id);
        }
    }

    listcat(){
      const token = localStorage.getItem("token");
      fetch(`${process.env.REACT_APP_BASEURL}/api/dashboard/category/special/offer`, {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      })
        .then((response) => response.json())
        .then((json) => {
          // var coba = this.state.coba.concat({cat: a.id ,course: json.data});
          this.setState({
            categories: json.data
          });
          console.log(this.state.categories);
          this.Courses(this.state.categories)
          // console.log(json);
        })
        .catch((error) => console.log("error", error));
    }

    Courses() {
              
      const token = localStorage.getItem("token");
      const search = this.state.search;
      const id = this.state.categories.map(a => {
        fetch(`${process.env.REACT_APP_BASEURL}/api/dashboard/new/course/${a.id}?limit=4&offset=0&search=${search}`, {
          method: "GET",
          headers: {
            Authorization: `Bearer ${token}`,
            "Content-Type": "application/json",
          },
        })
          .then((response) => response.json())
          .then((json) => {
            const temp = [];
              const diskon = json.data?.courses?.map((disc)=>{
                const price = parseInt(disc.price);
                const dic = parseInt(disc.discount);
                const jum = (dic/100)*price;
                const tot = price-parseInt(jum);
                temp.push({
                    ...disc,
                    hargadisc: tot,
                    course_name: disc.course_name,
                    cover: disc.cover,
                    courses_user: disc.courses_user,
                    description: disc.description,
                    discount: disc.discount,
                    id: disc.id,
                    instructors: disc.instructors,
                    price: disc.price,
                    rates: disc.rates,
                    total_duration: disc.total_duration,
                    total_user: disc.total_user
                })
                return tot
              });
              var coba = this.state.coba.concat({cat: a.category_name, id: a.id ,course: temp, sum: a.count_course});
            this.setState({
              coba: coba
            });
            console.log(this.state.coba);
          })
          .catch((error) => console.log("error", error));
        })
    }

      changeURL(url) {
        // this.setState({ redirect: url });
        this.props.history.push(url);
    }

    onChange = (event) => {
      console.log('onChange triggered');
      event.persist();

      if (!this.debouncedFn) {
          this.debouncedFn = _.debounce(() => {
              this.setState({ search: event.target.value });
              this.Courses();
          }, 1000);
      }
      this.debouncedFn();
  }

      render(){
        return(
            <>
            {
                (localStorage.getItem('token'))
                ?<Navigation history={this.props.history} selected={this.state.category_name}/>
                :   <>
                        <Navbar expand="lg">
                        <a className="logo" href="/"><img className="img-responsive logo" src={this.Logo} alt="" /></a>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                        <Navbar.Collapse id="basic-navbar-nav">
                            <Nav className="mr-auto">
                                <NavDropdown title="Category" id="basic-nav-dropdown" color="#6831B0" className="dropdown">
                                    <NavDropdown.Item href="#action/3.2">
                                        IT & Software
                                </NavDropdown.Item>
                                    <NavDropdown.Item href="#action/3.3">
                                        Design
                                </NavDropdown.Item>
                                    <NavDropdown.Item href="#action/3.4">
                                        Business
                                </NavDropdown.Item>
                                </NavDropdown>
                            </Nav>

                            <Form inline>
                                <div>
                                    <input type="search" className="srch" placeholder="Search for anything" />
                                    <span className="icon"><i className="fa fa-search"></i></span>
                                </div>
                                <a className="btn" href="/ShoppingChart" >Shopping Chart</a>
                                <a id="login" className="btn" onClick={this.onOpenLoginModal}>Login</a>
                                {(this.state.modalLoginIsOpen)? <Login redirect={(e)=>{this.props.history.push(e)}} state={this.state} modalLoginIsOpen={this.state.modalLoginIsOpen} redirect={(e)=>{this.props.history.push(e)}} onClose={() =>{this.setState({modalLoginIsOpen: false})}} />: null}
                                <a id="signup" className="btn" onClick={this.onOpenSignModal}>Sign Up</a>
                            </Form>
                            {/* <Login /> */}
                        </Navbar.Collapse>
                    </Navbar>
                    </>
            }
            <Container fluid style={{ width: "100%", padding: '50px', backgroundColor: "#F1F1F1" }}>
              <Row style={{ width: '85%', margin: 'auto' }}>
                    <Col>
                        <Navbar expand="lg" style={{ background: "rgba(58, 44, 125, 0.34)", marginBottom: "3%" }}>
                        <Link to={`/`}>
                          <button className="btn icon-back">
                            <span style={{ color: "#FFFFFF" }} className="fa fa-chevron-left"></span>
                          </button>
                        </Link>
                        <Navbar.Brand style={{ color: "#FFFFFF", fontFamily: "Gordita-bold" }}>All Categories</Navbar.Brand>
                            <Navbar.Toggle aria-controls="basic-navbar-nav" />
                            <Navbar.Collapse id="basic-navbar-nav">
                            <Nav className="mr-auto" />
                                <Form inline>
                                    <Form.Control type="text" id="search" placeholder="Search" style={{ borderRadius: "30px" }} onChange={this.onChange} debounce="1000" />
                                </Form>
                            </Navbar.Collapse>
                        </Navbar>
                    </Col>
                </Row>
                
                <Row style={{ width: '80%', margin: 'auto', marginTop: '-3%' }}>
                    {
                        this.state.coba?.map((list) => {
                          return <>
                            <Col lg={12}>
                              <Row className="justify-content-center">
                                <Col xl={11} sm={11} md={11} lg={11} ><h5 style={{ fontFamily: 'Gordita-bold', marginTop: '6%' }} >{list.cat}({list.sum})</h5></Col>
                                <Col style={{ marginTop: '5%' }} ><Link to={`/NewCourseCategory/${list.id}`} className="btn btn-outline-secondary" >View</Link></Col>
                              </Row>
                            </Col>
                            {list?.course?.map((test) =>{
                            return <Col style={{ paddingBottom: '3%' }} xs={12} sm={6} md={4} lg={3} xl={3}><NewCourseList id={test.id} key={test.id} list={test} history={((url) => { this.changeURL(url) })}></NewCourseList></Col>
                          })}
                          </>
                        })
                    }
               </Row>
            </Container>
            <Footer/>
            </>
        );
    }

}

export default NewCourses;