import React, { Component } from 'react';
import { Container, Row, Col, Form } from 'react-bootstrap';
import Footer from "../Footer/Footer";
import Navigation from "../Navigation/Navigation";
import { Link } from 'react-router-dom';
import _ from "lodash";
import List from './List';
import Instructor from './Instructor';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Tabs } from 'antd';
import Transaction from '../Transaction/Transaction';
import { checkRole, pageToLimitOffset } from '../Utils';
import Paginate from '../../components/Paginate/paginate';
const { TabPane } = Tabs;

class MyCourses extends Component{

    constructor(props) {
        super(props);

        this.state = {
          course: [],
          search: '',
          instructor_id: null,
          is_started: true,
          is_not_started: false,
          is_on_progress: false,
          is_completed: false,
          loading: false,
          pageNow: 1,
          count: 0
        };

    }

    componentDidMount(){
        const page = this.props.match.params.page ? this.props.match.params.page : 1;
        this.listCourses(page);
        console.log(this.props);
        console.log(`page cdm`, page);
    }

    listCourses(page) {
        console.log(`page on list course`, page);
        const token = localStorage.getItem("token");
        const search = this.state.search;
        this.setState({ loading: true });
        const { limit, offset } = pageToLimitOffset(page);
        const body = {
            limit: limit,
            offset: offset,
            instructor_id: this.state.instructor_id,
            search,
        }
        if (this.state.is_not_started ) {
            body.is_started = 0;
        } else
        if (this.state.is_on_progress) {
            body.is_on_progress = 1;
        } else
        if (this.state.is_completed) {
            body.is_completed = 1;
        }
        fetch(`${process.env.REACT_APP_BASEURL}/api/course/instructors/user`, {
            method: 'POST',
            headers: {
                "Authorization": `Bearer ${token}`,
                "Content-Type": "application/json"
            },
            body: JSON.stringify(body)
        })
          .then((response) => response.json())
          .then((json) => {
            this.setState({
              course: json.rows,
              loading: false,
              pageNow: page,
              count: json.count
            });
            // const array = [];
            console.log(JSON.stringify(json));
            console.log(json.count);
            console.log(`state `, this.state.count);
          })
          .catch((error) => console.log("error", error));
    }

    onChange = (event) => {
        event.persist();
        if (!this.debouncedFn) {
          this.debouncedFn = _.debounce(() => {
            this.setState({ search: event.target.value });
            this.listCourses(this.state.pageNow);
          }, 1000);
        }
        this.debouncedFn();
      };
    
    addInstructor = async(id) => {
        console.log(id);
        await this.setState({instructor_id: id})
        this.listCourses(this.state.pageNow)
    }

    checkOnlyOneBox = (async(obj, e) => {
        e.persist();
        const arr = ['is_not_started', 'is_on_progress', 'is_completed'];
        console.log(obj);
        console.log(this.state[obj])
        const arr2 = arr.map(a => {
            if (a === obj) {
                return !this.state[a];
            } else {
                return this.state[a]
            }
        });
        console.log('arr2', arr2)
        let arr3 = arr2;
        const index = arr.findIndex(a => a === obj);
        console.log(index);
        if (arr2[index] === true) { 
            arr3 = arr.map(a => {
                if (a === obj) {
                    return true;
                } else {
                    return false;
                }
            })
            console.log(arr3);
        }
        console.log(arr3);
        if (arr3.includes(true)){
            console.log('ada centang');
        } else {
            console.log('tidak ada centang');
        }
        await this.setState({
            is_not_started: arr3[0],
            is_started: !arr3[0],
            is_on_progress: arr3[1],
            is_completed: arr3[2]
        });
        this.listCourses(this.state.pageNow)
    });

    ChangePage(page) {
        console.log(`Change page to: ${page}`);
        this.listCourses(page);
    }
    
    render(){
        return(
            <>
            <Navigation history={this.props.history}/>
            <Container fluid style={{ width: "100%", minHeight: "600px", padding: "30px", backgroundColor: "#F1F1F1" }}>
            <ToastContainer/>

                <Row className="justify-content-center">
                    <Col md={10}><p style={{ color: '#6831B0', fontSize: '18px', fontFamily: 'Gordita-bold' }}>My Courses</p></Col>
                </Row>

                <Row style={{ width: '85%', margin: 'auto' }}>
                    <Col>
                        <Tabs defaultActiveKey="1">
                            <TabPane tab="All Courses" key="1">
                                <Row>
                                    <Col md={4}>
                                        <Instructor addInstructor={ (e) => {this.addInstructor(e)}} />
                                    </Col>
                                    <Col md={5}>
                                        <Form.Group>
                                            <Form.Label style={{ color: '#6831B0' }}>STATUS</Form.Label>
                                            {['checkbox'].map((type) => (
                                                <div key={`inline-${type}`} className="mb-3">
                                                <Form.Check inline style={{ color: '#6831B0' }} label="Not started" type={type} id={`inline-${type}-1`}
                                                    onChange={async(e)=> { this.checkOnlyOneBox('is_not_started',e); }}
                                                    checked={this.state.is_not_started}
                                                    />
                                                <Form.Check inline style={{ color: '#6831B0' }} label="In progress" type={type} id={`inline-${type}-2`}
                                                    onChange={async(e)=> {
                                                        this.checkOnlyOneBox('is_on_progress',e);
                                                    }} checked={this.state.is_on_progress}/>
                                                <Form.Check inline style={{ color: '#6831B0' }} label="Completed" type={type} id={`inline-${type}-3`}
                                                    onChange={async(e)=> {
                                                        this.checkOnlyOneBox('is_completed',e);
                                                    }} checked={this.state.is_completed}/>
                                                </div>
                                            ))}
                                        </Form.Group>
                                    </Col>
                                    <Col md={3}>
                                        <Form.Group>
                                            <Form.Label/>
                                            <Form.Control type="text" id="search" onChange={this.onChange} debounce="1000" placeholder="Search"/>
                                        </Form.Group>
                                    </Col>
                                </Row>
                                
                                {
                                    this.state.course.map((course) => {
                                        return <List id={course.id} key={course.id} course={course.course} ></List>
                                    })
                                }
                                
                                <Paginate className="paginate-user" pageNow={this.state.pageNow} count={this.state.count}
                                    changePage={(page) => this.ChangePage(page)}>
                                </Paginate>
                            </TabPane>
                            <TabPane tab="Transaction"  key="2">
                                <Transaction/>
                            </TabPane>
                        </Tabs> 
                    </Col>
                </Row>

            </Container>
            <Footer/>
            </>
        );
    }

}

export default MyCourses;