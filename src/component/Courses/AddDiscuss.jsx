import React, { Component} from 'react';
import { Comment, Avatar, Form, Button, BackTop, Input } from 'antd';
import Moment from 'moment';

const { TextArea } = Input;

class AddDiscuss extends Component {

    constructor(props) {
        super(props);
        
        this.state = {
            comments: [],
            submitting: false,
            value: '',
            reply: '',
            balas: false,
            id: 0
          };

    }

    componentDidMount(){
        this.tampil();
    }

    tampil(){
        const id = this.props.state.course.courseId;
        const token = localStorage.getItem('token');
        fetch(`${process.env.REACT_APP_BASEURL}/api/discussion/${id}`, {
            method: "GET",
            headers: {
                "Authorization": `Bearer ${token}`,
                "Content-Type": "application/json"
            },
        }).then((response) => {
            response.json()
                .then((result) => {
                    this.setState({comments: result.data});
                })
        })
    }

    replayDiscuss(){
        const body = {
            text: this.state.reply,
            discussion_id: this.state.balasId
        }

        const token = localStorage.getItem('token');
        fetch(`${process.env.REACT_APP_BASEURL}/api/discussion/reply`, {
            method: "POST",
            body: JSON.stringify(body),
            headers: {
                "Authorization": `Bearer ${token}`,
                "Content-Type": "application/json"
            },
        }).then((response) => {
            response.json()
                .then((result) => {
                    console.log(result);
                    this.setState({
                        submitting: false,
                        reply: '',
                        button: false
                      });
                    this.tampil();
                })
        })
    }

    handleSubmit = () => {
        
        if (!this.state.value) {
            return;
        }

        this.setState({
            submitting: true,
        });

        const body = {
            text: this.state.value,
            course_id: this.props.state.course.courseId
        }

        const token = localStorage.getItem('token');
        fetch(`${process.env.REACT_APP_BASEURL}/api/discussion`, {
            method: "POST",
            body: JSON.stringify(body),
            headers: {
                "Authorization": `Bearer ${token}`,
                "Content-Type": "application/json"
            },
        }).then((response) => {
            response.json()
                .then((result) => {
                    this.setState({
                        submitting: false,
                        value: '',
                        button: false
                      });
                    this.tampil();
                })
        })

      };
      
    handleChange = e => {
        this.setState({
          value: e.target.value,
        });
    };

    handleReply = e => {
        this.setState({
          reply: e.target.value,
        });
    };

    likeDiscuss(id){    

        const token = localStorage.getItem('token');
        fetch(`${process.env.REACT_APP_BASEURL}/api/discussion/like`, {
            method: "POST",
            body: JSON.stringify({
                discussion_id: id
            }),
            headers: {
                "Authorization": `Bearer ${token}`,
                "Content-Type": "application/json"
            },
        }).then((response) => {
            response.json()
                .then((result) => {
                    console.log(result);
                    this.tampil();
                })
        })
    }

    likeReply(id){    

        const token = localStorage.getItem('token');
        fetch(`${process.env.REACT_APP_BASEURL}/api/discussion/reply/like`, {
            method: "POST",
            body: JSON.stringify({
                reply_id: id
            }),
            headers: {
                "Authorization": `Bearer ${token}`,
                "Content-Type": "application/json"
            },
        }).then((response) => {
            response.json()
                .then((result) => {
                    console.log(result);
                    // this.setState({
                    //     resultLike: 1
                    // })
                    this.tampil();
                })
        })
    }
    
    render(){

        const { submitting, value, reply } = this.state;

        const style = {
            height: 40,
            width: 40,
            lineHeight: '40px',
            borderRadius: 4,
            backgroundColor: '#3B2881',
            color: '#fff',
            textAlign: 'center',
            fontSize: 14,
          };

        return(
            <>
                <Comment style={{ backgroundColor: '#f1f1f1', borderRadius: '5px' }}
                
                    avatar = { <Avatar src={this.props.state.user?.gambarr?.url} /> }

                    content = {
                        <Form>
                            <Form.Item>
                                <TextArea onClick={ () => { this.setState({ button: true }) } } placeholder="Start discussion ..." rows={2} onChange={this.handleChange} value={value} />
                            </Form.Item>
                            <Form.Item style={{ float: "right" }} >
                                <Button hidden={ this.state.value === '' ? !this.state.button : false } type="link" htmlType="submit" onClick={ () => { this.setState({ button: false }) }} style={{ marginLeft: '-3%', color: 'grey' }} >Cancel</Button>
                                <Button hidden={ this.state.value === '' ? !this.state.button : false } htmlType="submit" loading={submitting} onClick={this.handleSubmit} type="primary" style={{ backgroundColor: '#3B2881', border: '0',  }}>Post</Button>
                            </Form.Item>
                        </Form>
                    }
                />                
                
                <div style={{ padding: 10 }}>

                {this.state.comments.map((disc) => {
                    return <>
                    <Comment
                        actions={[
                            <span  style={{ fontFamily: 'Gordita-bold' }} onClick={ () => { this.likeDiscuss(disc.id) } } >{disc.count_like} Like</span>,
                            <span  style={{ fontFamily: 'Gordita-bold'}} onClick={ () => { this.setState({ balas: true, balasId: disc.id }) } } >Reply</span>,
                            <span>Last activity {Moment(disc.createdAt).endOf('hour').fromNow()}</span>
                        ]}

                        author={
                            <p style={{ fontFamily: 'Gordita-bold', color: '#020288', fontSize: '16px' }}>{disc.user?.first_name}<br/><i style={{ fontFamily: 'Gordita', fontSize: '12px' }}>{disc.user?.job_title}</i></p>
                        }

                        avatar={ 
                            <Avatar src={disc.user?.gambarr?.url} />
                        }

                        content={
                            <p style={{ color: 'black' }}>{disc.text}</p>
                        }
                    />

                    {disc.reply_discussions.map((reply) => {
                        return <Comment style={{ marginLeft: '5%', backgroundColor: '#F1f1f1', borderRadius: '5px', marginTop: '2px'}}
                        actions={[
                            <span style={{ fontFamily: 'Gordita-bold' }} onClick={ () => { this.likeReply(reply.id) } } >{reply.count_like} Like</span>,
                            <span>Last activity {Moment(disc.createdAt).endOf('hour').fromNow()}</span>
                        ]}

                        author={
                            <p style={{ fontFamily: 'Gordita-bold', color: '#020288', fontSize: '16px' }}>{reply.user?.first_name}<br/><i style={{ fontFamily: 'Gordita', fontSize: '12px' }}>{reply.user?.job_title}</i></p>
                        }

                        avatar={ 
                            <Avatar src={reply.user?.gambarr?.url} />
                        }

                        content={
                            <p style={{ color: 'black' }}>{reply.text}</p>
                        }
                    />
                    })}

                    {disc.id === this.state.balasId &&
                    
                    <Comment hidden={ (this.state.balas === false) ?true :false } style={{ marginLeft: '5%', backgroundColor: '#F1f1f1', borderRadius: '5px', marginTop: '2px' }}
                
                        avatar = { <Avatar src={this.props.state.user?.gambarr?.url} /> }

                        content = {
                            <Form>
                                <Form.Item>
                                    <TextArea placeholder="Leave a reply ..." onChange={this.handleReply} rows={2} value={reply} />
                                </Form.Item>
                                <Form.Item style={{ float: "right" }} >
                                    <Button type="link" htmlType="submit" onClick={ () => { this.setState({ balas: false }) }} style={{ marginLeft: '-3%', color: 'grey' }} >Cancel</Button>
                                    <Button htmlType="submit" loading={submitting} onClick={ () => { this.replayDiscuss() } } type="primary" style={{ backgroundColor: '#3B2881', border: '0',  }}>Reply</Button>
                                </Form.Item>
                            </Form>
                        }
                    />  

                    }
                    

                    </>                 
                })}

                <BackTop>
                    <div style={style}><i className="fa fa-arrow-up"></i></div>
                </BackTop>

                </div>

            </>
        )
    }

}

export default AddDiscuss;