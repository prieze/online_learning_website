import React, { Component } from 'react';
import Navigation from '../Navigation/Navigation';
import { Form, Nav, Navbar, NavDropdown, Container, Row, Col, Button } from 'react-bootstrap';
import Footer from '../Footer/Footer';
import BestSellingList from './BestSellingList';
import Login from '../Login';
import { Link } from 'react-router-dom';

class BestSelling extends Component{

    constructor(props) {
      super(props);

      this.state = {
        course: [],
        courses: [],
        coba: [],
        categories: [],
        id: 0,
        category_name: '',
        modalLoginIsOpen: false,
        list: [4],
      };
      console.log(this.state);
      console.log(props);
      console.log(JSON.parse(localStorage.getItem("categories")));
  }

    componentDidMount(){
        // const id = this.props.match.params.id;
        // const cat = JSON.parse(localStorage.getItem('categories'));
        // cat.map(a => {
        //   if (a.id === parseInt(id)) {
        //     this.setState({category_name: a.category_name})
        //   }
        // })
        // this.listCourse();
        // this.listCategory();
        this.listcat();
        this.Courses();
        console.log(this.state);
        // console.log();
    }

    // componentDidUpdate(prevProps){
    //     const id = this.props.match.params.id;
    //     const cat = JSON.parse(localStorage.getItem('categories'));
    //     if (prevProps.match.params.id !== id) {
    //       cat.map(a => {
    //         if (a.id === parseInt(id)) {
    //           this.setState({category_name: a.category_name})
    //         }
    //       })
    //       this.listCourse(id);
    //     }
    // }

    // listCategory() {
    //   const token = localStorage.getItem('token');
    //   fetch(`${process.env.REACT_APP_BASEURL}/api/read/categories?limit=6&offset=0&search=`, {
    //     method: 'GET',
    //     headers: {
    //       "Authorization": `Bearer ${token}`,
    //       "Content-Type": "application/json"
    //     },
    //   })
    //     .then(response => response.json())
    //     .then(json => {
    //       this.setState({
    //         categories: json.rows
    //       })
    //       // localStorage.setItem('categories', JSON.stringify(json.rows))
    //       console.log(this.state.categories);
    //     })
    //     .catch(error => console.log('error', error));
    // }

    listcat(){
      const token = localStorage.getItem("token");
      fetch(`${process.env.REACT_APP_BASEURL}/api/dashboard/category/special/offer`, {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      })
        .then((response) => response.json())
        .then((json) => {
          // var coba = this.state.coba.concat({cat: a.id ,course: json.data});
          this.setState({
            categories: json.data
          });
          console.log(this.state.categories);
          this.Courses(this.state.categories)
          // console.log(json);
        })
        .catch((error) => console.log("error", error));
    }

      Courses() {
              
        const token = localStorage.getItem("token");
        console.log(this.state.categories);
        const id = this.state.categories.map(a => {
          fetch(`${process.env.REACT_APP_BASEURL}/api/dashboard/best/selling/${a.id}?limit=4&offset=0&search=`, {
            method: "GET",
            headers: {
              Authorization: `Bearer ${token}`,
              "Content-Type": "application/json",
            },
          })
            .then((response) => response.json())
            .then((json) => {
              const temp = [];
              const diskon = json.data?.courses?.map((disc)=>{
                const price = parseInt(disc.price);
                const dic = parseInt(disc.discount);
                const jum = (dic/100)*price;
                const tot = price-parseInt(jum);
                temp.push({
                    ...disc,
                    hargadisc: tot,
                    course_name: disc.course_name,
                    cover: disc.cover,
                    courses_user: disc.courses_user,
                    description: disc.description,
                    discount: disc.discount,
                    id: disc.id,
                    instructors: disc.instructors,
                    price: disc.price,
                    rates: disc.rates,
                    total_duration: disc.total_duration,
                    total_user: disc.total_user
                })
                return tot
              });
              console.log(diskon);
              console.log(temp);
              var coba = this.state.coba.concat({cat: a.category_name, id: a.id ,course: temp, sum: a.count_course});
              this.setState({
                coba: coba
              });
              console.log(this.state.coba);
            })
            .catch((error) => console.log("error", error));
          })
      }

      changeURL(url) {
        // this.setState({ redirect: url });
        this.props.history.push(url);
    }

      render(){
        return(
            <>
            {
                (localStorage.getItem('token'))
                ?<Navigation history={this.props.history} selected={this.state.category_name}/>
                :   <>
                        <Navbar expand="lg">
                        <a className="logo" href="/"><img className="img-responsive logo" src={this.Logo} alt="" /></a>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                        <Navbar.Collapse id="basic-navbar-nav">
                            <Nav className="mr-auto">
                                <NavDropdown title="Category" id="basic-nav-dropdown" color="#6831B0" className="dropdown">
                                    <NavDropdown.Item href="#action/3.2">
                                        IT & Software
                                </NavDropdown.Item>
                                    <NavDropdown.Item href="#action/3.3">
                                        Design
                                </NavDropdown.Item>
                                    <NavDropdown.Item href="#action/3.4">
                                        Business
                                </NavDropdown.Item>
                                </NavDropdown>
                            </Nav>

                            <Form inline>
                                <div>
                                    <input type="search" className="srch" placeholder="Search for anything" />
                                    <span className="icon"><i className="fa fa-search"></i></span>
                                </div>
                                <a className="btn" href="/ShoppingChart" >Shopping Chart</a>
                                <a id="login" className="btn" onClick={this.onOpenLoginModal}>Login</a>
                                {(this.state.modalLoginIsOpen)? <Login redirect={(e)=>{this.props.history.push(e)}} state={this.state} modalLoginIsOpen={this.state.modalLoginIsOpen} redirect={(e)=>{this.props.history.push(e)}} onClose={() =>{this.setState({modalLoginIsOpen: false})}} />: null}
                                <a id="signup" className="btn" onClick={this.onOpenSignModal}>Sign Up</a>
                            </Form>
                            {/* <Login /> */}
                        </Navbar.Collapse>
                    </Navbar>
                    </>
            }
            <Container fluid style={{ width: "100%", padding: '50px', backgroundColor: "#F1F1F1" }}>
              <Row style={{ width: '85%', margin: 'auto' }}>
                    <Col>
                        <Navbar expand="lg" style={{ background: "rgba(58, 44, 125, 0.34)", marginBottom: "3%" }}>
                        <Link to={`/`}>
                          <button className="btn icon-back">
                            <span style={{ color: "#FFFFFF" }} className="fa fa-chevron-left"></span>
                          </button>
                        </Link>
                        <Navbar.Brand style={{ color: "#FFFFFF", fontFamily: "Gordita-bold" }}>All Categories</Navbar.Brand>
                            <Navbar.Toggle aria-controls="basic-navbar-nav" />
                            <Navbar.Collapse id="basic-navbar-nav">
                            <Nav className="mr-auto" />
                                <Form inline>
                                    <Form.Control type="text" id="search" placeholder="Search" style={{ borderRadius: "30px" }} onChange={this.onChange} debounce="1000" />
                                </Form>
                            </Navbar.Collapse>
                        </Navbar>
                    </Col>
                </Row>
                
                <Row style={{ width: '80%', margin: 'auto' }}>
                    {
                        this.state.coba?.map((list) => {
                          // console.log(list);
                          return <>
                            <Col lg={12}>
                              <Row className="justify-content-center">
                                <Col lg={11} ><h5 style={{ fontFamily: 'Gordita-bold', marginTop: '6%' }} >{list.cat}({list.sum})</h5></Col>
                                <Col style={{ marginTop: '5%' }} ><Link to={`/BestSellingCategory/${list.id}`} className="btn btn-outline-secondary" >View</Link></Col>
                              </Row>
                            </Col>
                            {
                              list.course?.map((e) => {
                                return <Col style={{ paddingBottom: '3%' }} xs={12} sm={6} md={4} lg={3} xl={3}><BestSellingList id={e.id} key={e.id} test={e} history={((url) => { this.changeURL(url) })}></BestSellingList></Col>
                              })
                            }
                          </>
                        })
                    }
               </Row>
            </Container>
            <Footer/>
            </>
        );
    }

}

export default BestSelling;