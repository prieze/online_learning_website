import React from 'react';
import { Row, Col, Card, Image, Button } from 'react-bootstrap';
import Shop from "../../gambar/shop.png";
import { useHistory } from "react-router-dom";
import { message } from 'antd';

const Read = (props) => {
    console.log(props);
    
    let history = useHistory();

    const addToChart = (async (id) => {
        const token = localStorage.getItem('token');
        fetch(`${process.env.REACT_APP_BASEURL}/api/cart`, {
            method: "POST",
            body: JSON.stringify({course_id: id}),
            headers: {
                "Authorization": `Bearer ${token}`,
                "Content-Type": "application/json"
            },
        }).then((response) => {
            response.json()
                .then((result) => {
                    console.warn("result", result);
                    if (result.sukses) {
                        console.log(props);
                        if(props.reloadData) {
                            props.reloadData();
                        }
                        message.success("Berhasil ditambahkan", 1)
                    } else {
                        message.error(result.msg, 1)
                    }
                })
        })
    })
    
    return(
        <Card className="h-100" style={{ width: "14rem", margin: 'auto', borderRadius: "12px", marginTop: '40px'}}>
            <Card.Img variant="top" style={{ height: '150px', borderRadius: '12px 12px 0 0' }} src={props.list?.cover?.url}/>
            <p hidden={ props.list.discount > "0" ? false : true } className="diskon">{props.list.discount}% Off</p>
            <Card.Body>
                <Card.Title style={{ cursor: 'pointer', fontFamily: 'Gordita-bold', fontSize: '15px', color: '#002333' }} onClick={ () => { props.history(`/DetailCourses/${props.list.id}`) }}>{props.list.course_name}</Card.Title>
                <p style={{ fontSize: '10px', color: 'rgba(0, 35, 51, 0.67)' }}><i className="fa fa-users"></i> {props.list.total_user} <i className="fa fa-star"></i> {parseFloat(props.list.rates).toFixed(1)}</p>
                <Row>
                    {props.list?.courses_user?.length > 0
                        ? null
                        : <>
                        <Col lg={12}>
                            <Row>
                                <Col><Card.Text style={{ fontSize: '10px', color: '#BAB9B9' }}><s>IDR {props.list.price}</s></Card.Text></Col>
                                <Col><Card.Text style={{ fontSize: '10px', color: '#BAB9B9', float: 'right' }}>{props.list.total_duration}</Card.Text></Col>
                            </Row>
                        </Col>
                        <Col>
                            <Card.Text style={{ color: '#3722D3', fontSize: '15px', fontFamily: 'Gordita-bold' }}>IDR {parseInt(props.list.hargadisc).toLocaleString("id-ID")}</Card.Text>
                        </Col>
                    </>
                    }
                </Row>
            </Card.Body>
            <Card.Footer>
                {props.list?.courses_user?.length > 0 
                    ? <Button onClick={()=> { props.history(`/ViewCourses/${props.list.id}`); }} size="sm" className="button-grape" style={{ borderRadius: '0', backgroundColor: 'rgba(165, 148, 255, 0.78)' }} block>See Course</Button>
                    : <Image onClick={()=> { addToChart(props.list.id) }} style={{ cursor: 'pointer', marginBottom: '-35px', boxShadow: '4px 4px 20px rgba(0, 0, 0, 0.2)', width: '60px', marginLeft: '65%' }} className="shop" src={Shop} roundedCircle />
                }
            </Card.Footer>
        </Card>
    );

}

export default Read;