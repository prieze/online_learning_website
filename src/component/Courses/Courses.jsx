import React, { Component } from 'react';
import Navigation from '../Navigation/Navigation';
import { Form, Nav, Navbar, NavDropdown, Container, Row, Col, CardDeck } from 'react-bootstrap';
import Footer from '../Footer/Footer';
import Listing from './Listing';
import Login from '../Login';

class Courses extends Component{

    state = {
        course: [],
        id: 0,
        category_name: '',
        modalLoginIsOpen: false
    }

    Logo = 'https://galilagi-media.oss-ap-southeast-5.aliyuncs.com/assets/logo.png';

    onOpenLoginModal = () => {
      this.setState({ modalLoginIsOpen: true, modalSignIsOpen: false, message: null });
  };

    componentDidMount(){
        const id = this.props.match.params.id;
        const cat = JSON.parse(localStorage.getItem('categories'));
        cat.map(a => {
          if (a.id === parseInt(id)) {
            this.setState({category_name: a.category_name})
          }
        })
        this.listCourse(id);
    }

    componentDidUpdate(prevProps){
      const id = this.props.match.params.id;
      const cat = JSON.parse(localStorage.getItem('categories'));
      if (prevProps.match.params.id !== id) {
        cat.map(a => {
          if (a.id === parseInt(id)) {
            this.setState({category_name: a.category_name})
          }
        })
        this.listCourse(id);
      }
    }

    listCourse(id = "0") {

        const token = localStorage.getItem("token");
        this.setState({ loading: true });
        let url = `${process.env.REACT_APP_BASEURL}/api/read/courses?limit=16&offset=0&search=`;

        if(id !== "0"){
          url = `${process.env.REACT_APP_BASEURL}/api/read/course/category/${id}?limit=16&offset=0&search=`;
        }else{
          let url = `${process.env.REACT_APP_BASEURL}/api/read/courses?limit=16&offset=0&search=`;
        }

        fetch(url, {
          method: "GET",
          headers: {
            Authorization: `Bearer ${token}`,
            "Content-Type": "application/json",
          },
        })
          .then((response) => response.json())
          .then((json) => {
            this.setState({
              course: json.rows,
              loading: false,
            });
          })
          .catch((error) => console.log("error", error));
      }

    render(){
        return(
            <>
             {
                (localStorage.getItem('token'))
                ?<Navigation history={this.props.history} selected={this.state.category_name}/>
                :   <>
                        <Navbar expand="lg">
                        <a className="logo" href="/"><img className="img-responsive logo" src={this.Logo} alt="" /></a>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                        <Navbar.Collapse id="basic-navbar-nav">
                            <Nav className="mr-auto">
                                <NavDropdown title="Category" id="basic-nav-dropdown" color="#6831B0" className="dropdown">
                                    <NavDropdown.Item href="#action/3.2">
                                        IT & Software
                                </NavDropdown.Item>
                                    <NavDropdown.Item href="#action/3.3">
                                        Design
                                </NavDropdown.Item>
                                    <NavDropdown.Item href="#action/3.4">
                                        Business
                                </NavDropdown.Item>
                                </NavDropdown>
                            </Nav>

                            <Form inline>
                                <div>
                                    <input type="search" className="srch" placeholder="Search for anything" />
                                    <span className="icon"><i className="fa fa-search"></i></span>
                                </div>
                                <a className="btn" href="/ShoppingChart" >Shopping Chart</a>
                                <a id="login" className="btn" onClick={this.onOpenLoginModal}>Login</a>
                                {(this.state.modalLoginIsOpen)? <Login redirect={(e)=>{this.props.history.push(e)}} state={this.state} modalLoginIsOpen={this.state.modalLoginIsOpen} redirect={(e)=>{this.props.history.push(e)}} onClose={() =>{this.setState({modalLoginIsOpen: false})}} />: null}
                                <a id="signup" className="btn" onClick={this.onOpenSignModal}>Sign Up</a>
                            </Form>
                            {/* <Login /> */}
                        </Navbar.Collapse>
                    </Navbar>
                    </>
            }
            <Container fluid style={{ width: "100%", paddingBottom: '30px',  backgroundColor: "#F1F1F1" }}>
                <Row style={{ width: '80%', margin: 'auto' }}>
                    {this.state.course.map((course) => {
                        return <Col style={{ paddingBottom: '3%' }} xs={12} sm={6} md={4} lg={3} xl={3}><Listing id={course.id} key={course.id} course={course}></Listing></Col>
                    })}
               </Row>
            </Container>
            <Footer/>
            </>
        );
    }

}

export default Courses;