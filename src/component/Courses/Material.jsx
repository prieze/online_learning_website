import React from 'react';
import {useContext} from 'react';
import { Row, Col, Table, Accordion, Card, Button, useAccordionToggle, AccordionContext } from 'react-bootstrap';
import 'font-awesome/css/font-awesome.min.css';
import { useHistory } from "react-router-dom";
import {countMinuteFromSecond, countMinutetoSecond, sumDurationSubTopic } from '../Utils'

function ContextAwareToggle({ children, eventKey, callback }) {
    const currentEventKey = useContext(AccordionContext);
  
    const decoratedOnClick = useAccordionToggle(
      eventKey,
      () => callback && callback(eventKey),
    );
  
    const isCurrentEventKey = currentEventKey === eventKey;
  
    return (
        <>
        { isCurrentEventKey 
            ? <i style={{ cursor: 'pointer' }} className="fa fa-minus" aria-hidden="true" onClick={decoratedOnClick}/>
            : <i style={{ cursor: 'pointer' }} className="fa fa-plus" aria-hidden="true" onClick={decoratedOnClick}/>
        }
        </>
        
    );
  }

const Material = (props) => {
    
    return (
        <Card style={{ borderRadius: '0', boxShadow: 'none' }} key={props.key}>
            <Card.Header style={{background: "white", color: "black" }}>
                <Row>
                    <Col xs={12} lg={4}>
                        {props.topic?.topic_name}
                    </Col>
                    <Col xs={12} lg={4}>
                        {props.topic?.sub_topics.length} Material
                    </Col>
                    <Col xs={2} lg={2}>
                        {sumDurationSubTopic(props.topic?.sub_topics)}
                    </Col>
                    
                    <Col xs={1} lg={1}>
                    <ContextAwareToggle eventKey={props.index.toString()}></ContextAwareToggle>
                    </Col>
                </Row>
            </Card.Header>
            <Accordion.Collapse eventKey={props.index.toString()}>
            <Card.Body style={{background: "#E8E8E8"}}>
                {props.topic?.sub_topics?.map(sub => {
                    return (
                        <Row className="justify-content-center">
                            <Col xs={4} lg={4}>
                                {sub.sub_topic_name}
                            </Col>
                            <Col xs={4} lg={4}>
                                {sub.video?.file_name}
                            </Col>
                            <Col xs={4} lg={4}>
                                {sub.duration}
                            </Col>
                        </Row>
                    )
                })}
            </Card.Body>
            </Accordion.Collapse>
        </Card>
    )
}

export default Material;