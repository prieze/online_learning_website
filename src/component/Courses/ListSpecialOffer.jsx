import React from 'react';
import { Link } from 'react-router-dom';
import { Nav } from 'react-bootstrap';

const ListSpecialOffer = (props) => {
    
    return(
        <Nav.Item>
            <div style={{ cursor: 'pointer', color: '#002333', fontSize: '16px', fontFamily: 'Gordita-bold', paddingLeft: '2rem' }} onClick={ () => { props.history(`/SpecialOffer/${props.categories.id}`) }}>{props.categories.category_name}</div>
        </Nav.Item>
    )

}

export default ListSpecialOffer;