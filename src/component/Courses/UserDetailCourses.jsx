import React, { Component } from 'react';
import { Container, Row, Col, Button, ProgressBar,  Accordion, Card, Form, Image } from 'react-bootstrap';
import Footer from "../Footer/Footer";
import Navigation from "../Navigation/Navigation";
import { Link } from 'react-router-dom';
import ListTopics from './ListTopics';
import Video from '../../components/Video/Video'
// import { Document } from 'react-pdf';
import { Document, Page } from 'react-pdf/dist/entry.webpack';
import { Tabs } from 'antd';
import AddDiscuss from './AddDiscuss';
import Reviews from '../Reviews/Reviews';
const { TabPane } = Tabs;

class UserDetailCourses extends Component{

    constructor(props) {
        super(props);
        let user = JSON.parse(localStorage.getItem('userData'));
        this.state = {
          user: user,
          course: [],
          list: [],
          id: 0,
          videoPlayNow: {},
          videoPlayList: [],
          videoPlayIndex: 0,
          docNumPages: 0,
          docPageNumber: 1,
          activeAccordion: "0"
        };

    }

    updateContent(update=false) {
        return new Promise((resolve, reject) => {
            const token = localStorage.getItem('token');
            // console.log(this.props)
            const id = this.props.match.params.id;
            fetch(`${process.env.REACT_APP_BASEURL}/api/course/user/${id}`, {
                method: 'GET',
                headers: {
                    "Authorization": `Bearer ${token}`,
                    "Content-Type": "application/json"
                },
            })
                .then(response => response.json())
                .then(async(json) => {
                    let videoPlayNow, videoPlayList = [];
                    if (json.sukses) {
                        for (const topic of json.data.course.topics) {
                            for (const sub of topic.sub_topics) {
                                // console.log(sub);
                                if (sub.video && sub.video.url) {
                                    if  (sub.video.url.toLowerCase().endsWith('.mp4') || sub.video.url.toLowerCase().endsWith('.pdf')) {
                                        const a = sub.video;
                                        a.is_finished = sub.is_finished;
                                        videoPlayList.push(a);
                                        if (!videoPlayNow) {
                                            videoPlayNow = a
                                        }
                                        // break;
                                    }
                                }
                            }
                        }
                        if (update) {
                            console.log('update Content');
                            console.log(videoPlayList);
                            await this.setState({
                                course: json.data,
                                videoPlayList,
                            })
                        } else {
                            await this.setState({
                                course: json.data,
                                videoPlayNow,
                                videoPlayList,
                                videoPlayIndex: 0
                            })
                        }
                        resolve(this.state.course);
                        console.log(this.state.course);
                    } else {
                        console.log('Failed');
                        console.log(json.msg);
                        reject(json.msg)
                    }
                })
                .catch(error => console.log('error', error));
        })
    }

    componentDidMount() {
        
        this.updateContent()
    }

    handleClickSub(sub){
        console.log(sub);
        const id = sub.video.id;
        this.state.videoPlayList.map((a, i) => {
            if (a.id === id) {
                console.log(a);
                this.setState({
                    videoPlayNow: a,
                    videoPlayIndex: i
                })
            }
        })
    }

    calcAccordion() {
        let index = 0;
        loop1:
        for (const topic of this.state.course.course.topics) {
            for (const sub of topic.sub_topics) {
                if (sub.video.url === this.state.videoPlayNow.url) {
                    break loop1;
                }
            }
            index += 1
        }
        this.setState({
            activeAccordion: index.toString()
        })
    }

    setActiveAccordion(ev) {
        this.setState({
            activeAccordion: ev
        })
    }

    prevButton() {
        const i = this.state.videoPlayIndex - 1;
        this.updateContent(true).then(() => {
            if (i >= 0) {
                this.setState({
                    videoPlayNow: this.state.videoPlayList[i],
                    videoPlayIndex: i
                })
            } else {
                this.setState({
                    videoPlayNow: this.state.videoPlayList[this.state.videoPlayList.length - 1],
                    videoPlayIndex: this.state.videoPlayList.length - 1
                })
            }
            this.calcAccordion();
        })
    }

    nextButton() {
        const i = this.state.videoPlayIndex + 1;
        this.updateContent(true).then(() => {
            if (i < this.state.videoPlayList.length) {
                this.setState({
                    videoPlayNow: this.state.videoPlayList[i],
                    videoPlayIndex: i
                })
            } else {
                this.setState({
                    videoPlayNow: this.state.videoPlayList[0],
                    videoPlayIndex: 0
                })
            }
            this.calcAccordion();
        })
    }

    onDocumentLoadSuccess = (numPages) => {
        this.setState({docNumPages: numPages.numPages});
    }

    downloadPDF = ((url) => {
        console.log('Download PDF: ', url);
    })

    render(){

        return(
            <>
            <Navigation history={this.props.history}/>
            <Container fluid style={{ width: "100%", padding: "30px" }}>
                <Row className="justify-content-center">
                    <Col md={6}>
                    <Row className="justify-content-center">
                            <Col lg={12}>
                                <p style={{ fontFamily: 'Gordita-bold' }}>{this.state.course.course?.course_name}</p>
                                <p style={{ color: '#A3A3A3' }}>{this.state.course.course?.instructors[0].instructor_name}, {this.state.course.course?.instructors[0].job_title}</p>
                            </Col>
                            <Col lg={12}>
                                { this.state.videoPlayNow?.url?.toLowerCase().endsWith('.mp4') &&
                                    <Video width="100%" height="auto" 
                                        src={this.state.videoPlayNow.url} 
                                        canSeek={this.state.videoPlayNow.is_finished}
                                        // canSeek={true}
                                        videoDonePlaying={()=> {this.nextButton()}}
                                        courseId={this.state.course.course.id}
                                        videoId={this.state.videoPlayNow.id}
                                    />
                                }
                                { this.state.videoPlayNow?.url?.endsWith('.pdf') &&
                                    <a href={this.state.videoPlayNow?.url} target="_blank" download>
                                        <div onClick={()=>{this.downloadPDF(this.state.videoPlayNow?.url)}}>
                                            <Document
                                            onLoadSuccess={this.onDocumentLoadSuccess}
                                            file={this.state.videoPlayNow.url}
                                            // file={{
                                            //     url: this.state.videoPlayNow.url
                                            //   }}
                                            >
                                                <Page pageNumber={this.state.docPageNumber} width="650" height="315"/>
                                            </Document>
                                        </div>
                                    </a>
                                }
                            </Col>
                            <Col xs={5} sm={3} md={5} lg={4} xl={3}>
                                <Button size="sm" className="button-grape" style={{ border: '5px solid #dbdaf1', width: '45px', height: '45px', borderRadius: '50px', }} onClick={()=> {this.prevButton()}}><i style={{ color: '#FFFFFF' }} className="fa fa-step-backward"></i></Button>
                                <Button size="sm" className="button-grape" style={{ border: '5px solid #dbdaf1', width: '45px', height: '45px', borderRadius: '50px',  marginLeft: '5%' }}  onClick={()=> {this.nextButton()}}><i style={{ color: '#FFFFFF' }} className="fa fa-step-forward"></i></Button>
                            </Col>
                        </Row>
                    </Col>
                    <Col md={4}>
                        <Row>
                            <Col lg={12}>
                                <div style={{ overflow: 'auto', height: '346px', marginTop: '19%' }}>
                                    <Accordion defaultActiveKey="0" activeKey={this.state.activeAccordion}>
                                    {
                                        this.state.course.course?.topics?.map((list, index) => {
                                            return <ListTopics
                                                id={list.id}
                                                key={list.id}
                                                topic={list}
                                                index={index}
                                                videoPlayNow={this.state.videoPlayNow}
                                                activeAccordion={(ev)=> {this.setActiveAccordion(ev)}}
                                                onClick={(sub) => {this.handleClickSub(sub)}}
                                                sub_topic={(list && list.topics && list.topics.sub_topic_name) ? list.topics.sub_topic_name : null}></ListTopics>
                                        })
                                    }
                                    </Accordion>
                                </div>
                            </Col>
                            {this.state.course.userScore?.lulus
                                ? <Col lg={12} style={{ marginTop: '3%' }}><Button className="button-grape" block
                                    onClick={() => {window.open(`${this.state.course?.certificate?.file?.url}`)}}
                                    >Download Certificate</Button>
                                </Col>
                                : <Col lg={12} style={{ marginTop: '3%' }}>
                                    <Link to={{ pathname: '/Exam', master_soals: this.state.course?.course?.master_soals, courseId: this.state.course?.courseId, course_user: this.state.course?.id }}>
                                        <b style={{ color: '#000000' }}><i className="fa fa-file-o"></i> Exam : {this.state.course.course?.course_name}</b>
                                    </Link>
                                  </Col>
                            }    
                        </Row>
                    </Col>
                </Row>

                <Row className="justify-content-center" style={{ marginTop: '5%' }}>
                    <Col lg={10}>
                        <Tabs size="large" defaultActiveKey="1">
                            <TabPane tab="About" key="1">
                                <p style={{ fontFamily: 'Gordita-bold', color: '#392C7E', fontSize: '18px' }}>About This Class</p>
                                <p style={{ fontSize: '16px' }}>{this.state.course.course?.description}</p>
                            </TabPane>
                            <TabPane tab="Discussions"  key="2">
                                <AddDiscuss state={this.state} />                          
                            </TabPane>
                            <TabPane tab="Reviews"  key="3">
                                <Reviews state={this.state} />
                            </TabPane>
                        </Tabs>
                    </Col>
                </Row>

            </Container>
            <Footer/>
            </>
        );
    }

}

export default UserDetailCourses;