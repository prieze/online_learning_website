import React, { Component } from "react";
import { Collapse, Divider } from 'antd';
import {PlusOutlined, MinusOutlined, EditOutlined} from '@ant-design/icons'
const { Panel } = Collapse;

const editButton = (onEdit) => {
  if (onEdit) {
    return <EditOutlined
      onClick={event => {
        // If you don't want click extra trigger collapse, you can prevent this:
        event.stopPropagation();
        console.log('edit button clicked')
      }}
    />
  } 
};

class ListInstructor extends Component{
  customExpandIcon(props) {
    if (props.isActive) {
      return <MinusOutlined onClick={((e) => {
        props.expandIcon(props, e);
      })}/>
    } else {
      return <PlusOutlined onClick={((e) => {
        props.expandIcon(props, e);
      })}/>
    }
  }

  defaultKey = (this.props.data && this.props.data[0]) ? this.props.data[0].id : '1'

  render() {
    return (
      <Collapse 
        accordion 
        style={{ width: "100%", margin: 'auto' }}
        expandIconPosition={"right"}
        defaultActiveKey={this.defaultKey}
        expandIcon={(props) => this.customExpandIcon(props)}
      >
        {this.props.data?.map(list => {
          return <Panel header={list.instructor_name} key={list.id}
            style={{backgroundColor: "rgba(86, 103, 255, 0.13)" }}
            extra={editButton(this.props.is_edit)}
          >
            <p>{list.biography}</p>
          </Panel>
        })}
      </Collapse>
    )
  }
}

export default ListInstructor;