import React from 'react';
import { Row, Col, Card, Image, Button } from 'react-bootstrap';
import Shop from "../../gambar/shop.png";
import { useHistory } from "react-router-dom";
import { message } from 'antd';
import Moment from 'moment';

const View = (props) => {

	console.log(props);
	console.log(props.test);

	let history = useHistory();

	const addToChart = (async (id) => {
		const token = localStorage.getItem('token');
		fetch(`${process.env.REACT_APP_BASEURL}/api/cart`, {
			method: "POST",
			body: JSON.stringify({course_id: id}),
			headers: {
				"Authorization": `Bearer ${token}`,
				"Content-Type": "application/json"
			},
		}).then((response) => {
			response.json()
				.then((result) => {
					console.warn("result", result);
					if (result.sukses) {
						console.log(props);
						if(props.reloadData) {
							props.reloadData();
						}
						message.success("Berhasil ditambahkan", 1)
					} else {
						message.error(result.msg, 1)
					}
				})
		})
	})
	const addToLocalChartOrDB = ( (list) => {
		const token = localStorage.getItem('token');
		if (token) {
			console.log('Login');
			addToChart(list.id)
		} else {
			console.log('Non_login');
			const non_login_cart = localStorage.getItem('non_login_cart');
			const cart = non_login_cart ? JSON.parse(non_login_cart) : [];
			//Cari apakah id sudah ada
			let ketemu = false;
			cart.map(a => {if (a.course.id === list.id) { ketemu = true}})
			if(!ketemu) {
				cart.push({course: list})
				localStorage.setItem('non_login_cart', JSON.stringify(cart))
				message.success("Berhasil ditambahkan");
				props.history(`/ShoppingChart`)
			}
		}
	})
	
	return(
		<Card className="h-100" style={{ width: "14rem", margin: 'auto', borderRadius: "12px", border: "0.2px solid #DFDFFF", marginTop: '10%' }}>
			<Card.Img variant="top" style={{ height: '150px', borderRadius: '12px 12px 0 0' }} src={props.test.cover?.url}/>
			<p hidden={ props.test.discount > "0" ? false : true } className="diskon2">{props.test.discount}% Off</p>
			<p className="bestselling">Best Seller</p>
			<Card.Body>
			<Card.Title style={{ cursor: 'pointer', fontFamily: 'Gordita-bold', fontSize: '15px', color: '#002333' }} onClick={ () => { props.history(`/DetailCourses/${props.test.id}`) }}>{props.test.course_name}</Card.Title>
				<p style={{ fontSize: '10px', color: 'rgba(0, 35, 51, 0.67)' }}><i className="fa fa-users"></i> {props.test.count} <i className="fa fa-star"></i> {parseFloat(props.test.rates).toFixed(1)}</p>
				<Row>
					{props.test?.courses_user?.length > 0
						? null
						: <>
							<Col lg={12}>
								<Row>
									<Col><Card.Text style={{ fontSize: '10px', color: '#BAB9B9' }}><s>IDR {props.test.price}</s></Card.Text></Col>
									<Col><Card.Text style={{ fontSize: '10px', color: '#BAB9B9', float: 'right' }}>{props.test.total_duration}</Card.Text></Col>
								</Row>
							</Col>
							<Col>
								<Card.Text style={{ color: '#3722D3', fontSize: '15px', fontFamily: 'Gordita-bold' }}>IDR {parseInt(props.test.hargadisc).toLocaleString("id-ID")}</Card.Text>
							</Col>
						</>
					}
				</Row>
			</Card.Body>
			<Card.Footer>
				{props.test?.courses_user?.length > 0 
					? <Button onClick={()=> { props.history(`/ViewCourses/${props.test.id}`); }} size="sm" className="button-grape" style={{ borderRadius: '0', backgroundColor: 'rgba(165, 148, 255, 0.78)' }} block>See Course</Button>
					: <Image onClick={async()=> { await addToLocalChartOrDB(props.test) }} style={{ cursor: 'pointer', marginBottom: '-35px', boxShadow: '4px 4px 20px rgba(0, 0, 0, 0.2)', width: '60px', marginLeft: '65%' }} className="shop" src={Shop} roundedCircle />
				}
			</Card.Footer>
		</Card>
	);

}

export default View;