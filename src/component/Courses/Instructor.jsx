import React, {Component} from 'react';
import { Form } from 'react-bootstrap';

class Instructor extends Component{

    state = {
        list: []
    }

    componentDidMount(){

        this.listInstructor();
    }

    listInstructor() {
        const token = localStorage.getItem("token");

        this.setState({ loading: true });
        fetch(`${process.env.REACT_APP_BASEURL}/api/course/master/instructors/user`, {
            method: 'GET',
            headers: {
                "Authorization": `Bearer ${token}`,
                "Content-Type": "application/json"
            },
        })
          .then((response) => response.json())
          .then((json) => {
            this.setState({
              list: json.rows,
              loading: false
            });
            console.log("json" + json);
            console.log(this.state.list);
          })
          .catch((error) => console.log("error", error));
    }

    render(){
        return(
            <Form.Group>
                <Form.Label style={{ color: '#6831B0' }}>INSTRUCTOR</Form.Label>
                <Form.Control as="select" onChange={(e) => {this.props.addInstructor(e.target.value)}}>
                    <option value="0">All</option>
                    {this.state.list?.map((list) => {

                        return <option value={list.id}>{list.instructor_name}</option>

                    })}
                </Form.Control>
            </Form.Group>
        );
    }

}

export default Instructor;