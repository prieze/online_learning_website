import React from 'react';
import { CardDeck, Card, Image, Col, Row, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import Shop from "../../gambar/shop.png";
import { ToastContainer, toast } from 'react-toastify';
import { useHistory } from "react-router-dom";

const Listing = (props) => {
    let history = useHistory();
    const addToChart = (async (id) => {
        const token = localStorage.getItem('token');
        fetch(`${process.env.REACT_APP_BASEURL}/api/cart`, {
            method: "POST",
            body: JSON.stringify({course_id: id}),
            headers: {
                "Authorization": `Bearer ${token}`,
                "Content-Type": "application/json"
            },
        }).then((response) => {
            response.json()
                .then((result) => {
                    console.warn("result", result);
                    if (result) {
                        console.log(props);
                        history.push(`/ShoppingChart`)
                        toast.success("Berhasil ditambahkan")
                    } else {
                        toast.error(result.msg)
                    }
                })
        })
    })
    console.log(props);
    return(
        <Card className="h-100" style={{ width: "14rem", margin: 'auto', borderRadius: "12px", border: "0.2px solid #DFDFFF", marginTop: '40px'}}>
            <Card.Img variant="top" style={{ height: '150px', borderRadius: '10px 10px 0 0' }} src={props.course.cover?.url}/>
            <Card.Body style={{ lineHeight: '0.5' }}>
                <Link to={`/DetailCourses/${props.course.id}`}><Card.Title style={{ fontFamily: 'Gordita-bold', fontSize: '15px', color: '#002333' }}>{props.course.course_name}</Card.Title></Link>
                <p style={{ fontSize: '10px', color: 'rgba(0, 35, 51, 0.67)' }}><i className="fa fa-users"></i> 18k <i className="fa fa-star"></i> 3.8</p>
                <Row>
                    {props.course?.courses_user?.length > 0
                        ? null
                        : <> <Col><Card.Text style={{ color: '#3722D3', fontSize: '12px', fontFamily: 'Gordita-bold' }}>{props.course.price} IDR</Card.Text></Col> <Col style={{ marginLeft: '-35%' }}><Card.Text style={{ fontSize: '10px', color: '#BAB9B9' }}><s>{props.course.price}</s></Card.Text></Col> </>
                    }
                </Row>
            </Card.Body>
            <Card.Footer>
                {props.course?.courses_user?.length > 0 
                    ? <Button onClick={()=> { history.push(`/ViewCourses/${props.course.id}`); }} size="sm" className="button-grape" style={{ borderRadius: '0', backgroundColor: 'rgba(165, 148, 255, 0.78)' }} block>See Course</Button>
                    : <Image style={{ cursor: 'pointer', marginBottom: '-30px', boxShadow: '4px 4px 20px rgba(0, 0, 0, 0.2)', width: '60px', marginLeft: '65%' }} className="shop" src={Shop} roundedCircle onClick={()=> {addToChart(props.course.id)}}/>
                }
            </Card.Footer>
        </Card>
    )
}

export default Listing;