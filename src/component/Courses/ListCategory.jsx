import React from 'react';
import { Link } from 'react-router-dom';
import { Nav } from 'react-bootstrap';

const ListCategory = (props) => {
    
    return(
        <Nav.Item style={{ padding: '15px' }}>
            <div
                onClick={ () => { props.history(`/Courses/${props.categories.id}`)}} 
                
                style={{ 
                    paddingTop: '3%',
                    height: '32px',
                    width: '165px', 
                    cursor: 'pointer' , 
                    textAlign: 'center', 
                    backgroundColor: '#EBE6FF', 
                    borderRadius: '3px', 
                    color: '#392C7E', 
                    fontSize: '14px'
                }}

            >{props.categories.category_name}</div>
        </Nav.Item>
    )

}

export default ListCategory;