import React, { Component } from 'react';
import Navigation from '../Navigation/Navigation';
import { Form, Nav, Navbar, NavDropdown, Container, Row, Col, Button } from 'react-bootstrap';
import Footer from '../Footer/Footer';
import View from './BestSellingList';
import Login from '../Login';
import { Link } from 'react-router-dom';

class BestSellingCat extends Component{

    state = {
        course: [],
        id: 0,
        category_name: '',
        modalLoginIsOpen: false
    }
    Logo = 'https://galilagi-media.oss-ap-southeast-5.aliyuncs.com/assets/logo.png';

    onOpenLoginModal = () => {
      this.setState({ modalLoginIsOpen: true, modalSignIsOpen: false, message: null });
  };

    componentDidMount(){
        const id = this.props.match.params.id;
        const cat = JSON.parse(localStorage.getItem('categories'));
        cat.map(a => {
          if (a.id === parseInt(id)) {
            this.setState({category_name: a.category_name})
          }
        })
        this.listCourse(id);
    }

    componentDidUpdate(prevProps){
        const id = this.props.match.params.id;
        const cat = JSON.parse(localStorage.getItem('categories'));
        if (prevProps.match.params.id !== id) {
          cat.map(a => {
            if (a.id === parseInt(id)) {
              this.setState({category_name: a.category_name})
            }
          })
          this.listCourse(id);
        }
    }

    listCourse() {

      const token = localStorage.getItem("token");
      const id = this.props.match.params.id

      fetch(`${process.env.REACT_APP_BASEURL}/api/dashboard/best/selling/${id}?limit=16&offset=0&search=`, {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      })
        .then((response) => response.json())
        .then((json) => {
          const temp = [];
          const diskon = json.data?.courses?.map((disc)=>{
            const price = parseInt(disc.price);
            const dic = parseInt(disc.discount);
            const jum = (dic/100)*price;
            const tot = price-parseInt(jum);
            temp.push({
                ...disc,
                hargadisc: tot,
                course_name: disc.course_name,
                cover: disc.cover,
                courses_user: disc.courses_user,
                description: disc.description,
                discount: disc.discount,
                id: disc.id,
                instructors: disc.instructors,
                price: disc.price,
                rates: disc.rates,
                total_duration: disc.total_duration,
                total_user: disc.total_user
            })
            return tot
          });
          this.setState(prevstate => ({
            ...prevstate,
            category_name: json.data.category_name,
            course: temp
        }));
          console.log(this.state);
        })
        .catch((error) => console.log("error", error));
    }
      changeURL(url) {
        // this.setState({ redirect: url });
        this.props.history.push(url);
    }

      render(){
        return(
            <>
            {
                (localStorage.getItem('token'))
                ?<Navigation history={this.props.history} selected={this.state.category_name}/>
                :   <>
                        <Navbar expand="lg">
                        <a className="logo" href="/"><img className="img-responsive logo" src={this.Logo} alt="" /></a>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                        <Navbar.Collapse id="basic-navbar-nav">
                            <Nav className="mr-auto">
                                <NavDropdown title="Category" id="basic-nav-dropdown" color="#6831B0" className="dropdown">
                                    <NavDropdown.Item href="#action/3.2">
                                        IT & Software
                                </NavDropdown.Item>
                                    <NavDropdown.Item href="#action/3.3">
                                        Design
                                </NavDropdown.Item>
                                    <NavDropdown.Item href="#action/3.4">
                                        Business
                                </NavDropdown.Item>
                                </NavDropdown>
                            </Nav>

                            <Form inline>
                                <div>
                                    <input type="search" className="srch" placeholder="Search for anything" />
                                    <span className="icon"><i className="fa fa-search"></i></span>
                                </div>
                                <a className="btn" href="/ShoppingChart" >Shopping Chart</a>
                                <a id="login" className="btn" onClick={this.onOpenLoginModal}>Login</a>
                                {(this.state.modalLoginIsOpen)? <Login redirect={(e)=>{this.props.history.push(e)}} state={this.state} modalLoginIsOpen={this.state.modalLoginIsOpen} redirect={(e)=>{this.props.history.push(e)}} onClose={() =>{this.setState({modalLoginIsOpen: false})}} />: null}
                                <a id="signup" className="btn" onClick={this.onOpenSignModal}>Sign Up</a>
                            </Form>
                            {/* <Login /> */}
                        </Navbar.Collapse>
                    </Navbar>
                    </>
            }
            <Container fluid style={{ width: "100%", padding: '2rem', backgroundColor: "#F1F1F1" }}>
              <Row style={{ width: '85%', margin: 'auto' }}>
                    <Col>
                        <Navbar expand="lg" style={{ background: "rgba(58, 44, 125, 0.34)", marginBottom: "3%" }}>
                        <Link to={`/BestSelling`}>
                          <button className="btn icon-back">
                            <span style={{ color: "#FFFFFF" }} className="fa fa-chevron-left"></span>
                          </button>
                        </Link>
                        <Navbar.Brand style={{ color: "#FFFFFF", fontFamily: "Gordita-bold" }}>{this.state.category_name}</Navbar.Brand>
                            <Navbar.Toggle aria-controls="basic-navbar-nav" />
                            <Navbar.Collapse id="basic-navbar-nav">
                            <Nav className="mr-auto" />
                                <Form inline>
                                    <Form.Control type="text" id="search" placeholder="Search" style={{ borderRadius: "30px" }} onChange={this.onChange} debounce="1000" />
                                </Form>
                            </Navbar.Collapse>
                        </Navbar>
                    </Col>
                </Row>
                
                <Row style={{ width: '80%', margin: 'auto' }}>
                    {
                        this.state.course?.map((list) => {
                          return <Col style={{ paddingBottom: '3%' }} xs={12} sm={6} md={4} lg={3} xl={3}><View id={list.id} key={list.id} test={list} history={((url) => { this.changeURL(url) })}></View></Col>
                        })
                    }
               </Row>
            </Container>
            <Footer/>
            </>
        );
    }

}

export default BestSellingCat;