import React from 'react';
import {useContext} from 'react';
import { Row, Col, Table, Accordion, Card, Button, useAccordionToggle, AccordionContext } from 'react-bootstrap';
import 'font-awesome/css/font-awesome.min.css';
import { useHistory } from "react-router-dom";
import {countMinuteFromSecond, countMinutetoSecond, sumDurationSubTopic } from '../Utils'

function ContextAwareToggle({ children, eventKey, parentProps, callback }) {
    const currentEventKey = useContext(AccordionContext);
  
    const decoratedOnClick = useAccordionToggle(
      eventKey,
      () => parentProps.activeAccordion(eventKey),
      () => callback && callback(eventKey),
    );
  
    const isCurrentEventKey = currentEventKey === eventKey;
  
    return (
        <>
        { isCurrentEventKey 
            ? <i style={{ cursor: 'pointer' }} className="fa fa-angle-down" aria-hidden="true" onClick={decoratedOnClick}/>
            : <i style={{ cursor: 'pointer' }} className="fa fa-angle-up" aria-hidden="true" onClick={decoratedOnClick}/>
        }
        </>
        
    );
  }

const ListTopics = (props) => {
    console.log(props);
    return (
        <Card style={{ boxShadow: 'none' }} key={props.key}>
            <Card.Header style={{background: "white", color: "black"}}>
                <Row>
                    <Col xs={10} sm={11} md={10} lg={10} xl={11}>
                        {props.topic?.topic_name}
                    </Col>
                    <Col xs={1} sm={1} md={1} lg={1} xl={1}>
                    <ContextAwareToggle eventKey={props.index.toString()} parentProps={props}></ContextAwareToggle>
                    </Col>
                </Row>
            </Card.Header>
            <Accordion.Collapse eventKey={props.index.toString()}>
            <Card.Body style={{ padding: '0' }}>
                {props.topic?.sub_topics?.map(sub => {
                    return (
                        <Row onClick={() => props.onClick(sub)} style={{ paddingTop: '3%', backgroundColor: sub.video?.url === props.videoPlayNow?.url ? '#dbdaf1': 'white' }}>
                            <Col xs={10} sm={10} lg={sub.is_finished ? 10 : 12}>
                                <b style={{ marginLeft: '7%', cursor: 'pointer' }}><i className="fa fa-play-circle-o" style={{ width: '5%', fontSize: '19px' }}></i> {sub.sub_topic_name}</b>
                                <p style={{ fontSize: '12px', marginLeft:'7%' }}>{ sub.video.url.toLowerCase().endsWith('.pdf') ? 'PDF' : 'MP4' } | {sub.duration}</p>
                            </Col>
                            {sub.is_finished &&
                                <Col xs ={2} sm={2} lg={2}>
                                    <i className="fa fa-check" aria-hidden="true" style={{color:"green"}}></i>
                                </Col>
                            }
                        </Row>
                    )
                })}
            </Card.Body>
            </Accordion.Collapse>
        </Card>
    )
}

export default ListTopics;