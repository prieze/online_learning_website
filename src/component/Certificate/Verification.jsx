import React, { Component } from 'react';
import { Button, Col, Container, Form, Row, InputGroup, Card } from 'react-bootstrap';
import Footer from '../Footer/Footer';
import Navigation from '../Navigation/Navigation';
import { message } from 'antd';
import Moment from 'moment';

class Verificarion extends Component{

    constructor(props) {
        super(props);
        
        this.state = {
           certificate: {},
           hidden: true,    
           certificate_id: ''
          };

    }

    // componentDidMount(){
    //     this.Verification();
    // }

    Verification(){

        const isi = this.state.certificate_id;
        const token = localStorage.getItem('token');
        fetch(`${process.env.REACT_APP_BASEURL}/api/certificate/verification`, {
            method: "POST",
            body: JSON.stringify({
                certificate_id: this.state.certificate_id
            }),
            headers: {
                "Authorization": `Bearer ${token}`,
                "Content-Type": "application/json"
            },
        }).then((response) => {
            response.json()
                .then((result) => {
                    if(result.sukses){
                        console.log(result);
                        this.setState({
                            certificate: result.data,
                            hidden: false
                        });
                        console.log(this.state.certificate);
                    }else{
                        message.error(`Tidak ada sertifikat dengan ID “${isi}” masukkan ID sertifikat yang sesuai`, 3);
                    }
                })
        })
    }

    render(){

        const title = {
            fontSize: '15px',
            fontFamily: 'Gordita-bold',
            color: '#392C7E'
        }

        const result = {
            fontSize: '15px',
            fontFamily: 'Gordita-bold',
            color: '#392C7E',
            textAlign: 'center'
        }

        const text = {
            fontSize: '13px',
            color: '#868686'
        }

        return(
            <>
            <Navigation/>
            <Container fluid style={{ backgroundColor: '#f1f1f1', padding: '50px', height: 'auto' }} >

                <Row style={{ width: '40%', margin: 'auto' }}>
                    <Col>
                        <p style={{ color: '#3B2881', textAlign: 'center' }} >Masukan serifikat untuk memeriksa keaslian sertifikat</p>
                        <Form>
                            <Form.Group>
                                <InputGroup>
                                    <InputGroup.Prepend>
                                        <InputGroup.Text style={{ backgroundColor: 'white', border: '0' }} id="inputGroupPrepend"><i className="fa fa-search" style={{ color: 'rgba(78, 78, 78, 0.45)' }} /></InputGroup.Text>
                                    </InputGroup.Prepend>
                                    <Form.Control onChange={ (event) => { this.setState({ certificate_id: event.target.value }) } } onClick={ () => { this.setState({ button: true }) } } style={{ border: '0' }} placeholder="Masukan disini" />
                                </InputGroup>
                            </Form.Group>
                            <Form.Group>
                                <Button disabled={(this.state.certificate_id === '')?true :false} onClick={ () => { this.Verification(); } } block className="button-grape">Periksa</Button>
                            </Form.Group>
                        </Form>
                    </Col>
                </Row>

                {
                    this.state.certificate?.certificate_id                    
                }

                <Row hidden={this.state.hidden} className="justify-content-center" style={{ width: '40%', margin: 'auto' }}>
                    <Col lg={9}>
                        <Card style={{ border: '0.5px solid #392C7E', boxShadow: 'none', backgroundColor: '#f1f1f1' }}>
                            <Card.Body style={{ padding: '0', paddingTop: '3%' }}>
                                <Card.Title style={result}><i className="fa fa-check-circle" style={{ color: '#2eb851' }} /> Sertifikat terverifikasi</Card.Title>
                            </Card.Body>
                        </Card><br/>
                        <Card style={{ border: '0.5px solid #392C7E', boxShadow: 'none', backgroundColor: '#f1f1f1' }}>
                            <Card.Body style={{ paddingBottom: '2%', paddingTop: '5%' }}>
                                <Card.Text style={text}>Jenis sertifikat</Card.Text>
                                <Card.Title style={title}>Sertifikat Lulus Exam</Card.Title>
                                <Card.Text style={text}>Judul</Card.Text>
                                <Card.Title style={title}>{this.state.certificate.course_name}</Card.Title>
                                <Card.Text style={text}>Nama</Card.Text>
                                <Card.Title style={title}>{this.state.certificate.name}</Card.Title>
                                <Card.Text style={text}>Diperoleh</Card.Text>
                                <Card.Title style={title}>{Moment(this.state.certificate.added_at).format( 'DD MMMM YYYY')}</Card.Title>
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>

            </Container>
            <Footer/>
            </>
        )

    }

}

export default Verificarion;