import React, { Component, Fragment } from 'react';
import '../../style/role.css'
import Navigation from '../Navigation/Navigation'
import { Button, Container, Card, Row, Form, Col } from "react-bootstrap"
import { Link } from "react-router-dom"
import 'react-bootstrap'
import {  message } from 'antd';
import { checkRole } from '../Utils';
import Footer from "../Footer/Footer"

class EditRole extends Component {
    state = {
        role: { role_name: '' },
        id: 0
    }

    updaterole() {
        const token = localStorage.getItem('token');
        // const token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwidXNlcm5hbWUiOiJzaWFoYWFuIiwiZW1haWwiOiJzaWFoYWFuQGdtYWlsLmNvbSIsImZpcnN0X25hbWUiOiJuZXR0eSIsImxhc3RfbmFtZSI6InNpYWhhYW4iLCJtb2JpbGVfbnVtYmVyIjoiMDkyIiwiY291bnRyeSI6InR5IiwiYWRkcmVzcyI6InN1a2EiLCJzdGF0dXMiOjEsImdhbWJhciI6ImJhc2U2NCBJbWFnZSBVUkwiLCJjcmVhdGVkQXQiOiIyMDIwLTA3LTA4VDAyOjI4OjQ1LjQzN1oiLCJ1cGRhdGVkQXQiOiIyMDIwLTA3LTA4VDA4OjEyOjA3LjI2MloiLCJyb2xlcyI6WyJhZG1pbiJdLCJpYXQiOjE1OTQyNzM1NzN9.VLNpcAS3KAn6z5nKAI4Vskh7UMsXM4x2VFHu7dFDjeI";
        const id = this.props.match.params.id;
        console.log("id dari url : " + id);

        if (this.state.role.role_name === "" || this.state.role.role_name === null) {
            message.error("Fill in The Role Name!", 1)
        } else {
            fetch(`http://149.129.255.54/api/update/role/${id}`, {
                method: "PUT",
                headers: {
                    "Authorization": `Bearer ${token}`,
                    "Content-Type": "application/json"
                },
                body: JSON.stringify({
                    id: id,
                    role_name: this.state.role.role_name
                })
            })
                .then(response => response.json())
                .then(json => {
                    if (json.sukses) {
                        this.setState({
                            role: json.data
                        })
                        console.log("Data yang mau di update : ");
                        console.log(json.data);
                        console.log("Sukses : ");
                        console.log(json.sukses);
                        console.log("id dari api : " + json.data.id);
                        this.props.history.push('/Role')
                        message.success("Role berhasil diupdate", 1)
                    } else {
                        console.log('Failed');
                        console.log(json.msg);
                        message.error(json.msg, 1)
                    }
                })
                .catch(error => {
                    console.log('error', error);
                    message.error('Error on App', 1)
                });
        }
    }

    componentDidMount() {
        const token = localStorage.getItem('token');
        const id = this.props.match.params.id;
        this.setState({
            role: { role_name: '' }
        });
        fetch(`http://149.129.255.54/api/read/role/${id}`, {
            method: "GET",
            headers: {
                "Authorization": `Bearer ${token}`,
                "Content-Type": "application/json"
            },
        })
            .then(response => response.json())
            .then(json => {
                if (json.sukses) {
                    this.setState({
                        role: json.data
                    })
                } else {
                    console.log('Failed');
                    console.log(json.msg);
                }
            })
            .catch(error => console.log('error', error));
    }
    changeRoleName(event) {
        event.persist();
        this.setState(prevState => ({
            role: {
                ...prevState.role,
                role_name: event.target.value
            }
        }))
    }

    render() {
        const { role, id } = this.state;
        if (!checkRole('admin').sukses) {
            return (
                <div>Tidak Bisa Mengakses</div>
            )
        }
        return (

            <Fragment>
                <Navigation history={this.props.history}/>
                <Container fluid
                    style={{
                        height: "800px",
                        padding: "50px",
                        backgroundColor: "#f1f1f1"
                    }}>
                    <Row>
                        <Col>
                        <p style={{ marginLeft: '38%', color: '#000000' }}><Link to={`/Role`} style={{ color: '#000000' }}>Roles</Link> / <strong>Edit Role</strong></p>
                            <Card
                                style={{
                                    width: '19rem',
                                    margin: 'auto'
                                }}>
                                <p style={{ fontSize: '19px', marginTop: '5%', marginLeft: '3%', color: '#3722D3' }}>Edit Role</p>
                                <hr style={{ border: '1px solid #3722D3', width: '95%', margin: 'auto', marginBottom: '5%'}} />                               
                                <Card.Body style={{ margin: 'auto' }}>
                                    <Form>
                                        <Form.Row>
                                            <Form.Group as={Col} md="auto" controlId="validationCustom02">
                                                <Form.Label>Role ID</Form.Label>
                                                <Form.Control
                                                    type="text"
                                                    disabled
                                                    value={this.state.role.id}
                                                    onChange={(event) => { this.changeRoleName(event) }}
                                                />
                                            </Form.Group>
                                        </Form.Row>
                                        <Form.Row>
                                            <Form.Group as={Col} md="auto" controlId="validationCustom03">
                                                <Form.Label>Role Name: </Form.Label>
                                                <Form.Control
                                                    required
                                                    type="text"
                                                    onChange={(event) => { this.changeRoleName(event) }}
                                                    value={role.role_name}
                                                />
                                                <Form.Control.Feedback>
                                                    Please fill the role name
                                                </Form.Control.Feedback>
                                            </Form.Group>
                                        </Form.Row>
                                        <Link to={`/Role`} style={{ marginLeft: '7%' ,marginRight: '5%' }} className="btn btn-outline-secondary">Cancel</Link>
                                        <Button className="button-grape" onClick={() => { this.updaterole() }}>Update</Button>
                                    </Form>
                                </Card.Body>
                            </Card>
                        </Col>
                    </Row>
                </Container>
                <Footer />
            </Fragment>
        );
    }
}

export default EditRole;