import React, { Component } from "react";
import Navigation from "../Navigation/Navigation";
import PropTypes from 'prop-types';
import { Button, Container, Card, Row, Col, Form } from "react-bootstrap";
import { Link, Redirect } from 'react-router-dom';
import { message } from 'antd';
import { checkRole } from '../Utils';
import Footer from "../Footer/Footer"
import { Formik } from 'formik';
import * as yup from 'yup';

class CreateRole extends Component {
    constructor(props) {
        super(props);
    
        this.state = {
            role_name: "",
            redirect: false
        };
    }
        // state = {
        //     role_name: '',
        //     redirect: false
        // };
        formRef = React.createRef();
        schema = yup.object({
            role_name: yup.string().required(),
        });

    createrole(val) {
        // console.log(val);
        // console.log(this.props);
        // this.setState({redirect: true});
        if (this.state.redirect) { return <Redirect to='/Role' />  }
        const { history } = this.props;
        if (history) {history.push('/Role');} 
        const token = localStorage.getItem('token');
        fetch(`http://149.129.255.54/api/create/role`, {
            method: "POST",
            body: JSON.stringify(val),
            headers: {
                "Authorization": `Bearer ${token}`,
                "Content-Type": "application/json"
            },
        }).then((response) => {
            response.json()
                .then((result) => {
                    console.warn("result", result);
                    if (result && result.role_name) {
                        this.props.history.push('/Role')
                        message.success("Role berhasil ditambahkan", 1)
                    } 
                    else {
                        message.error(result.msg, 1)
                    }
                })
        })
    }

    render() {
        if (!checkRole('admin').sukses) {
            return (
                <div>Tidak Bisa Mengakses</div>
            )
        }
        // const history = this.props;
        // const {redirect} = this.state;
        // if (this.state.redirect) { return <Redirect to='/Role' />  }
        // const handleChange = ({ target }) => {
        //     this.setState({ role_name: target.value });
        //  };
        return (
                <Formik
                validationSchema={this.schema}
                onSubmit={(values) => {this.createrole(values)}}
                // onSubmit={console.log}
                initialValues={{
                    role_name: this.state.role_name
                }}
                >
                {({
                    handleSubmit,
                    handleChange,
                    handleBlur,
                    values,
                    touched,
                    isValid,
                    errors,
                }) => (
                <Form noValidate onSubmit={handleSubmit}>
                <Navigation history={this.props.history}/>
                <Container fluid
                    style={{
                        height: "800px",
                        padding: "50px",
                        backgroundColor: "#f1f1f1"
                    }}>
                    <p style={{ marginLeft: '38%', color: '#000000' }}><Link to={`/Role`} style={{ color: '#000000' }}>Roles</Link> / <strong>Add Role</strong></p>
                            <Card
                                style={{
                                    width: '19rem',
                                    margin: 'auto'
                                }}
                            >
                                <p style={{ fontSize: '19px', marginTop: '5%', marginLeft: '3%', color: '#3722D3' }}>Add Role</p>
                                <hr style={{ border: '1px solid #3722D3', width: '95%', margin: 'auto', marginBottom: '5%'}} />                               
                                <Card.Body style={{ margin: 'auto' }}>
                                    <Form.Row>
                                        <Form.Group>
                                            <Form.Label>Role Name *</Form.Label>
                                            <Form.Control
                                                type="text"
                                                placeholder="Role Name"
                                                name="role_name"
                                                value={values.role_name}
                                                // onChange={(event) => { this.setState({ role_name: event.target.value }) }}
                                                onChange={handleChange}
                                                isInvalid={!!errors.role_name}
                                            />
                                            <Form.Control.Feedback type="invalid">
                                              {errors.role_name}
                                            </Form.Control.Feedback>
                                        </Form.Group>
                                    </Form.Row>
                                    <Link to={`/Role`} style={{ marginLeft: '6%' ,marginRight: '5%' }} className="btn btn-outline-secondary">Cancel</Link>
                                    <Button type="submit" className="button-grape" 
                                    // onClick={() => { this.createrole() }}
                                    >Create</Button>
                                </Card.Body>
                            </Card>
                       
                </Container>
                <Footer />
                </Form>
            )}
            </Formik>
        )
    }
}

export default CreateRole;