import React, { Component, Fragment } from 'react';
import '../../style/role.css'
import Role from './Roles'
import Navigation from "../Navigation/Navigation"
import { Button, Form } from "react-bootstrap"
import { unstable_renderSubtreeIntoContainer } from 'react-dom'
import { Link } from 'react-router-dom';
import { Container, Row, Col } from "react-bootstrap";
import {  message } from 'antd';
import _ from "lodash";
import Paginate from '../../components/Paginate/paginate';
import { checkRole, pageToLimitOffset } from '../Utils'
import Footer from "../Footer/Footer"

class role extends Component {

    state = {
        role: [],
        search: '',
        loading: false,
        pageNow: 1,
        count: 0
    }

    handleRemove(id) {
        console.log('handle remove')
        console.log(id);
        const token = localStorage.getItem('token');
        fetch(`${process.env.REACT_APP_BASEURL}/api/delete/role/${id}`, {
            method: 'DELETE',
            headers: {
                "Authorization": `Bearer ${token}`,
                "Content-Type": "application/json"
            },
        })
            .then(response => response.json())
            .then(json => {
                if (json.sukses) {
                    this.listTable(1);
                    message.success("Role berhasil dihapus", 1)
                }
            })
            .catch(error => console.log('error', error));
    }

    componentDidMount() {
        const page = this.props.match.params.page ? this.props.match.params.page : 1;
        if (checkRole('admin').sukses) {
            this.listTable(page);
        }
    }

    listTable(page) {
        console.log(`listTable page: ${page}`);
        const token = localStorage.getItem('token');
        // const limit = 5;
        // const offset = 0;
        const { limit, offset } = pageToLimitOffset(page);
        console.log(`limit: ${limit}`);
        console.log(`offset: ${offset}`);
        const search = this.state.search;
        this.setState({ loading: true });
        fetch(`${process.env.REACT_APP_BASEURL}/api/read/roles?limit=${limit}&offset=${offset}&search=${search}`, {
            method: 'GET',
            headers: {
                "Authorization": `Bearer ${token}`,
                "Content-Type": "application/json"
            },
        })
            .then(response => response.json())
            .then(json => {
                this.setState({
                    role: json.rows,
                    loading: false,
                    pageNow: page,
                    count: json.count
                })
            })
            .catch(error => console.log('error', error));
    }
    onChange = (event) => {
        /* signal to React not to nullify the event object */
        console.log('onChange triggered');
        event.persist();

        if (!this.debouncedFn) {
            this.debouncedFn = _.debounce(() => {
                //  let searchString = event.target.value;
                this.setState({ search: event.target.value });
                this.listTable(1);
            }, 1000);
        }
        this.debouncedFn();
    }

    ChangePage(page) {
        console.log(`Change page to: ${page}`)
        this.listTable(page);
    }

    render() {
        if (!checkRole('admin').sukses) {
            return (
                <div>Tidak Bisa Mengakses</div>
            )
        }
        return (
            <Fragment>
                <Navigation history={this.props.history}/>
                <Container fluid
                    style={{
                        minHeight: "800px",
                        padding: "50px",
                        backgroundColor: "#f1f1f1"
                    }}
                >
                    <Fragment>
                        <Row className="justify-content-center">
                            <Col lg="5"><h3>Roles</h3></Col>
                        </Row>
                        <Row className="justify-content-center">
                            <Col xs={8} md={{ span: 4, offset: 3 }}>
                                    <Form.Control
                                        placeholder="Search"
                                        style={{ borderRadius: '30px' }}
                                        type="text"
                                        value={this.state.role.id}
                                        onChange={this.onChange} debounce="1000"
                                    />
                            </Col>
                            <Col xs={4} md={4}>
                                <Link to={`/AddRole`}><Button className="button-grape" size="md">Add New</Button></Link>
                            </Col>
                        </Row>
                        <Role role={this.state.role} remove={(id) => this.handleRemove(id)} loading={this.state.loading} />
                        <Row className="justify-content-center">
                            <Col lg="1">
                                <Paginate pageNow={this.state.pageNow} count={this.state.count}
                                    changePage={(page) => this.ChangePage(page)}>
                                </Paginate>
                            </Col>
                        </Row>
                    </Fragment>
                </Container>
                <Footer />
            </Fragment >
        );

    }
}

export default role;