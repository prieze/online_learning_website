import React, { Fragment } from 'react';
import 'font-awesome/css/font-awesome.min.css';
import { Link } from "react-router-dom";
import { Row, Col, Table } from 'react-bootstrap';
import '../../style/role.css';
import { Popconfirm } from 'antd';

const Role = (props) => (

    <Row className="justify-content-center">
        <Col lg="5" style={{ marginTop: '2%' }} >
            <Table responsive hover>
                <thead style={{ backgroundColor: "#E3E0E0" }}>
                    <tr>
                        <th>Role ID</th>
                        <th>Role Name</th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {!props.loading ? (
                        props.role.length > 0 ? (
                            props.role.map(roles => (
                                <tr className="tr-role" key={roles.id}>
                                    <td>{roles.id}</td>
                                    <td>{roles.role_name}</td>
                                    <td><Link to={`/Roles/Role/${roles.id}`}><i className="fa fa-pencil-square-o" aria-hidden="true"></i></Link></td>
                                    <td>
                                        <Popconfirm
                                                title="Remove Role ?"
                                                onConfirm={() => { props.remove(roles.id) }}
                                                okText="Yes"
                                                cancelText="No"
                                            >
                                                <i style={{ cursor: 'pointer' }} className="fa fa-trash" aria-hidden="true"></i>
                                        </Popconfirm>
                                    </td>
                                </tr>
                            ))
                        ) : (
                                <tr>
                                    <td>
                                        No data
                                </td>
                                </tr>
                            )
                    ) : (
                            <tr>
                                <td>
                                    Loading ...
                            </td>
                            </tr>
                        )}
                </tbody>
            </Table>
        </Col>
    </Row>
)

export default Role;