import React, { Component } from 'react';
import Navigation from '../Navigation/Navigation'
import '../Category/Category.css'
import { checkRole } from '../Utils'
import './Dashboard.css'
import { Link } from "react-router-dom";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css'
import _ from "lodash";
import Footer from "../Footer/Footer";
import { Container, Row, Col, Nav, Form, Button, Card, CardDeck, Image, Navbar, FormControl, CardColumns } from "react-bootstrap";


class DashboardCategory extends Component {
    constructor(props) {
        super(props)
        this.state = {
            list: [],
            bestselling: [],
            loading: false,
            store: null,
            search: "",
            image: ""
        }
    }

    componentDidMount() {
        this.bestSelling()
    }

    bestSelling() {
        console.log(this.state);
        const token = localStorage.getItem("token");
        const search = this.state.search;
        this.setState({ loading: true });
        fetch(`${process.env.REACT_APP_BASEURL}/api/dashboard/category/best/selling?limit=16&offset=0&search=`, {
            method: "GET",
            headers: {
                Authorization: `Bearer ${token}`,
                "Content-Type": "application/json",
            },
        })
            .then((response) => response.json())
            .then((json) => {
                this.setState({
                    bestselling: json.data,
                    loading: false,
                });
            })
            .catch((error) => console.log("error", error));
    }

    onChange = (event) => {
        /* signal to React not to nullify the event object */
        event.persist();
        if (!this.debouncedFn) {
            this.debouncedFn = _.debounce(() => {
                //  let searchString = event.target.value;
                this.setState({ search: event.target.value });
                this.bestSelling();
            }, 1000);
        }
        this.debouncedFn();
    };


    render() {
        return (
            <div>
                <Navigation history={this.props.history} />
                <ToastContainer />
                <Container fluid
                    style={{
                        width: "100%",
                        minHeight: "800px",
                        padding: "50px",
                        backgroundColor: "#F1F1F1"
                    }}>
                    <Row className="justify-content-center">
                        <Col md={10}>
                            <Navbar expand="lg" style={{ background: "rgba(58, 44, 125, 0.34)", marginBottom: "3%" }}>
                                <Navbar.Brand style={{ color: "#FFFFFF", fontFamily: "Gordita-bold" }}>Best Selling</Navbar.Brand>
                                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                                <Navbar.Collapse id="basic-navbar-nav">
                                    <Nav className="mr-auto" />
                                    <Form inline>
                                        <FormControl type="text" id="search" placeholder="Search" style={{ borderRadius: "30px" }} onChange={this.onChange} debounce="1000" />
                                    </Form>
                                </Navbar.Collapse>
                            </Navbar>
                        </Col>
                    </Row>


                    <Row style={{ width: '85%', margin: 'auto' }}>
                        {
                            this.state.bestselling.map((list) => {
                                return <>
                                    <Col lg={12}>
                                        <Row className="justify-content-center">
                                            <Col lg={11} ><h5 style={{ fontFamily: 'Gordita-bold' }} >{list.category_name}</h5></Col>
                                            <Col ><Link style={{ marginRight: '5%' }} className="btn btn-outline-secondary" to={`/DashboardCourse/${list.id}`}>View</Link></Col>
                                        </Row>
                                    </Col><br/>
                                    {list?.courses.map((bestselling) => {
                                        return <><Card className="course-dashboard">
                                            <ToastContainer />
                                            <Card.Img variant="top" style={{ height: '150px', borderRadius: '10px 10px 0 0' }} src={bestselling.cover?.url} />
                                            <p hidden={bestselling.discount > "0" ? false : true} className="diskon">{bestselling.discount}% Off</p>
                                            <Card.Body>
                                                <Link to={`/DetailCourse/${bestselling.id}`}><Card.Title className="title-topic">{bestselling.course_name}</Card.Title></Link>
                                                <p className="icon-topic"><i className="fa fa-users"></i> {bestselling.count} <i className="fa fa-star"></i> 3.8</p>
                                                <Row>
                                                    <Col><Card.Text className="price-course-dash">{bestselling.price} IDR</Card.Text></Col>
                                                    <Col style={{ marginLeft: '-10%' }}><Card.Text style={{ fontSize: '10px', color: '#BAB9B9' }}><s>{bestselling.price} IDR</s></Card.Text></Col>
                                                </Row>
                                            </Card.Body>
                                        </Card>
                                    </>
                                    })}
                                </>
                            })
                        }
                        
                    <br />
                    </Row>
                        <br/><br/>

                </Container >
                <Footer />


            </div >
        )
    }
}

export default DashboardCategory;