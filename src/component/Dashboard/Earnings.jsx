import React, { Component } from 'react';
import { Bar, Line, Pie } from 'react-chartjs-2';
import Navigation from '../Navigation/Navigation'
import { ToastContainer, toast } from 'react-toastify';
import { Link } from 'react-router-dom';
import 'react-toastify/dist/ReactToastify.css';
import './Dashboard.css'
import Footer from '../Footer/Footer';
import { Container, Row, Col, Table, Image } from 'react-bootstrap';
import { DatePicker, Select } from 'antd';
import moment from 'moment';

class Earnings extends Component {
  constructor(props) {
    super(props);
    this.state = {
      chartData: {},
      date: [],
      list: [],
      instructor: [],
      instructor_id: 0,
      options: {
        title: {
          display: this.props.displayTitle,
          text: "Largest Cities In " + this.props.location,
          fontSize: 25,
        },
        legend: {
          display: this.props.displayLegend,
          position: this.props.legendPosition,
        },
        scales: {
          yAxes: [
            {
              ticks: {
                callback(value) {
                  return Number(value).toLocaleString("id");
                },
              },
            },
          ],
        },
        tooltips: {
          callbacks: {
            label: function (tooltipItem, data) {
              const dataLabel = data.textLabel[tooltipItem.index];
              const value = Number(
                data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]
              ).toLocaleString("id");
              return [dataLabel, value];
            },
          },
        },
      },
    };
    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount() {
    this.getEarningChartData();
    this.listEarnings();
    this.listInstructor();
  }

  getEarningChartData() {
    // Ajax calls here
    const token = localStorage.getItem("token");
    this.setState({ loading: true });
    fetch(`${process.env.REACT_APP_BASEURL}/api/dashboard/earnings-chart`, {
      method: "POST",
      body: JSON.stringify({
        start: this.state.date[0],
        end: this.state.date[1],
        instructor_id: this.state.instructor_id,
      }),
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      },
    })
      .then((response) => response.json())
      .then((json) => {
        const data = json.data;
        const labels = data.map((a) => a.mon);
        const textLabel = data.map((a) => a.mon_year_text);
        const dataChart = data.map((a) => a.earnings);
        console.log(data, labels, dataChart);
        this.setState({
          earningData: {
            labels: labels,
            textLabel: textLabel,
            datasets: [
              {
                label: "Earnings",
                data: dataChart,
                backgroundColor: [
                  "rgba(255, 99, 132, 0.6)",
                  "rgba(54, 162, 235, 0.6)",
                  "rgba(255, 206, 86, 0.6)",
                  "rgba(75, 192, 192, 0.6)",
                  "rgba(153, 102, 255, 0.6)",
                  "rgba(255, 159, 64, 0.6)",
                  "rgba(255, 99, 132, 0.6)",
                  "rgba(54, 162, 235, 0.6)",
                  "rgba(255, 206, 86, 0.6)",
                  "rgba(75, 192, 192, 0.6)",
                  "rgba(153, 102, 255, 0.6)",
                  "rgba(255, 159, 64, 0.6)",
                ],
              },
            ],
            backgroundColor: "#FFFFFF",
          },
        });
      })
      .catch((error) => console.log("error", error));
  }

  listEarnings() {
    const token = localStorage.getItem("token");
    this.setState({ loading: true });
    fetch(
      `${process.env.REACT_APP_BASEURL}/api/dashboard/earnings?limit=&offset=&search=`,
      {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      }
    )
      .then((response) => response.json())
      .then((json) => {
        this.setState({
          list: json.data.sort((a, b) => b.price - a.price),
          loading: false,
        });
        console.log(json.data.sort((a, b) => b.price - a.price));
      })
      .catch((error) => console.log("error", error));
  }

  listInstructor() {
    const token = localStorage.getItem("token");

    this.setState({ loading: true });
    fetch(
      `${process.env.REACT_APP_BASEURL}/api/course/master/instructors/user`,
      {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      }
    )
      .then((response) => response.json())
      .then((json) => {
        this.setState({
          instructor: json.rows,
          loading: false,
        });
        console.log("json" + JSON.stringify(json));
        console.log(this.state.instructor);
      })
      .catch((error) => console.log("error", error));
  }
  onChange = async (value, dateString) => {
    await this.setState({ date: dateString });
    this.getEarningChartData();
    console.log(this.state.date);
    console.log(this.state.date[0]);
    console.log(typeof this.state.date[1]);
    console.log(value);
    console.log(dateString);
    console.log(typeof dateString);
  };

  handleChange = async (e) => {
    await this.setState({  instructor_id: e  });;
    console.log(this.state);
    this.getEarningChartData();
  };

  render() {
    const { RangePicker } = DatePicker;
    const dateFormat = "YYYY-MM-DD";
    const { Option } = Select;
    return (
      <>
        <Navigation history={this.props.history} />
        <Container
          fluid
          style={{
            width: "100%",
            minHeight: "800px",
            paddingTop: "50px",
            backgroundColor: "#F1F1F1",
          }}
        >
          <ToastContainer />

          <Row style={{ width: "85%" }}>
            <Col
              style={{
                fontFamily: "Gordita-bold",
                fontSize: "26px",
                color: "#392C7E",
                marginLeft: "120px",
              }}
            >
              Earnings
            </Col>
            <Col>
              <Select
                style={{ width: 200, backgroundColor: "#e5e5e5" }}
                defaultValue=""
                onChange={(e) => {
                  this.handleChange(e);
                }}
              >
                <option value="">All Instructor</option>
                {this.state.instructor.map((inst) => {
                  return (
                    <Option value={inst.id}>{inst.instructor_name}</Option>
                  );
                })}
              </Select>
            </Col>
            <Col>
              <RangePicker
                style={{ backgroundColor: "#e5e5e5" }}
                format={dateFormat}
                onChange={this.onChange}
              />
            </Col>
          </Row>
          <br />
          <Row style={{ width: "85%", margin: "auto" }}>
            <Bar
              data={this.state.earningData}
              width={100}
              height={50}
              options={this.state.options}
            />
          </Row>
          <br />
          <br />

          <Row className="justify-content-md-center">
            <Col>
              <Table
                responsive
                hover
                style={{ width: "85%", margin: "auto", background: "#FFFFFF" }}
              >
                <thead style={{ fontFamily: "gordita-bold" }}>
                  <tr>
                    <th>Course</th>
                    <th>Price</th>
                    <th>Revenue</th>
                  </tr>
                </thead>
                {this.state.list.map((list) => {
                  return (
                    <tr>
                      <td>
                        <td>
                          <tr>
                            <Image
                              style={{ width: "109px", height: "63px" }}
                              src={list.cover?.url}
                            />
                          </tr>
                        </td>
                        <td>
                          <tr style={{ fontFamily: "gordita-bold" }}>
                            {list.course_name}
                          </tr>
                          <tr>{list.count}</tr>
                        </td>
                      </td>
                      <td>
                        <tr>{list.price}</tr>
                      </td>
                      <td>
                        <tr>{list.revenue}</tr>
                      </td>
                    </tr>
                  );
                })}
              </Table>
            </Col>
          </Row>

          <br />
          <br />
        </Container>
        <Footer />
      </>
    );
  }
}

export default Earnings;
