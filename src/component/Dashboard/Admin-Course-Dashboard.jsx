import React, { Component } from "react";
import Navigation from "../Navigation/Navigation";
import Plus from "../../gambar/plus.png";
import { Link } from "react-router-dom";
import Footer from "../Footer/Footer";
import { checkRole } from "../Utils";
import './Dashboard.css'
import { ToastContainer, toast } from "react-toastify";
import { CSSTransitionGroup } from 'react-transition-group'
import "react-toastify/dist/ReactToastify.css";
import _ from "lodash";
import { Container, Row, Col, Nav, Form, Button, Card, CardDeck, Image, Navbar, FormControl, Modal } from "react-bootstrap";

class DashboardCourse extends Component {
  constructor(props) {
    super(props);
    this.state = {
      course: [],
      loading: false,
      id: 0,
      store: null,
      search: "",
      display: false,
    };
  }

  componentDidMount() {
    const token = localStorage.getItem("token");
    const id = this.props.match.params.id;
    console.log(id);
    console.log(this.props);
    // this.setState({
    //   course: [],
    //   id: id,
    // });
    // if (checkRole("admin").sukses) {
    console.log(id);
    this.listCourse(id);
    // }
  }

  listCourse(catId = "0") {
    console.log(this.state);
    const token = localStorage.getItem("token");
    const search = this.state.search;
    console.log(typeof catId);
    let url = `${process.env.REACT_APP_BASEURL}/api/dashboard/best/selling?limit=12&offset=0&search=${search}`;
    if (catId !== "0") {
      console.log("here");
      url = `${process.env.REACT_APP_BASEURL}/api/dashboard/best/selling/${catId}?limit=12&offset=0&search=${search}`;
    } else {
      url = `${process.env.REACT_APP_BASEURL}/api/dashboard/best/selling?&limit=12&offset=0&search=${search}`;
    }
    this.setState({ loading: true });
    fetch(url, {
      method: "GET",
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      },
    })
      .then((response) => response.json())
      .then((json) => {
        this.setState({
          course: json.data,
          loading: false,
        });
        console.log(this.state.cover_photo);
        console.log(this.state.course);
        console.log(json.data);
        console.log(json.data.course_name);
      })
      .catch((error) => console.log("error", error));
  }

  onChange = (event) => {
    /* signal to React not to nullify the event object */
    event.persist();
    if (!this.debouncedFn) {
      this.debouncedFn = _.debounce(() => {
        //  let searchString = event.target.value;
        this.setState({ search: event.target.value });
        this.listCourse();
      }, 1000);
    }
    this.debouncedFn();
  };


  render() {
    return (
      <>
        <Navigation history={this.props.history} />
        <Container fluid style={{ width: "100%", minHeight: "800px", paddingTop: "50px", backgroundColor: "#F1F1F1" }}>
          <ToastContainer />
          <Row className="justify-content-center">
            <Col md={10}>
              <Navbar expand="lg" style={{ background: "rgba(58, 44, 125, 0.34)", marginBottom: "3%" }}>
                <Navbar.Brand style={{ color: "#FFFFFF", fontFamily: "Gordita-bold" }}>Course</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                  <Nav className="mr-auto" />
                  <Form inline>
                    <FormControl type="text" id="search" placeholder="Search" style={{ borderRadius: "30px" }} onChange={this.onChange} debounce="1000" />
                  </Form>
                </Navbar.Collapse>
              </Navbar>
            </Col>
          </Row>

          

          <Row style={{ width: '85%', margin: 'auto' }}>

            {this.state.course.courses?.map((list) => {
              return (
                <Card className="course-dashboard">
                  <ToastContainer />
                  <Card.Img variant="top" style={{ height: '150px', borderRadius: '10px 10px 0 0' }} src={list.cover?.url} />
                  <p hidden={list.discount > "0" ? false : true} className="diskon">{list.discount}% Off</p>
                  <Card.Body>
                    <Link to={`/DetailCourse/${list.id}`}><Card.Title className="title-topic">{list.course_name}</Card.Title></Link>
                    <p className="icon-topic"><i className="fa fa-users"></i> {list.count} <i className="fa fa-star"></i> 3.8</p>
                    <Row>
                      <Col><Card.Text className="price-course-dash">{list.price} IDR</Card.Text></Col>
                      <Col style={{ marginLeft: '-10%' }}><Card.Text style={{ fontSize: '10px', color: '#BAB9B9' }}><s>{list.price} IDR</s></Card.Text></Col>
                    </Row>
                  </Card.Body>
                </Card>
              );
            })}


          </Row>
          {/* </div> */}

          <br /><br />
        </Container>
        <Footer />
      </>
    );
  }
}

export default DashboardCourse;