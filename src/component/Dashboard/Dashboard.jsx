import React, { Component } from 'react';
import { checkRole } from '../Utils'
import AdminDashboard from './Admin-Dashboard'

class Dashboard extends Component {
    render() {

        if (checkRole('admin').sukses) {
            return (
                <AdminDashboard />
            )
        }
    }
}
export default Dashboard;