import React, { Component } from 'react';
import { Bar, Line, Pie } from 'react-chartjs-2';
import { Link } from 'react-router-dom';
import { Container, Row, Col, Card, Nav, Image } from 'react-bootstrap';
import { DatePicker } from 'antd';
import moment from 'moment';

class Chart extends Component {

  constructor(props) {
    super(props);
    this.state = {
      date: [],
      chartData: {},
      labels: ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"],
      optionsEarnings: {
				title: {
					display: this.props.displayTitle,
					text: 'Largest Cities In ' + this.props.location,
					fontSize: 25
				},
				legend: {
					display: this.props.displayLegend,
					position: this.props.legendPosition
				},
				scales: {
					yAxes: [{
						ticks: {
							callback(value) {
								return Number(value).toLocaleString('id')
							}
						}
					}]
				},
				tooltips: {
					callbacks: {
						label: function(tooltipItem, data) {
							const dataLabel = data.textLabel[tooltipItem.index];
							const value = Number(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]).toLocaleString('id');
							return [dataLabel, value];
						}
					}
				},
      },
    }
  }

  componentWillMount() {
    this.getVisitorChartData();
    this.getEarningChartData()
  }

  getVisitorChartData() {
    const token = localStorage.getItem('token');
		this.setState({ loading: true });
		fetch(`${process.env.REACT_APP_BASEURL}/api/dashboard/visitors-chart`, {
      method: 'POST',
      body: JSON.stringify({
				// start: this.state.date[0],
				// end: this.state.date[1]
			}),
			headers: {
				"Authorization": `Bearer ${token}`,
				"Content-Type": "application/json"
			},
		})
    .then(response => response.json())
    .then(json => {
      const data = json.data;
      const labels = data.map(a => a.mon);
      const textLabel = data.map(a => a.mon_year_text);
      const dataChart = data.map(a => a.user_session);
      this.setState({
        chartData: {
          labels: textLabel,
          datasets: [
            {
              label: 'Web Visitor',
              data: dataChart,
              backgroundColor: '',
              borderColor: '#E3B898',
              borderWidth: 2,
              hoverBackgroundColor: '#A599FF',
              hoverBorderColor: '#A599FF',
            }
          ]
        },
      });
    })
  }

  getEarningChartData() {
		// Ajax calls here
		const token = localStorage.getItem('token');
		this.setState({ loading: true });
		fetch(`${process.env.REACT_APP_BASEURL}/api/dashboard/earnings-chart`, {
      method: 'POST',
      body: JSON.stringify({
				start: this.state.date[0],
				end: this.state.date[1]
			}),
			headers: {
				"Authorization": `Bearer ${token}`,
				"Content-Type": "application/json"
			},
		})
			.then(response => response.json())
			.then(json => {
				const data = json.data;
				const labels = data.map(a => a.mon);
				const textLabel = data.map(a => a.mon_year_text);
				const dataChart = data.map(a => a.earnings);
        console.log(data, labels, dataChart);
				this.setState({
					earningData: {
						labels: labels,
						textLabel: textLabel,
						datasets: [
							{
								label: 'Earnings',
								data: dataChart,
								backgroundColor: [
									'rgba(255, 99, 132, 0.6)',
									'rgba(54, 162, 235, 0.6)',
									'rgba(255, 206, 86, 0.6)',
									'rgba(75, 192, 192, 0.6)',
									'rgba(153, 102, 255, 0.6)',
									'rgba(255, 159, 64, 0.6)',
									'rgba(255, 99, 132, 0.6)',
									'rgba(54, 162, 235, 0.6)',
									'rgba(255, 206, 86, 0.6)',
									'rgba(75, 192, 192, 0.6)',
									'rgba(153, 102, 255, 0.6)',
									'rgba(255, 159, 64, 0.6)'
								]
							}
						],
						backgroundColor: '#FFFFFF',
					}
				}); 
			})
			.catch(error => console.log('error', error));
  }

  onChange = async(value, dateString) => {
		await this.setState({date: dateString})
		this.getEarningChartData()
		console.log(this.state.date);
		console.log(this.state.date[0]);
		console.log(typeof(this.state.date[1]));
		console.log(value);
		console.log(dateString);
		console.log(typeof(dateString));
	}

  render() {
    const {RangePicker} = DatePicker;
    const dateFormat = 'YYYY-MM-DD';
    return (
      <div>

        <Row style={{ width: '85%' }}>
         <Col style={{ fontFamily: 'Gordita-bold', fontSize: '26px', color: '#392C7E', marginLeft: '120px' }}>Earnings</Col> 
          <Link style={{ fontFamily: 'Gordita', color: '#002333', paddingLeft: '3rem' }} to={`/Earnings`}>View Earnings</Link>
        </Row>
        <br/>
        <Row style={{ width: '85%', margin: 'auto' }}>
          <Col>
            <Card body style={{ padding: '2rem' }}>
              <Row>
                <Col md={{ span: 3, offset: 9}}  >
                  <RangePicker style={{ backgroundColor: '#e5e5e5' }} picker={dateFormat} onChange={this.onChange} />
                </Col>
              </Row>
              <Bar
                data={this.state.earningData}
                width={100}
                height={50}
                options={this.state.optionsEarnings}
              />
            </Card>
          </Col>     
        </Row>
        <br/><br/>


        <Row style={{ width: '85%', fontFamily: 'Gordita-bold', fontSize: '26px', color: '#392C7E', marginLeft: '120px' }}>
          Web Visitor
        </Row>
        <br/>
        <Row style={{ width: '85%', margin: 'auto' }}>
        <Line
          data={this.state.chartData}
          width={50}
          height={30}
        />
        </Row>

      </div>
    )
  }
}

export default Chart;
