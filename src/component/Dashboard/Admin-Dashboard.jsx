import React, { Component } from 'react';
import Navigation from '../Navigation/Navigation'
import { ToastContainer, toast } from 'react-toastify';
import { Link } from 'react-router-dom';
import 'react-toastify/dist/ReactToastify.css';
import './Dashboard.css'
import dashboard1 from "../../gambar/dashboard1.png";
import dashboard2 from "../../gambar/dashboard2.png";
import cx from 'classnames';
import VisitorChart from "./Visitor.jsx"
import Footer from '../Footer/Footer';
import { Container, Row, Col, Card, Nav, Image, Button } from 'react-bootstrap';
import DashboardCourse from './Admin-Category-Dashboard';
import BestSellingAdm from "./BestSellingList-Admin";

class AdminDashboard extends Component {
    constructor(props) {
        super(props);

        this.state = {
            student: [],
            instructor: [],
            course: [],
            id: 0,
            category: [],
            course: [],
            categories: [],
            newCourse: [],
            bestSelling: [],
            bestSellings: [],
            catId: 0,
            next: 0,
            more: 0,
            loading: false,
            display: false,
            more: 0,
            morebest: 0
        };
    }


    componentDidMount() {
        // this.cardCount();
        this.listCategory();
        this.bestSelling();
        this.count()
    }

    componentDidUpdate(prevProps) {
        // console.log(this.props);
        if (prevProps.selected !== this.props.selected) {
            this.setState({ selected: this.props.selected })
        }
    }

    count() {
        const token = localStorage.getItem('token');
        this.setState({ loading: true });
        fetch(`${process.env.REACT_APP_BASEURL}/api/dashboard/count`, {
            method: 'GET',
            headers: {
                "Authorization": `Bearer ${token}`,
                "Content-Type": "application/json"
            },
        })
            .then((response) => response.json())
            .then((json) => {
                this.setState({
                    student: json.students,
                    instructor: json.instructors,
                    course: json.courses,
                    category: json.categories,
                    loading: false,
                });
            })
            .catch(error => console.log('error', error));

    }


    listCategory() {
        const token = localStorage.getItem('token');
        this.setState({ loading: true });
        fetch(`${process.env.REACT_APP_BASEURL}/api/read/categories?limit=6&offset=0&search=`, {
            method: 'GET',
            headers: {
                "Authorization": `Bearer ${token}`,
                "Content-Type": "application/json"
            },
        })
            .then(response => response.json())
            .then(json => {
                this.setState({
                    categories: json.rows,
                    loading: false,
                })
                localStorage.setItem('categories', JSON.stringify(json.rows))
                // console.log(this.state.categories);
            })
            .catch(error => console.log('error', error));
    }



    // bestSelling() {

    //     const token = localStorage.getItem("token");
    //     const id = this.state.catId;
    //     const next = this.state.next;
    //     console.log(id);
    //     let url = `${process.env.REACT_APP_BASEURL}/api/dashboard/best/selling?limit=4&offset=${next}&search=`;
    //     if (id !== 0) {
    //         url = `${process.env.REACT_APP_BASEURL}/api/dashboard/best/selling/${id}?limit=4&offset=${next}&search=`;
    //     }
    //     // arrow offset nambah 4 jadi 0 + 4 = 4 offset 4 dan seterusnya
    //     this.setState({ loading: true });
    //     console.log(url);
    //     fetch(url, {
    //         method: "GET",
    //         headers: {
    //             Authorization: `Bearer ${token}`,
    //             "Content-Type": "application/json",
    //         },
    //     })
    //         .then((response) => response.json())
    //         .then((json) => {
    //             if (id !== 0) {
    //                 const temp = [];
    //                 const discount = json.data.courses.map((disc)=>{
    //                     const price = parseInt(disc.price);
    //                     const dic = parseInt(disc.discount);
    //                     const jum = (dic/100)*price;
    //                     const tot = price-parseInt(jum);
    //                     temp.push({
    //                         ...disc,
    //                         hargadisc: tot,
    //                         category_name: json.data.category_name,
    //                         id: json.data.id
    //                     })
    //                     return tot
    //                 });
    //                 console.log(`temp`, temp);
    //                 console.log(`discount`, discount);
    //                 this.setState({
    //                     // bestSelling: json.data,
    //                     bestSelling: temp,
    //                     more: json.data.courses.length,
    //                     loading: false,
    //                 });
    //                 console.log(`with id`, this.state.bestSelling);
    //                 // console.log(`id !== 0`,json.data);
    //             } else {
    //                 const temp = [];
    //                 const discount = json.data.map((disc)=>{
    //                     const price = parseInt(disc.price);
    //                     const dic = parseInt(disc.discount);
    //                     const jum = (dic/100)*price;
    //                     const tot = price-parseInt(jum);
    //                     temp.push({
    //                     ...disc,
    //                     hargadisc: tot
    //                     })
    //                     return tot
    //                 });
    //                 console.log(`temp`, temp);
    //                 console.log(`discount`, discount);
    //                 this.setState({
    //                     // bestSellings: json.data,
    //                     bestSellings: temp,
    //                     more: json.data.length,
    //                     loading: false,
    //                 });
    //                 // console.log(json.data);
    //                 console.log(`no id`, this.state.bestSellings);
    //             }
    //             // console.log(json.data);
    //             // console.log(this.state.bestSelling);
    //             // console.log(this.state.bestSellings);
    //         })
    //         .catch((error) => console.log("error", error));
    // }

    bestSelling() {

        const token = localStorage.getItem("token");
        const id = this.state.catId;
        const next = this.state.next;
        let url = `${process.env.REACT_APP_BASEURL}/api/dashboard/best/selling?limit=4&offset=${next}&search=`;
        if (id !== 0) {
          url = `${process.env.REACT_APP_BASEURL}/api/dashboard/best/selling/${id}?limit=4&offset=${next}&search=`;
        }
        this.setState({ loading: true });
        console.log(url);
        fetch(url, {
          method: "GET",
          headers: {
            Authorization: `Bearer ${token}`,
            "Content-Type": "application/json",
          },
        })
          .then((response) => response.json())
          .then((json) => {
            if (id !== 0) {
              // this.setState({
              //   bestSelling: json.data,
              //   more: json.data.courses.length,
              //   loading: false,
              // });
              // console.log(json.data.courses.length);
              const obj = {category_name: json.data.category_name, courses: [] ,id: json.data.id};
              const temp = [];
              const discount = json.data.courses.map((disc)=>{
                const price = parseInt(disc.price);
                const dic = parseInt(disc.discount);
                const jum = (dic/100)*price;
                const tot = price-parseInt(jum);
                temp.push({
                  ...disc,
                  hargadisc: tot,
                //   category_name: json.data.category_name,
                //   id: json.data.id
                })
                return tot
              });
              temp.push(discount);
              obj.courses.push(temp)
              console.log(`temp`, temp);
              this.setState(prevstate => ({
                ...prevstate,
                bestSellings: temp,
                morebest: json.data.courses.length,
                loading: false,
              }));
              // this.state.bestSelling.courses.forEach(function(jum, index) {
              //   jum.hargadisc = discount[index]
              //   // console.log(typeof(jum));
              //   // console.log(typeof(discount));
              //   // console.log(index);
              // })
              console.log(`best selling disc `,obj);
              console.log(`state best `,this.state);
              console.log(`JSON Data `,json.data);
            } else {
              // this.setState({
              //   bestSellings: json.data,
              //   morebest: json.data.length,
              //   loading: false,
              // });
              const temp = [];
              const discount = json.data.map((disc)=>{
                const price = parseInt(disc.price);
                const dic = parseInt(disc.discount);
                const jum = (dic/100)*price;
                const tot = price-parseInt(jum);
                temp.push({
                  ...disc,
                  hargadisc: tot
                })
                return tot
              });
              this.setState({
                bestSelling: temp,
                morebest: json.data.length,
                loading: false,
              })
              // this.state.bestSellings.forEach(function(jum, index) {
              //   jum.hargadisc = discount[index]
              //   // console.log(typeof(jum));
              //   // console.log(typeof(discount));
              //   // console.log(index);
              // })
              console.log(`best selling disc `,temp);
              console.log(`state best `,this.state);
            //   console.log(`state bests `,this.state.bestSellings);
            }
            // console.log(`best selling`,json.data);
            // console.log(this.state.categories);
            // console.log(this.state.bestSellings);
          })
          .catch((error) => console.log("error", error));
      }

    klikcat = async (id) => {
        // if (id === 0) {
        //   await this.setState({ catId: null });
        // } else {
        // }
        await this.setState({ catId: id, next: 0 });
        console.log(this.state.catId);
        this.bestSelling();
    }

    nextarr = async (next) => {
        const lanjut = this.state.next + next
        await this.setState({ next: lanjut });
        console.log(this.state.next);
        console.log(lanjut);
        this.bestSelling();
    }

    prevarr = async (prev) => {
        const lanjut = this.state.next - prev
        if (this.state.next === 0) { } else {
            await this.setState({ next: lanjut });
        }
        console.log(this.state.next);
        console.log(lanjut);
        this.bestSelling();
    }


    render() {
        return (
            <>
                <Navigation history={this.props.history} />
                <Container fluid
                    style={{
                        width: "100%",
                        minHeight: "800px",
                        paddingTop: "50px",
                        backgroundColor: "#F1F1F1"
                    }}
                >
                    <ToastContainer />
                    <Row style={{ width: '85%', margin: 'auto' }}>
                        <Col xs={12} sm={6} md={3} lg={3}>
                            <Card className="card-dash" style={{ backgroundColor: '#BFC7FE' }}>
                                <Card.Body >
                                    <Row style={{ margin: 'auto' }}>
                                        <Col md={3}>
                                            <Image className="dashboardpict" src={dashboard1} />
                                        </Col>
                                        <Col style={{ margin: '20px' }}>
                                            {
                                                this.state.student.map((student) => {
                                                    return <Card.Title style={{ fontFamily: 'Gordita-bold', color: '#000000', fontSize: '30px' }}>{student.count}</Card.Title>
                                                })}
                                            <Card.Text>Students</Card.Text>
                                        </Col>
                                    </Row>
                                </Card.Body>
                            </Card>
                        </Col>
                        <Col xs={12} sm={6} md={3} lg={3}>
                            <Card className="card-dash" style={{ backgroundColor: '#FFC5AB' }}>
                                <Card.Body>
                                    <Row style={{ margin: 'auto' }}>
                                        <Col md={3}>
                                            <Image className="dashboardpict" src={dashboard1} />
                                        </Col>
                                        <Col style={{ margin: '20px' }}>
                                            {
                                                this.state.instructor.map((instructor) => {
                                                    return <Card.Title style={{ fontFamily: 'Gordita-bold', color: '#000000', fontSize: '30px' }}>{instructor.count}</Card.Title>
                                                })}
                                            <Card.Text>Instructor</Card.Text>
                                        </Col>
                                    </Row>
                                </Card.Body>
                            </Card>
                        </Col>
                        <Col xs={12} sm={6} md={3} lg={3}>
                            <Card className="card-dash" style={{ backgroundColor: '#FFC963' }}>
                                <Card.Body>
                                    <Row style={{ margin: 'auto' }}>
                                        <Col md={3}>
                                            <Image className="dashboardpict" src={dashboard2} />
                                        </Col>
                                        <Col style={{ margin: '20px' }}>
                                            {
                                                this.state.course.map((course) => {
                                                    return <Card.Title style={{ fontFamily: 'Gordita-bold', color: '#000000', fontSize: '30px' }}>{course.count}</Card.Title>
                                                })}
                                            <Card.Text>Courses</Card.Text>
                                        </Col>
                                    </Row>
                                </Card.Body>
                            </Card>
                        </Col>
                        <Col xs={12} sm={6} md={3} lg={3}>
                            <Card className="card-dash" style={{ backgroundColor: '#FFE3BD' }}>
                                <Card.Body>
                                    <Row style={{ margin: 'auto' }}>
                                        <Col md={3}>
                                            <Image className="dashboardpict" src={dashboard1} />
                                        </Col>
                                        <Col style={{ margin: '20px' }}>
                                            {
                                                this.state.category.map((category) => {
                                                    return <Card.Title style={{ fontFamily: 'Gordita-bold', color: '#000000', fontSize: '30px' }}>{category.count}</Card.Title>
                                                })}
                                            <Card.Text>Categories</Card.Text>
                                        </Col>
                                    </Row>
                                </Card.Body>
                            </Card>
                        </Col>
                    </Row>
                    <br />

                    <VisitorChart />
                    <br />

                  <Row className="justify-content-center" style={{ marginTop: "5%" }} >
                    <h3 style={{ fontFamily: "Gordita-bold", color: "#392C7E" }} >
                      Best Selling
                    </h3>
                  </Row>

                  <Row className="justify-content-center" style={{ marginTop: "1%" }} >
                    <Col lg={8}>
                      <Nav className="justify-content-center" activeKey="/home">
                        <div style={{ cursor: 'pointer', color: '#002333', fontSize: '16px', fontFamily: 'Gordita-bold', paddingLeft: '2rem' }} onClick={() => { this.klikcat(0) }}>All Categories</div>
                        {this.state.categories.map((categories) => {
                          return (
                            <div style={{ cursor: 'pointer', color: '#002333', fontSize: '16px', fontFamily: 'Gordita-bold', paddingLeft: '2rem' }} onClick={() => { this.klikcat(categories.id) }}>{categories.category_name}</div>
                          );
                        })}
                      </Nav>
                    </Col>
                    <Col lg={2}>
                      {/* <div onClick={() => { this.changeURL(`/BestSelling`); }} className="btn btn-outline-secondary" > */}
                      <div onClick={() => { this.props.history.push({  pathname: '/BestSelling', data: { state: this.state.categories } }); }} className="btn btn-outline-secondary" >
                        View All
                          </div>
                    </Col>
                  </Row>

                  <Row style={{ width: "80%", margin: "auto" }}>
                    {(this.state.catId)
                      ? this.state.bestSellings?.courses?.map((list) => {
                        // console.log("ada id");
                        return (
                          <Col style={{ paddingBottom: "15%" }} xs={12} sm={6} md={4} lg={3} xl={3} >
                            <BestSellingAdm id={list.id} key={list.id} test={list} history={(url) => { this.changeURL(url); }} ></BestSellingAdm>
                          </Col>
                        );
                      })
                      : this.state.bestSelling.map((list) => {
                        // console.log("gak ada id");
                        return (
                          <Col style={{ paddingBottom: "15%" }} xs={12} sm={6} md={4} lg={3} xl={3} >
                            <BestSellingAdm id={list.id} key={list.id} test={list} history={(url) => { this.changeURL(url); }} ></BestSellingAdm>
                          </Col>
                        );
                      })
                    }
                  </Row>
                {console.log(`state more`,this.state.more)}
              {(this.state.more > 3)
                ?<Row style={{ width: "95%", margin: "auto", marginTop: "-21%" }}>
                  <Col>
                    <Button variant="light" disabled={this.state.prevs} style={{ width: "60px", height: "60px", borderRadius: "50px", boxShadow: "4px 4px 20px rgba(0, 0, 0, 0.2)" }} onClick={() => { this.prevarr(4) }}>
                      <i style={{ color: "#3B2881" }} className="fa fa-arrow-left" ></i>
                    </Button>
                  </Col>
                  <Col>
                    <Button variant="light" style={{ width: "60px", height: "60px", borderRadius: "50px", boxShadow: "4px 4px 20px rgba(0, 0, 0, 0.2)", marginLeft: "90%", }} onClick={() => { this.nextarr(4) }} >
                      <i style={{ color: "#3B2881" }} className="fa fa-arrow-right" ></i>
                    </Button>
                  </Col>
                </Row>
                :<Row style={{ width: "95%", margin: "auto", marginTop: "-21%" }}>
                  <Col>
                    <Button variant="light" disabled style={{ width: "60px", height: "60px", borderRadius: "50px", boxShadow: "4px 4px 20px rgba(0, 0, 0, 0.2)" }} onClick={() => { this.prevarr(4) }}>
                      <i style={{ color: "#3B2881" }} className="fa fa-arrow-left" ></i>
                    </Button>
                  </Col>
                  <Col>
                    <Button variant="light" disabled style={{ width: "60px", height: "60px", borderRadius: "50px", boxShadow: "4px 4px 20px rgba(0, 0, 0, 0.2)", marginLeft: "90%", }} onClick={() => { this.nextarr(4) }} >
                      <i style={{ color: "#3B2881" }} className="fa fa-arrow-right" ></i>
                    </Button>
                  </Col>
                </Row>
              }

                   
                    <br /><br /><br /><br /><br /><br />
                </Container>
                <Footer />
            </>
        )
    }

}

export default AdminDashboard;