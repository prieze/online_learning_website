import React, { Component, Fragment, } from 'react';
import { Navbar, Nav, NavDropdown, Form, Button, Row, Col, Card, Image, Container, Modal } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../style/styles.css';
import 'font-awesome/css/font-awesome.min.css';
import { Link } from 'react-router-dom';
import Shop from "../gambar/shop.png";
import { BrowserRouter as Router, Redirect, NavLink, withRouter } from "react-router-dom";
import GoogleLogin from 'react-google-login'
import Footer from "./Footer/Footer"
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
// import ForgotPassword from '../component/ForgotPassword/ForgotPassword';
import Dashboard from './Dashboard/Dashboard';
import Confirm from "./Email/Confirm";
import { message } from 'antd';
import { resolveOnChange } from 'antd/lib/input/Input';
import { Formik } from "formik";
import * as yup from 'yup';

const validEmailRegex = RegExp(/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i);
const validateForm = errors => {
    let valid = true;
    Object.values(errors).forEach(val => val.length > 0 && (valid = false));
    return valid;
};


class Login extends Component {

    constructor(props) {
        super(props)
        this.state = {
            course: [],
            modalLoginIsOpen: this.props.modalLoginIsOpen,
            modalSignIsOpen: true,
            email: null,
            password: null,
            compass: null,
            first_name: null,
            last_name: null,
            login: false,
            register: false,
            redirect: false,
            store: null,
            message: null,
            forgot: false
        };
        this.onOpenSignModal = this.onOpenSignModal.bind(this);
    }

    Logo = 'https://galilagi-media.oss-ap-southeast-5.aliyuncs.com/assets/logo.png';
    Desktop = 'https://galilagi-media.oss-ap-southeast-5.aliyuncs.com/assets/banner-main.png';
    User1 = 'https://galilagi-media.oss-ap-southeast-5.aliyuncs.com/assets/user1.png'

    onOpenLoginModal = () => {
        this.setState({ modalLoginIsOpen: this.props.modalLoginIsOpen, modalSignIsOpen: false, message: null });
    };

    onOpenSignModal = () => {
    //   this.setState({
    //     modalSignIsOpen: this.props.modalSignIsOpen,
    //     modalLoginIsOpen: false,
    //     message: null,
    //   });
        this.props.modalSignIsOpen(this.state.modalSignIsOpen);
    };;

    onCloseLoginModal = () => {
        // this.setState({ modalLoginIsOpen: false, message: null });
        this.props.onClose();
    };

    onForgotPassword = () => {
        this.setState({ modalLoginIsOpen: false, message: null });
        // this.setState({ forgot: true });
        this.changeURL('/ForgotPassword');
    };

    changeURL(url) {
        this.setState({ redirect: url });
    }

    onCloseSignModal = () => {
        this.setState({ modalSignIsOpen: false, message: null });
    };

    responseGoogle = (response) => {
        console.log(response);
        console.log(response.profileObj);

    }

    componentDidMount() {
        console.log(this.props);
        // this.listCourse();
        // console.log(this.state.redirect);
        // const token = localStorage.getItem('token');
        // if (token) {
        //     this.setState({ login: true });
        //     console.log(this.props);
        //     this.props.history.push('/Dashboard');
        //     // this.setState({ redirect: "/Dashboard" });
        // }
    }

    listCategory = (() => {
        return new Promise ((resolve, reject) => {
            const token = localStorage.getItem('token');
            this.setState({ loading: true });
            fetch(`${process.env.REACT_APP_BASEURL}/api/read/categories?limit=6&offset=0&search=`, {
                method: 'GET',
                headers: {
                    "Authorization": `Bearer ${token}`,
                    "Content-Type": "application/json"
                },
            })
                .then(response => response.json())
                .then(json => {
                    localStorage.setItem('categories',JSON.stringify(json.rows))
                    resolve(true);
                })
                .catch(error => reject(error));
        })
    })

    listCourse(catId) {
        const token = localStorage.getItem("token");
        let url = `${process.env.REACT_APP_BASEURL}/api/read/courses?limit=3&offset=0&search=`;
        if (catId) {
          url = `${process.env.REACT_APP_BASEURL}/api/read/course/category/${catId}?limit=3&offset=0&search=`;
        }
        
        this.setState({ loading: true });
        fetch(url, {
          method: "GET",
          headers: {
            Authorization: `Bearer ${token}`,
            "Content-Type": "application/json",
          },
        })
          .then((response) => response.json())
          .then((json) => {
            this.setState({
              course: json.rows,
              loading: false,
            });
          })
          .catch((error) => console.log("error", error));
      }

    redirect(){
        return <Redirect to="/Dashboard" />
    } 
    login() {
        fetch(`${process.env.REACT_APP_BASEURL}/api/login`, {
            method: "POST",
            body: JSON.stringify(this.state),
            headers: {
                "Content-Type": "application/json"
            },
        }).then((response) => {
            response.json()
                .then(async (result) => {
                    if (result.sukses === true) {
                        console.log(this.props);
                        console.warn("result", result);
                        console.log("Result"+result);
                        await this.listCategory();
                        localStorage.setItem('login', JSON.stringify({
                            login: true,
                            token: result.token
                        }))
                        localStorage.setItem('token', result.token);
                        localStorage.setItem('userData', JSON.stringify(result.data));
                        localStorage.setItem('roles', result.data.roles)
                        localStorage.setItem('id', result.data.id);
                        this.setState({ login: true });
                        // this.setState({ redirect: "/Dashboard" });
                        let role = JSON.stringify(result.data.roles[0])
                        let roles = role.substring(1,5); 
                        console.log(roles);
                        if(roles === 'user'){
                            // console.log("User");
                            // this.redirect();
                            this.onCloseLoginModal();
                            this.props.redirect("/");
                            // window.location.assign("/")
                            // this.props.history.push("/");
                            // window.location.reload();
                        }else{
                            // this.redirect();
                            this.onCloseLoginModal();
                            // window.location.assign("/Dashboard")
                            this.props.redirect("/Dashboard");
                            // window.location.reload();
                        }
                        // console.log(role[0]);
                        console.log(JSON.stringify(result.data.roles[0]));
                        console.log(result);
                        console.log(this.state);
                    } else {
                        // this.setState({ message: "Username or Password Doesn't Match" })
                        toast.error(result.msg);
                    }
                })
        })
            .catch(function (error) {
                console.log(error);
            })
    }

    render() {
        // const { modalLoginIsOpen, modalSignIsOpen, redirect, errors } = this.state;

        // if (redirect) {
        //     return <Redirect to={redirect} />;
        // }
        return (
            <>
                {/* <Form inline>
                    <a id="login" className="btn" onClick={this.onOpenLoginModal}>Login</a>
                </Form> */}
                <Modal
                    size="lg"
                    // onClick={this.onOpenLoginModal}
                    show={this.props.modalLoginIsOpen}
                    // show={this.onOpenLoginModal}
                    onHide={this.props.onClose}
                    // onHide={() =>{ this.setState({modalLoginIsOpen: false}) }}
                    // onHide={this.onCloseLoginModal}
                    aria-labelledby="contained-modal-title-vcenter">
                    <Modal.Header closeButton bsPrefix></Modal.Header>
                    <Modal.Body className="show-grid">
                        <Container>
                            <Row className="justify-content-center">
                                <Col xs={9} md={6}>
                                    <Row className="justify-content-center">
                                        <Col md={{ span: 9, offset: 3 }}><Image src={this.User1} rounded /></Col>
                                    </Row>
                                    <Row className="justify-content-center">
                                        <Col md={12}>
                                            <p className="text-center" style={{ fontFamily: "Gordita-Bold", fontSize: "22px" }}>Hello, Galilagi !</p>
                                        </Col>
                                    </Row>
                                    <Row className="justify-content-center">
                                        <Col md={{ span: 10, offset: 1 }}>
                                            <p text-center style={{ fontFamily: "Gordita", fontSize: "15px" }}>Login and start learning with video</p>
                                        </Col>
                                    </Row>
                                </Col>
                                <Col xs={9} md={6}>
                                    <Form.Label style={{ color: "red" }}>{this.state.message}</Form.Label>
                                    <Row className="justify-content-center">
                                        <GoogleLogin
                                            clientId="944247106925-m1vaiosjtsdsv876ctg54qdpgfcvkpht.apps.googleusercontent.com"
                                            buttonText="Login with Google"
                                            onSuccess={this.responseGoogle}
                                            onFailure={this.responseGoogle}
                                            cookiePolicy={'single_host_origin'}
                                        />
                                    </Row>
                                    <p className="text-center">or</p>
                                    <Row className="justify-content-center">
                                        <Col>
                                            <Fragment>
                                                <Form>
                                                    <Form.Group controlId="formBasicEmail">
                                                        <Form.Control type="email" placeholder="Enter email" onChange={(event) => { this.setState({ email: event.target.value }) }} required />
                                                    </Form.Group>
                                                    <Form.Group controlId="formBasicPassword">
                                                        <Form.Control type="password" placeholder="Password" onChange={(event) => { this.setState({ password: event.target.value }) }} required />
                                                    </Form.Group>
                                                    <Form.Group>
                                                        <Link to="/ForgotPassword" onClick={ this.onForgotPassword } className="blue-text">forgot password?</Link>
                                                    </Form.Group>
                                                    <Button variant="dark" onClick={() => { this.login() }} block>Log In</Button>
                                                    <Form.Group>
                                                        <br></br>
                                                        <Form.Label>Not a member yet? <a onClick={this.onOpenSignModal} className="blue-text" style={{ color: "#219ddb" }}>Sign Up</a></Form.Label>
                                                    </Form.Group>
                                                </Form>
                                            </Fragment>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Container>
                    </Modal.Body>
                </Modal>
        </>
        );
    }
}

export default withRouter(Login);