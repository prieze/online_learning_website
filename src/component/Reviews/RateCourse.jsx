import React, { Component } from 'react';
import Navigation from '../Navigation/Navigation';
import { Container, Row, Col, Form, Button, Image } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import Footer from '../Footer/Footer';
import { Rate, message } from 'antd';

class RateCourse extends Component{

    state = {
        star: 0,
        review: ''
    }   
    
    // componentDidMount(){
    //     console.log(this.props);
    // }

    clickRate(e){
        this.setState({
            star: e
        })
    }

    addRate() {

        if(this.state.star){

            const id = this.props.match.params.id;
            const body = {
                course_id: id,
                star: this.state.star,
                review: this.state.review
            }
            // console.log(body);
            const token = localStorage.getItem('token');
            fetch(`${process.env.REACT_APP_BASEURL}/api/rating`, {
                method: "POST",
                body: JSON.stringify(body),
                headers: {
                    "Authorization": `Bearer ${token}`,
                    "Content-Type": "application/json"
                },
            }).then((response) => {
                response.json()
                .then((result) => {
                    console.warn("result", result);
                    if (result.sukses) {
                        this.props.history.push('/ReviewCourses')
                        message.success("Review berhasil ditambahkan", 1)
                    } 
                    else {
                        message.error(result.msg, 1)
                    }
                })
            })

        }else{
            message.error("Rate harus dipilih", 1);

        }
    }


    render(){
        return(
            <>
            <Navigation />
            <Container fluid style={{ width: '100%', backgroundColor: '#E5E5E5', padding: '30px' }}>
                <Row className="justify-content-center" style={{ width: '80%', margin: 'auto' }}>
                    <Col><Link to={`/ReviewCourses`} style={{ color: '#6831B0', fontSize: '18px', fontFamily: 'Gordita-bold', marginTop: '10%' }}><i className="fa fa-chevron-left"></i> Review Courses</Link></Col>
                </Row><br/>

                <Row className="justify-content-center" style={{ width: '80%', margin: 'auto' }}>
                    <Col lg={4}><Image style={{ width: '100px', height: '100px' }} rounded src={this.props.location.data?.cover?.url} /></Col>
                    <Col style={{ marginLeft: '-20%' }} >
                        <p style={{ fontFamily: 'Gordita-bold', fontSize: '20px' }} >{this.props.location.data?.course_name}</p>
                        <Rate onChange={ (e) => { this.clickRate(e) } }/>
                    </Col>
                    </Row><br/>

                <Row className="justify-content-center" style={{ width: '80%', margin: 'auto' }}>
                    {/* <Col style={{ paddingBottom: '20px', marginLeft: '5%' }} lg={4}> 
                        <Rate onChange={ (e) => { this.clickRate(e) } }/>
                    </Col> */}
                    <Col lg={12}>
                    <Form>
                        <Form.Group>
                            <Form.Control onChange={ (event) => { this.setState({ review: event.target.value }) } } style={{ backgroundColor: '#F1F1F1' }} placeholder="Share your experience and help others make better choices! " as="textarea" rows="3" />
                        </Form.Group>
                        <Form.Group>
                            <Button onClick={ () => {this.addRate()} } className="button-grape" block>Submit</Button>
                        </Form.Group>
                    </Form>
                    </Col>
                </Row>

            </Container>
            <Footer />
            </>
        )
    }

}

export default RateCourse;