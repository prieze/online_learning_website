import React, { Component } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import Navigation from '../Navigation/Navigation';
import Footer from '../Footer/Footer';
import { Tabs, message } from 'antd';
import WaitingReviews from './WaitingReviews';
import MyReviews from './MyReviews';
const { TabPane } = Tabs;

class ReviewCourses extends Component{

    state = {
        course:[],
        list: []
    }

    componentDidMount(){
        this.waitingReviews();
        this.myReviews();
    }

    waitingReviews() {

        const token = localStorage.getItem("token");
        this.setState({ loading: true });
        fetch(`${process.env.REACT_APP_BASEURL}/api/wait_rating`, {
          method: "GET",
          headers: {
            Authorization: `Bearer ${token}`,
            "Content-Type": "application/json",
          },
        })
          .then((response) => response.json())
          .then((json) => {
            this.setState({
              course: json.data,
              loading: false,
            });
            // console.log(json.data);
          })
          .catch((error) => console.log("error", error));
    }

    myReviews() {

        const token = localStorage.getItem("token");
        this.setState({ loading: true });
        fetch(`${process.env.REACT_APP_BASEURL}/api/rating`, {
          method: "GET",
          headers: {
            Authorization: `Bearer ${token}`,
            "Content-Type": "application/json",
          },
        })
          .then((response) => response.json())
          .then((json) => {
            this.setState({
              list: json.data,
              loading: false,
            });
            // console.log(json.data);
          })
          .catch((error) => console.log("error", error));
    }

    render(){
        return(
            <>
            <Navigation />
            <Container fluid style={{ width: '100%', backgroundColor: '#F1F1F1', paddingBottom: '30px' }}>
                <Row className="justify-content-center">
                    <Col md={10}><p style={{ color: '#6831B0', fontSize: '18px', fontFamily: 'Gordita-bold', marginTop: '5%' }}>My Courses</p></Col>
                </Row>

                <Row style={{ width: '85%', margin: 'auto' }}>
                    <Col>
                        <Tabs defaultActiveKey="1">
                            <TabPane tab="Waiting for Review" key="1">
                            {
                                this.state.course.map((course) => {
                                    return <WaitingReviews id={course.id} key={course.id} course={course} state={ this.state } ></WaitingReviews>
                                })
                            }
                            </TabPane>
                            <TabPane tab="My Reviews"  key="2">
                            {
                                this.state.list.map((course) => {
                                    return <MyReviews id={course.id} key={course.id} course={course} ></MyReviews>
                                })
                            }
                            </TabPane>
                        </Tabs> 
                    </Col>
                </Row>


            </Container>
            <Footer />
            </>
        )
    }

}

export default ReviewCourses;