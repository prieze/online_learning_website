import React from 'react';
import { Row, Col, Card, ProgressBar, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

const List1 = (props) => {

    console.log(props);

    return(
        <Card style={{ width: '76%', marginTop: '1%' }}>
            <Row>   
                <Col xs={12} sm={7} md={6} lg={3}>
                    <Card.Img className="gambar-course" style={{ borderRadius: '5px 0 0 5px', height: '100%' }} src={props.course.course.cover?.url} />
                </Col>
                <Col>
                    <Card.Body style={{ padding: '1.5rem' }}>
                        <Row>
                            <Col style={{ lineHeight: '0.5' }}>
                                <Card.Title style={{ color: '#414141' }} >{props.course.course.course_name}</Card.Title>
                                <Card.Text style={{ color: '#392C7E' }} ><i className="fa fa-book"></i> Module <i className="fa fa-clock-o"></i> {props.course?.duration?.text}</Card.Text>
                            </Col>
                            <Col lg={5}>
                                <Link to={{ pathname: `/RateCourse/${props.course.course.id}`, data: props.course.course }}><Button className="button-grape" block style={{ height: '50px', marginTop: '11%' }} >RATE COURSE</Button></Link>
                            </Col>
                        </Row>
                    </Card.Body>
                </Col>
            </Row>
        </Card>
    );

}

export default List1;