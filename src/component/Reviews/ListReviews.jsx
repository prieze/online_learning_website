import React from 'react';
import { Row, Col, Card, Image } from 'react-bootstrap';
import { Rate } from 'antd';
import Moment from 'moment';

const ListReviews = (props) => {
    
    return(
        <>
        <Card style={{ marginTop: '1%', boxShadow: 'none' }} >
            <Card.Body style={{ padding: '2rem', paddingBottom: '0' }}>
                <Row>
                    <Col lg={3}>
                        <Image src={props.list.user?.gambarr?.url} roundedCircle />
                    </Col>
                    <Col lg={3} style={{ marginTop: '3%' }}>
                        <a style={{ fontFamily: "Gordita-Bold", fontSize: "19px", color: "#020288" }}>{props.list.user?.first_name}</a><br></br>
                        <a style={{ fontSize: "14px", color: "#020288" }}>{props.list.user?.job_title}</a>
                    </Col>
                    <Col lg={6}>
                        <Rate disabled defaultValue={props.list.star} />
                        <p>{props.list.review}</p>
                    </Col>
                </Row>
            </Card.Body>
            <Card.Footer>
                <small className="text-muted" style={{ float: 'right' }}>Last updated {Moment(props.list.createdAt).endOf('hour').fromNow()}</small>
            </Card.Footer>
        </Card>
        </>
    )
}

export default ListReviews;