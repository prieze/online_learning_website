import React from 'react';
import { Row, Col, Card, ProgressBar, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { Rate } from 'antd';

const List2 = (props) => {

    console.log(props);

    return(
        <Card style={{ width: '76%', marginTop: '1%' }}>
            <Row>   
                <Col xs={12} sm={7} md={6} lg={3}>
                    <Card.Img className="gambar-course" style={{ borderRadius: '5px 0 0 5px', height: '100%' }} src={props.course.course.cover?.url} />
                </Col>
                <Col>
                    <Card.Body style={{ padding: '1.5rem' }}>
                        <Row>
                            <Col style={{ lineHeight: '0.5' }}>
                                <Card.Title style={{ color: '#414141' }} >{props.course.course.course_name}</Card.Title>
                                <Card.Text style={{ color: '#392C7E' }} >{props.course.course.ratings[0].review}</Card.Text>
                            </Col>
                            <Col lg={5}>
                                <Card style={{ border: 'none', borderRadius: '10px', boxShadow: 'none', backgroundColor: '#EDEDED' }}>
                                    <Card.Body style={{ margin: 'auto', padding: '1rem' }}>
                                        <Rate disabled defaultValue={props.course.course.ratings[0].star} />
                                    </Card.Body>
                                </Card>                                
                            </Col>
                        </Row>
                    </Card.Body>
                </Col>
            </Row>
        </Card>
    );

}

export default List2;