import React, { Component} from 'react';
import { Row, Col, Card, ProgressBar, Form, Image, Button } from 'react-bootstrap';
import { Rate } from 'antd';
import ListReviews from './ListReviews';
import _ from "lodash";

class Reviews extends Component{

    constructor(props) {
        super(props);
        
        this.state = {
           reviews: [],
           review: [],
           star: [],
           hasil: 0,
           search: '',
           index: 0,
           star_id: 0,
           sort: [],
           jumlah: [5, 4, 3, 2, 1]
          };

    }

    componentDidMount(){
        this.listReviews();
    }

    listReviews(){
        const id = this.props.state.course.courseId;
        const search = this.state.search;
        const token = localStorage.getItem('token');
        const next = this.state.index;
        fetch(`${process.env.REACT_APP_BASEURL}/api/rating/${id}?limit=2&offset=${next}&search=${search}`, {
            method: "GET",
            headers: {
                "Authorization": `Bearer ${token}`,
                "Content-Type": "application/json"
            },
        }).then((response) => {
            response.json()
                .then((result) => {
                    console.log(result);
                    this.setState({
                        reviews: result.rows,
                        star: result.star,
                        avg: result.avg,
                        hasil: parseFloat(result.avg.average).toFixed(1)
                    });
                    console.log(this.state);
                })
        })
    }

    onChange = (event) => {
        console.log('onChange triggered');
        event.persist();

        if (!this.debouncedFn) {
            this.debouncedFn = _.debounce(() => {
                this.setState({ search: event.target.value });
                this.listReviews(1);
            }, 1000);
        }
        this.debouncedFn();
    }

    loadMore = async(index) => {
        const next = this.state.index + index;
        await this.setState({
            index: next
        });
        console.log(next);
        console.log(this.state.index);
        this.listReviews();
    }

    dropRate = async(id) => {
        console.log(id);
        const courseID = this.props.state.course.course.id;
        const token = localStorage.getItem('token');
        fetch(`${process.env.REACT_APP_BASEURL}/api/rating/course/sort/${courseID}`, {
            method: "POST",
            body: JSON.stringify({
                star_id: id
            }),
            headers: {
                "Authorization": `Bearer ${token}`,
                "Content-Type": "application/json"
            },
        }).then((response) => {
            response.json()
                .then((result) => {
                    this.setState({
                        sort: result.rows,
                        star_id: id
                    })
                    console.log(this.state.sort);
                    console.log(this.state.star_id);
                })
        })
    }

    render(){

        return(
            <>
            <Row>
                <Col>
                    <p style={{ fontFamily: 'Gordita-bold', color: '#392C7E', fontSize: '18px' }}>How students rated this class</p>
                    <Card style={{ backgroundColor: '#F9F8F8', boxShadow: 'none', border: '0' }}>
                        <Card.Body>
                            <Row className="justify-content-center">
                                <Col style={{ textAlign: 'center' }} lg={4}>
                                    <p style={{ fontFamily: "Gordita-Bold", fontSize: "19px", color: "#020288", marginTop: "13%" }}>{this.state.hasil}</p>
                                    <Rate disabled defaultValue={5} />
                                    <p style={{ color: '#020288' }}>Course Rating</p>
                                    <p style={{ color: '#020288', lineHeight: '0.5' }}>(Based on {this.state.avg?.count_review} Reviews)</p>
                                </Col>
                                <Col lg={8}>
                                    <div style={{ paddingTop: "4%" }}>
                                        {   this.state.jumlah.map(a => {
                                            return  <Row>
                                            <Col xs={1} sm={1} md={1} lg={1} style={{ color: '#020288' }} ></Col>
                                            <Col lg={10}style={{ marginLeft: '-6%', marginTop: '4px' }}><ProgressBar now={this.state.star.filter(z => z.star === a).map(e => {return e.count})} variant="warning" /><br/></Col>
                                            <Col xs={1} sm={1} md={1} lg={1} style={{ marginLeft: '-3%', color: '#020288' }} >{a} <i className="fa fa-star" style={{ color: '#FF8A00' }} /></Col>
                                        </Row>
                                        })
                                        }
                                    </div>
                                </Col>
                            </Row>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>

            <Row style={{ marginTop: '3%' }}>
                <Col>
                    <p style={{ fontFamily: 'Gordita-bold', color: '#392C7E', fontSize: '18px' }}>Reviews</p>
                    <Form >
                        <Form.Group>
                            <Row>
                                <Col lg={6}>
                                    <Form.Control type="text" placeholder="Search reviews" onChange={this.onChange} debounce="1000" />
                                    <i className="fa fa-search" style={{ marginLeft: '90%', position: 'absolute', marginTop: '-5%' }}></i>
                                </Col>
                                <Col lg={6}>
                                    <Form.Control as="select" onChange={(e) => {this.dropRate(e.target.value)}} >
                                        <option value="0">All Ratings</option>
                                        {
                                            this.state.star.map((list) => {
                                                return <option value={list.star} >Bintang {list.star}</option>
                                            })
                                        }
                                    </Form.Control>
                                </Col>
                            </Row>
                        </Form.Group>
                    </Form>
                        {(this.state.star_id !== 0)
                            ? this.state.sort.map((list) => {
                                return <ListReviews id={list.id} key={list.id} list={list} />
                            })
                            : this.state.reviews.map((list) => {
                                return <ListReviews id={list.id} key={list.id} list={list} />
                            })
                        }
                </Col>
            </Row>

            <Row className="justify-content-center" style={{ marginTop: '3%' }}>
                <Col lg={3}>
                    <Button onClick={ () => { this.loadMore(2) } } className="button-grape" block >Load more reviews</Button>
                </Col>
            </Row>
            </>
        )

    }

}

export default Reviews;