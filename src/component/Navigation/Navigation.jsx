import React, { Component, Fragment } from 'react';
import { Navbar, Form, NavDropdown, Nav, Container, Image,Dropdown, DropdownButton, ButtonGroup } from "react-bootstrap";
// import { BrowserRouter as Router, Link } from 'react-router-dom';
import { BrowserRouter as Router, Redirect, Link } from "react-router-dom";

import { checkRole, pageToLimitOffset } from '../Utils'

import '../../style/user.css'
import Logo from '../../gambar/logo.png';

class Navigation extends Component {
    constructor(props) {
        super(props)
        let user = JSON.parse(localStorage.getItem('userData'));
        this.state = {
            login: false,
            redirect: false,
            id: "",
            gambarr: user,
            cat: [],
            selected: 'Category'
        }
    }
    componentDidMount() {
        const token = localStorage.getItem('token');
        if (token) {
            this.setState({ login: true, id: localStorage.getItem('id') });
        }
        const cat = localStorage.getItem('categories');
        if (cat) {
            this.setState({ cat: JSON.parse(cat) })
        }
    }

    componentDidUpdate(prevProps) {
        // console.log(this.props);
        if (prevProps.selected !== this.props.selected) {
            this.setState({ selected: this.props.selected })
        }
    }

    profile() {
        const id = localStorage.getItem('id');
        this.changeURL(`/Profile/${id}`)
    }
    logout() {
        const token = localStorage.getItem("token");
        fetch(`${process.env.REACT_APP_BASEURL}/api/logout`, {
            method: "POST",
            headers: {
                Authorization: `Bearer ${token}`,
                "Content-Type": "application/json"
            },
        }).then((response) => {
            response.json()
                .then((result) => {
                    console.log(result);
                    localStorage.removeItem("token");
                    localStorage.removeItem("login");
                    localStorage.removeItem('userData');
                    this.changeURL('/');
                    window.location.reload();
                    // if (result.sukses === true) {
                    //     console.warn("result", result);
                    //     console.log("Result"+result);
                    //     await this.listCategory();
                    //     localStorage.setItem('login', JSON.stringify({
                    //         login: true,
                    //         token: result.token
                    //     }))
                    //     localStorage.setItem('token', result.token);
                    //     localStorage.setItem('userData', JSON.stringify(result.data));
                    //     localStorage.setItem('id', result.data.id);
                    //     this.setState({ login: true });
                    //     this.setState({ redirect: "/Dashboard" });
                    //     console.log("result" + JSON.stringify(result.data));
                    //     console.log(this.result);
                    //     console.log(this.state);
                    // } else {
                    // this.setState({ message: "Username or Password Doesn't Match" })
                    // toast.error(result.msg);
                    // }
                })
        })
            .catch(function (error) {
                console.log(error);
            })
        // this.changeURL('/');
    }

    changeURL(url) {
        if (this.props && this.props.history) {
            this.props.history.push(url)
        }
        this.setState({ redirect: url });
    }


    render() {
        const { redirect, gambarr } = this.state;

        if (redirect) {
            this.state.redirect = false;
            return <Redirect to={redirect} />;
        }

        return (

            <Router>
                <Container fluid>
                    <Fragment>
                        <Navbar expand="lg">
                            {
                                (localStorage.getItem('roles') === 'admin')
                                    ? <img style={{ cursor: 'pointer' }} className="img-responsive logo" src={Logo} alt="" onClick={() => { this.changeURL('/Dashboard') }} />
                                    : <img style={{ cursor: 'pointer' }} className="img-responsive logo" src={Logo} alt="" onClick={() => { this.changeURL('/') }} />
                            }
                            <Navbar.Toggle aria-controls="basic-navbar-nav" />
                            <Navbar.Collapse id="basic-navbar-nav">
                                <Nav className="mr-auto">
                                    {!checkRole('admin').sukses &&
                                        <NavDropdown
                                            title={
                                                this.state.selected ? this.state.selected : 'Category'
                                            }
                                            id="basic-nav-dropdown" color="#6831B0" className="dropdown">
                                            {this.state.cat.map(cat => {
                                                return <NavDropdown.Item
                                                
                                                    onClick={() => { this.changeURL(`/Courses/${cat.id}`) }}>
                                                    {cat.category_name}
                                                </NavDropdown.Item>
                                            })}
                                        </NavDropdown>
                                    }
                                </Nav>
                                {checkRole('admin').sukses
                                    ? <Fragment>
                                        <button className="btn" style={{ color: '#6831B0' }} onClick={() => { this.changeURL('/Dashboard') }}>Dashboard</button>
                                        <Nav>
                                            <NavDropdown className="dropdown" style={{ color: '#6831B0' }} title="Partnership">
                                                <NavDropdown.Item onClick={() => { this.changeURL('/Instructor') }}>Instructor</NavDropdown.Item>
                                                <NavDropdown.Divider />
                                                <NavDropdown.Item >Mitra</NavDropdown.Item>
                                            </NavDropdown>
                                        </Nav>
                                        <button className="btn" style={{ color: '#6831B0' }} onClick={() => { this.changeURL('/Course') }}>Course</button>
                                        <button className="btn" style={{ color: '#6831B0' }} onClick={() => { this.changeURL('/Webinar') }}>Webinar</button>
                                        <Nav className="mr-5" style={{ color: 'red' }}>
                                            <NavDropdown className="dropdown" style={{ fontFamily: 'Gordita'}} title="Master Data">
                                                <NavDropdown.Item onClick={() => { this.changeURL('/User') }}>Users</NavDropdown.Item>
                                                <NavDropdown.Divider />
                                                <NavDropdown.Item onClick={() => { this.changeURL('/Role') }}>Roles</NavDropdown.Item>
                                                <NavDropdown.Divider />
                                                <NavDropdown.Item onClick={() => { this.changeURL('/Category') }}>Categories</NavDropdown.Item>
                                                <NavDropdown.Divider />
                                                {['right'].map((direction) => (
                                                    <DropdownButton
                                                        as={ButtonGroup}
                                                        key={direction}
                                                        id={`dropdown-button-drop-${direction}`}
                                                        drop={direction}
                                                        variant="none"
                                                        title={` Payment `}
                                                        // style={{backgroundColor: 'white', color: 'black', border: 'none'}}
                                                        >
                                                        <Dropdown.Item eventKey="1" onClick={() => { this.changeURL('/Payment') }}>Info Payment</Dropdown.Item>
                                                        <Dropdown.Divider />
                                                        <Dropdown.Item eventKey="2" onClick={() => { this.changeURL('/Confirmation') }}>Approval Payment</Dropdown.Item>
                                                    </DropdownButton>
                                                    ))}
                                            </NavDropdown>
                                        </Nav>
                                    </Fragment>
                                    : <Fragment>
                                        <div>
                                            <input type="search" className="srch" placeholder="Search for anything" />
                                            <span className="icon"><i className="fa fa-search"></i></span>
                                        </div>
                                        <Nav>
                                            <NavDropdown className="dropdown" style={{ color: '#6831B0' }} title="Webinar">
                                                <NavDropdown.Item onClick={() => { this.changeURL('/MyWebinar') }}>My Webinar</NavDropdown.Item>
                                                <NavDropdown.Divider />
                                                <NavDropdown.Item onClick={() => { this.changeURL('/WebinarReviews') }}>Review Webinar</NavDropdown.Item>
                                            </NavDropdown>
                                        </Nav>
                                        <Nav>
                                            <NavDropdown className="dropdown" style={{ color: '#6831B0' }} title="Course">
                                                <NavDropdown.Item onClick={() => { this.changeURL('/MyCourses') }}>My Course</NavDropdown.Item>
                                                <NavDropdown.Divider />
                                                <NavDropdown.Item onClick={() => { this.changeURL('/ReviewCourses') }}>Review Course</NavDropdown.Item>
                                            </NavDropdown>
                                        </Nav>
                                        <button style={{ color: '#6831B0' }} className="btn" onClick={() => { this.changeURL('/ShoppingChart') }}>Shopping Cart</button>
                                        {/* <span style={{ color: '#6831B0' }}><i className="fa fa-bell"></i></span> */}
                                    </Fragment>
                                }

                                <Nav className="mr-auto">
                                    <Image src={gambarr?.gambarr?.url} roundedCircle style={{ width: '50px', height: '48px' }} />
                                    <NavDropdown title="" id="basic-nav-dropdown" className="dropdown">
                                        <NavDropdown.Item onClick={() => { this.profile() }}>Profil</NavDropdown.Item>
                                        <NavDropdown.Divider />
                                        <NavDropdown.Item onClick={() => { this.logout() }}><i className="fa fa-sign-out"></i> Log Out</NavDropdown.Item>
                                    </NavDropdown>
                                </Nav>

                            </Navbar.Collapse>
                        </Navbar>
                    </Fragment>
                </Container>
            </Router>
        )
    }
}

export default Navigation;