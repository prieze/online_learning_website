import React from 'react';
import { Row, Col, Card, Form } from 'react-bootstrap';
import { Popconfirm, message } from 'antd';

const MAX_LENGTH = 50;

const List = (props) => {
    // console.log(props);
    return(
        <Row>
            <Col xs={1} sm={1} md={1} lg={1} style={{ marginTop: '1%' }}>
                <Form>
                    {['checkbox'].map((type) => (
                        <div key={`default-${type}`} className="mb-3">
                            <Form.Check type={type} id={`default-${type}`}
                                onChange={ () => { props.handleChange( props.cart.id ) } }
                                checked={props.cart.selected}
                            />
                        </div>
                    ))}
                </Form>
            </Col>
            <Col xs={11} sm={11} md={11} lg={11}>
                <Card style={{ marginTop: '1%' }}>
                    <Row>
                        <Col xs={12} sm={7} md={6} lg={4}>
                            <Card.Img className="gambar-course" style={{ borderRadius: '5px 0 0 5px', width: '100%', height: '12rem' }} src={(props.cart.course!==null)?props.cart?.course?.cover?.url:props.cart?.webinar?.cover?.url} />
                        </Col>
                        <Col>
                            <Card.Body style={{ padding: '1rem' }}>
                                <Row>
                                    <Col style={{ lineHeight: '1' }}>
                                        <Card.Title style={{ fontFamily: 'Gordita-bold', fontSize: '14px' }}>{(props.cart.course!==null)?props.cart?.course?.course_name:props.cart?.webinar?.webinar_name}</Card.Title>
                                        <Card.Text style={{ fontSize: '14px' }}>{(props.cart.course!==null)?`${props.cart.course?.description?.substring(0, MAX_LENGTH)} ...`:`${props.cart.webinar?.description?.substring(0, MAX_LENGTH)} ...`}</Card.Text>
                                        <Card.Title style={{ fontFamily: 'Gordita-bold',color: '#392C7E', fontSize: '14px' }}>IDR {(props.cart.course!==null)?parseInt(props.cart?.course?.setelah_disc).toLocaleString("id-ID"):parseInt(props.cart?.webinar?.setelah_disc).toLocaleString("id-ID")}</Card.Title>
                                        <Card.Title style={{fontFamily: 'Gordita', fontSize: '12px'}}><s>IDR {(props.cart.course!==null)?parseInt(props.cart?.course?.price).toLocaleString("id-ID"):parseInt(props.cart?.webinar?.price).toLocaleString("id-ID")}</s></Card.Title>
                                    </Col>
                                    <Col lg={2}>
                                        <Popconfirm
                                            title="Apakah anda ingin menghapus course ini ?"
                                            onConfirm={() => { {props.remove(props.cart.id)} }}
                                            okText="Yes"
                                            cancelText="No"
                                        >
                                            <p style={{ cursor: 'pointer', color: '#000000', fontSize: '13px' }} >Delete</p>
                                        </Popconfirm>
                                    </Col>
                                </Row>
                            </Card.Body>
                        </Col>
                    </Row>
                </Card>
                
            </Col>
        </Row>
    )

}

export default List;