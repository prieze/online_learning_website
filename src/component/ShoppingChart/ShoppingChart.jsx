import React, { Component } from 'react';
import { Container, Row, Col,Nav, NavDropdown,Navbar, Card, Form, Button, Image, Modal } from 'react-bootstrap';
import Footer from "../Footer/Footer";
import { Link } from 'react-router-dom';
import Navigation from "../Navigation/Navigation";
import { Popconfirm, message } from 'antd';
import List from './List';
import SpecialOfferList from '../Courses/SpecialOfferList';
import Login from '../Login';

class ShoppingChart extends Component{

    constructor(props) {
        super(props);

        this.state = {
          course: [],
          list: [],
          modalAddIsOpen: false,
          checkedAll: true,
          cash: {},
          open: false,
          modalLoginIsOpen: false,
          method: [],
          bankId: 0,
          radio: false,
          setelah_diskon: 0
        };

    }

    Logo = 'https://galilagi-media.oss-ap-southeast-5.aliyuncs.com/assets/logo.png';

    onOpenAddModal = () => {
        this.setState({ modalAddIsOpen: true });
    };

    onCloseAddModal = () => {
        this.setState({ modalAddIsOpen: false, radio: false });
    };

    onOpenLoginModal = () => {
        this.setState({ modalLoginIsOpen: true, modalSignIsOpen: false, message: null });
    };

    componentDidMount(){
        const token = localStorage.getItem("token");
        if (token) {
            this.listChart();
        } else {
            this.listCourseNonLogin();
        }
        this.listCourse();
        this.paymentMethod();
        this.diskon();
        // console.log(this.props);
    }

    listCourseNonLogin = (async() => {
        const non_login_cart = localStorage.getItem('non_login_cart');
        const cart = non_login_cart ? JSON.parse(non_login_cart) : [];
        for (const a of cart) {
            a.course.selected = true;
        }
        const courseid = cart.map(a => {return a.course.id})
        // console.log('courseid: ', courseid);
        await this.setState({
          course: cart,
          loading: false,
          checkedAll: true
        });
        this.calcSelectAll()
        // console.log(this.state.course);
    })

    listCourse = (async() => {

        const token = localStorage.getItem("token");
        this.setState({ loading: true });
        return await fetch(`${process.env.REACT_APP_BASEURL}/api/read/courses?limit=3&offset=0&search=`, {
          method: "GET",
          headers: {
            Authorization: `Bearer ${token}`,
            "Content-Type": "application/json",
          },
        })
          .then((response) => response.json())
          .then((json) => {
            this.setState({
              list: json.rows,
              loading: false,
            });
          })
          .catch((error) => console.log("error", error));
      })

    listChart() {

        const token = localStorage.getItem("token");
        this.setState({ loading: true });
        fetch(`${process.env.REACT_APP_BASEURL}/api/cart`, {
          method: "GET",
          headers: {
            Authorization: `Bearer ${token}`,
            "Content-Type": "application/json",
          },
        })
          .then((response) => response.json())
          .then((json) => {
            //   console.log(json);
            for (const a of json.rows) {
                a.selected = true;
                if (a.course) {
                    const disc = parseInt(a.course.discount) / 100 * parseInt(a.course.price)
                    a.course.disc = disc
                    a.course.setelah_disc = parseInt(a.course.price) - disc
                }
                if (a.webinar) {
                    const disc = parseInt(a.webinar.discount) / 100 * parseInt(a.webinar.price)
                    a.webinar.disc = disc
                    a.webinar.setelah_disc = parseInt(a.webinar.price) - disc
                }
                // if(a.course){
                //     a.selected = true;
                //     console.log(a);
                // }
                // if(a.webinar){
                //     a.selected = true;
                //     console.log(a.webinar);
                // }
            }
            if(json.rows.course){
                const courseid = json.rows.map(a => {return a.course.id})
                console.log('courseid: ', courseid);
            }
            if(json.rows.webinar){
                const webinarid = json.rows.map(a => {return a.webinar.id})
                console.log('webinarid: ', webinarid);
            }
            this.setState({
              course: json.rows,
              loading: false,
              checkedAll: true
            });
            this.calcSelectAll()
            this.diskon()
            console.log(this.state.course);
            // console.log(json.rows);
          })
          .catch((error) => console.log("error", error));
    }

    removeChart(e){
        const chart = this.state.course.filter(obj => {
            return obj.id !== e
        })
        this.setState({course: chart});
    }

    removeOne(id) {
        return new Promise((resolve, reject) => {
            const token = localStorage.getItem('token');
            fetch(`${process.env.REACT_APP_BASEURL}/api/cart/${id}`, {
                method: 'DELETE',
                headers: {
                    "Authorization": `Bearer ${token}`,
                    "Content-Type": "application/json"
                },
            })
                .then(response => response.json())
                .then(json => {
                    // console.log(json);
                    if (json.sukses) {
                        resolve(true);
                    } else {
                        reject(false);
                    }
                })
                .catch(error => reject(error));
        })
    }

    handleRemove = (async(id) =>{
        console.log('handle remove')
        console.log(id);
        try{
            const hasil = await this.removeOne(id);
            if (hasil) {
                this.listChart();
                message.success("Successfully !", 1)
            }
        }catch(error){
            message.error('Error!', 1);
            // console.log(error);
        }
    })

    handleChange = (async(id) => {
        console.log(id);
        console.log(this.state.course);
        const hasil = this.state.course.map(a => {
            if (a.id === id)
                a.selected = !a.selected
            return a;
        })
        console.log(hasil);
        await this.setState({
            course: hasil,
            selected: false
        })
        this.calcSelectAll();
        
    })

    paymentApi(id){
        return new Promise((resolve, reject) => {
            const token = localStorage.getItem('token');
            fetch(`${process.env.REACT_APP_BASEURL}/api/course/paid`, {
                method: "POST",
                body: JSON.stringify({course_id: id}),
                headers: {
                    "Authorization": `Bearer ${token}`,
                    "Content-Type": "application/json"
                },
            }).then((response) => {
                response.json()
                    .then((result) => {
                        console.warn("result", result);
                        if (result.sukses === true ) {
                            // this.props.history.push(`/MyCourses`)
                            // message.success("Berhasil ditambahkan")
                            resolve(true);
                        } else {
                            // message.error(result.msg)
                            reject(false);
                        }
                    })
            }).catch(error => reject(error));
        })
    }

    addPayment = (async () => {
        const allId = [];
        this.state.course.map(a=> {
            if (a.selected) {
                if (a.course && a.course.id) {
                    allId.push(a.course.id)
                }
            }
        })
        try{
            const status = await Promise.all(allId.map(id => this.paymentApi(id)));
            console.log(status);
            this.props.history.push(`/MyCourses`)
            message.success("Berhasil ditambahkan", 1)
        }catch(err) {
            message.error(err.msg, 1)
        }
    })

    diskon(){
        let angka = {
            total: 0,
            diskon: 0,
            payment: 0
        }

        this.state.course.map(a => {
            if (a.selected) {
                if (a.course && a.course.price && a.course.discount ) {
                    const total = parseInt(a.course.price)
                    const diskon = parseInt(a.course.discount) / 100 * parseInt(a.course.price)
                    angka.diskon =+ diskon
                    const payment = total - diskon;
                    angka.payment =+ payment
                    console.log("Total",total);
                    console.log("Diskon",diskon);
                    console.log("Payment",payment);
                }
                if (a.webinar && a.webinar.price && a.webinar.discount ) {
                    const total = parseInt(a.webinar.price)
                    const diskon = parseInt(a.webinar.discount) / 100 * parseInt(a.webinar.price)
                    angka.diskon =+ diskon
                    const payment = total - diskon;
                    angka.payment =+ payment
                    console.log("Total",total);
                    console.log("Diskon",diskon);
                    console.log("Payment",payment);
                    this.setState({
                        setelah_diskon: payment
                    })
                }
            }
        })
        console.log(this.state.setelah_diskon);


    }

    calcSelectAll() {
        let hasil = true;
        let cash = {
            total: 0,
            disc: 0,
            payment: 0,
            percent: 0
        }
        this.state.course.map(a => {
            if (!a.selected) {
                hasil = false;
            }
            if (a.selected) {
                if (a.course && a.course.price) {
                    cash.total += parseInt(a.course.price)
                }
                if (a.webinar && a.webinar.price) {
                    cash.total += parseInt(a.webinar.price)
                }
                if (a.course && a.course.discount) {
                    const disc = parseInt(a.course.discount) / 100 * parseInt(a.course.price)
                    cash.disc += disc
                }
                if (a.webinar && a.webinar.discount) {
                    const disc = parseInt(a.webinar.discount) / 100 * parseInt(a.webinar.price)
                    cash.disc += disc
                }
            }
        })
        cash.payment = cash.total - cash.disc;
        cash.percent = (cash.disc / cash.total) * 100
        // console.log(cash);
        this.setState({checkedAll: hasil, cash});
    }

    selectAll = (async(e) => {
        e.persist();
        let cash = {
            total: 0,
            disc: 0,
            payment: 0,
            percent: 0
        }
        const hasil = this.state.course.map(a => {
            a.selected = e.target.checked;
            if (e.target.checked) {
                if (a.course && a.course.price) {
                    cash.total += parseInt(a.course.price)
                }
                if (a.webinar && a.webinar.price) {
                    cash.total += parseInt(a.webinar.price)
                }
                if (a.course && a.course.discount) {
                    const disc = parseInt(a.course.discount) / 100 * parseInt(a.course.price)
                    cash.disc += disc
                    cash.payment += (cash.total - disc)
                }
                if (a.webinar && a.webinar.discount) {
                    const disc = parseInt(a.webinar.discount) / 100 * parseInt(a.webinar.price)
                    cash.disc += disc
                    cash.payment += (cash.total - disc)
                }
            }
            return a;
        })
        if (e.target.checked) {
            cash.payment = cash.total - cash.disc;
            cash.percent = 10;
        }
        await this.setState({
            course: hasil,
            checkedAll: e.target.checked,
            cash
        })
    })

    deleteAll = (async() => {
        const allId = [];
        this.state.course.map(a=> {
            if (a.selected) {
                allId.push(a.id)
            }
        })
        try{
            console.log('allId: ', allId);
            const status = await Promise.all(allId.map(id => this.removeOne(id)));
            console.log(status);
            this.listChart();
            message.success("Berhasil menghapus", 1);
        }catch(err) {
            message.error(err.msg, 1)
        }
    })

    onclickpayment(){
        const token = localStorage.getItem('token');
        console.log(token);
        if (token) {
            this.onOpenAddModal();
        }else{
            this.setState({open: true});
        }
    }

    paymentMethod(){

        const token = localStorage.getItem('token');
        fetch(`${process.env.REACT_APP_BASEURL}/api/read/payment_methods`, {
            method: "GET",
            headers: {
                "Authorization": `Bearer ${token}`,
                "Content-Type": "application/json"
            },
        }).then((response) => {
            response.json()
                .then((result) => {
                    this.setState({method: result.rows});
                })

        })
    }

    choosePayment(){
        const id = {
            course_id: [],
            webinar_id: []
        }
        this.state.course.filter(z => z.selected).map(a => {
            if (a.course && a.course.id) {
                id.course_id.push(a.course.id);
            }
            if (a.webinar && a.webinar.id) {
                id.webinar_id.push(a.webinar.id);
            }
        })

        const token = localStorage.getItem('token');
            fetch(`${process.env.REACT_APP_BASEURL}/api/choose/payment`, {
                method: "POST",
                body: JSON.stringify({
                    jumlah_pembayaran: this.state.cash.payment,
                    payment_method_id: this.state.bankId,
                    courses: id.course_id,
                    webinar: id.webinar_id,
                }),
                headers: {
                    "Authorization": `Bearer ${token}`,
                    "Content-Type": "application/json"
                },
            }).then((response) => {
                response.json()
                    .then((result) => {
                        console.log(result);
                        if(result.sukses){
                            if (this.state.cash.payment === 0) {
                                this.props.history.push(`/MyCourses`); 
                            }else{
                                this.setState({
                                  pay: result.data
                                });
                                this.props.history.push(`/Checkout/${result.data[0].id}`);
                            }
                        }
                        // else{
                        //     this.props.history.push(`/MyCourses`);
                        //     message.success("Berhasil", 1);
                        // }
                    })
            })
            .catch(error => console.log('error', error));
    }

    
    render(){

        const { modalAddIsOpen } = this.state;

        return(
            <>
            {
                (localStorage.getItem('token'))
                ?<Navigation history={this.props.history}/>
                :   <>
                        <Navbar expand="lg">
                        <a className="logo" href="/"><img className="img-responsive logo" src={this.Logo} alt="" /></a>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                        <Navbar.Collapse id="basic-navbar-nav">
                            <Nav className="mr-auto">
                                <NavDropdown title="Category" id="basic-nav-dropdown" color="#6831B0" className="dropdown">
                                    <NavDropdown.Item href="#action/3.2">
                                        IT & Software
                                </NavDropdown.Item>
                                    <NavDropdown.Item href="#action/3.3">
                                        Design
                                </NavDropdown.Item>
                                    <NavDropdown.Item href="#action/3.4">
                                        Business
                                </NavDropdown.Item>
                                </NavDropdown>
                            </Nav>

                            <Form inline>
                                <div>
                                    <input type="search" className="srch" placeholder="Search for anything" />
                                    <span className="icon"><i className="fa fa-search"></i></span>
                                </div>
                                <a className="btn" href="/ShoppingChart" >Shopping Chart</a>
                                <a id="login" className="btn" onClick={this.onOpenLoginModal}>Login</a>
                                {(this.state.modalLoginIsOpen)? <Login redirect={(e)=>{this.props.history.push(e)}} state={this.state} modalLoginIsOpen={this.state.modalLoginIsOpen} redirect={(e)=>{this.props.history.push(e)}} onClose={() =>{this.setState({modalLoginIsOpen: false})}} />: null}
                                <a id="signup" className="btn" onClick={this.onOpenSignModal}>Sign Up</a>
                            </Form>
                            {/* <Login /> */}
                        </Navbar.Collapse>
                    </Navbar>
                    </>
            }
            <Container fluid style={{ width: "100%", padding: "30px", paddingBottom: '80px', backgroundColor: "#F1F1F1" }}>
                {(this.state.open)? <Login state={this.state} modalLoginIsOpen={this.state.open} onClose={() =>{this.setState({open: false})}} history={this.props.history} /> : null}
                <Row className="justify-content-center">
                    <Col xs={12} sm={10} md={7} lg={7}>
                        <Row>
                            <Col xs={10} sm={10} md={10} lg={10}>
                                <Form.Group id="formGridCheckbox">
                                    <Form.Check type="checkbox" label="Choose All Courses" checked={this.state.checkedAll} onChange={(e)=> {this.selectAll(e)}} />
                                </Form.Group>
                            </Col>
                            <Col xs={2} sm={2} md={2} lg={2}>
                                <Popconfirm
                                    title="Apakah anda ingin menghapus semua course ?"
                                    onConfirm={()=>{this.deleteAll()}}
                                    okText="Yes"
                                    cancelText="No"
                                >
                                    <i className="fa fa-trash" style={{ cursor: 'pointer', color: '#000000', float: 'right' }}></i>
                                </Popconfirm>
                            </Col>
                        </Row>

                        <hr style={{ marginTop: '-1%' }}/>

                        {
                            this.state.course.map((list, index) => {
                                // console.log(list);
                                return <List id={list.id} key={list.id} cart={list} index={index} remove={(id) => {this.handleRemove(id)}} handleChange={(id) => {this.handleChange(id)}} ></List>
                            })
                        }
                    </Col>
                    <Col xs={10} sm={10} md={10} lg={3}>
                        <Card>
                            <Card.Body style={{ padding: '2rem' }}>
                                <Card.Text>Total Price : </Card.Text>
                                <Card.Title style={{ color: '#392C7E', fontFamily: 'Gordita-bold' }}>IDR {this.state.cash.payment?.toLocaleString("id-ID")}</Card.Title>
                                {/* <Card.Text>{(this.state.cash.percent?.toLocaleString("id-ID") !== "NaN")? this.state.cash.percent?.toLocaleString("id-ID"): 0}% off</Card.Text> */}
                                <Card.Text><s>IDR {this.state.cash.total?.toLocaleString("id-ID")}</s></Card.Text>
                                <Button disabled={this.state.cash.total?.toLocaleString("id-ID") == "0" ? true : false} onClick={() => { this.onclickpayment() }} className="button-grape" block>Choose Payment</Button>
                            </Card.Body>
                        </Card> 
                    </Col>
                </Row>
{/* 
                <Row className="justify-content-center">
                    <Col md={8}><p style={{ color: '#000000', fontSize: '20px', fontFamily: 'Gordita-bold', marginLeft: '4%', marginTop: '5%' }}>You might also like</p></Col>
                </Row>

                <Row style={{ width: '65%', margin: 'auto'}}>
                    {this.state.list.map((list) => {
                        return <Col xs={12} sm={12} md={4} lg={4} ><SpecialOfferList id={list.id} key={list.id} list={list} reloadData={()=>{this.listChart()}}></SpecialOfferList></Col>
                    })}
                </Row> */}
                <Modal size="sm" show={modalAddIsOpen} onHide={this.onCloseAddModal} animation={false}>
                <Modal.Header closeButton>
                    <Modal.Title style={{ fontFamily: 'Gordita-bold', fontSize: '18px' }}>Payment Method</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Row>
                        <Col lg={5}><p style={{ fontSize: '13px', textAlign: 'center' }} >Choose Manual Bank Transfer</p></Col>
                        <Col>
                            <Form>
                                <Form.Group controlId="formGridAddress1">
                                    {
                                        this.state.method.map((list) => {
                                            if(list.status === 1){
                                            return <>
                                                <Card style={{ marginBottom: '5px', boxShadow: 'none', border: '0' }}>
                                                    <Card.Body style={{ paddingBottom: '0', paddingTop: '0' }}>
                                                        <Row>
                                                            <Col style={{ marginTop: '2%' }} xs={1} sm={1} md={1} lg={1}><Form.Check name="formHorizontalRadios" type="radio" required onClick={ () => { this.setState({ bankId: list.id, radio: true  }) } } /></Col>
                                                            <Col><Image style={{ width: '70px', height: '30px' }} src={list.file?.url} /></Col>
                                                        </Row> 
                                                    </Card.Body>
                                                </Card>                                       
                                            </>
                                            }
                                        })
                                    }
                                </Form.Group>
                            </Form>
                        </Col>
                    </Row>
                </Modal.Body>
                <Modal.Body style={{ backgroundColor: '#E7E7E7', borderRadius: '8px 8px 0 0'}}>
                    <p style={{ fontFamily: 'Gordita-bold', fontSize: '15px' }}>Summary</p>
                    <p style={{ fontSize: '13px' }}>Order Detail :</p>
                    {
                        this.state.course.map((list) => {
                            if(list.selected){
                                return <>
                                    <Row>
                                        <Col><Image style={{ width: '70px', height: '50px' }} src={(list.course!==null)?list?.course?.cover?.url:list?.webinar?.cover?.url} /></Col>
                                        <Col style={{ marginTop: '-1%' }}><p style={{ fontSize: '12px' }} >{(list.course!==null)?list?.course?.course_name:list?.webinar?.webinar_name} </p></Col>
                                        <Col style={{ marginTop: '-2%'}}>
                                            <s style={{ color: '#A4A4A4', fontSize: '12px'}} >{(list.course!==null)?list?.course?.price:list?.webinar?.price}</s>
                                            <p style={{ color: '#392C7E',fontSize: '12px' }}>{(list.course!==null)?list?.course?.setelah_disc:list?.webinar?.setelah_disc}</p>
                                        </Col>
                                    </Row>
                                </>
                            }
                        })
                    }
                    <hr/>
                    <p style={{ fontFamily: 'Gordita-bold', fontSize: '13px', float: 'right' }}>Total : {this.state.cash.payment?.toLocaleString('id-ID')}</p>                    
                </Modal.Body>
                <Modal.Footer style={{ backgroundColor: '#E7E7E7' }}>
                    <Button disabled={ this.state.radio ?false :true } onClick = { () => { this.choosePayment() }} className="button-grape" block>Checkout</Button>
                    {/* <Link to={{ pathname: '/Checkout', state: this.state }}><Button style={{  width: '266px'}} disabled={ this.state.radio ?false :true } className="button-grape">Checkout</Button></Link> */}
                </Modal.Footer>
            </Modal>

            </Container>
            <Footer/>
            </>
        );
    }

}

export default ShoppingChart;