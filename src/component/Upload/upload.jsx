import React, { Component, Fragment } from 'react';
import { checkRole, pageToLimitOffset } from '../Utils';
import socketIOClient from "socket.io-client";

class Upload extends Component {
  SelectedFile;
  reader = new FileReader();
  socket;
  state = {
    url:'',
    socketUploading: false,
    socketPercent: 0,
    socketUploaded: 0,
    socketTotalSize: 0,
    socketMBDone : 0,
    socketTotalSizeMB: 0,
    socketUrl: ''
  }
  componentDidMount() {
    const page = this.props.match.params.page ? this.props.match.params.page : 1;
    // if (checkRole('admin').sukses) {
    //     this.listTable(page);
    // }
    this.socket = socketIOClient(process.env.REACT_APP_BASEURL);
    this.socket.on('MoreData', (data =>{
      this.setState({
        socketPercent: (Math.round(data['Percent']*100)/100),
        socketMBDone: Math.round(((data['Percent']/100.0) * this.state.socketTotalSize) / 1048576)
      })
      const Place = data['Place'] * 524288;
      let NewFile;
      if(this.SelectedFile.slice)
        NewFile = this.SelectedFile.slice(Place, Place + Math.min(524288, (this.SelectedFile.size-Place)));
      else if(this.SelectedFile.webkitSlice)
        NewFile = this.SelectedFile.webkitSlice(Place, Place + Math.min(524288, (this.SelectedFile.size-Place)));
      else
        NewFile = this.SelectedFile.mozSlice(Place, Place + Math.min(524288, (this.SelectedFile.size-Place)));
      this.reader.readAsBinaryString(NewFile);
    }));
    this.socket.on('Done', (data => {
      console.log(data)
      if (data.sukses) {
        this.setState({
          socketPercent: 100,
          socketMBDone: this.state.socketTotalSizeMB,
          socketUploaded: this.state.socketTotalSize,
          socketUploading: false,
          socketUrl: data.data.url
        })
      }
    }))
  }
  uploadSocket(e) {
    if (e) {
      const mb = Math.round(e.size / 1048576);
      this.SelectedFile = e;
      this.setState({
        socketUploading: true,
        socketPercent: 0,
        socketTotalSize: e.size,
        socketMBDone: 0,
        socketTotalSizeMB: mb
      });
      // const reader = new FileReader();
      this.reader.onload = (evnt => {
        console.log('Emiting Upload');
        this.socket.emit('Upload', { 'Name' : e.name, Data : evnt.target.result });
      })
      const token = localStorage.getItem('token');
      console.log('Emiting Start');
      this.socket.emit('Start', { 'Name' : e.name, 'Size' : e.size, 'Type': e.type, token: `Bearer ${token}` });
      }
  }
  toBase64 = file => new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
  });

  testApiCources(e) {
    if (!e) {return};
    this.toBase64(e).then(f => {
      f = f.split(';base64,');
      if (f.length > 1) {
        f = f[1]
      } else {
        f = f[0]
      }
      const token = localStorage.getItem('token');
      fetch(`http://149.129.255.54/api/create/course/1`, {
          method: "POST",
          body: JSON.stringify({
            course_name: 'coba1',
            cover_photo: 'apa aja',
            description: 'coba coba',
            price: 100,
            discount: 10,
            status: 1, 
            categoryId: 1,
            cover: {
              file: f,
              file_name: e.name,
              file_size: e.size,
              file_type: e.type
            }
          }),
          headers: {
              "Authorization": `Bearer ${token}`,
              "Content-Type": "application/json"
          },
      }).then((response) => {
        response.json()
        .then((result) => {
          console.log(result)
          console.log(result.files.url);
          this.setState({url: result.files.url});
        })
      })
    })
  }

  saveFile(e) {
    if (!e) {return};
    this.toBase64(e).then(f => {
      // console.log(`base64: ${f}`);
      // REMOVE BEFORE ;base64,
      f = f.split(';base64,')[1];
      const token = localStorage.getItem('token');
      fetch(`http://149.129.255.54/api/upload`, {
          method: "POST",
          body: JSON.stringify({
            file: f,
            file_name: e.name,
            file_size: e.size,
            file_type: e.type
          }),
          headers: {
              "Authorization": `Bearer ${token}`,
              "Content-Type": "application/json"
          },
      }).then((response) => {
        response.json()
        .then((result) => {
          console.log(result)
          console.log(result.files.url);
          this.setState({url: result.files.url});
        })
      })
    })
  }

  render() {
    // if (!checkRole('admin').sukses) {
    //     return (
    //         <div>Tidak Bisa Mengakses</div>
    //     )
    // }
    return (
      <div>
        <Fragment>
          <div>
            <input type="file" id="file" placeholder="Upload File" className="form-control" onChange={e => {
              this.saveFile(e.target.files[0]);
            }}/>
          </div>
          <div>
            <div>Test Api Cources</div>
            <input type="file" id="file" placeholder="Upload File" className="form-control" onChange={e => {
              this.testApiCources(e.target.files[0]);
            }}/>
            <div>url: {this.state.url}</div>
          </div>
          <div>
            <div>Test Upload Video (Socket.io)</div>
            <input type="file" id="file" placeholder="Upload File" className="form-control" onChange={e => {
              this.uploadSocket(e.target.files[0]);
            }}/>
            {this.state.socketUploading &&
            <div>Uploading ({this.state.socketMBDone} / {this.state.socketTotalSizeMB} MB) ( {this.state.socketPercent}% )</div>
            }
            {this.state.socketPercent === 100 &&
              <div>Uploaded ({this.state.socketTotalSize.toLocaleString()}) ( {this.state.socketPercent}% )</div>
            }
            {this.state.socketUrl !== '' &&
            <div><a target='_blank' href={this.state.socketUrl}>Link</a></div>
            }
          </div>
        </Fragment>
      </div>
    )
  }
}
export default Upload;