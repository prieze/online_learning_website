import React, { Component } from 'react';
import { checkRole, pageToLimitOffset } from '../Utils';
import UploadVideo from './uploadVideo';

class TestVideo extends Component {
  state = {
    selectedVideo: {},
    uploadState: {} //Upload State dari Component untuk merubah data dari page ini
  }
  componentDidMount() {
    
  }

  setParentState(states) {
    this.setState(
      { uploadState: states }
    )
    console.log(this.state.uploadState);
    console.log(this.state.uploadState?.socketUploadDataReturn?.data?.id);
  }

  render() {
    return (
      <div>
          <div>
            <div>Test Upload Video (Socket.io)</div>
            <input type="file" id="file" placeholder="Upload File" className="form-control" onChange={e => {
              this.setState({selectedVideo: e.target.files[0]});
            }}/>
            <UploadVideo
              selectedFile={this.state.selectedVideo}
              uploadState={(states) => this.setParentState(states)}
            >
            </UploadVideo>
            {(this.state.uploadState.socketUploading && !this.state.uploadState.socketIsByte) &&
              <div>Uploading ({this.state.uploadState.socketMBDone} / {this.state.uploadState.socketTotalSizeMB} MB) ( {this.state.uploadState.socketPercent}% )</div>
            }
            {(this.state.uploadState.socketUploading && this.state.uploadState.socketIsByte) &&
              <div>Uploading ({this.state.uploadState.socketUploaded} / {this.state.uploadState.socketTotalSize} Byte) ( {this.state.uploadState.socketPercent}% )</div>
            }
            {this.state.uploadState.socketPercent === 100 &&
              <div>Uploaded ({this.state.uploadState.socketTotalSize.toLocaleString()}) ( {this.state.uploadState.socketPercent}% )</div>
            }
            {( this.state.uploadState.socketUrl && this.state.uploadState.socketUrl !== '') &&
            <div><a target='_blank' href={this.state.uploadState.socketUrl}>Link</a></div>
            }
          </div>
      </div>
    )
  }
}
export default TestVideo;