import React, { Component, Fragment } from 'react';
import { checkRole, pageToLimitOffset } from '../Utils';
import socketIOClient from "socket.io-client";

class UploadVideo extends Component {
  reader = new FileReader();
  socket;
  state = {
    url:'',
    socketUploading: false,
    socketPercent: 0,
    socketUploaded: 0,
    socketTotalSize: 0,
    socketMBDone : 0,
    socketTotalSizeMB: 0,
    socketUrl: '',
    socketIsByte: false,
    socketUploadDataReturn:{}
  }
  componentDidMount() {
    this.socket = socketIOClient(process.env.REACT_APP_BASEURL);
    this.socket.on('MoreData', (data =>{
      this.setState({
        socketPercent: (Math.round(data['Percent']*100)/100),
        socketMBDone: Math.round(((data['Percent']/100.0) * this.state.socketTotalSize) / 1048576),
        socketUploaded: Math.round(((data['Percent']/100.0) * this.state.socketTotalSize))
      })
      this.props.uploadState(this.state)
      const Place = data['Place'] * 524288;
      let NewFile;
      if(this.props.selectedFile.slice)
        NewFile = this.props.selectedFile.slice(Place, Place + Math.min(524288, (this.props.selectedFile.size-Place)));
      else if(this.props.selectedFile.webkitSlice)
        NewFile = this.props.selectedFile.webkitSlice(Place, Place + Math.min(524288, (this.props.selectedFile.size-Place)));
      else
        NewFile = this.props.selectedFile.mozSlice(Place, Place + Math.min(524288, (this.props.selectedFile.size-Place)));
      this.reader.readAsBinaryString(NewFile);
    }));
    this.socket.on('Done', (data => {
      console.log(data)
      if (data.sukses && data.data.file_name === this.state.fileName) {
        this.setState({
          socketPercent: 100,
          socketMBDone: this.state.socketTotalSizeMB,
          socketUploaded: this.state.socketTotalSize,
          socketUploading: false,
          socketUploadDataReturn: data,
          socketUrl: data.data.url
        })
        this.props.uploadState(this.state)
      }
    }))
  }

  componentDidUpdate(prevProps) {
    if (prevProps.selectedFile !== this.props.selectedFile) {
      this.uploadSocket(this.props.selectedFile);
    }
  }

  uploadSocket(e) {
    console.log('trigger uploadSocket');
    if (e) {
      const mb = Math.round(e.size / 1048576);
      // this.props.selectedFile = e;
      this.setState({
        socketUploading: true,
        socketPercent: 0,
        socketTotalSize: e.size,
        socketMBDone: 0,
        socketTotalSizeMB: mb,
        socketIsByte: mb < 10 ? true : false,
        fileName: e.name
      });
      this.props.uploadState(this.state)
      // const reader = new FileReader();
      this.reader.onload = (evnt => {
        console.log('Emiting Upload');
        this.socket.emit('Upload', { 'Name' : e.name, Data : evnt.target.result });
      })
      const token = localStorage.getItem('token');
      console.log('Emiting Start');
      this.socket.emit('Start', { 'Name' : e.name, 'Size' : e.size, 'Type': e.type, token: `Bearer ${token}` });
      }
  }

  render() {
    return (
      <div></div>
    )
  }
}
export default UploadVideo;