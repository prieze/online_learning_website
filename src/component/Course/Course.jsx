import React, { Component } from "react";
import Navigation from "../Navigation/Navigation";
import Plus from "../../gambar/plus.png";
import { Link } from "react-router-dom";
import Footer from "../Footer/Footer";
import { checkRole } from "../Utils";
import "./Course.css";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import _ from "lodash";
import { Container, Row, Col, Nav, Form, Button, Card, CardDeck, Image, Navbar, FormControl, Modal } from "react-bootstrap";

class Course extends Component {
  constructor(props) {
    super(props);
    this.state = {
      course: [],
      modalSortIsOpen: false,
      course_name: null,
      cover_photo: "base64 Image URL",
      description: false,
      loading: false,
      id: 0,
      store: null,
      search: "",
    };
    this.sortByPriceAsc = this.sortByPriceAsc.bind(this);
    this.sortByPriceDesc = this.sortByPriceDesc.bind(this);
  }

  onOpenSortModal = () => {
    this.setState({ modalSortIsOpen: true });
  };

  onCloseSortModal = () => {
    this.setState({ modalSortIsOpen: false });
  };

  componentDidMount() {
    const token = localStorage.getItem("token");
    const id = this.props.match.params.id;
    console.log(id);
    console.log(this.props);
    // this.setState({
    //   course: [],
    //   id: id,
    // });
    // if (checkRole("admin").sukses) {
    console.log(id);
    this.listCourse(id);
    // }
  }

  listCourse(catId = "0") {
    console.log(this.state);
    const token = localStorage.getItem("token");
    const search = this.state.search;
    console.log(typeof catId);
    let url = `${process.env.REACT_APP_BASEURL}/api/read/courses?limit=5&offset=0&search=${search}`;
    if (catId !== "0") {
      console.log("here");
      url = `${process.env.REACT_APP_BASEURL}/api/read/course/category/${catId}?limit=0&offset=0&search=${search}`;
    }else{
      url = `${process.env.REACT_APP_BASEURL}/api/read/courses?limit=0&offset=0&search=${search}`;
    }
    this.setState({ loading: true });
    fetch(url, {
      method: "GET",
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      },
    })
      .then((response) => response.json())
      .then((json) => {
        this.setState({
          course: json.rows,
          loading: false,
        });
        console.log(this.state.cover_photo);
        console.log(this.state.course);
        console.log(json.rows);
        console.log(json.rows.course_name);
      })
      .catch((error) => console.log("error", error));
  }

  sortByPriceAsc() {
    this.setState((prevState) => {
      this.state.course.sort((a, b) => a.price - b.price);
    });
    console.log("testAsc");
  }

  sortByPriceDesc() {
    this.setState((prevState) => {
      this.state.course.sort((a, b) => b.price - a.price);
    });
    console.log("testDesc");
  }

  onChange = (event) => {
    /* signal to React not to nullify the event object */
    event.persist();
    if (!this.debouncedFn) {
      this.debouncedFn = _.debounce(() => {
        //  let searchString = event.target.value;
        this.setState({ search: event.target.value });
        this.listCourse();
      }, 1000);
    }
    this.debouncedFn();
  };

  handleRemove(id) {
    console.log("handle remove");
    console.log(id);
    const token = localStorage.getItem("token");
    fetch(`${process.env.REACT_APP_BASEURL}/api/delete/course/${id}`, {
      method: "DELETE",
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      },
    })
      .then((response) => response.json())
      .then((json) => {
        if (json.sukses) {
          this.listCourse("1");
          this.props.history.push("/Course");
          toast.success("Course berhasil dihapus");
        }
      })
      .catch((error) => console.log("error", error));
  }

  render() {
    const { modalSortIsOpen } = this.state;
    const MAX_LENGTH = 20;
    return (
      <>
      <Navigation history={this.props.history}/>
      <Container fluid style={{ width: "100%", minHeight: "800px", paddingTop: "50px", backgroundColor: "#F1F1F1" }}>
      <ToastContainer />
        <Row className="justify-content-center">
          <Col md={10}>
            <Navbar expand="lg" style={{ background: "rgba(58, 44, 125, 0.34)", marginBottom: "3%" }}>
              <Navbar.Brand style={{ color: "#FFFFFF", fontFamily: "Gordita-bold" }}>Add New Course</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                  <Nav className="mr-auto" />
                    <Form inline>
                      <FormControl type="text" id="search" placeholder="Search" style={{ borderRadius: "30px" }} onChange={this.onChange} debounce="1000" />
                    </Form>
                    
                    <Button onClick={this.onOpenSortModal} style={{ border: "none", backgroundColor: "transparent", marginBottom: "3px" }}>
                      <svg width="29" height="28" viewBox="0 0 29 28" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <g clip-path="url(#clip0)">
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M13.647 1.75V5.173H27.1712V1.75H13.647Z" fill="white"/>
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M0 1.75H3.36912V5.20625H0V1.75Z" fill="white"/>
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M0 12.25V15.729H8.54818V12.25H0Z" fill="white"/>
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M18.7646 12.25V15.7133H27.2719V12.25H18.7646Z" fill="white"/>
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M0 22.75H13.6795V26.1835H0V22.75Z" fill="white"/>
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M23.8823 22.75H27.2941V26.1818H23.8823V22.75Z" fill="white"/>
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M18.7108 28.07C20.5651 28.07 22.1362 26.6315 22.1362 24.78C22.1362 22.925 20.5651 21 18.7108 21C16.8565 21 15.4287 22.6818 15.4287 24.535C15.4287 26.3883 16.8565 28.07 18.7108 28.07Z" fill="white"/>
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M13.4868 17.4878C15.2831 17.4878 17.0742 15.708 17.0742 13.8548C17.0742 12.0015 15.2831 10.5 13.4868 10.5C12.622 10.5028 11.7937 10.8577 11.184 11.4868C10.5743 12.1159 10.2331 12.9676 10.2354 13.8548C10.2354 15.708 11.6905 17.4878 13.4868 17.4878Z" fill="white"/>
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M8.62497 6.96675C10.4315 6.96675 11.9787 5.208 11.9787 3.35475C11.9787 1.5015 10.1944 0 8.38785 0C7.52055 0 6.68877 0.353446 6.07549 0.982584C5.46221 1.61172 5.11768 2.46502 5.11768 3.35475C5.11768 5.208 6.81844 6.96675 8.62497 6.96675Z" fill="white"/>
                        </g>
                        <defs>
                        <clipPath id="clip0">
                        <rect width="29" height="28" fill="white"/>
                        </clipPath>
                        </defs>
                      </svg>
                    </Button>
                </Navbar.Collapse>
            </Navbar>
          </Col>
        </Row>

        <Row style={{ width: '95%', margin: 'auto' }}>
          <Col xs={12} sm={6} md={2} lg={2}>
              <Card style={{ height: '290px',borderRadius: '10px', border: '0.2px solid #DFDFFF', backgroundColor: '#DFDFFF' }}>
                <Link to={`/AddCourse`}>
                <Card.Img variant="top" src={Plus} style={{ width: '30%', margin: 'auto', marginTop: '40%' }} />
                <Card.Body>
                    <Card.Title style={{ fontFamily: 'Gordita-bold', fontSize: '15px', color: '#020288', textAlign: 'center' }}>Add New Course</Card.Title>
                </Card.Body>
                </Link>
              </Card>
          </Col>
                    
          {this.state.course.map((course) => {
            return (
              <Col style={{ paddingBottom: "2%" }} xs={12} sm={6} md={2} lg={2}>
                <Card className="h-100" style={{ borderRadius: '10px' }}>
                  <Card.Img style={{ borderRadius: '10px 10px 0 0', height: '100px' }} src={course.cover.url} />
                  <Link to={`/EditCourse/${course.id}`} style={{ marginLeft: '90%', color: '#444444' }} ><span><i className="fa fa-pencil"></i></span></Link>
                  <Card.Body>
                    <Card.Title style={{ fontWeight: 'bold', fontSize: '15px', color: '#000000' }}>{course.course_name}</Card.Title>
                    <Card.Text style={{ fontSize: '14px', color: '#000000' }}>{`${course.description.substring(0, MAX_LENGTH)} ...`}</Card.Text>
                  </Card.Body>
                  <Card.Footer>
                    <Link to={`/DetailCourse/${course.id}`}>
                      <Button className="tombol-primary" size="sm" block>More Detail</Button>
                    </Link>
                  </Card.Footer>
                </Card>
              </Col>
            );
          })} 
        
        
        </Row>
        {/* </div> */}

          <Modal
          size="sm"
            show={modalSortIsOpen}
            onHide={this.onCloseSortModal}
            animation={false}
          >
            <Modal.Body>
              <fieldset>
                <Form.Group as={Row}>
                  <Form.Label
                    style={{ fontFamily: "Gordita-bold", textAlign: "center" }}
                    column
                    sm={12}
                  >
                    Sort Berdasarkan
                  </Form.Label>
                  <Col>
                    <Form.Check
                      style={{ fontFamily: "Gordita" }}
                      onClick={() => {this.sortByPriceAsc(); this.onCloseSortModal();}}
                      type="radio"
                      label="Harga Terendah - Harga Tertinggi"
                      name="formHorizontalRadios"
                      id="formHorizontalRadios2"
                    />
                    <Form.Check
                      style={{ fontFamily: "Gordita" }}
                      onClick={() => {this.sortByPriceDesc(); this.onCloseSortModal();}}
                      type="radio"
                      label="Harga Tertinggi - Harga Terendah"
                      name="formHorizontalRadios"
                      id="formHorizontalRadios3"
                    />
                  </Col>
                </Form.Group>
              </fieldset>
            </Modal.Body>
          </Modal>
        </Container>
        <Footer />
      </>
    );
  }
}

export default Course;