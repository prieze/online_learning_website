const fetchInstructorById = ((id) => {
  return new Promise((resolve, reject) => {
    const token = localStorage.getItem("token");
    return fetch(`${process.env.REACT_APP_BASEURL}/api/read/instructor/${id}`, {
      method: "GET",
      headers: {
          "Authorization": `Bearer ${token}`,
          "Content-Type": "application/json"
      },
    }).then((response) => {
      response.json()
      .then((result) => {
        console.log(result)
        resolve(result)
      })
    }).catch(err => {
      reject(err)
    })
  })
})

const fetchInstructorSearch = ((search) => {
  return new Promise((resolve, reject) => {
    const token = localStorage.getItem("token");
    return fetch(
      `${process.env.REACT_APP_BASEURL}/api/read/instructors?limit=10&search=${search}`,
      {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      }
    )
      .then((response) => response.json())
      .then((json) => {
        resolve(json)
      })
      .catch((error) => reject(error));
  })
})

const fetchCourseById = ((id) => {
  return new Promise((resolve, reject) => {
    const token = localStorage.getItem("token");
    fetch(`${process.env.REACT_APP_BASEURL}/api/read/course/${id}`, {
      method: "GET",
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      },
    }).then((response) => response.json())
    .then((json) => {
      if (json.sukses) {
        console.log(json.rows);
        console.log(json);
        resolve(json.rows)
      } else {
        console.log("Failed");
        console.log(json.msg);
        reject(json.msg)
      }
    })
    .catch((error) => reject(error));
  })
})

const fetchCategories = (() => {
  return new Promise((resolve, reject) => {
    const token = localStorage.getItem("token");
    fetch(
      `${process.env.REACT_APP_BASEURL}/api/read/categories?offset=0&search=`,
      {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      }
    )
    .then((response) => response.json())
    .then((json) => {
      if (json.sukses) {
        console.log(json.rows);
        resolve(json.rows)
      } else {
        console.log("Failed");
        console.log(json.msg);
        reject(json.msg)
      }
    })
    .catch((error) => reject(error));
  })
})

const updateCourse = ((id, body) => {
  return new Promise((resolve, reject) => {
    const token = localStorage.getItem("token");
    fetch(`${process.env.REACT_APP_BASEURL}/api/update/course/${id}`, {
      method: "PUT",
      body: JSON.stringify({
        ...body
      }),
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      },
    })
      .then((response) => response.json())
      .then((json) => {
        if (json.sukses) {
          resolve(json.data)
          // this.props.history.push("/Course");
          // toast.success("Course berhasil diupdate");
        } else {
          reject(json.msg)
        }
      })
      .catch((error) => {
        reject(error)
      });
  })
})

export {
  fetchInstructorById,
  fetchInstructorSearch,
  fetchCourseById,
  fetchCategories,
  updateCourse
}