import React from 'react';
import {useContext} from 'react';
import { Row, Col, Table, Accordion, Card, Button, useAccordionToggle, AccordionContext } from 'react-bootstrap';
import 'font-awesome/css/font-awesome.min.css';
import { useHistory } from "react-router-dom";
import {countMinuteFromSecond, countMinutetoSecond, sumDurationSubTopic } from '../Utils'

function ContextAwareToggle({ children, eventKey, callback }) {
    const currentEventKey = useContext(AccordionContext);
  
    const decoratedOnClick = useAccordionToggle(
      eventKey,
      () => callback && callback(eventKey),
    );
  
    const isCurrentEventKey = currentEventKey === eventKey;
  
    return (
        <>
        { isCurrentEventKey 
            ? <i className="fa fa-minus" aria-hidden="true" onClick={decoratedOnClick}/>
            : <i className="fa fa-plus" aria-hidden="true" onClick={decoratedOnClick}/>
        }
        </>
        
    );
  }

const InstructorList = (props) => {
    console.log("props list instructor : "+props);
    return (
        <Card style={{ width: "83%", margin: 'auto' }} key={props.key}>
            <Card.Header style={{background: "white",color: "black"}}>
                <Row>
                    <Col xs={4} lg={4}>
                    {props.instructor?.instructors?.map(sub => {
                    return (
                            <Col xs={4} lg={4}>
                                {sub.instructor_name}
                            </Col>
                    )
                })}
                    {/* {props.instructor.instructors.instructor_name} */}
                    {/* {console.log(props);} */}
                    </Col>
                    {/* <Col xs={4} lg={4}>
                        {props.topic?.sub_topics.length} Material
                    </Col>
                    <Col xs={2} lg={2}>
                        {sumDurationSubTopic(props.topic?.sub_topics)}
                    </Col> */}
                    
                    <Col xs={1} lg={1}>
                        <i className="fa fa-pencil pencil" aria-hidden="true" onClick={() => props.edit(props.id)}></i>
                    </Col>
                    <Col xs={1} lg={1}>
                        {/* <i className="fa fa-pencil pencil" aria-hidden="true" onClick={() => props.edit(props.id)}></i> */}
                        <ContextAwareToggle eventKey={props.index.toString()}></ContextAwareToggle>
                        {/* <Accordion.Toggle as={Button} variant="link" eventKey={props.index.toString()}>
                            <i className="fa fa-plus" aria-hidden="true"></i>
                        </Accordion.Toggle> */}
                    </Col>
                </Row>
            </Card.Header>
            <Accordion.Collapse eventKey={props.index.toString()}>
            <Card.Body>
                {props.instructor?.instructors?.map(sub => {
                    return (
                        <Row className="justify-content-center" style={{background: "#E8E8E8"}}>
                            <Col xs={4} lg={4}>
                                {sub.instructor_name}
                            </Col>
                            <Col xs={4} lg={4}>
                                {sub.job_title}
                            </Col>
                            <Col xs={4} lg={4}>
                                {sub.biography}
                            </Col>
                        </Row>
                    )
                })}
            </Card.Body>
            </Accordion.Collapse>
        </Card>
    //     <Table>
    //     <Row className="justify-content-center">
    //     <Col >
    //             <th width="200px">
    //             <tr>{props.instructor_name}</tr>
    //             {/* <tr>{props.sub_topic}</tr> */}
    //         </th>
    //         <th width="50px"><i className="fa fa-plus" aria-hidden="true"></i>
    //         {/* <i style={{ color: '#020288', marginLeft: '88%'}} className="fa fa-pencil pencil" aria-hidden="true" onClick={() => props.edit(props.id)}></i> */}
    //         </th>
            
    //     </Col>
    // </Row></Table>
    )
}

export default InstructorList;