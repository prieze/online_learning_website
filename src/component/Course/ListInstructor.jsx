import React, { Component } from "react";
import { Collapse, Divider, Button } from 'antd';
import {PlusOutlined, MinusOutlined, EditOutlined, SettingOutlined} from '@ant-design/icons'
import ModalInstructor from './ModalInstructor'
const { Panel } = Collapse;

class ListInstructor extends Component{
  state = {
    modalAddIsOpen: false,
    instructor: {}
  }
  formRef = React.createRef();
  customExpandIcon(props) {
    if (props.isActive) {
      return <MinusOutlined onClick={((e) => {
        props.expandIcon(props, e);
      })}/>
    } else {
      return <PlusOutlined onClick={((e) => {
        props.expandIcon(props, e);
      })}/>
    }
  }
  
  // editButton(id){
  //   // console.log('edit clicked');
  //   // this.props.genExtra(id);
  //   // console.log(this.props);
  //   // if (onEdit) {
  //   //   return <EditOutlined
  //   //     onClick={event => {
  //   //       console.log(event);
  //   //       event.stopPropagation();
  //   //       console.log('edit button clicked')
  //   //       this.onOpenAddModal(data);
  //   //     }}
  //   //   />
  //   // } 
  // };
  // genExtra(id) {
  //   console.log(id);
  //   console.log(this.props.data);
  //   console.log(this.props.data.filter(inst => inst.id !== id));
  //   const instructor = this.props.data.filter(inst => inst.id !== id);
  //   this.setState({inst});
  //   console.log(inst);
  //   // console.log(this.state);
  // }

  onOpenAddModal = (data) => {
    this.setState({ 
      modalAddIsOpen: true,
      instructor: data
    });
  };

  defaultKey = (this.props.data && this.props.data[0]) ? this.props.data[0].id : '1'

  render() {
    // const genExtra = (id) => (
    //   <SettingOutlined
    //     onClick={event => {
    //       // If you don't want click extra trigger collapse, you can prevent this:
    //       console.log('test');
    //       console.log(this.state);
    //       console.log(id);
    //     }}
    //   />
    // );
    // console.log(`instructor`,this.props.data.length);
    return (
      <>
        <Collapse 
          accordion 
          style={{ width: "83%", margin: 'auto' }}
          expandIconPosition={"right"}
          defaultActiveKey={this.defaultKey}
          expandIcon={(props) => this.customExpandIcon(props)}
        >
          {this.props.data?.map(list => {
            // console.log(`instructor `,list);
            if (this.props.detail) {
              return <Panel header={list.instructor_name} key={list.id}
                style={{backgroundColor: "#EFEFEF"}}
                // extra={
                //   // <SettingOutlined onClick={this.editButton(list.id)}/>
                //   // <Button onClick={()=>{this.props.onDelete(list.id)}}>Delete</Button>
                // }
              >
                <p><b>Job Title</b></p>
                <p>{list.job_title}</p>
                <Divider />
                <p><b>BioGraphy</b></p>
                <p>{list.biography}</p>
              </Panel>  
            }else{
              return <Panel header={list.instructor_name} key={list.id}
                style={{backgroundColor: "#EFEFEF"}}
                extra={
                  // <SettingOutlined onClick={this.editButton(list.id)}/>
                  <Button onClick={()=>{this.props.onDelete(list.id)}}>Delete</Button>
                }
              >
                <p><b>Job Title</b></p>
                <p>{list.job_title}</p>
                <Divider />
                <p><b>BioGraphy</b></p>
                <p>{list.biography}</p>
              </Panel>
            }
          })}
        </Collapse>
        { this.props.is_edit &&
          <ModalInstructor
            formRef={this.formRef}
            modalAddIsOpen={this.state.modalAddIsOpen}
            onCloseAddModal={() => {this.setState({modalAddIsOpen: false})}}
            addInstructor={(data) => {this.props.addInstructor(data)}}
            is_edit={this.props.is_edit}
            instructors={this.state.instructor}
          />
        }
      </>
    )
  }
}

export default ListInstructor;