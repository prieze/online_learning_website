import React, { Component } from "react";
import Navigation from "../Navigation/Navigation";
import { Link } from "react-router-dom";
import "./Course.css";
import Footer from "../Footer/Footer";
import { Popconfirm, message } from 'antd';
import {
  Container,
  Row,
  Col,
  Form,
  Card,
  Navbar,
  Button,
  Modal,
  Accordion,
  ProgressBar
} from "react-bootstrap";
import UploadVideo from '../Upload/uploadVideo';
import AddTopic from './AddTopic';
import List from './List';
import AddInstructor from './AddInstructor';
import ListInstructor from './ListInstructor';
import AddExam from './AddExam'
import 'font-awesome/css/font-awesome.min.css';
import { fetchCategories, fetchCourseById, updateCourse} from './api'

class CourseAll extends Component {
  addTopicRef = React.createRef();
  SelectedFile;
  reader = new FileReader();
  socket;
  state = {
    list: [],
    topics: [],
    exams: [],
    instructors: [],
    course_name: null,
    cover_photo: null,
    description: null,
    discount: 0,
    price: null,
    status: 1,
    categoryId: null,
    file: null,
    url: "base64 url",
    videoid: "",
    videoId: "",
    duration: 0,
    topic_name_edit: "",
    sub_topic_name_edit: "",
    duration_edit: "",
    image: "",
    selectedVideo: {},
    uploadState: {},
    incIdTopic: 1,
    is_edit: false,
    is_detail: false,
    courses: {},
    toggle: 'Add',
    master_soals: [],
    passing_grade: 0,
    errors: {}
  };

  componentDidMount = async() => {
    const id = this.props.match.params.id;
    this.listCategory();
    // const list = await fetchCategories()
    console.log(`id: ${id}`);
    console.log(`is_edit: ${this.props.is_edit}`);
    if (id && this.props.is_edit) {
      const course = await fetchCourseById(id);
      const master_soals = course.master_soals?.map(a => {
        delete a.id;
        return a
      })
      const course2 = {
        ...course,
        master_soals
      }
      const toggle = this.calcAddEditDetail();
      await this.setState((state) => {
        return {
          ...state,
          ...course2,
          is_edit: id,
          toggle,
        }
      })
      console.log(this.state);
      console.log(course2);
    } else {
      this.setState({
        toggle: 'Add',
        is_edit: false,
        is_detail: false
      })
    }
  }

  calcAddEditDetail = () => {
    let hasil = 'Add';
    if (this.props.is_edit) {
      hasil = 'Edit'
    } else if (this.props.is_detail) {
      hasil = 'Detail'
    }
    return hasil;
  }

  onFileChange = (event) => {
    // Update the state
    this.setState({ image: event.target.files[0] });
  };

  setParentState(states) {
    this.setState(
      { uploadState: states }
    )
  }

  setAddTopicState(states) {
    if (states.id) {
      const hasil = this.state.topics.map(a => {
        if (a.id === states.id) {
          a = states;
        }
        return a;
      })
      this.setState({
        topics: hasil
      })
    }{
      states.id = `add${this.state.incIdTopic}`
      this.setState({incIdTopic: parseInt(this.state.incIdTopic) + 1})
    }
    const topics = this.state.topics.concat(states);
    this.setState({ topics })
  }

  setAddExamState(states) {
    // const exams = this.state.master_soal.concat(states);
    console.log(states);
    const passing_grade = states.passing_grade;
    this.setState({ master_soals: states.soal, passing_grade })
  }

  setAddInstructorsState(states) {
    const instructors = this.state.instructors.concat(states);
    this.setState({instructors})
    console.log(instructors);
  }

  listCategory(){
    fetchCategories().then(list => {
      console.log('categories:');
      console.log(list);
      this.setState({list});
    }).catch(err => {
      console.log(err);
    })
  }

  toBase64 = (file) =>
    new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = (error) => reject(error);
    });

  findf = (f) => {
    f = f.split(";base64,");
    if (f.length > 1) {
      f = f[1];
    } else {
      f = f[0];
    }
    return f;
  }

  validasi() {
    let hasil = true;
    if (this.state.errors){
      const keys = Object.keys(this.state.errors)
      keys.map(a => {
        if (a && this.state.errors[a] && this.state.errors[a] !== '') {
          hasil = false;
        }
      })
    }
    console.log('Validasi', hasil);
    return hasil
  }

  testApiCources() {
    if (!this.state.image) {
      message.error("image can not be empty", 1)
      return;
    }
    if (this.state.instructors.length === 0) {
      message.error("instructor can not be empty", 1)
      return;
    }
    if (!this.validasi()) { return ;}
    this.toBase64(this.state.image).then((f) => {
      f = this.findf(f)
      console.log(this.state);
      const token = localStorage.getItem("token");
      console.log(f);
      console.log(this.state.instructors);
      const topics = this.state.topics.map(a => {
        a.sub_topics.map(b => {
          delete b.video
        })
        delete a.id;
        return a;
      })
      fetch(
        `${process.env.REACT_APP_BASEURL}/api/create/course/${this.state.categoryId}`,
        {
          method: "POST",
          body: JSON.stringify({
            course_name: this.state.course_name,
            cover_photo: "base64 url",
            description: this.state.description,
            price: this.state.price,
            discount: this.state.discount,
            status: 1,
            video_id: this.state.uploadState.socketUploadDataReturn.data.id,
            topics: topics,
            // instructors: this.state.instructors,
            cover: {
              file: f,
              file_name: this.state.image.name,
              file_size: this.state.image.size,
              file_type: this.state.image.type,
            },
            soal: this.state.master_soals,
            passing_grade: this.state.passing_grade,
            instructors: this.state.instructors.map(i => i.id)
          }),
          headers: {
            Authorization: `Bearer ${token}`,
            "Content-Type": "application/json",
          },
        }
      ).then((response) => {
        response.json().then((result) => {
          if (result.sukses) {
            console.log(result);
            this.props.history.push("/Course");
            message.success("Successfully !", 1);
          } else {
            message.error(result.msg, 1);
          }
        });
      });
    });
  }
  
  onSaveEditApi = async() => {
    if (!this.validasi()) { return ;}
    if (this.state.instructors.length === 0) {
      message.error("instructor can not be empty", 1)
      return;
    }
    const topics = this.state.topics.map(a => {
      a.sub_topics.map(b => {
        delete b.video
        delete b.id
        delete b.topicId
      })
      delete a.id;
      delete a.courseId;
      return a;
    })
    const body = {
      course_name: this.state.course_name,
      cover_photo: "base64 url",
      description: this.state.description,
      price: this.state.price,
      discount: this.state.discount,
      status: 1,
      topics: topics,
      instructors: this.state.instructors.map(i => i.id),
      soal: this.state.master_soals,
      passing_grade: this.state.passing_grade
    }
    if (this.state.image) {
      let f = await this.toBase64(this.state.image);
      f = this.findf(f)
      body.cover = {
        file: f,
        file_name: this.state.image.name,
        file_size: this.state.image.size,
        file_type: this.state.image.type,
      }
    } else {
      body.coverId = this.state.coverId
    }
    // if (this.state.videoId) {
    // } 
    
    if (this.state.uploadState && this.state.uploadState.socketUploadDataReturn &&
      this.state.uploadState.socketUploadDataReturn.data && this.state.uploadState.socketUploadDataReturn.data.id) {
        body.video_id = this.state.uploadState.socketUploadDataReturn.data.id
    }else {
        body.video_id = this.state.videoId
      }
    updateCourse( this.state.id, body ).then(hasil => {
      this.props.history.push("/Course");
      message.success("Course Updated", 1);
    }).catch(err => {
      message.error("Failed update course", 1);
    });
  }

  setAddInstructorsState(states) {
    console.log(states);
    const instructors = this.state.instructors.concat(states);
    this.setState({instructors})
    console.log(instructors);
    console.log(this.state.instructors);
  }
  genExtra(id) {
    console.log(id);
    console.log(this.state.instructors.filter(inst => inst.id !== id));
    const instructors = this.state.instructors.filter(inst => inst.id !== id);
    this.setState({instructors});
    console.log(instructors);
    console.log(this.state.instructors);
  }
  // (
  //   <SettingOutlined
  //     onClick={event => {
  //       // If you don't want click extra trigger collapse, you can prevent this:
  //       console.log('test');
  //       console.log(this.state);
  //       console.log(id);
  //     }}
  //   />
  // );

  setAddTopicState(states) {
    console.log(states);
    if (states.id) {
      const hasil = this.state.topics.map(a => {
        if (a.id === states.id) {
          a = states;
        }
        return a;
      })
      console.log(hasil);
      this.setState({topics: hasil})
    } else {
      if (states.topic_name && states.sub_topics) {
        states.id = `add${this.state.incIdTopic}`
        const topics = this.state.topics.concat(states);
        this.setState({ 
          topics,
          incIdTopic: parseInt(this.state.incIdTopic) + 1
        })
      }
    }
  }

  onOpenEditTopicModal(states) {
    console.log(states);
    this.addTopicRef.current.onOpenAddModal(states);
  }

  addInstructor = (data) => {
    console.log(this.state.instructors)
    console.log(data);
    const b = [];
    this.state.instructors.map(a => {
      if (a.id === data.id) {
        b.push(data)
      } else {
        b.push(a);
      }
    })
    this.setState({instructors: b})
  }

  render() {
    return (
      <>
        <Navigation history={this.props.history}/>
        <Container
          fluid
          style={{
            width: "100%",
            minHeight: "2000px",
            paddingTop: "50px",
            backgroundColor: "#F1F1F1",
          }}
        >
          <Row className="justify-content-center">
            <Col xs={10} md={10}>
              <Navbar
                expand="lg" style={{ background: "rgba(58, 44, 125, 0.34)" }}>
                <Link to={`/Course`}>
                  <button className="btn icon-back">
                    <span style={{ color: "#FFFFFF" }} className="fa fa-chevron-left"></span>
                  </button>
                </Link>
                <Navbar.Brand style={{ color: "#FFFFFF", fontFamily: "Gordita-bold" }}>{this.state.toggle} Course</Navbar.Brand>
              </Navbar>
            </Col>
          </Row>

          <Row className="justify-content-center">
            <Col xs={10} md={10}>
              <h4 className="title-course">1. Course</h4>
            </Col>
          </Row>

          <Row>
            <Col>
              <Card style={{ width: "83%", margin: 'auto' }}>
                <Card.Body style={{ padding: '2rem' }}>
                  <Row>
                    <Col xs={12} lg={5}>
                      <Form>
                        <Form.Group>
                          <Form.Label className="form-label">Course Name *</Form.Label>
                          <Form.Control type="text" onChange={(event) => {
                            this.setState({
                              course_name: event.target.value,
                            });
                          }}
                          value={this.state.course_name}
                          />
                        </Form.Group>

                        <Form.Group>
                          <input type="file" name="file" id="file" className="inputfile" style={{ overflow: 'hidden' }} onChange={this.onFileChange} />
                          <label style={{ width: "100%" }} for="file"><i className="fa fa-upload"></i> Add Cover Photo</label>
                          {/* <div>url: {this.state.url}</div> */}
                        </Form.Group>

                        <Form.Group>
                          <Form.Label>Course Categories *</Form.Label>
                          <Form.Control as="select"
                          onChange={(event) => {
                            this.setState({
                              categoryId: event.target.value,
                            });
                          }}
                          onChange={(event) => {
                            let categoryId = '';
                            let error;
                            if (event.target.value) {
                              const temp = parseInt(event.target.value);
                              if (temp === 0) {
                                error = `categoryId is Required`
                              }
                              categoryId = temp;
                            }
                            this.setState({ 
                              categoryId,
                              errors: {
                                ...this.state.errors,
                                categoryId: error
                              }
                            });
                          }}
                          value={this.state.categoryId}
                          >
                            <option key={0}>
                              Choose Category
                            </option>
                            {this.state.list?.map((list) => (
                              <option key={list.id} value={list.id}>
                                {list.category_name}
                              </option>
                            ))}
                          </Form.Control>
                        </Form.Group>

                        <Form.Group>
                          <Form.Label>Description *</Form.Label>
                          <Form.Control as="textarea" rows="3" onChange={(event) => {
                            this.setState({
                              description: event.target.value,
                            });
                          }}
                          value={this.state.description}
                          />
                        </Form.Group>
                      </Form>
                    </Col>
                    <Col lg={2} />
                    <Col xs={12} lg={5}>
                      <Form>
                        <Form.Group>
                          <Card style={{ width: "100%", marginTop: '6%', backgroundColor: "#EFEFEF", boxShadow: "none", border: "none" }}>
                            <Card.Body style={{ padding: '2rem' }}>
                              <Card.Title style={{ color: '#392C7E', textAlign: 'center', fontFamily: 'Gordita-bold', fontSize: '16px' }}>Submit a Video Demo</Card.Title>
                              <Card.Text style={{ color: '#392C7E', textAlign: 'center', fontFamily: 'Gordita', fontSize: '11px' }}>CLICK ON THE BUTTON OR DRAG & DROP FILE HERE</Card.Text>
                            </Card.Body>
                            {/* <input type="file" id="file" className="nputfile" onChange={(e) => {
                              this.setParentState(e.target.files[0]);
                            }} 
                            />*/}
                            <input type="file" id="file" placeholder="Upload File" style={{ overflow: 'hidden' }} className="form-control" onChange={e => {
                              this.setState({ selectedVideo: e.target.files[0] });
                            }} />
                            <UploadVideo
                              selectedFile={this.state.selectedVideo}
                              uploadState={(states) => this.setParentState(states)}
                            >
                            </UploadVideo>
                            {(this.state.uploadState?.socketUploading && this.state.uploadState?.socketPercent !== 100) &&
                              <ProgressBar now={this.state.uploadState?.socketPercent} label={`${this.state.uploadState?.socketPercent}%`} />
                            }
                          </Card>
                        </Form.Group>
                        <Form.Group>
                          <Form.Label>Price *</Form.Label>
                          <Form.Control
                            type="text"
                            onChange={(event) => {
                              this.setState({ price: event.target.value });
                            }}
                            value={this.state.price}
                          />
                        </Form.Group>
                        <Form.Group>
                          <Form.Label>Discount (%)</Form.Label>
                          <Form.Control
                            type="number"
                            onChange={(event) => {
                              let discount = '';
                              let error;
                              if (event.target.value) {
                                const temp = parseInt(event.target.value);
                                if (!(temp >= 0 && temp <= 100)) {
                                  error = `Discount must be from 0% to 100%`
                                }
                                discount = temp;
                              }
                              this.setState({ 
                                discount,
                                errors: {
                                  ...this.state.errors,
                                  discount: error
                                }
                              });
                            }}
                            value={this.state.discount}
                          />
                          {this.state.errors.discount &&
                          <Form.Label style={{color: 'red'}}>{this.state.errors.discount}</Form.Label>
                          }
                        </Form.Group>
                      </Form>
                    </Col>
                  </Row>
                </Card.Body>
              </Card>
            </Col>
          </Row>
          <Row className="justify-content-center">
            <Col xs={10} md={10}>
              <h4 className="title-course">2. Instructor</h4>
            </Col>
          </Row>
          <AddInstructor addInstructor={(states) => {this.setAddInstructorsState(states)}}/>
            <ListInstructor data={this.state.instructors} onDelete={(id) => {this.genExtra(id)}} />
          <Row className="justify-content-center">
            <Col xs={10} md={10}>
              <h4 className="title-course">3. Content & Material</h4>
            </Col>
          </Row>
          <Card
            className="card-grid"
            style={{ width: "83%", margin: 'auto' }}
          >
            <Row>
              
                <AddTopic
                  addTopic={(states) => { this.setAddTopicState(states) }}
                  ref={this.addTopicRef}
                  topic={this.state.selectedTopic}
                />
                <AddExam addExam={(states) => { this.setAddExamState(states) }}
                  exam={this.state.master_soals}
                  passing_grade={this.state.passing_grade}
                />
              
            </Row>
          </Card>
          <Accordion defaultActiveKey="0">
            {
                this.state.topics?.map((list, index) => {
                    return <List
                        id={list.id}
                        key={list.id}
                        topic={list}
                        index={index}
                        is_edit
                        edit={(hasil) => this.onOpenEditTopicModal(hasil)}
                        sub_topic={(list && list.topics && list.topics.sub_topic_name) ? list.topics.sub_topic_name : null}></List>
                })
            }
            </Accordion>

          <div style={{ fontFamily: 'Gordita', marginLeft: '1070px' }}><br />
            {this.state.is_edit
              ? <Button className="button-grape" 
                  onClick={() => {
                    this.onSaveEditApi();
                  }}
                >Edit</Button>
              : <Button className="button-grape" 
                  onClick={() => {
                    this.testApiCources();
                  }}
                  // disabled={this.state.uploadState.socketPercent=="100" ? false : true}
                >Save</Button>
            }
        </div>

        </Container>
        <Footer />
      </>
    );
  }
}

export default CourseAll;