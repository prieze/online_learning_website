import React, { Component } from "react";
import ModalInstructor from "./ModalInstructor";
import {
  Row,
  Col,
  Card,
  Button,
} from "react-bootstrap";

class AddInstructor extends Component{
  state = {
    instructors:[],
    instructor:{},
    id: null,
    file: null,
    fileList: [],
    photo: null,
    fileListPhoto: [],
    photoResult: null,
    cvResult: null,
    choosing: false,
    instructor_dropdown: [],
    modalAddIsOpen: false
  };
  formRef = React.createRef();
  onOpenAddModal = () => {
    this.setState({ 
      modalAddIsOpen: true,
      instructor:{},
      file: null,
      photo: null,
      fileList: [],
      fileListPhoto: [],
      photoResult: null,
      cvresult: null,
      choosing: false,
      instructor_dropdown: []
    });
    if (this.formRef && this.formRef.current) {
      this.formRef.current.resetFields();
    }
  };
  render(){
    return(
      <>
        <Row>
          <Col>
            <Card
              className="card-grid"
              style={{ width: "83%", margin: 'auto' }}
            >
              <Card.Body style={{ padding: '2rem' }}>
                <Button
                  onClick={this.onOpenAddModal}
                  className="button-grape btn-sm"
                  style={{ width: "30%" }}
                >
                  Add New Instructor
                </Button>
              </Card.Body>
            </Card>
          </Col>
        </Row>
        <ModalInstructor
          formRef={this.formRef}
          modalAddIsOpen={this.state.modalAddIsOpen}
          onCloseAddModal={() => {this.setState({modalAddIsOpen: false})}}
          addInstructor={(data) => {this.props.addInstructor(data)}}
        />
      </>
    )
  }
}

export default AddInstructor;