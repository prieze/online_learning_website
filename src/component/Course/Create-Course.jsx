import React, { Component } from "react";
import Navigation from "../Navigation/Navigation";
import { Link } from "react-router-dom";
import "./Course.css";
import Footer from "../Footer/Footer";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import {
  Container,
  Row,
  Col,
  Form,
  Card,
  Navbar,
  Button,
  Modal,
  Accordion,
  ProgressBar
} from "react-bootstrap";
import socketIOClient from "socket.io-client";
import UploadVideo from '../Upload/uploadVideo';
import AddTopic from './AddTopic';
import List from './List';
import AddInstructor from './AddInstructor';
import ListInstructor from './ListInstructor';
import AddExam from './AddExam'
import 'font-awesome/css/font-awesome.min.css';

class AddCourse extends Component {
  SelectedFile;
  reader = new FileReader();
  socket;
  state = {
    list: [],
    topics: [],
    soal: [],
    instructors: [],
    course_name: null,
    cover_photo: null,
    description: null,
    discount: 0,
    price: null,
    status: 1,
    categoryId: null,
    file: null,
    url: "base64 url",
    videoid: "",
    videoId: "",
    duration: 0,
    topic_name_edit: "",
    sub_topic_name_edit: "",
    duration_edit: "",
    // socketUploading: false,
    // socketPercent: 0,
    // socketUploaded: 0,
    // socketTotalSize: 0,
    // socketMBDone: 0,
    // socketTotalSizeMB: 0,
    // socketUrl: "",
    image: "",
    modalAddIsOpen: false,
    selectedVideo: {},
    uploadState: {}, //Upload State dari Component untuk merubah data dari page ini
    edittopicModal: false,
    editinsModal: false,
    incIdTopic: 1
  };

  onOpenAddModal = () => {
    this.setState({ modalAddIsOpen: true });
  };

  onCloseAddModal = () => {
    this.setState({ modalAddIsOpen: false });
  };

  onOpenEditTopicModal = () =>{
    this.setState({ edittopicModal: true });
  };

  onCloseEditTopicModal = () => {
    this.setState({ edittopicModal: false });
  };

  onOpenEditInsModal = () =>{
    this.setState({ editinsModal: true });
  };

  onCloseEditInsModal = () => {
    this.setState({ editinsModal: false });
  };

  removetopic(e){
    console.log("isi e : " + e);
    const filter = this.state.topics.filter(obj => {
      console.log(`TCL obj : `, obj)
      return obj.id !== e
    })
    this.setState({topics:filter, edittopicModal: false});
  }

  onUpdateItem(z){
    console.log(this.state);
    var array = [];
    array.push({sub_topic_name: this.state.sub_topic_name_edit, duration: this.state.duration_edit,video_id: this.state.uploadState.socketUploadDataReturn.data.id, video:{file_name: this.state.uploadState.socketUploadDataReturn.data.file_name}})
    this.setState(prevState => ({
      topics: prevState.topics.map(
        obj => (obj.id === z ? {...obj, topic_name : this.state.topic_name_edit, sub_topics : array} : obj)
    )
  }));
  }

  componentDidMount() {
    this.listCategory();
    // const page = this.props.match.params.page
    //   ? this.props.match.params.page
    //   : 1;
    // console.log(this.state);
    // if (checkRole('admin').sukses) {
    //     this.listTable(page);
    // }
    // this.socket = socketIOClient(process.env.REACT_APP_BASEURL);
    // this.socket.on("MoreData", (data) => {
    //   this.setState({
    //     socketPercent: Math.round(data["Percent"] * 100) / 100,
    //     socketMBDone: Math.round(
    //       ((data["Percent"] / 100.0) * this.state.socketTotalSize) / 1048576
    //     ),
    //   });
    //   const Place = data["Place"] * 524288;
    //   let NewFile;
    //   if (this.SelectedFile.slice)
    //     NewFile = this.SelectedFile.slice(
    //       Place,
    //       Place + Math.min(524288, this.SelectedFile.size - Place)
    //     );
    //   else if (this.SelectedFile.webkitSlice)
    //     NewFile = this.SelectedFile.webkitSlice(
    //       Place,
    //       Place + Math.min(524288, this.SelectedFile.size - Place)
    //     );
    //   else
    //     NewFile = this.SelectedFile.mozSlice(
    //       Place,
    //       Place + Math.min(524288, this.SelectedFile.size - Place)
    //     );
    //   this.reader.readAsBinaryString(NewFile);
    // });
    // this.socket.on("Done", (data) => {
    //   console.log(data);
    //   if (data.sukses) {
    //     this.setState({
    //       videoid: data.data.id,
    //       socketPercent: 100,
    //       socketMBDone: this.state.socketTotalSizeMB,
    //       socketUploaded: this.state.socketTotalSize,
    //       socketUploading: false,
    //       socketUrl: data.data.url,
    //     });
    //   }
    // });
  }

  onFileChange = (event) => {
    // Update the state
    this.setState({ image: event.target.files[0] });
  };

  setParentState(states) {
    this.setState(
      { uploadState: states }
    )
    console.log(this.state.uploadState);
    console.log(this.state.uploadState?.socketUploadDataReturn?.data?.id);
  }

  setAddTopicState(states) {
    if (states.id) {
      const hasil = this.state.topics.map(a => {
        if (a.id === states.id) {
          a = states;
        }
        return a;
      })
      this.setState({
        topics: hasil
      })
    }{
      states.id = `add${this.state.incIdTopic}`
      this.setState({incIdTopic: parseInt(this.state.incIdTopic) + 1})
    }
    const topics = this.state.topics.concat(states);
    this.setState({ topics })
  }


  setAddExamState(states) {
    if (states.id) {
      const hasil = this.state.soal.map(a => {
        if (a.id === states.id) {
          a = states;
        }
        return a;
      })
      this.setState({
        soal: hasil
      })
    }
    const soal = this.state.soal.concat(states);
    this.setState({ soal })
  }

  // setAddExamState(states) {
  //   const exams = this.state.exams.concat(states);
  //   this.setState({ exams })
  // }

  setAddInstructorsState(states) {
    const instructors = this.state.instructors.concat(states);
    this.setState({instructors})
    console.log(instructors);
  }

  listCategory() {
    const token = localStorage.getItem("token");
    this.setState({ loading: true });
    fetch(
      `${process.env.REACT_APP_BASEURL}/api/read/categories?offset=0&search=`,
      {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      }
    )
      .then((response) => response.json())
      .then((json) => {
        this.setState({
          list: json.rows,
          loading: false,
        });
        console.log(this.state.list);
        console.log("this.state.list");
      })
      .catch((error) => console.log("error", error));
  }

  setParentState(states) {
    this.setState(
      { uploadState: states }
    )
    console.log(this.state.uploadState);
    console.log(this.state.uploadState?.socketUploadDataReturn?.data?.id);
  }

  // uploadSocket(e) {
  //   if (e) { 
  //     const mb = Math.round(e.size / 1048576);
  //     this.SelectedFile = e;
  //     this.setState({
  //       socketUploading: true,
  //       socketPercent: 0,
  //       socketTotalSize: e.size,
  //       socketMBDone: 0,
  //       socketTotalSizeMB: mb,
  //     });
  //     // const reader = new FileReader();
  //     console.log(e);
  //     this.reader.onload = (evnt) => {
  //       console.log("Emiting Upload");
  //       this.socket.emit("Upload", { Name: e.name, Data: evnt.target.result });
  //     };
  //     const token = localStorage.getItem("token");
  //     console.log("Emiting Start");
  //     this.socket.emit("Start", {
  //       Name: e.name,
  //       Size: e.size,
  //       Type: e.type,
  //       token: `Bearer ${token}`,
  //     });
  //   }
  // }

  toBase64 = (file) =>
    new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = (error) => reject(error);
    });

  testApiCources() {
    if (!this.state.image) {
      toast.error("image can not be empty")
      return;
    }
    this.toBase64(this.state.image).then((f) => {
      f = f.split(";base64,");
      if (f.length > 1) {
        f = f[1];
      } else {
        f = f[0];
      }
      console.log(this.state);
      const token = localStorage.getItem("token");
      console.log(f);
      console.log(this.state.instructors);
      const topics = this.state.topics.map(a => {
        a.sub_topics.map(b => {
          delete b.video
        })
        delete a.id;
        return a;
      })
      fetch(
        `${process.env.REACT_APP_BASEURL}/api/create/course/${this.state.categoryId}`,
        {
          method: "POST",
          body: JSON.stringify({
            course_name: this.state.course_name,
            cover_photo: "base64 url",
            description: this.state.description,
            price: this.state.price,
            discount: this.state.discount,
            status: 1,
            video_id: this.state.uploadState.socketUploadDataReturn.data.id,
            topics: topics,
            soal: this.state.soal.map(i => i.id),
            instructors: this.state.instructors,
            // instructors:
            // {
            //   instructor_name: this.state.instructor_name,
            //   job_title: "test",
            //   biography: "abc",
            //   cv: "aku pengajar"
            // },
            cover: {
              file: f,
              file_name: this.state.image.name,
              file_size: this.state.image.size,
              file_type: this.state.image.type,
            },
            instructors: this.state.instructors.map(i => i.id)
          }),
          headers: {
            Authorization: `Bearer ${token}`,
            "Content-Type": "application/json",
          },
        }
      ).then((response) => {
        response.json().then((result) => {
          if (result.sukses) {
            console.log(result);
            // console.log(result.files.url);
            // this.setState({ url: result.files.url });
            this.props.history.push("/Course");
            toast.success("Successfully !");
          } else {
            toast.error(result.msg);
          }
        });
      });
    });
  }
  saveFile(e) {
    if (!e) {
      return;
    }
    this.toBase64(e).then((f) => {
      // console.log(`base64: ${f}`);
      // REMOVE BEFORE ;base64,
      f = f.split(";base64,")[1];
      const token = localStorage.getItem("token");
      fetch(`${process.env.REACT_APP_BASEURL}/api/upload`, {
        method: "POST",
        body: JSON.stringify({
          file: f,
          file_name: e.name,
          file_size: e.size,
          file_type: e.type,
        }),
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      }).then((response) => {
        response.json().then((result) => {
          console.log(result);
          console.log(result.files.url);
          this.setState({ url: result.files.url });
        });
      });
    });
  }

  render() {
    const { modalAddIsOpen, list } = this.state;
    // var create;
    // if (this.state.uploadState.socketPercent !== "100") {

    // }
    return (
      <>
        <Navigation history={this.props.history}/>
        <Container
          fluid
          style={{
            width: "100%",
            minHeight: "2000px",
            paddingTop: "50px",
            backgroundColor: "#F1F1F1",
          }}
        >
          <ToastContainer />
          <Row className="justify-content-center">
            <Col xs={10} md={10}>
              <Navbar
                expand="lg" style={{ background: "rgba(58, 44, 125, 0.34)" }}>
                <Link to={`/Course`}>
                  <button className="btn icon-back">
                    <span style={{ color: "#FFFFFF" }} className="fa fa-chevron-left"></span>
                  </button>
                </Link>
                <Navbar.Brand style={{ color: "#FFFFFF", fontFamily: "Gordita-bold" }}>Add Course</Navbar.Brand>
              </Navbar>
            </Col>
          </Row>

          <Row className="justify-content-center">
            <Col xs={10} md={10}>
              <h4 className="title-course">1. Course</h4>
            </Col>
          </Row>

          <Row>
            <Col>
              <Card style={{ width: "83%", margin: 'auto' }}>
                <Card.Body style={{ padding: '2rem' }}>
                  <Row>
                    <Col xs={12} lg={5}>
                      <Form>

                        <Form.Group>
                          <Form.Label className="form-label">Course Name *</Form.Label>
                          <Form.Control type="text" onChange={(event) => {
                            this.setState({
                              course_name: event.target.value,
                            });
                          }}
                          />
                        </Form.Group>

                        <Form.Group>
                          <input type="file" name="file" id="file" className="inputfile" onChange={this.onFileChange} />
                          <label style={{ width: '100%' }} for="file"><i className="fa fa-upload"></i> Add Cover Photo</label>
                          {/* <div>url: {this.state.url}</div> */}
                        </Form.Group>

                        <Form.Group>
                          <Form.Label>Course Categories *</Form.Label>
                          <Form.Control as="select" onChange={(event) => {
                            this.setState({
                              categoryId: event.target.value,
                            });
                          }}
                          >
                            <option disabled selected>
                              Choose Category
                            </option>
                            {this.state.list.map((list) => (
                              <option key={list.id} value={list.id}>
                                {list.category_name}
                              </option>
                            ))}
                          </Form.Control>
                        </Form.Group>

                        <Form.Group>
                          <Form.Label>Description *</Form.Label>
                          <Form.Control as="textarea" rows="3" onChange={(event) => {
                            this.setState({
                              description: event.target.value,
                            });
                          }}
                          />
                        </Form.Group>

                      </Form>
                    </Col>
                    <Col lg={2} />
                    <Col xs={12} lg={5}>
                      <Form>
                        <Form.Group>
                          <Card style={{ width: "100%", marginTop: '6%', backgroundColor: "#EFEFEF", boxShadow: "none", border: "none" }}>
                            <Card.Body style={{ padding: '2rem' }}>
                              <Card.Title style={{ color: '#392C7E', textAlign: 'center', fontFamily: 'Gordita-bold', fontSize: '16px' }}>Submit a Video Demo</Card.Title>
                              <Card.Text style={{ color: '#392C7E', textAlign: 'center', fontFamily: 'Gordita', fontSize: '11px' }}>CLICK ON THE BUTTON OR DRAG & DROP FILE HERE</Card.Text>
                            </Card.Body>
                            {/* <input type="file" id="file" className="nputfile" onChange={(e) => {
                              this.setParentState(e.target.files[0]);
                            }} 
                            />*/}
                            <input type="file" id="file" placeholder="Upload File" className="form-control" onChange={e => {
                              this.setState({ selectedVideo: e.target.files[0] });
                            }} />
                            <UploadVideo
                              selectedFile={this.state.selectedVideo}
                              uploadState={(states) => this.setParentState(states)}
                            >
                            </UploadVideo>
                            {(this.state.uploadState.socketUploading && !this.state.uploadState.socketIsByte) &&
                              <div>Uploading ({this.state.uploadState.socketMBDone} / {this.state.uploadState.socketTotalSizeMB} MB) ( {this.state.uploadState.socketPercent}% )</div>
                            }
                            {(this.state.uploadState.socketUploading && this.state.uploadState.socketIsByte) &&
                              <div>Uploading ({this.state.uploadState.socketUploaded} / {this.state.uploadState.socketTotalSize} Byte) ( {this.state.uploadState.socketPercent}% )</div>
                            }
                            {this.state.uploadState.socketPercent === 100 &&
                              <div>Uploaded ({this.state.uploadState.socketTotalSize.toLocaleString()}) ( {this.state.uploadState.socketPercent}% )</div>
                            }
                            {(this.state.uploadState.socketUrl && this.state.uploadState.socketUrl !== '') &&
                              <div><a target='_blank' href={this.state.uploadState.socketUrl}>Link</a></div>
                            }
                            {/* <label for="file" className="btn" style={{ margin: 'auto', width: '30%' }}><i className="fa fa-upload"></i> Upload</label> */}
                            {/*{this.state.socketUploading && (
                              <div>
                                Uploading ({this.state.socketMBDone} /{" "}
                                {this.state.socketTotalSizeMB} MB) ({" "}
                                {this.state.socketPercent}% )
                              </div>
                            )}
                            {this.state.socketPercent === 100 && (
                              <div>
                                Uploaded (
                                {this.state.socketTotalSize.toLocaleString()}) ({" "}
                                {this.state.socketPercent}% )
                              </div>
                            )}
                             {this.state.socketUrl !== "" && (
                              <div>
                                <a target="_blank" href={this.state.socketUrl}>
                                  Link
                                </a>
                              </div>
                            )} */}
                          </Card>
                        </Form.Group>
                        <Form.Group>
                          <Form.Label>Price *</Form.Label>
                          <Form.Control
                            type="text"
                            onChange={(event) => {
                              this.setState({ price: event.target.value });
                            }}
                          />
                        </Form.Group>
                        <Form.Group>
                          <Form.Label>Discount (%)</Form.Label>
                          <Form.Control
                            type="text"
                            onChange={(event) => {
                              this.setState({ discount: event.target.value });
                            }}
                          />
                        </Form.Group>
                        <Form.Group>
                          {/* <Link to={`/Course`} style={{ marginRight: '5%' }} className="btn btn-outline-secondary">Cancel</Link> */}
                          {/* {(this.state.uploadState.socketPercent && this.state.uploadState.socketPercent !== "") && */}
                          {/* <Button className="button-grape" onClick={() => {
                                this.testApiCources(this.state.gambar);
                              }}
                            disabled={this.state.uploadState.socketPercent=="100" ? false : true}>Create</Button> */}
                          {/* } */}
                        </Form.Group>
                      </Form>
                    </Col>
                  </Row>
                </Card.Body>
              </Card>
            </Col>
          </Row>
          <Row className="justify-content-center">
            <Col xs={10} md={10}>
              <h4 className="title-course">2. Instructor</h4>
            </Col>
          </Row>
          <AddInstructor addInstructor={(states) => {this.setAddInstructorsState(states)}}/>
          {/* <Accordion defaultActiveKey="0">
            {
                // console.log(this.state.instructors)
                this.state.instructors?.map((list, index) => {
                    return <ListInstructor
                        id={list.id}
                        key={list.id}
                        instructor={list}
                        index={index}
                        edit={() => this.onOpenEditInsModal(list.id)}
                        instructors={(list && list.instructor ) ? list.instructor.instructor_name : null}
                        ></ListInstructor>
                })
            }
            </Accordion> */}
            <ListInstructor data={this.state.instructors}/>
            <Modal show={this.state.editinsModal} onHide={this.onCloseEditInsModal} size='lg'>
              <p>Some contents...</p>
              <p>Some contents...</p>
              <p>Some contents...</p>
              <Modal.Footer>
                <Button className="button-grape" onClick={() => { alert('delete') }}><i class="fa fa-trash"></i></Button>
                <Button className="button-grape" onClick={() => { this.onCloseEditInsModal() }}><i class="fa fa-check"></i></Button>
            </Modal.Footer>
            </Modal>
          {/* <ListInst props={} /> */}
          <Row className="justify-content-center">
            <Col xs={10} md={10}>
              <h4 className="title-course">3. Content & Material</h4>
            </Col>
          </Row>
          <Card
                className="card-grid"
                style={{ width: "83%", margin: 'auto' }}
              >
            <Row>
              
                <AddTopic
                  addTopic={(states) => { this.setAddTopicState(states) }}
                  topic={this.state.selectedTopic}
                />
                <AddExam addExam={(states) => { this.setAddExamState(states) }} />
              
            </Row>
          </Card>
          <Accordion defaultActiveKey="0">
            {
                this.state.topics?.map((list, index) => {
                    // return console.log(this.state);
                    return <List
                        id={list.id}
                        key={list.id}
                        topic={list}
                        index={index}
                        // edit={() => this.onOpenEditTopicModal(list.id)}
                        is_edit
                        edit={(hasil) => this.setEditTopicState(hasil)}
                        // edit={(states) => {this.setAddInstructorsState(states)}}
                        sub_topic={(list && list.topics && list.topics.sub_topic_name) ? list.topics.sub_topic_name : null}></List>
                })
            }
            </Accordion>
            {
                this.state.topics?.map((list, index) => {
                    // return console.log(list.sub_topics.sub_topic_name);
                    return <Modal show={this.state.edittopicModal} onHide={this.onCloseEditTopicModal} size='lg'>
                          <Form.Label style={{ fontFamily: "Gordita-bold" }}>
                            Topic Name* ({list.topic_name})
                          </Form.Label>
                          <Form.Control type="text" 
                          onChange={(event) => { this.setState({ topic_name_edit: event.target.value }) }}/>

                          {/* <Form.Group>
                            <Button className="button-grape btn-sm"
                              style={{ width: "30%" }}
                              onClick={() => this.add_subtopic_array()}
                              >
                                + Add Sub Topic
                            </Button>
                          </Form.Group> */}
                          {
                          list?.sub_topics?.map((sub, subindex) => {
                              // return console.log(sub.video);
                              return <>
                                  <Form.Group>
                                  <Form.Label style={{ fontFamily: "Gordita-bold" }}>
                                    Sub Topic Name* ({sub.sub_topic_name})
                                  </Form.Label>
                                  <Form.Control type="text"
                                    onChange={(event) => { this.setState({ sub_topic_name_edit: event.target.value }) }}/>
                                  </Form.Group>
                                  <Form.Group>
                                      <Form.Label style={{ fontFamily: "Gordita-bold" }}>
                                        Video* ({sub.video?.file_name})
                                      </Form.Label>
                                      {/* <Form.Control type="text"
                                        /> */}
                                      {/* <input type="file" id="file" placeholder="Upload File" className="form-control" onChange={e => {
                                        e.persist();
                                        const selectedVideos = this.state.selectedVideos.map((obj, i) => {
                                          if (index === i) {
                                            return {
                                              file: e.target.files[0]
                                            }
                                          } else {
                                            return {
                                              ...this.state.selectedVideos[index]
                                            }
                                          }
                                        })
                                        this.setState({ selectedVideos });
                                      }} />
                                      <UploadVideo
                                        selectedFile={this.state.selectedVideos[index].file}
                                        uploadState={(states) => this.setParentState(states, index)}
                                      >
                                      </UploadVideo>
                                      {(this.state.video_upload[index].socketUploading && this.state.video_upload[index].socketPercent !== 100) &&
                                        <ProgressBar now={this.state.video_upload[index].socketPercent} label={`${this.state.video_upload[index].socketPercent}%`} />
                                      } */}
                                      <input type="file" id="file" placeholder="Upload File" className="form-control" onChange={e => {
                                        this.setState({ selectedVideo: e.target.files[0] });
                                      }} />
                                      <UploadVideo
                                        selectedFile={this.state.selectedVideo}
                                        uploadState={(states) => this.setParentState(states)}
                                      >
                                      </UploadVideo>
                                      {(this.state.uploadState.socketUploading && !this.state.uploadState.socketIsByte) &&
                                        <div>Uploading ({this.state.uploadState.socketMBDone} / {this.state.uploadState.socketTotalSizeMB} MB) ( {this.state.uploadState.socketPercent}% )</div>
                                      }
                                      {(this.state.uploadState.socketUploading && this.state.uploadState.socketIsByte) &&
                                        <div>Uploading ({this.state.uploadState.socketUploaded} / {this.state.uploadState.socketTotalSize} Byte) ( {this.state.uploadState.socketPercent}% )</div>
                                      }
                                      {this.state.uploadState.socketPercent === 100 &&
                                        <div>Uploaded ({this.state.uploadState.socketTotalSize.toLocaleString()}) ( {this.state.uploadState.socketPercent}% )</div>
                                      }
                                      {(this.state.uploadState.socketUrl && this.state.uploadState.socketUrl !== '') &&
                                        <div><a target='_blank' href={this.state.uploadState.socketUrl}>Link</a></div>
                                      }
                                  </Form.Group>
                                  <Form.Group>
                                      <Form.Label style={{ fontFamily: "Gordita-bold" }}>
                                        Duration* ({sub.duration})
                                      </Form.Label>
                                      <Form.Control type="text"
                                      onChange={(event) => { this.setState({ duration_edit: event.target.value }) }}/>
                                  </Form.Group>
                              </>
                              //   })
                              // }
                              //  </Form.Group>
                            })
                          }
                          {/* <Form.Control type="text" 
                          onChange={(event) => { this.setState({ sub_topic_name_edit: event.target.value }) }}/> */}
                      <Modal.Footer>
                        <Button className="button-grape" onClick={() => { this.removetopic(list.id) }}><i className="fa fa-trash"></i></Button>
                        <Button className="button-grape" onClick={() => { this.onUpdateItem(list.id) }}><i className="fa fa-check"></i></Button>
                      </Modal.Footer>
                    </Modal>
                //     return <Modal show={this.state.edittopicModal} onHide={this.onCloseEditTopicModal} size='lg'>
                //   <Modal.Header bsPrefix closeButton />
                      //   <Modal.Body>
                      //   <Form.Group>
                      //     <Form.Label style={{ fontFamily: "Gordita-bold" }}>
                      //       Topic Name*
                      //     </Form.Label>
                      //     <Form.Control type="text" 
                     // onChange={(event) => { this.setState({ topic_name: event.target.value }) }} 
                      // value={list.topic_name} />
                      //   </Form.Group>
                      //     <Form.Group>
                      //       <Button className="button-grape btn-sm"
                      //         style={{ width: "30%" }}
                      //         onClick={() => this.add_subtopic_array()}
                      //         >
                      //           + Add Sub Topic
                      //       </Button>
                      //       <Form>
                      //         {this.state.sub_topics.map((sub_topic, index) => (
                      //         <Form.Row className="justify-content-center" key={index}>
                      //           <Form.Group as={Col}>
                      //             <Form.Label style={{ fontFamily: "Gordita-bold" }}>
                      //               Sub Topic Name*
                      //             </Form.Label>
                      //             <Form.Control onChange={(event) => {
                      //               event.persist();
                      //               console.log(event.target);
                      //               this.setState(prevState =>{
                      //                 const subtopics = [
                      //                   ...prevState.sub_topics
                      //                 ];
                      //                 subtopics[index] = {
                      //                   ...prevState.sub_topics[index],
                      //                   sub_topic_name: event.target.value
                      //                 }
                      //                 return {
                      //                   ...prevState,
                      //                   sub_topics: subtopics
                      //                 }
                      //               })
                      //             }} />
                      //           </Form.Group>

                      //           <Form.Group as={Col}>
                      //             <Form.Label style={{ fontFamily: "Gordita-bold" }}>
                      //               Course Material
                      //             </Form.Label>
                      //             <input type="file" id="file" placeholder="Upload File" className="form-control" onChange={e => {
                      //               e.persist();
                      //               const selectedVideo = this.state.selectedVideo.map((obj, i) => {
                      //                 if (index === i) {
                      //                   return {
                      //                     file: e.target.files[0]
                      //                   }
                      //                 } else {
                      //                   return {
                      //                     ...this.state.selectedVideo[index]
                      //                   }
                      //                 }
                      //               })
                      //               this.setState({ selectedVideo });
                      //             }} />
                      //             <UploadVideo
                      //               selectedFile={this.state.selectedVideo[index].file}
                      //               uploadState={(states) => this.setParentState(states, index)}
                      //             >
                      //             </UploadVideo>
                      //             {(this.state.video_upload[index].socketUploading && this.state.video_upload[index].socketPercent !== 100) &&
                      //               <ProgressBar now={this.state.video_upload[index].socketPercent} label={`${this.state.video_upload[index].socketPercent}%`} />
                      //             }
                      //           </Form.Group>

                      //         <Form.Group as={Col}>
                      //           <Form.Label style={{ fontFamily: "Gordita-bold" }}>
                      //             Duration
                      //             </Form.Label>
                      //             <Form.Control onChange={(event) => {
                      //               event.persist();
                      //               console.log(event.target);
                      //               this.setState(prevState =>{
                      //                 const subtopics = [
                      //                   ...prevState.sub_topics
                      //                 ];
                      //                 subtopics[index] = {
                      //                   ...prevState.sub_topics[index],
                      //                   duration: event.target.value
                      //                 }
                      //                 return {
                      //                   ...prevState,
                      //                   sub_topics: subtopics
                      //                 }
                      //               })
                      //             }} />
                      //         </Form.Group>
                      //         </Form.Row>
                      //         ))}
                      //       </Form>
                      //     </Form.Group>
                      // </Modal.Body>
                //     <Modal.Footer>
                //       {list.topic_name}
                //     <Button className="button-grape" onClick={() => { this.removetopic(list.topic_name) }}><i className="fa fa-trash"></i></Button>
                //       <Button className="button-grape" onClick={() => { this.onCloseEditTopicModal() }}><i className="fa fa-check"></i></Button>
                //     </Modal.Footer>
                //   </Modal>
                })
            }

          <div style={{ fontFamily: 'Gordita', marginLeft: '1070px' }}><br />
          <Button className="button-grape" onClick={() => {
                                this.testApiCources(this.state.gambar);
                              }}
                            disabled={this.state.uploadState.socketPercent=="100" ? false : true}
                            >Save</Button>
        </div>

        </Container>
        <Footer />
      </>
    );
  }
}

export default AddCourse;