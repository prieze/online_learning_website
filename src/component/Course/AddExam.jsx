import React, { Component } from "react";
import { Link } from "react-router-dom";
import "./Course.css";
import "react-toastify/dist/ReactToastify.css";
import {
  Container,
  Row,
  Col,
  Form,
  Card,
  Button,
  Modal,
} from "react-bootstrap";
import { Formik } from 'formik';
import * as yup from 'yup';

class AddExam extends Component {
  state = {
    modalAddIsOpen: false,
    master_soals: [],
    number: null,
    passing_grade: null
  };
  formRef = React.createRef();
  schema = yup.object({
    number: yup.number().required(),
    passing_grade: yup.number().required(),
    master_soals: yup.array().of(
      yup.object().shape({
        question: yup.string().min(5, 'Must be at least 5 characters')
          .required('Question is required'),
        answer: yup.string().required('Answer is required'),
        a: yup.string().required('Answer A is required'),
        b: yup.string().required('Answer B is required'),
        c: yup.string().required('Answer C is required'),
        d: yup.string().required('Answer D is required'),
        ordering: yup.number().required('Ordering is required')
      })
    )
  });

  onOpenAddModal = () => {
    const exam = this.props.exam ? this.props.exam : [];
    const passing_grade = this.props.passing_grade ? this.props.passing_grade : null;
    this.setState({
      modalAddIsOpen: true,
      question: '',
      master_soals: exam,
      number: exam.length >= 1 ? exam.length : '',
      passing_grade
    });
  };

  onCloseAddModal = async () => {
    this.setState({
      modalAddIsOpen: false,
      master_soals: [],
      passing_grade: 0
    });
  };

  onSaveModal = async () => {
    console.log(this.formRef);
    this.formRef.current.validateForm().then( () => {
      if (this.formRef.current.isValid) {
        console.log('valid');
        if (this.state.master_soals) {
          this.props.addExam({
            soal: this.formRef.current.values.master_soals,
            passing_grade: this.formRef.current.values.passing_grade
          });
        }
        this.setState({
          modalAddIsOpen: false,
          master_soals: [],
          passing_grade: 0
        });
      }
    })
  }

  add_question_array_formik(values, number) {
    const master_soals = [];
    if (number > 0) {
      for (let i = 0; i < number; i++) {
        if (values.master_soals[i]) {
          master_soals.push(values.master_soals[i])
        } else {
          master_soals.push({
            question: '',
            a: '',
            b: '',
            c: '',
            d: '',
            answer: '',
            ordering: i + 1
          })
        }
      }
      this.formRef.current.setValues({
        number,
        master_soals,
        passing_grade: values.passing_grade
      })
    } else {
      this.formRef.current.setValues({
        number: '',
        master_soals: values.master_soals,
        passing_grade: values.passing_grade
      })
    }
  }

  render() {
    return (
      <>
        <Col xs={5} md={3}>
          <Card.Body style={{ padding: '2rem' }}>
            <Form>
              <Form.Group>
                <Button
                  onClick={this.onOpenAddModal}
                  className="button-grape btn-sm"
                  style={{ width: "100%"}}
                >
                  Add Exam
                    </Button>
              </Form.Group>
            </Form>
          </Card.Body>
        </Col>

        <Modal show={this.state.modalAddIsOpen} onHide={this.onCloseAddModal} size='lg' >
          <Modal.Header bsPrefix closeButton >
            <Modal.Title className="justify-content-center">Add Exam</Modal.Title>
          </Modal.Header>
          <Modal.Body>
          <Formik
            innerRef={this.formRef} 
            validationSchema={this.schema}
            validateOnChange={false}
            validateOnBlur={false}
            onSubmit={console.log}
            initialValues={{
              passing_grade: this.state.passing_grade,
              number: this.state.number,
              master_soals: this.state.master_soals
            }}
          >
            {({
              handleSubmit,
              values,
              setFieldValue,
              errors,
            }) => (
          <Form noValidate onSubmit={handleSubmit}>
            <Form.Group as={Col}>
              <Form.Row >Question amount:
                <Form.Control type="number" style={{ width: '70px', margin: '8px' }}
                  name="number"
                  required
                  onChange={(event) => {
                    event.persist();
                    this.add_question_array_formik(values, event.target.value);
                  }}
                  isInvalid={ errors.number}
                  value={values.number}
                />
                <Form.Control.Feedback type="invalid">{errors.number}</Form.Control.Feedback>
                Passing Grade:
                <Form.Control type="number" style={{ width: '70px', margin: '8px' }}
                  name="passing_grade"
                  required
                  onChange={(event) => {
                    event.persist();
                    const val = event.target.value !== '' ? parseInt(event.target.value) : '';
                    setFieldValue('passing_grade', val)
                  }}
                  isInvalid={ errors.passing_grade}
                  value={values.passing_grade}
                />
                <Form.Control.Feedback type="invalid">{errors.passing_grade}</Form.Control.Feedback>
              </Form.Row>
              <br/>
              
              {values.master_soals?.map((exams, index) => (
                <Form.Group as={Col} key={index}>
                  <Form.Row>{index+1}<Form.Group as={Col}>
                    <Form.Control as="textarea"
                      name={`Question${index + 1}`}
                      onChange={(event) => {
                        event.persist();
                        setFieldValue(`master_soals.[${index}].question`, event.target.value)
                      }}
                      placeholder={`Input Question Number ${index + 1}`}
                      isInvalid={ errors.master_soals?.[index]?.question}
                      value={exams.question}/>
                    <Form.Control.Feedback type="invalid">{errors.master_soals?.[index]?.question}</Form.Control.Feedback>
                  </Form.Group>
                  </Form.Row>
                  <Form.Row>
                    <Form.Group as={Col}>
                    <Form.Check
                      type='radio'
                      label={`A`}
                      id={`default-radio`}
                      name={`jawab${exams.ordering}`}
                      onChange={(event) => {
                        event.persist();
                        setFieldValue(`master_soals.[${index}].answer`, 'a')
                      }}
                      isInvalid={!!errors.master_soals?.[index]?.answer}
                      feedback={errors.master_soals?.[index]?.answer}
                      checked={exams.answer === 'a' ? true : false}
                    />

                      <Form.Control as="textarea" rows="3" 
                        onChange={(event) => {
                          event.persist();
                          setFieldValue(`master_soals.[${index}].a`, event.target.value)
                        }}
                        isInvalid={!!errors.master_soals?.[index]?.a}
                        placeholder="input content"
                        value={exams.a}
                      />
                      <Form.Control.Feedback type="invalid">{errors.master_soals?.[index]?.a}</Form.Control.Feedback>
                    </Form.Group>
                    <Form.Group as={Col}>
                      <Form.Check
                        type='radio'
                        label={`B`}
                        id={`default-radio`}
                        name={`jawab${exams.ordering}`}
                        onChange={(event) => {
                          event.persist();
                          setFieldValue(`master_soals.[${index}].answer`, 'b')
                        }}
                        isInvalid={!!errors.master_soals?.[index]?.answer}
                        feedback={errors.master_soals?.[index]?.answer}
                        checked={exams.answer === 'b' ? true : false}
                      />
                      <Form.Control as="textarea" rows="3" 
                        onChange={(event) => {
                          event.persist();
                          setFieldValue(`master_soals.[${index}].b`, event.target.value)
                        }}
                        isInvalid={!!errors.master_soals?.[index]?.b}
                        placeholder="input content"
                        value={exams.b}
                      />
                      <Form.Control.Feedback type="invalid">{errors.master_soals?.[index]?.b}</Form.Control.Feedback>
                    </Form.Group>
                    <Form.Group as={Col}>
                      <Form.Check
                        type='radio'
                        label={`C`}
                        id={`default-radio`}
                        name={`jawab${exams.ordering}`}
                        onChange={(event) => {
                          event.persist();
                          setFieldValue(`master_soals.[${index}].answer`, 'c')
                        }}
                        isInvalid={!!errors.master_soals?.[index]?.answer}
                        feedback={errors.master_soals?.[index]?.answer}
                        checked={exams.answer === 'c' ? true : false}
                      />
                      <Form.Control as="textarea" rows="3" 
                        onChange={(event) => {
                          event.persist();
                          setFieldValue(`master_soals.[${index}].c`, event.target.value)
                        }}
                        isInvalid={!!errors.master_soals?.[index]?.c}
                        placeholder="input content"
                        value={exams.c}
                      />
                      <Form.Control.Feedback type="invalid">{errors.master_soals?.[index]?.c}</Form.Control.Feedback>
                    </Form.Group>
                    <Form.Group as={Col}>
                      <Form.Check
                        type='radio'
                        label={`D`}
                        id={`default-radio`}
                        name={`jawab${exams.ordering}`}
                        onChange={(event) => {
                          event.persist();
                          setFieldValue(`master_soals.[${index}].answer`, 'd')
                        }}
                        isInvalid={!!errors.master_soals?.[index]?.answer}
                        feedback={errors.master_soals?.[index]?.answer}
                        checked={exams.answer === 'd' ? true : false}
                      />
                      
                      <Form.Control as="textarea" rows="3" 
                        onChange={(event) => {
                          event.persist();
                          setFieldValue(`master_soals.[${index}].d`, event.target.value)
                        }}
                        isInvalid={!!errors.master_soals?.[index]?.d}
                        placeholder="input content"
                        value={exams.d}
                      />
                      <Form.Control.Feedback type="invalid">{errors.master_soals?.[index]?.d}</Form.Control.Feedback>
                    </Form.Group>
                  </Form.Row>
                </Form.Group>
               ))}
            </Form.Group>
          </Form>
          )}
        </Formik>
          </Modal.Body>
          <Modal.Footer>

            <Button className="button-grape" onClick={() => { this.onSaveModal() }}>Save</Button>
          </Modal.Footer>
        </Modal>
      </>
    )
  }
}

export default AddExam;