import React, { Component } from 'react';
import Navigation from "../Navigation/Navigation";
import { Link } from 'react-router-dom';
import './Course.css';
import List from './List'
import _ from "lodash";
import Footer from '../Footer/Footer'
import { Container, Row, Col, Form, Card, Navbar, Image, Nav, Button, Table, Accordion } from 'react-bootstrap';
import AddTopic from './AddTopic';
import ListInstructor from './ListInstructor';
import ListExam from './ListExam';

class DetailCourse extends Component {

    state = {
        course: [],
        list: [],
        courseId: 0,
        id: 0,
        detail: 1
    }

    componentDidMount() {
        
        this.listTopic();
        const token = localStorage.getItem('token');
        const id = this.props.match.params.id;
        this.setState({
            course: [],
            id: id,
        });
        fetch(`${process.env.REACT_APP_BASEURL}/api/read/course/${id}`, {
            method: "GET",
            headers: {
                "Authorization": `Bearer ${token}`,
                "Content-Type": "application/json"
            },
        })
            .then(response => response.json())
            .then(json => {
                if (json.sukses) {
                    this.setState({
                        course: json.rows
                    })
                    console.log(this.state.course);
                    console.log(json.data);
                } else {
                    console.log('Failed');
                    console.log(json.msg);
                }
                console.log(this.state.course);
                console.log(json);
                console.log(json.data);
            })
            .catch(error => console.log('error', error));
    }

    listTopic() {
        const token = localStorage.getItem("token");
        const id = this.props.match.params.id;
        this.setState({
            list: [],
            id: id,
        });
        fetch(`${process.env.REACT_APP_BASEURL}/api/read/topic/course/${id}`,

            {
                method: "GET",
                headers: {
                    Authorization: `Bearer ${token}`,
                    "Content-Type": "application/json",
                },
            }
        )
            .then((response) => response.json())
            .then((json) => {
                this.setState({
                    list: json.rows,
                    loading: false,
                });
                console.log(this.state.list);
                console.log("this.state.list");
            })
            .catch((error) => console.log("error", error));
    }

    render() {
        const { list } = this.state;
        console.log(this.state);
        return (
            <>
                <Navigation history={this.props.history}/>
                <Container fluid
                    style={{
                        width: "100%",
                        minHeight: "2000px",
                        paddingTop: "50px",
                        backgroundColor: "#F1F1F1"
                    }}
                >
                    <Row className="justify-content-center">
                        <Col xs={10} md={10}>
                            <Navbar expand="lg" style={{ background: 'rgba(58, 44, 125, 0.34)' }}>
                                <Link to={`/Course`}><button className="btn icon-back"><span style={{ color: '#FFFFFF' }} className="fa fa-chevron-left"></span></button></Link>
                                <Navbar.Brand style={{ color: '#FFFFFF', fontFamily: 'Gordita-bold' }}>Detail Course</Navbar.Brand>
                            </Navbar>
                        </Col>
                    </Row>

                    <Row className="justify-content-center">
                        <Col xs={10} md={10}>
                            <h4 className="title-course">1. Course</h4>
                        </Col>
                    </Row>

                <Row>
                    <Col>
                        <Card style={{ width: "83%", margin: 'auto' }}>
                            <Card.Body style={{ padding: '2rem' }}>
                                <Row>
                                    <Col xs={12} lg={5}>
                                        <Form>
                                            <Form.Group>
                                                <Form.Label>Course Name *</Form.Label>
                                                <Form.Control style={{ backgroundColor: "white" }} type="text" disabled value={this.state.course.course_name} />
                                            </Form.Group>
                                            <Form.Group>
                                                <Form.Label>Course Photo *</Form.Label>
                                                <Image src={this.state.course.cover?.url} />
                                            </Form.Group>
                                            <Form.Group>
                                                <Form.Label>Course Categories *</Form.Label>
                                                <Form.Control style={{ backgroundColor: "white" }} type="text" disabled value={this.state.course.categoryId} />
                                            </Form.Group>
                                            <Form.Group>
                                                    <Form.Label>Description *</Form.Label>
                                                <Form.Control style={{ backgroundColor: "white" }} disabled as="textarea" rows="3" value={this.state.course.description} />
                                            </Form.Group>
                                        </Form>
                                    </Col>
                                    <Col lg={2} />
                                    <Col xs={12} lg={5}>
                                        <Form>
                                            <Form.Group>
                                                {(this.state.course.video?.url && this.state.course.video?.url.toLowerCase().endsWith("mp4") ) &&
                                                    <video width="400" height="240" controls autoPlay>
                                                        <source src={this.state.course.video?.url} type={this.state.course.video?.file_type} />
                                                    </video>
                                                }
                                            </Form.Group>
                                            <Form.Group>
                                                <Form.Label>Price *</Form.Label>
                                                <Form.Control style={{ backgroundColor: "white" }} type="text" disabled value={this.state.course.price} />
                                            </Form.Group>
                                            <Form.Group>
                                                <Form.Label>Discount (%)</Form.Label>
                                                <Form.Control style={{ backgroundColor: "white" }} type="text" disabled value={this.state.course.discount} />
                                            </Form.Group>
                                        </Form>
                                    </Col>
                                </Row>
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
                    <Row className="justify-content-center">
                        <Col xs={10} md={10}>
                            <h4 className="title-course">2. Instructor</h4>
                        </Col>
                    </Row>
                    <ListInstructor data={this.state.course.instructors} detail={this.state.detail} />
                    
                    <Row className="justify-content-center">
                        <Col xs={10} md={10}>
                            <h4 className="title-course">3. Content & Material</h4>
                        </Col>
                    </Row>
                    {/* <AddTopic addTopic={(states) => {this.setAddTopicState(states)}}/> */}
                    <Card style={{ color: "white", background: "rgba(58, 44, 125, 0.34)", width: "83%", height: "100%", margin: 'auto' }} >
                        <Card.Body>
                        <Row className="justify-content-center">
                            <Col xs={4} lg={4}>
                            Topic Name
                            </Col>
                            <Col xs={4} lg={4}>
                            Course Material
                            </Col>
                            <Col xs={4} lg={4}>
                            Duration
                            </Col>
                        </Row>
                        </Card.Body>
                    </Card>
                    <Accordion defaultActiveKey="0">
                    {
                        this.state.course.topics?.map((list, index) => {
                            return <List
                                id={list.id}
                                key={list.id}
                                topic={list}
                                index={index}
                                sub_topic={(list && list.topics && list.topics.sub_topic_name) ? list.topics.sub_topic_name : null}></List>
                        })
                    }
                    </Accordion>
                    <ListExam data={this.state.course.master_soals}/>

                </Container>
                <Footer />
            </>
        )
    }
}

export default DetailCourse;