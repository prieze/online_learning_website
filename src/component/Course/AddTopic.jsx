import React, { Component } from "react";
import Navigation from "../Navigation/Navigation";
import { Link } from "react-router-dom";
import "./Course.css";
import Footer from "../Footer/Footer";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import {
  Container,
  Row,
  Col,
  Form,
  Card,
  Navbar,
  Button,
  Modal,
  ProgressBar
} from "react-bootstrap";
import UploadVideo from '../Upload/uploadVideo';
import { Formik } from 'formik';
import * as yup from 'yup';

class AddTopic extends Component{
  state = {
    id: null,
    topic:[],
    topic_name: '',
    sub_topics: [],
    video_upload: [],
    modalAddIsOpen: false,
    selectedVideo: [{file: null}],
    uploadState: {}, //Upload State dari Component untuk merubah data dari page ini
    currentUploadState: false,
    is_edit: false,
  };
  formRef = React.createRef();
  sub = yup.object({
    sub_topic_name: yup.string().required().default(''),
    duration: yup.string().required().default('')
  })
  schema = yup.object({
      topic_name: yup.string().required('Topic Name is Required'),
      sub_topics: yup.array().of(
        yup.object().shape({
          sub_topic_name: yup.string().required('Sub Topic Name is Required'),
          duration: yup.string().required('Duration is Required')
          .matches(/^[0-5][0-9]:[0-5][0-9]$/, "Duration must be mm:ss"),
            // .matches(/^([0-1]?[0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]$/, "Duration must be hh:mm:ss"),
          videoId: yup.number().nullable().required('Video is required')
        })
      ).required('Sub Topic is Required'),
  });

  onOpenAddModal = (data) => {
    // console.log(this.state.sub_topics);
    if (data) {
      const video = [];
      const is_edit = data.topic_name ? true : false;
      if (data.sub_topics) {
        data.sub_topics.map(a => {
          video.push(a.video);
        })
      }
      this.setState({ 
        modalAddIsOpen: true,
        selectedVideo: video,
        id: data.id,
        topic_name: data.topic_name,
        sub_topics: data.sub_topics,
        video_upload: video,
        is_edit,
        errorsSelectedVideo: []
      });
    } else {
      this.setState({ 
        modalAddIsOpen: true,
        selectedVideo: [{file: null}],
        topic_name: '',
        sub_topics: [],
        video_upload: [],
        is_edit: false,
        errorsSelectedVideo: []
      });
    }
  };

  addtopic(values){
    // alert('clicked');
    console.log('bisa klik');
    console.log("values : " + JSON.stringify(values));
    console.log("state : " + JSON.stringify(this.state));
    // console.log("state sub topic : " + JSON.stringify(this.state.sub_topics));
    // console.log("state sub topic : " + JSON.stringify(typeof(this.state.sub_topics.videoId)));
    if (this.state.video_upload === null || this.state.video_upload === "") {
      alert("coba doang");
    }
    if (this.state.sub_topics.videoId === null || this.state.sub_topics.videoId === "" || this.state.sub_topics.videoId === "undefined") {
      alert("coba doang videoid");
    }
    this.props.addTopic({
      id: this.state.id,
      topic_name: values.topic_name,
      sub_topics: values.sub_topic
    });
    this.onCloseAddModal();
    // this.setState({ modalAddIsOpen: false });
  }
  onCloseAddModal = values => {
    // if (!this.state.currentUploadState) {
      console.log(values);
      this.setState({ modalAddIsOpen: false });
      // console.log(this.state.sub_topics)
      // this.props.addTopic({
      //   id: this.state.id,
      //   topic_name: this.state.topic_name,
      //   sub_topics: this.state.sub_topics
      // });
    //   this.props.addTopic({
    //     id: this.state.id,
    //     topic_name: values.topic_name,
    //     sub_topics: values.sub_topic
    //   });
    // }
  };

  onSaveModalTopic = () => {
    console.log(this.formRef);
    this.formRef.current.validateForm().then( () => {
      if (this.formRef.current.isValid) {
        console.log('valid');
        this.props.addTopic({
          id: this.state.id,
          topic_name: this.formRef.current.values.topic_name,
          sub_topics: this.formRef.current.values.sub_topics
        });
        this.setState({
          modalAddIsOpen: false,
          topic_name: '',
          sub_topics: []
        });
      }
    }).catch(err => {
      console.log(err);
    })
  }


  add_subtopic_array = (e) => {
    e.persist();
    console.log(this.formRef.current.values);
    let newSub;
    const sub = {
      sub_topic_name: '',
      duration: '',
      videoId: null
    }
    if (this.formRef.current.values.sub_topics && this.formRef.current.values.sub_topics.length > 0) {
      newSub = [
        ...this.formRef.current.values.sub_topics,
        sub
      ]
    } else {
      newSub = [
        sub
      ]
    }
    this.formRef.current.setFieldValue('sub_topics', newSub);
    const video_upload = this.state.video_upload.concat({});
    const selectedVideo = this.state.selectedVideo.concat({});
    this.setState({
      video_upload,
      selectedVideo
    })
    console.log(this.formRef.current.values.sub_topics);
  }

  checkAnyUpload(video_upload) {
    let hasil = false;
    for (const a of video_upload) {
      if (a.socketUploading) {
        hasil = true;
        break;
      }
    };
    return hasil
  }

  setParentState(states, index) {
    console.log(this.state.video_upload[index])
    console.log(states);
    if (states.socketUploadDataReturn?.data?.id) {
      console.log(states.socketUploadDataReturn?.data?.id);
      //Form
      const form = this.formRef.current.values.sub_topics.map((a, i) => {
        if (i === index) {
          return { ...a, videoId: states.socketUploadDataReturn?.data?.id }
        } else {
          return { ...a }
        }
      })
      this.formRef.current.setFieldValue('sub_topics', form);
      //States
      const subtopic = this.state?.sub_topics?.map((obj, i) => {
        if (i === index) {
          return {
            ...obj,
            videoId: states.socketUploadDataReturn?.data?.id,
            video: {
              file_name: states.socketUploadDataReturn?.data?.file_name
            }
          }
        } else {
          return {
            ...obj
          }
        }
      });
      console.log(subtopic);
      this.setState({
        sub_topics: subtopic,
      })
    }
    const video_upload = this.state.video_upload.map((obj, i) => {
      if (i === index) {
        return {
          ...states,
          url: states.socketUrl
        }
      } else {
        return {
          ...obj
        }
      }
    })
    console.log(video_upload);
    const anyUpload = this.checkAnyUpload(video_upload);
    console.log('anyUpload: ', anyUpload);
    this.setState(
      { video_upload, currentUploadState: anyUpload }
    )
  }

  render(){
    return(
      <>
        <Col xs={5} md={3}>
              <Card.Body style={{ padding: '2rem' }}>
                <Form>
                  <Form.Group>
                    <Button
                      onClick={this.onOpenAddModal}
                      className="button-grape btn-sm"
                      style={{ width: "100%"}}
                    >
                      Add Topic
                    </Button>
                  </Form.Group>
                </Form>
              </Card.Body>
        </Col>

        <Modal show={this.state.modalAddIsOpen} onHide={this.onCloseAddModal} size='lg'>
          <Modal.Header bsPrefix closeButton />
          <Modal.Body>
            <Formik
              innerRef={this.formRef}
              validationSchema={this.schema}
              validateOnChange={false}
              validateOnBlur={false}
              onSubmit={() => this.onSaveModalTopic()}
              initialValues={{
                topic_name: this.state.topic_name,
                sub_topics: this.state.sub_topics
              }}
            >
              {({
                    handleSubmit,
                    handleChange,
                    values,
                    setFieldValue,
                    errors,
                }) => (
            <Form noValidate onSubmit={handleSubmit}>
            <Form.Group>
              <Form.Label style={{ fontFamily: "Gordita-bold" }}>
                Topic Name*
              </Form.Label>
              <Form.Control type="text"
                // onChange={(event) => { this.setState({ topic_name: event.target.value }) }} 
                placeholder="Topic Name"
                name="topic_name"
                value={values.topic_name}
                onChange={handleChange}
                // value={this.state.topic_name}
                isInvalid={!!errors.topic_name}
              />
                <Form.Control.Feedback type="invalid">
                  {errors.topic_name}
                </Form.Control.Feedback>
            </Form.Group>
              <Form.Group>
                <Button className="button-grape btn-sm"
                  style={{ width: "30%" }}
                  onClick={(e) => this.add_subtopic_array(e)}
                  >
                    + Add Sub Topic
                </Button>
                  {values.sub_topics?.map((sub_topic, index) => (
                  <Form.Row className="justify-content-center" key={index}>
                    <Form.Group as={Col}>
                      <Form.Label style={{ fontFamily: "Gordita-bold" }}>
                        Sub Topic Name*
                      </Form.Label>
                      <Form.Control
                        type="text"
                        name={`sub_topic_name${index+1}`}
                        onChange={(event) => {
                          event.persist();
                          setFieldValue(`sub_topics.[${index}].sub_topic_name`, event.target.value)
                        }}
                        isInvalid={errors.sub_topics?.[index]?.sub_topic_name}
                        value={values.sub_topics?.[index]?.sub_topic_name}
                      />
                    <Form.Control.Feedback type="invalid">{errors.sub_topics?.[index]?.sub_topic_name}</Form.Control.Feedback>
                    </Form.Group>

                    <Form.Group as={Col}>
                      <Form.Label style={{ fontFamily: "Gordita-bold" }}>
                        Course Material
                      </Form.Label>
                      <Form.Control type="file" id="file" placeholder="Upload File" className="form-control"
                        onChange={e => {
                          e.persist();
                          const selectedVideo = this.state.selectedVideo.map((obj, i) => {
                            if (index === i) {
                              return {
                                file: e.target.files[0]
                              }
                            } else {
                              return {
                                ...this.state.selectedVideo[i]
                              }
                            }
                          })
                          this.setState({ selectedVideo });
                        }}
                        isInvalid={!!errors.sub_topics?.[index]?.videoId}
                      />
                      <UploadVideo
                        selectedFile={this.state.selectedVideo[index]?.file}
                        uploadState={(states) => this.setParentState(states, index)}
                      >
                      </UploadVideo>
                      {(this.state.video_upload[index]?.socketUploading && this.state.video_upload[index]?.socketPercent !== 100) &&
                        <ProgressBar now={this.state.video_upload[index]?.socketPercent} label={`${this.state.video_upload[index]?.socketPercent}%`} />
                      }
                      {/* <Form.Control.Feedback type="invalid">{errors.video_upload}</Form.Control.Feedback> */}
                      <Form.Control.Feedback type="invalid">{errors.sub_topics?.[index]?.videoId}</Form.Control.Feedback>
                      {/* {(this.state.selectedVideo[index]?.file)} */}
                    </Form.Group>

                  <Form.Group as={Col}>
                    <Form.Label style={{ fontFamily: "Gordita-bold" }}>
                      Duration
                      </Form.Label>
                      <Form.Control 
                        type="text"
                        name={`duration${index+1}`}
                        // name="duration"
                        // onChange={handleChange}
                        onChange={(event) => {
                          event.persist();
                          setFieldValue(`sub_topics.[${index}].duration`, event.target.value)
                        }}
                        isInvalid={!!errors.sub_topics?.[index]?.duration}
                        value={values.sub_topics?.[index]?.duration}
                      />
                      <Form.Control.Feedback type="invalid">{errors.sub_topics?.[index]?.duration}</Form.Control.Feedback>
                  </Form.Group>
                  </Form.Row>
                  ))}
              </Form.Group>
              {this.state.currentUploadState
                ? <Button disabled className="button-grape">Uploading ...</Button>
                : this.state.is_edit
                  ? <Button className="button-grape"
                      onClick={() => { this.onSaveModalTopic() }}
                  >Edit</Button>
                  : <Button className="button-grape"
                      onClick={() => { this.onSaveModalTopic() }}
                  >Create</Button>
              }
                </Form>
                )}
              </Formik>
          </Modal.Body>
        <Modal.Footer>

        </Modal.Footer>
        </Modal>
      </>
    )
  }
}

export default AddTopic;