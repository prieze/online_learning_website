import React, { Component } from "react";
import { Collapse, Divider } from 'antd';
import { Row, Col, Table, Accordion, Card, Button, Form, Modal } from 'react-bootstrap';
import { PlusOutlined, MinusOutlined, EditOutlined } from '@ant-design/icons'
import ModalInstructor from './ModalInstructor'
const { Panel } = Collapse;


class ListExam extends Component {
  state = {
    modalAddIsOpen: false,
    master_soals: {},
    data: [],
    course: [],
    ordering: '',
    id: 0
  }

  onOpenAddModal = () => {
    this.setState({
      modalAddIsOpen: true,
    });
  };

  onCloseAddModal = async () => {
    this.setState({
      modalAddIsOpen: false,
    });
  };

  render() {
    return (
      <>

        <Card style={{ width: "83%", margin: 'auto' }} >
            <Row >
              <Col xs={9} lg={9} style={{ marginLeft: '18px' }}>
                Exam 
              </Col>
              <Col xs={2} lg={2} style={{ marginLeft: '70px' }}>
                <Button className="button-grape" onClick={this.onOpenAddModal}>View Exam</Button>
              </Col>
            </Row>


            <Modal show={this.state.modalAddIsOpen} onHide={this.onCloseAddModal} size='lg'>
              <Modal.Header bsPrefix closeButton >
                <Modal.Title className="justify-content-center">Exam</Modal.Title>
              </Modal.Header>
              <Modal.Body>
                <Form.Group as={Col}>
                  <Form.Row>Question amount: <Form.Control type="text" style={{ width: '70px', margin: '8px' }} disabled value={this.state.ordering}/></Form.Row>
                  <br />
              {this.props.data?.map((list, index) => (
                  <Form.Group as={Col}  key={index}>
                  <Form.Row>{index+1}
                    <Form.Group as={Col}>
                      <Form.Control as="textarea" disabled value={list.question} />
                    </Form.Group>
                    </Form.Row>
                    <Form.Row>
                      <Form.Group as={Col}>
                      <Form.Check
                      type='radio'
                      label={`A`}
                      id={`default-radio`}
                      name={`jawab${list.ordering}`}
                      checked={list.answer === 'a' ? true : false}
                    />

                        <Form.Control as="textarea" rows="3" disabled value={list.a} />
                      </Form.Group>
                      <Form.Group as={Col}>
                      <Form.Check
                      type='radio'
                      label={`B`}
                      id={`default-radio`}
                      name={`jawab${list.ordering}`}
                      checked={list.answer === 'b' ? true : false}
                    />
                        <Form.Control as="textarea" rows="3" placeholder="input answer" disabled value={list.b} />
                      </Form.Group>
                      <Form.Group as={Col}>
                      <Form.Check
                      type='radio'
                      label={`C`}
                      id={`default-radio`}
                      name={`jawab${list.ordering}`}
                      checked={list.answer === 'c' ? true : false}
                    />
                        <Form.Control as="textarea" rows="3" placeholder="input answer" disabled value={list.c} />
                      </Form.Group>
                      <Form.Group as={Col}>
                      <Form.Check
                      type='radio'
                      label={`D`}
                      id={`default-radio`}
                      name={`jawab${list.ordering}`}
                      checked={list.answer === 'd' ? true : false}
                    />

                        <Form.Control as="textarea" rows="3" placeholder="input answer" disabled value={list.d} />
                      </Form.Group>
                    </Form.Row>
                  </Form.Group>
              ))}
                </Form.Group>
              </Modal.Body>
            </Modal>
          </Card>
      </>
    )
  }
}

export default ListExam;
