import React, { Component } from "react";
import Navigation from "../Navigation/Navigation";
import { Link, Redirect } from "react-router-dom";
import "./Course.css";
import List from './List'
import Footer from "../Footer/Footer";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import UploadVideo from '../Upload/uploadVideo';
import AddTopic from './AddTopic';
import AddInstructor from './AddInstructor';
import ListInstructor from './ListInstructor';
import {
  Container,
  Row,
  Col,
  Form,
  Card,
  Navbar,
  FormControl,
  Nav,
  Button,
  Table,
  Modal,
  Accordion,
} from "react-bootstrap";

class EditCourse extends Component {
  addTopicRef = React.createRef();
  SelectedFile;
  reader = new FileReader();
  socket;
  incIdTopic = 0;
  state = {
    course: [],
    list: [],
    categoryId: null,
    topic_name: null,
    sub_topic_name: null,
    modalEditIsOpen: false,
    modalAddIsOpen: false,
    edited_list: {},
    selectedVideo: {},
    uploadState: {},
    video_id: "",
    topic: "",
    id: 0,
    cover_photo: "base64 url"
  };

  onOpenAddModal = () => {
    this.setState({ modalAddIsOpen: true });
  };

  onCloseAddModal = () => {
    this.setState({ modalAddIsOpen: false });
  };

  onOpenEditModal = async (e) => {
    console.log(e);
    await this.setState({
      modalEditIsOpen: true,
      edited_list: e,
    });
    console.log(this.state.edited_list.id);
  };

  onCloseEditModal = () => {
    this.setState({ modalEditIsOpen: false });
  };

  componentDidMount() {
    this.listCategory();
    this.listCourse();
    this.listTopic();
  }

  setParentState(states) {
    this.setState(
      { uploadState: states }
    )
    console.log(this.state.uploadState);
    console.log(this.state.uploadState?.socketUploadDataReturn?.data?.id);
  }

  listCourse() {
    const token = localStorage.getItem("token");
    const id = this.props.match.params.id;
    this.setState({
      course: [],
      id: id,
    });
    fetch(`http://149.129.255.54/api/read/course/${id}`, {
      method: "GET",
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      },
    })
      .then((response) => response.json())
      .then((json) => {
        if (json.sukses) {
          this.setState({
            course: json.data,
          });
        } else {
          console.log("Failed");
          console.log(json.msg);
        }
        console.log(this.state.course);
      })
      .catch((error) => console.log("error", error));
  }

  listCategory() {
    const token = localStorage.getItem("token");
    this.setState({ loading: true });
    fetch(
      `${process.env.REACT_APP_BASEURL}/api/read/categories?offset=0&search=`,
      {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      }
    )
      .then((response) => response.json())
      .then((json) => {
        this.setState({
          list: json.rows,
          loading: false,
        });
        console.log(this.state.list);
        console.log("this.state.list");
      })
      .catch((error) => console.log("error", error));
  }

  listTopic() {
    const token = localStorage.getItem("token");
    const id = this.props.match.params.id;
    this.setState({
      list: [],
      id: id,
    });
    fetch(`${process.env.REACT_APP_BASEURL}/api/read/topic/course/${id}`,

      {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      }
    )
      .then((response) => response.json())
      .then((json) => {
        this.setState({
          list: json.rows,
          loading: false,
        });
        console.log(this.state.list);
        console.log("this.state.list");
      })
      .catch((error) => console.log("error", error));
  }

  updateCourse() {
    const token = localStorage.getItem("token");
    // const token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwidXNlcm5hbWUiOiJzaWFoYWFuIiwiZW1haWwiOiJzaWFoYWFuQGdtYWlsLmNvbSIsImZpcnN0X25hbWUiOiJuZXR0eSIsImxhc3RfbmFtZSI6InNpYWhhYW4iLCJtb2JpbGVfbnVtYmVyIjoiMDkyIiwiY291bnRyeSI6InR5IiwiYWRkcmVzcyI6InN1a2EiLCJzdGF0dXMiOjEsImdhbWJhciI6ImJhc2U2NCBJbWFnZSBVUkwiLCJjcmVhdGVkQXQiOiIyMDIwLTA3LTA4VDAyOjI4OjQ1LjQzN1oiLCJ1cGRhdGVkQXQiOiIyMDIwLTA3LTA4VDA4OjEyOjA3LjI2MloiLCJyb2xlcyI6WyJhZG1pbiJdLCJpYXQiOjE1OTQyNzM1NzN9.VLNpcAS3KAn6z5nKAI4Vskh7UMsXM4x2VFHu7dFDjeI";
    const id = this.props.match.params.id;
    console.log("id dari url : " + id);
    console.log(this.state);
    fetch(`http://149.129.255.54/api/update/course/${id}`, {
      method: "PUT",
      body: JSON.stringify({
        ...this.state
      }),
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      },
    })
      .then((response) => response.json())
      .then((json) => {
        if (json.sukses) {
          this.setState({
            course: json.data,
          });
          console.log("Data yang mau di update : ");
          console.log(json.data);
          console.log("Sukses : ");
          console.log(json.sukses);
          console.log("id dari api : " + json.data.id);
          this.props.history.push("/Course");
          toast.success("Course berhasil diupdate");
        } else {
          console.log("Failed");
          console.log(json.msg);
          toast.error(json.msg);
        }
      })
      .catch((error) => {
        console.log("error", error);
        toast.error("Error on App");
      });
  }

  updatetopic() {
    const token = localStorage.getItem('token');
    console.log(this.props)
    const id = this.props.match.params.id;
    this.setState({
      list: [],
      id: id,
    });
    fetch(`${process.env.REACT_APP_BASEURL}/api/update/topic/${id}`, {
      method: 'PUT',
      body: JSON.stringify({
        topic_name: this.state.topic_name,
      }),
      headers: {
        "Authorization": `Bearer ${token}`,
        "Content-Type": "application/json"
      },
    })
      .then((response) => {
        response.json()
          .then((result) => {
            console.warn("result", result);
            localStorage.setItem('updatecategory', JSON.stringify({
              updatetopic: true,
            }))
            this.listTopic();
            this.onCloseEditModal();
            toast.success("Successfully !")
          })
      })
      .catch(error => console.log('error', error));
  }


  handleRemove(id) {
    console.log("handle remove");
    console.log(id);
    const token = localStorage.getItem("token");
    fetch(`${process.env.REACT_APP_BASEURL}/api/delete/course/${id}`, {
      method: "DELETE",
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      },
    })
      .then((response) => response.json())
      .then((json) => {
        if (json.sukses) {
          this.listCourse(1);
        }
        this.props.history.push("/Course");
        toast.error("Course berhasil dihapus");
      })
      .catch((error) => console.log("error", error));
  }

  handleRemoveTop(id) {
    console.log("handle remove");
    console.log(id);
    const token = localStorage.getItem("token");
    fetch(`${process.env.REACT_APP_BASEURL}/api/delete/topic/${id}`, {
      method: "DELETE",
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      },
    })
      .then((response) => response.json())
      .then((json) => {
        if (json.sukses) {
          this.listTopic()
          this.onCloseEditModal();
        }
        toast.error("Topic berhasil dihapus");
      })
      .catch((error) => console.log("error", error));
  }

  createtopic() {
    const token = localStorage.getItem('token');
    const id = this.props.match.params.id;
    this.setState({
      list: [],
      id: id,
    });
    fetch(`http://149.129.255.54/api/create/topic/${id}`, {
      method: "POST",
      body: JSON.stringify({
        topic_name: this.state.topic_name,
        sub_topics: {
          sub_topic_name: this.state.sub_topic_name,
          upload_content: "apa aja",
          duration: "12:20",
          video_id: this.state.uploadState.socketUploadDataReturn.data.id
        }
      }),
      headers: {
        "Authorization": `Bearer ${token}`,
        "Content-Type": "application/json"
      },
    }).then((response) => {
      response.json()
        .then((result) => {
          if (result.sukses === true) {
            console.warn("result", result);
            localStorage.setItem('createtopic', JSON.stringify({
              createtopic: true,
            }))
            this.onCloseAddModal()
            this.listTopic()
            toast.success("Successfully !")
          } else {
            toast.error(result.msg)
          }
        })
    })
  }

  onFileChange = (event) => {
    // Update the state
    this.setState({ image: event.target.files[0] });
  };

  setAddInstructorsState(states) {
    const instructors = this.state.course.instructors.concat(states);
    this.setState({
      course: {
        ...this.state.course,
        instructors
      }
    })
    console.log(instructors);
  }

  setAddTopicState(states) {
    console.log(states);
    if (states.id) {
      const hasil = this.state.course.topics.map(a => {
        if (a.id === states.id) {
          a = states;
        }
        return a;
      })
      console.log(hasil);
      this.setState({
        course: {
          ...this.state.course,
          topics: hasil
        }
      })
    } else {
      states.id = `add${this.state.incIdTopic}`
      // this.setState({incIdTopic: parseInt(this.state.incIdTopic) + 1})
      const topics = this.state.course.topics.concat(states);
      this.setState({ 
        course: {
          ...this.state.course,
          topics,
        },
        incIdTopic: parseInt(this.state.incIdTopic) + 1
      })
    }
  }

  onOpenEditTopicModal(states) {
    this.addTopicRef.current.onOpenAddModal(states);
  }

  addInstructor = (data) => {
    console.log(this.state.course.instructors)
    console.log(data);
    const b = [];
    this.state.course.instructors.map(a => {
      if (a.id === data.id) {
        b.push(data)
      } else {
        b.push(a);
      }
    })
    this.setState({
      course: {
        ...this.state.course,
        instructors: b
      }
    })
  }

  render() {
    const { modalEditIsOpen, modalAddIsOpen, list } = this.state;
    return (
      <>
        <Navigation history={this.props.history}/>
        <Container
          fluid
          style={{
            width: "100%",
            minHeight: "2000px",
            paddingTop: "50px",
            backgroundColor: "#F1F1F1",
          }}
        >
          <ToastContainer />
          <Row className="justify-content-center">
            <Col xs={10} md={10}>
              <Navbar
                expand="lg"
                style={{ background: "rgba(58, 44, 125, 0.34)" }}
              >
                <Link to={`/Course`}>
                  <button className="btn icon-back">
                    <span
                      style={{ color: "#FFFFFF" }}
                      className="fa fa-chevron-left"
                    ></span>
                  </button>
                </Link>
                <Navbar.Brand
                  style={{ color: "#FFFFFF", fontFamily: "Gordita-bold" }}
                >
                  Edit Course
                </Navbar.Brand>
              </Navbar>
            </Col>
          </Row>

          <Row className="justify-content-center">
            <Col xs={10} md={10}>
              <h4 className="title-course">1. Course</h4>
            </Col>
          </Row>

          <Row>
            <Col>
              <Card style={{ width: "83%", margin: 'auto' }}>
                <Card.Body style={{ padding: '2rem' }}>
                  <Row>
                    <Col xs={12} lg={5}>
                      <Form>
                        <Form.Group>
                          <Form.Label>
                            Course Name ({this.state.course.course_name})
                          </Form.Label>
                          <Form.Control
                            type="text"
                            onChange={(event) => {
                              this.setState({
                                course_name: event.target.value,
                              });
                            }}
                          />
                        </Form.Group>

                        <Form.Group>
                          <input type="file" name="file" id="file" className="inputfile" onChange={this.onFileChange} />
                          <label style={{ width: '100%' }} for="file"><i className="fa fa-upload"></i> Add Cover Photo</label>
                          {/* <div>url: {this.state.url}</div> */}
                        </Form.Group>

                        <Form.Group>
                          <Form.Label>Course Category *</Form.Label>
                          <Form.Control
                            as="select"
                            onChange={(event) => {
                              // console.log(event.target.value)
                              this.setState({
                                categoryId: event.target.value,
                              });
                            }}
                          >
                            {this.state.list.map((list) => (
                              <option key={list.id} value={list.id}>
                                {list.category_name}
                              </option>
                            ))}
                            {/* <option>{list}</option> */}
                          </Form.Control>
                        </Form.Group>
                        <Form.Group>
                          <Form.Label>
                            Description ({this.state.course.description})
                          </Form.Label>
                          <Form.Control
                            // value={this.state.course.description}
                            as="textarea"
                            rows="3"
                            onChange={(event) => {
                              this.setState({
                                description: event.target.value,
                              });
                            }}
                          />
                        </Form.Group>
                      </Form>
                    </Col>
                    <Col lg={2} />
                    <Col xs={12} lg={5}>
                      <Form>
                        <Form.Group>
                          <Card style={{ width: "100%", marginTop: '6%', backgroundColor: "#EFEFEF", boxShadow: "none", border: "none" }}>
                            <Card.Body style={{ padding: '2rem' }}>
                              <Card.Title style={{ color: '#392C7E', textAlign: 'center', fontFamily: 'Gordita-bold', fontSize: '16px' }}>Submit a Video Demo</Card.Title>
                              <Card.Text style={{ color: '#392C7E', textAlign: 'center', fontFamily: 'Gordita', fontSize: '11px' }}>CLICK ON THE BUTTON OR DRAG & DROP FILE HERE</Card.Text>
                            </Card.Body>
                            <input type="file" id="file" placeholder="Upload File" className="form-control" onChange={e => {
                              this.setState({ selectedVideo: e.target.files[0] });
                            }} />
                            <UploadVideo
                              selectedFile={this.state.selectedVideo}
                              uploadState={(states) => this.setParentState(states)}
                            >
                            </UploadVideo>
                            {(this.state.uploadState.socketUploading && !this.state.uploadState.socketIsByte) &&
                              <div>Uploading ({this.state.uploadState.socketMBDone} / {this.state.uploadState.socketTotalSizeMB} MB) ( {this.state.uploadState.socketPercent}% )</div>
                            }
                            {(this.state.uploadState.socketUploading && this.state.uploadState.socketIsByte) &&
                              <div>Uploading ({this.state.uploadState.socketUploaded} / {this.state.uploadState.socketTotalSize} Byte) ( {this.state.uploadState.socketPercent}% )</div>
                            }
                            {this.state.uploadState.socketPercent === 100 &&
                              <div>Uploaded ({this.state.uploadState.socketTotalSize.toLocaleString()}) ( {this.state.uploadState.socketPercent}% )</div>
                            }
                            {(this.state.uploadState.socketUrl && this.state.uploadState.socketUrl !== '') &&
                              <div><a target='_blank' href={this.state.uploadState.socketUrl}>Link</a></div>
                            }
                          </Card>
                        </Form.Group>
                        <Form.Group>
                          <Form.Label>
                            Price ({this.state.course.price})
                          </Form.Label>
                          <Form.Control
                            type="text"
                            // value={this.state.course.price}
                            onChange={(event) => {
                              this.setState({
                                price: event.target.value,
                              });
                            }}
                          />
                        </Form.Group>
                        <Form.Group>
                          <Form.Label>
                            Discount ({this.state.course.discount}%)
                          </Form.Label>
                          <Form.Control
                            type="text"
                            // value={this.state.course.discount}
                            onChange={(event) => {
                              this.setState({
                                discount: event.target.value,
                              });
                            }}
                          />
                        </Form.Group>
                        {/* <Button variant="danger">Delete</Button> */}
                        {/* <Button variant="danger" style={{ fontFamily: 'Gordita', marginRight: '5%' }} onClick={() => {
                            if (window.confirm(`Are You sure ?`)) {
                              this.handleRemove(this.state.course.id);
                            }
                          }}
                        >Delete</Button>
                        <Button className="button-grape" onClick={() => {
                            this.updateCourse();
                          }}
                        >Update</Button> */}
                      </Form>
                    </Col>
                  </Row>
                </Card.Body>
              </Card>
            </Col>
          </Row>

          {/* <Row className="justify-content-center">
                    <Col xs={10} md={10}>
                        <h4 className="title-course">2. Instructor</h4>
                    </Col>
                </Row>

                <Row>
                    <Col>
                        <Card className="card-grid" style={{ width: "83%", height: "100%" }}>
                            <Card.Body>
                                <Form>
                                    <Form.Group>
                                        <Form.Label>Course Name *</Form.Label>
                                        <Form.Control type="text"  />
                                    </Form.Group>
                                    <Form.Group>
                                        <Form.Label>Course Name *</Form.Label>
                                        <Form.Control type="text"  />
                                    </Form.Group>
                                    <Form.Group>
                                        <Form.Label>Course Name *</Form.Label>
                                        <Form.Control type="text"  />
                                    </Form.Group>
                                    <Form.Group>
                                        <Form.Control type="file" />
                                    </Form.Group>
                                </Form>
                            </Card.Body>
                        </Card>
                    </Col>
                </Row> */}

          <Row className="justify-content-center">
              <Col xs={10} md={10}>
                  <h4 className="title-course">2. Instructor</h4>
              </Col>
          </Row>
          <AddInstructor addInstructor={(states) => {this.setAddInstructorsState(states)}}/>
          <ListInstructor 
            data={this.state.course.instructors} 
            is_edit
            addInstructor={(data)=>{this.addInstructor(data)}}
          />
          <Row className="justify-content-center">
            <Col xs={10} md={10}>
              <h4 className="title-course">3. Content & Material</h4>
            </Col>
          </Row>
          <AddTopic 
            addTopic={(states) => {this.setAddTopicState(states)}}
            ref={this.addTopicRef}
            topic={this.state.selectedTopic}
          />
          <Card style={{ color: "white", background: "rgba(58, 44, 125, 0.34)", width: "83%", height: "100%", margin: 'auto' }} >
            <Card.Body>
              <Row className="justify-content-center">
                <Col xs={4} lg={4}>
                  Topic Name
                </Col>
                <Col xs={4} lg={4}>
                  Course Material
                </Col>
                <Col xs={4} lg={4}>
                  Duration
                </Col>
              </Row>
            </Card.Body>
        </Card>
        <Accordion defaultActiveKey="0">
          {
              this.state.course.topics?.map((list, index) => {
                  return <List
                      id={list.id}
                      key={list.id}
                      topic={list}
                      index={index}
                      edit={(hasil) => this.onOpenEditTopicModal(hasil)}
                      sub_topic={(list && list.topics && list.topics.sub_topic_name) ? list.topics.sub_topic_name : null}></List>
              })
          }
        </Accordion>

          {/* <Row>
            <Col>
              <Form>
                <Form.Group>
                  <br />
                  <Button
                    onClick={this.onOpenAddModal}
                    className="button-grape btn-sm"
                    style={{ width: "25%", marginLeft: "100px" }}
                  >
                    + Add Topic
                      </Button>
                </Form.Group>
              </Form>
            </Col>
          </Row> */}
          
          {/* <Row>
            <Table style={{ background: "#F9F9F9", width: "83%", height: "100%", margin: 'auto' }} >

              <thead style={{ backgroundColor: "rgba(58, 44, 125, 0.34)" }}>
                <tr>
                  <th>Topic name</th>
                  <th>Course Material</th>
                  <th>Duration</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                {
                  this.state.list.map(list => {
                    return <List
                      id={list.id}
                      key={list.id}
                      topic={list.topic_name}
                      edit={() => this.onOpenEditModal(list)}></List>
                  })
                }
              </tbody>
            </Table>
          </Row> */}

          <Modal show={modalAddIsOpen} onHide={this.onCloseAddModal}>
            <Modal.Header bsPrefix closeButton />
            <Modal.Body>
              <Accordion defaultActiveKey="0">
                <Form.Group>
                  <Form.Label style={{ fontFamily: "Gordita-bold" }}>
                    Topic Name*
                  </Form.Label>
                  <Form.Control type="text" onChange={(event) => { this.setState({ topic_name: event.target.value }) }} />
                </Form.Group>
                <Form.Group>
                  <Accordion.Toggle
                    as={Button}
                    className="button-grape btn-sm"
                    style={{ width: "30%" }}
                    eventKey="1"
                  >
                    Add Sub Topic
                  </Accordion.Toggle><br />
                  <Accordion.Collapse eventKey="1">
                    <Form>
                      <Form.Row className="justify-content-center">
                        <Form.Group as={Col}>
                          <Form.Label style={{ fontFamily: "Gordita-bold" }}>
                            Sub Topic Name*
                          </Form.Label>
                          <Form.Control onChange={(event) => { this.setState({ sub_topic_name: event.target.value }) }} />
                        </Form.Group>

                        <Form.Group as={Col}>
                          <Form.Label style={{ fontFamily: "Gordita-bold" }}>
                            Course Material
                          </Form.Label>
                          <input type="file" id="file" placeholder="Upload File" className="form-control" onChange={e => {
                            this.setState({ selectedVideo: e.target.files[0] });
                          }} />
                          <UploadVideo
                            selectedFile={this.state.selectedVideo}
                            uploadState={(states) => this.setParentState(states)}
                          >
                          </UploadVideo>
                          {(this.state.uploadState.socketUploading && !this.state.uploadState.socketIsByte) &&
                            <div>Uploading ({this.state.uploadState.socketMBDone} / {this.state.uploadState.socketTotalSizeMB} MB) ( {this.state.uploadState.socketPercent}% )</div>
                          }
                          {(this.state.uploadState.socketUploading && this.state.uploadState.socketIsByte) &&
                            <div>Uploading ({this.state.uploadState.socketUploaded} / {this.state.uploadState.socketTotalSize} Byte) ( {this.state.uploadState.socketPercent}% )</div>
                          }
                          {this.state.uploadState.socketPercent === 100 &&
                            <div>Uploaded ({this.state.uploadState.socketTotalSize.toLocaleString()}) ( {this.state.uploadState.socketPercent}% )</div>
                          }
                          {(this.state.uploadState.socketUrl && this.state.uploadState.socketUrl !== '') &&
                            <div><a target='_blank' href={this.state.uploadState.socketUrl}>Link</a></div>
                          }
                        </Form.Group>

                      <Form.Group as={Col}>
                        <Form.Label style={{ fontFamily: "Gordita-bold" }}>
                          Duration
                          </Form.Label>
                        <Form.Control onChange={(event) => { this.setState({ last_name: event.target.value }) }} />
                      </Form.Group>
                      </Form.Row>
                    </Form>
                  </Accordion.Collapse>
                </Form.Group>
              </Accordion>
            </Modal.Body>
          <Modal.Footer>

            <Button className="button-grape" onClick={() => { this.createtopic() }}>Create</Button>
          </Modal.Footer>
          </Modal>

        <Modal show={modalEditIsOpen} onHide={this.onCloseEditModal}>
          <Modal.Header bsPrefix closeButton />
          <Modal.Body>
            <Accordion defaultActiveKey="0">
              <Form.Group>
                <Form.Label style={{ fontFamily: "Gordita-bold" }}>
                  Topic Name*: ({this.state.edited_list.topic_name})
                  </Form.Label>
                <Form.Control type="text" />
              </Form.Group>
              <Form.Group>
                <Accordion.Toggle
                  as={Button}
                  className="button-grape btn-sm"
                  style={{ width: "30%" }}
                  eventKey="1"
                >
                  Add Sub Topic
                  </Accordion.Toggle>
                <Accordion.Collapse eventKey="1">
                  <Form.Group>
                    <Form.Label>Topic Name*</Form.Label>
                    <Form.Control type="text" onChange={(event) => {
                      this.setState({ topic_name: event.target.value });
                    }} />
                  </Form.Group>
                </Accordion.Collapse>
              </Form.Group>
            </Accordion>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="danger" onClick={() => {
              if (
                window.confirm(
                  `Are You sure ?`
                )
              ) {
                this.handleRemoveTop(this.state.edited_list.id)
              }
            }}><i className="fa fa-trash-o" aria-hidden="true"></i></Button>
            <Button variant="success" onClick={() => { this.updatetopic() }}><i className="fa fa-check" aria-hidden="true"></i></Button>
          </Modal.Footer>
        </Modal>
        <div style={{ fontFamily: 'Gordita', marginLeft: '1070px' }}><br />
          <Button variant="danger" style={{ fontFamily: 'Gordita', marginRight: '5%' }} onClick={() => {
            if (window.confirm(`Are You sure ?`)) {
              this.handleRemove(this.state.course.id);
            }
          }}
          >Delete</Button>
          <Button className="button-grape" onClick={() => {
            this.updateCourse();
          }}
          >Update</Button>
        </div>
      </Container>
      </>
    );
  }
}

export default EditCourse;
