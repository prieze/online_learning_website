import React, { Component, Fragment } from 'react';
import { Navbar, Nav, NavDropdown, Carousel, Form, Button, Row, Col, Card, Image, Container, Modal } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../style/styles.css';
import 'font-awesome/css/font-awesome.min.css';
import { Link } from 'react-router-dom';
import Shop from "../gambar/shop.png";
import { BrowserRouter as Router, Redirect, NavLink } from "react-router-dom";
import GoogleLogin from 'react-google-login'
import Footer from "./Footer/Footer"
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
// import Dashboard from './Dashboard/Dashboard';
// import Confirm from "./Email/Confirm";
// import { message } from 'antd';
// import { resolveOnChange } from 'antd/lib/input/Input';
// import { Formik } from "formik";
// import * as yup from 'yup';
import Login from './Login';
import Navigation from './Navigation/Navigation';
import ListCategory from './Courses/ListCategory';
import BestSellingList from './Courses/BestSellingList';
import NewCourseList from './Courses/NewCourseList';
import SpecialOfferList from './Courses/SpecialOfferList';
import ListWebinar from './UserWebinar/ListWebinar';

const validEmailRegex = RegExp(/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i);
const validateForm = errors => {
  let valid = true;
  Object.values(errors).forEach(val => val.length > 0 && (valid = false));
  return valid;
};


class Landing extends Component {

  constructor(props) {
    super(props)
    this.state = {
      course: [],
      modalLoginIsOpen: false,
      modalSignIsOpen: false,
      email: null,
      password: null,
      compass: null,
      first_name: null,
      last_name: null,
      login: false,
      register: false,
      redirect: false,
      store: null,
      message: null,
      forgot: false,
      specialOffer: [],
      specialOffers: [],
      categories: [],
      newCourse: [],
      newCourses: [],
      bestSelling: [],
      bestSellings: [],
      cat: [],
      catId: 0,
      newsId: 0,
      specId: 0,
      next: 0,
      prevs: true,
      nexts: true,
      nextcour: 0,
      prevscour: true,
      nextscour: true,
      nextoff: 0,
      prevsoff: true,
      nextsoff: true,
      webinar: [],
      discount: []
    };
  }

  Logo = 'https://galilagi-media.oss-ap-southeast-5.aliyuncs.com/assets/logo.png';
  Desktop = 'https://galilagi-media.oss-ap-southeast-5.aliyuncs.com/assets/banner-main.png';
  User1 = 'https://galilagi-media.oss-ap-southeast-5.aliyuncs.com/assets/user1.png'

  onOpenLoginModal = () => {
    this.setState({ modalLoginIsOpen: true, modalSignIsOpen: false, message: null });
  };

  onOpenSignModal = (sign) => {
    this.setState({ modalSignIsOpen: sign, modalLoginIsOpen: false, message: null });
  };

  onCloseLoginModal = () => {
    this.setState({ modalLoginIsOpen: false, message: null });
  };

  onForgotPassword = () => {
    this.setState({ modalLoginIsOpen: false, message: null });
    this.changeURL('/ForgotPassword');
  };

  changeURL(url) {
    this.props.history.push(url);
  }

  onCloseSignModal = () => {
    this.setState({ modalSignIsOpen: false, message: null });
  };

  responseGoogle = (response) => {
    console.log(response);
    console.log(response.profileObj);

  }

  componentDidUpdate(prevProps) {
    if (prevProps.selected !== this.props.selected) {
      this.setState({ selected: this.props.selected })
    }
  }

  componentDidMount() {
    this.specialOffer();
    this.listCategory();
    this.newCourse();
    this.Webinar();
    this.bestSelling();
    console.log(this.state.redirect);

    const token = localStorage.getItem('token');
    if (token) {
      this.setState({ login: true, id: localStorage.getItem('id') });
    }
    const cat = localStorage.getItem('categories');
    if (cat) {
      this.setState({ cat: JSON.parse(cat) })
    }
  }

  Webinar(){
    const token = localStorage.getItem('token');
    fetch(`${process.env.REACT_APP_BASEURL}/api/webinar?limit=4&offset=&search=`, {
        method: "GET",
        headers: {
            "Authorization": `Bearer ${token}`,
            "Content-Type": "application/json"
        },
    }).then((response) => {
        response.json()
            .then((result) => {
              console.log(`webinar `,result);
              this.setState({
                  webinar: result.rows
              });
              const discount = result.rows.map((disc)=>{
                const price = parseInt(disc.price);
                const dic = parseInt(disc.discount);
                const jum = (dic/100)*price;
                const tot = price-parseInt(jum);
                return tot
              });
              // discount.map(id =>{this.state.webinar.map})
              this.state.webinar.forEach(function(jum, index) {
                jum.hargadisc = discount[index]
                // console.log(typeof(jum));
                // console.log(typeof(discount));
                // console.log(index);
              })
              // console.log(`discount`,discount);
                // this.state.webinar.concat(discount);
                console.log(this.state);
                // console.log("WEBINAR",this.state.webinar)
            })
    })
  }

  specialOffer() {

    const token = localStorage.getItem("token");
    this.setState({ loading: true });
    const id = this.state.specId;
    const nextoff = this.state.nextoff;
    let url = `${process.env.REACT_APP_BASEURL}/api/dashboard/special/offer?limit=4&offset=${nextoff}&search=`;
    if (id) {
      url = `${process.env.REACT_APP_BASEURL}/api/dashboard/special/offer/${id}?limit=4&offset=${nextoff}&search=`
    }
    fetch(url, {
      method: "GET",
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      },
    })
      .then((response) => response.json())
      .then((json) => {
        if (id !== 0) {
          console.log(`special offers id !== 0 `,json.data.courses);
          const obj = {category_name: json.data.category_name, courses: [] ,id: json.data.id};
          const temp = [];
          const discountspe = json.data.courses.map((disc)=>{
            const price = parseInt(disc.price);
            const dic = parseInt(disc.discount);
            const jum = (dic/100)*price;
            const tot = price-parseInt(jum);
            temp.push({
              ...disc,
              hargadisc: tot,
              category_name: json.data.category_name,
              id: json.data.id
            })
            return tot
          });
          this.setState(prevstate => ({
            ...prevstate,
            specialOffers: temp,
            nextof: json.data.courses.length,
            loading: false,
          }));
          // this.setState({
          //   specialOffers: json.data.courses,
          //   nextof: json.data.courses.length,
          //   loading: false,
          //   discountspe: discountspe
          // });
          // this.state.specialOffers.map(function(jumsp, index){
          //   jumsp.hargadisc = discountspe[index];
          // })
          // this.setState(prevState => ({
          //   specialOffers: {
          //                 ...prevState.specialOffers,
          //                 hargadisc: discountspe
          //   }
          // }))
          console.log(`typeof spesial offer `, json.data.courses);
          console.log(`typeof spesial offer `, typeof(this.state.specialOffers));
          console.log(`typeof spesial offer state `, this.state);
          console.log(`special offer discount `, discountspe);
          // console.log(`spesial offer if`, json.data);
          // console.log(`spesial offer if`, json.data.courses.length);
        }else{
          console.log(`special offer `, json.data);
          const temp = [];
          const discountspe = json.data.map((disc)=>{
            const price = parseInt(disc.price);
            const dic = parseInt(disc.discount);
            const jum = (dic/100)*price;
            const tot = price-parseInt(jum);
            temp.push({
              ...disc,
              hargadisc: tot
            })
            return tot
          });
          this.setState({
            specialOffer: temp,
            nextof: json.data.length,
            loading: false,
            discountspe: discountspe
          });
          // this.state.specialOffer.map(function(jumsp, index){
          //   jumsp.hargadisc = discountspe[index];
          // })
          console.log(`special offer discount `, discountspe);
          console.log(`special offer state `, this.state);
          console.log(`special offer state `, json.data);
        }
        // console.log(`spesial offer`, json.data);
        // console.log(`spesial offer state `, this.state.specialOffer);
        // console.log(`spesial offer states`, this.state.specialOffers);
      })
      .catch((error) => console.log("error", error));
  }

  klikspes = async(id) => {
    // console.log(id);
    await this.setState({ specId: id, nextoff: 0,prevsoff: true });
    // console.log(this.state.specId);
    this.specialOffer();
  }

  nextoff = async(next) => {
    const lanjut = this.state.nextoff + next
    await this.setState({
      nextoff: lanjut,
      prevsoff: false
    });
    console.log("nextof", this.state.nextoff);
    console.log(lanjut);
    this.specialOffer();
  }

  prevoff = async(prev) => {
    const lanjut = this.state.nextoff - prev
    if (lanjut === 0) {
      await this.setState({ prevsoff: true });
    }
    await this.setState({ nextoff: lanjut });
    console.log(this.state.nextoff);
    console.log(lanjut);
    this.specialOffer();
  }

  bestSelling() {

    const token = localStorage.getItem("token");
    const id = this.state.catId;
    const next = this.state.next;
    let url = `${process.env.REACT_APP_BASEURL}/api/dashboard/best/selling?limit=4&offset=${next}&search=`;
    if (id !== 0) {
      url = `${process.env.REACT_APP_BASEURL}/api/dashboard/best/selling/${id}?limit=4&offset=${next}&search=`;
    }
    this.setState({ loading: true });
    console.log(url);
    fetch(url, {
      method: "GET",
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      },
    })
      .then((response) => response.json())
      .then((json) => {
        if (id !== 0) {
          // this.setState({
          //   bestSelling: json.data,
          //   more: json.data.courses.length,
          //   loading: false,
          // });
          // console.log(json.data.courses.length);
          const obj = {category_name: json.data.category_name, courses: [] ,id: json.data.id};
          const temp = [];
          const discount = json.data.courses.map((disc)=>{
            const price = parseInt(disc.price);
            const dic = parseInt(disc.discount);
            const jum = (dic/100)*price;
            const tot = price-parseInt(jum);
            temp.push({
              ...disc,
              hargadisc: tot,
              category_name: json.data.category_name,
              id: json.data.id
            })
            return tot
          });
          temp.push(discount);
          obj.courses.push(temp)
          console.log(`temp`, temp);
          this.setState(prevstate => ({
            ...prevstate,
            bestSellings: temp,
            more: json.data.courses.length,
            loading: false,
          }));
          // this.state.bestSelling.courses.forEach(function(jum, index) {
          //   jum.hargadisc = discount[index]
          //   // console.log(typeof(jum));
          //   // console.log(typeof(discount));
          //   // console.log(index);
          // })
          console.log(`best selling disc `,obj);
          console.log(`state best `,this.state.bestSellings);
          console.log(`JSON Data `,json.data);
        } else {
          // this.setState({
          //   bestSellings: json.data,
          //   more: json.data.length,
          //   loading: false,
          // });
          const temp = [];
          const discount = json.data.map((disc)=>{
            const price = parseInt(disc.price);
            const dic = parseInt(disc.discount);
            const jum = (dic/100)*price;
            const tot = price-parseInt(jum);
            temp.push({
              ...disc,
              hargadisc: tot
            })
            return tot
          });
          this.setState({
            bestSelling: temp,
            more: json.data.length,
            loading: false,
          })
          // this.state.bestSellings.forEach(function(jum, index) {
          //   jum.hargadisc = discount[index]
          //   // console.log(typeof(jum));
          //   // console.log(typeof(discount));
          //   // console.log(index);
          // })
          console.log(`best selling disc `,temp);
          console.log(`state best `,this.state.bestSelling);
          console.log(`state bests `,this.state.bestSellings);
        }
        // console.log(`best selling`,json.data);
        // console.log(this.state.categories);
        // console.log(this.state.bestSellings);
      })
      .catch((error) => console.log("error", error));
  }

  klikcat = async (id) => {
    await this.setState({ catId: id, next: 0, prevs: true });
    console.log(this.state.catId);
    this.bestSelling();
  }

  nextarr = async (next) => {
    const lanjut = this.state.next + next
    await this.setState({
      next: lanjut,
      prevs: false
    });
    console.log("next", this.state.next);
    console.log(lanjut);
    this.bestSelling();
  }

  prevarr = async (prev) => {
    const lanjut = this.state.next - prev
    if (lanjut === 0) {
      await this.setState({ prevs: true });
    }
    await this.setState({ next: lanjut });
    console.log(this.state.next);
    console.log(lanjut);
    this.bestSelling();
  }

  listCategory() {
    const token = localStorage.getItem('token');
    this.setState({ loading: true });
    fetch(`${process.env.REACT_APP_BASEURL}/api/read/categories?limit=2&offset=0&search=`, {
      method: 'GET',
      headers: {
        "Authorization": `Bearer ${token}`,
        "Content-Type": "application/json"
      },
    })
      .then(response => response.json())
      .then(json => {
        this.setState({
          categories: json.rows,
          loading: false,
        })
        console.log(json.rows);
        console.log(this.state.categories);
        // localStorage.setItem('categories', JSON.stringify(json.rows))
      })
      .catch(error => console.log('error', error));
  }

  newCourse() {
    const token = localStorage.getItem("token");
    this.setState({ loading: true });
    const id = this.state.newsId;
    const nextcour = this.state.nextcour;
    console.log(nextcour);
    let url = `${process.env.REACT_APP_BASEURL}/api/dashboard/new/course?limit=4&offset=${nextcour}&search=`;
    if(id){
      url = `${process.env.REACT_APP_BASEURL}/api/dashboard/new/course/${id}?limit=4&offset=${nextcour}&search=`;
    }
    fetch(url, {
      method: "GET",
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      },
    })
      .then((response) => response.json())
      .then((json) => {
        if(id !== 0){
          const obj = {category_name: json.data.category_name, courses: [] ,id: json.data.id};
          const temp = [];
          const discount = json.data.courses.map((disc)=>{
            const price = parseInt(disc.price);
            const dic = parseInt(disc.discount);
            const jum = (dic/100)*price;
            const tot = price-parseInt(jum);
            temp.push({
              ...disc,
              hargadisc: tot,
              category_name: json.data.category_name,
              id: json.data.id
            })
            return tot
          });
          this.setState(prevstate => ({
            ...prevstate,
            newCourses: temp,
            nextcorse: json.data.courses.length,
            loading: false,
          }));
          // this.setState({
          //   newCourses: json.data.courses,
          //   nextcorse: json.data.courses.length,
          //   loading: false,
          // });
        console.log(`new courses if`,json.data.courses);
        console.log(`new courses temp`,temp);
      }else{
        const temp = [];
          const discount = json.data.map((disc)=>{
            const price = parseInt(disc.price);
            const dic = parseInt(disc.discount);
            const jum = (dic/100)*price;
            const tot = price-parseInt(jum);
            temp.push({
              ...disc,
              hargadisc: tot
            })
            return tot
          });
          this.setState({
            newCourse: temp,
            nextcorse: json.data.length,
            loading: false,
          });
        console.log(`new courses else`,this.state.newCourse);
          // this.setState({
          //   newCourse: json.data,
          //   nextcorse: json.data.length,
          //   loading: false,
          // });
        }
        console.log(`new courses`,json.data);
        console.log(`new courses`,json.data.length);
      })
      .catch((error) => console.log("error", error));
  }
  
  kliknew = async (id) => {
    await this.setState({ newsId: id, nextcour: 0, prevscour: true });
    this.newCourse();
  }

  nextcour = async(next) => {
    const lanjut = this.state.nextcour + next
    await this.setState({
      nextcour: lanjut,
      prevscour: false
    });
    console.log("next", this.state.nextcour);
    console.log(lanjut);
    this.newCourse();
  }

  prevcour = async(prev) => {
    const lanjut = this.state.nextcour - prev
    if (lanjut === 0) {
      await this.setState({ prevscour: true });
    }
    await this.setState({ nextcour: lanjut });
    console.log(this.state.nextcour);
    console.log(lanjut);
    this.newCourse();
  }

  register() {
    console.log(this.state);
    console.log(this.validate());
    if (this.state.compass !== this.state.password) {
      toast.error("Password Doesn't match");
    } else {
      fetch(`${process.env.REACT_APP_BASEURL}/api/register`, {
        method: "POST",
        body: JSON.stringify({
          email: this.state.email,
          password: this.state.password,
          first_name: this.state.first_name,
          last_name: this.state.last_name
        }),
        headers: {
          "Content-Type": "application/json"
        },
      }).then((response) => {
        response.json()
          .then((result) => {
              if (result.sukses) {
                let input = {};
                input['name'] = "";
                input['email'] = "";
                console.warn("result", result);
                console.log("result", result);
                localStorage.setItem('register', JSON.stringify({
                  register: true,
                  token: result.token
                }));
                // localStorage.setItem('token', result.token);
                // localStorage.setItem('userData', JSON.stringify(result.data));
                // localStorage.setItem('roles', result.data.roles)
                // localStorage.setItem('id', result.data.id);
                // console.log(`result`,result);
                // console.log(`result data`,result.data);
                this.onOpenLoginModal();
                toast.success("Berhasil Register", 1);
              }else{
                console.log('gagal if');
                if (result.msg.includes("email")) {
                  toast.error("enter valid email");
                }else if (result.msg.includes("first_name")){
                  toast.error("first name cannot be empty");
                }else if (result.msg.includes("last_name")){
                  toast.error("last name cannot be empty");
                }else{
                  toast.error(result.msg, 1);
                }
                console.log(typeof(result.msg));
              }
            // } else {
            //   message.error(this.validate.errors, 1);
            // }
          })
      })
    }
  }

  validate() {
    let input = this.state;
    let errors = {};
    let isValid = true;

    if (!input["name"]) {
      isValid = false;
      errors["name"] = "Please enter your name.";
    }

    if (!input["email"]) {
      isValid = false;
      errors["email"] = "Please enter your email Address.";
    }

    if (typeof input["email"] !== "undefined") {

      var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
      if (!pattern.test(input["email"])) {
        isValid = false;
        errors["email"] = "Please enter valid email address.";
      }
    }

    if (!input["comment"]) {
      isValid = false;
      errors["comment"] = "Please enter your comment.";
    }

    this.setState({
      errors: errors
    });

    return isValid;
  }


  render() {
    const { modalLoginIsOpen, modalSignIsOpen, redirect, errors } = this.state;

    return (
      <>
        <Router>
          <ToastContainer />
          <Container fluid style={{ paddingBottom: "180px" }}>
            <Fragment>
              {localStorage.getItem("token") === null ? (
                <Navbar expand="lg">
                  <a className="logo" href="/">
                    <img className="img-responsive logo" src={this.Logo} alt="" />
                  </a>
                  <Navbar.Toggle aria-controls="basic-navbar-nav" />
                  <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="mr-auto">
                      <NavDropdown
                        title={this.state.selected ? this.state.selected : "Category"} id="basic-nav-dropdown" color="#6831B0" className="dropdown" >
                        {this.state.cat.map((cat) => {
                          return (
                            <NavDropdown.Item style={{ fontFamily: "Gordita" }} onClick={() => { this.changeURL(`/Courses/${cat.id}`); }} >
                              {cat.category_name}
                            </NavDropdown.Item>
                          );
                        })}
                      </NavDropdown>
                    </Nav>

                    <Form inline>
                      <div>
                        <input type="search" className="srch" placeholder="Search for anything" />
                        <span className="icon">
                          <i className="fa fa-search"></i>
                        </span>
                      </div>
                      <a className="btn" style={{ fontSize: "13px" }} onClick={() => { this.changeURL("/ShoppingChart"); }} >
                        Shopping Chart
                          </a>
                      <a id="login" className="btn" onClick={this.onOpenLoginModal} >
                        Login
                          </a>
                      {this.state.modalLoginIsOpen ? (
                        <Login state={this.state} modalLoginIsOpen={this.state.modalLoginIsOpen} modalSignIsOpen={this.onOpenSignModal} redirect={(e) => { this.props.history.push(e); }} onClose={() => { this.setState({ modalLoginIsOpen: false }); }} />
                      ) : null}
                      <a id="signup" className="btn" onClick={this.onOpenSignModal} >
                        Sign Up
                          </a>
                    </Form>
                  </Navbar.Collapse>
                </Navbar>
              ) : (
                  <>
                    <Navigation history={this.props.history} />
                    <Nav style={{ backgroundColor: "#F9F8F8" }} className="justify-content-center" activeKey="/home" >
                      {this.state.categories.map((categories) => {
                        return (
                          <ListCategory id={categories.id} key={categories.id} categories={categories} history={(url) => { this.changeURL(url); }} ></ListCategory>
                        );
                      })}
                    </Nav>
                  </>
                )}

              <Carousel>
                <Carousel.Item>
                  <img className="d-block w-100" src={this.Desktop} />
                </Carousel.Item>
                <Carousel.Item>
                  <img className="d-block w-100" src={this.Desktop} />
                </Carousel.Item>
                <Carousel.Item>
                  <img className="d-block w-100" src={this.Desktop} />
                </Carousel.Item>
              </Carousel>

              <Card className="tulisan" style={{ display: "block", width: "35%", boxShadow: "none", border: "none", borderRadius: "10px", background: "rgba(65, 68, 65, 0.54)", position: "absolute", top: "150px", left: "50px", }}>
                <Card.Body className="tulisan">
                  <Card.Title style={{ color: "#FFFFFF", fontSize: "50px", fontFamily: "Amiri", }}>
                    Empower Yourself
                      </Card.Title>
                  <Card.Text style={{ color: "#FFFFFF" }}>
                    Join Galilagi to watch, learn, make, and discover
                      </Card.Text>
                </Card.Body>
              </Card>

              <Row className="justify-content-center" style={{ marginTop: "5%" }} >
                <h3 style={{ fontFamily: "Gordita-bold" }}>
                  Explore our courses to find your perfect program
                </h3>
              </Row>

              <Row className="justify-content-center" style={{ marginTop: "5%", width: "79%", marginRight: 'auto', marginLeft: 'auto' }} >
                <Col lg={7}>
                  <h3 style={{ fontFamily: "Gordita-bold", color: "#392C7E", float: 'right' }} >
                    Webinar
                  </h3>
                </Col>
                <Col>
                {/* onClick={() => { this.props.history.push({  pathname: '/Webinars', data: { state: this.state.webinars } }); }} */}
                  <div style={{ float: 'right' }} onClick={() => { this.props.history.push({  pathname: '/Webinars', data: { state: this.state.webinars } }); }} className="btn btn-outline-secondary" >
                    View All
                  </div>
                </Col>
              </Row>

              <Row style={{ width: "80%", margin: "auto" }}>
                  {
                    this.state.webinar?.map((webinar) => {
                      // console.log(`webinars `, webinar);
                      return <Col style={{ paddingBottom: "15%" }} xs={12} sm={6} md={4} lg={3} xl={3} >
                        <ListWebinar key={webinar.id} id={webinar.id} webinar={webinar} history={(url) => { this.changeURL(url); }} />
                      </Col>
                    })
                  }
              </Row>

              <Row className="justify-content-center" style={{ marginTop: "5%" }} >
                <h3 style={{ fontFamily: "Gordita-bold", color: "#392C7E" }} >
                  Best Selling
                </h3>
              </Row>

              <Row className="justify-content-center" style={{ marginTop: "1%" }} >
                <Col lg={8}>
                  <Nav className="justify-content-center" activeKey="/home">
                    <div style={{ cursor: 'pointer', color: '#002333', fontSize: '16px', fontFamily: 'Gordita-bold', paddingLeft: '2rem' }} onClick={() => { this.klikcat(0) }}>All Categories</div>
                    {this.state.categories.map((categories) => {
                      return (
                        <div style={{ cursor: 'pointer', color: '#002333', fontSize: '16px', fontFamily: 'Gordita-bold', paddingLeft: '2rem' }} onClick={() => { this.klikcat(categories.id) }}>{categories.category_name}</div>
                      );
                    })}
                  </Nav>
                </Col>
                <Col lg={2}>
                  {/* <div onClick={() => { this.changeURL(`/BestSelling`); }} className="btn btn-outline-secondary" > */}
                  <div onClick={() => { this.props.history.push({  pathname: '/BestSelling', data: { state: this.state.categories } }); }} className="btn btn-outline-secondary" >
                    View All
                      </div>
                </Col>
              </Row>

              <Row style={{ width: "80%", margin: "auto" }}>
                {(this.state.catId)
                  ? this.state.bestSellings?.courses?.map((list) => {
                    // console.log("ada id");
                    return (
                      <Col style={{ paddingBottom: "15%" }} xs={12} sm={6} md={4} lg={3} xl={3} >
                        <BestSellingList id={list.id} key={list.id} test={list} history={(url) => { this.changeURL(url); }} ></BestSellingList>
                      </Col>
                    );
                  })
                  : this.state.bestSelling.map((list) => {
                    // console.log("gak ada id");
                    return (
                      <Col style={{ paddingBottom: "15%" }} xs={12} sm={6} md={4} lg={3} xl={3} >
                        <BestSellingList id={list.id} key={list.id} test={list} history={(url) => { this.changeURL(url); }} ></BestSellingList>
                      </Col>
                    );
                  })
                }
              </Row>

              {(this.state.more > 3)
                ?
                <Row style={{ width: "95%", margin: "auto", marginTop: "-21%" }}>
                  <Col>
                    <Button variant="light" disabled={this.state.prevs} style={{ width: "60px", height: "60px", borderRadius: "50px", boxShadow: "4px 4px 20px rgba(0, 0, 0, 0.2)" }} onClick={() => { this.prevarr(4) }}>
                      <i style={{ color: "#3B2881" }} className="fa fa-arrow-left" ></i>
                    </Button>
                  </Col>
                  <Col>
                    <Button variant="light" style={{ width: "60px", height: "60px", borderRadius: "50px", boxShadow: "4px 4px 20px rgba(0, 0, 0, 0.2)", marginLeft: "90%", }} onClick={() => { this.nextarr(4) }} >
                      <i style={{ color: "#3B2881" }} className="fa fa-arrow-right" ></i>
                    </Button>
                  </Col>
                </Row>
                : <Row style={{ width: "95%", margin: "auto", marginTop: "-21%" }}>
                  <Col>
                    <Button variant="light" disabled={this.state.prevs} style={{ width: "60px", height: "60px", borderRadius: "50px", boxShadow: "4px 4px 20px rgba(0, 0, 0, 0.2)" }} onClick={() => { this.prevarr(4) }}>
                      <i style={{ color: "#3B2881" }} className="fa fa-arrow-left" ></i>
                    </Button>
                  </Col>
                  <Col>
                    <Button variant="light" disabled={this.state.nexts} style={{ width: "60px", height: "60px", borderRadius: "50px", boxShadow: "4px 4px 20px rgba(0, 0, 0, 0.2)", marginLeft: "90%", }} onClick={() => { this.nextarr(4) }} >
                      <i style={{ color: "#3B2881" }} className="fa fa-arrow-right" ></i>
                    </Button>
                  </Col>
                </Row>
              }

              <Row className="justify-content-center" style={{ marginTop: "20%" }} >
                <h3 style={{ fontFamily: "Gordita-bold", color: "#392C7E" }} >
                  New This Month
                </h3>
              </Row>

              <Row className="justify-content-center" style={{ marginTop: "1%" }} >
                <Col lg={8}>
                  <Nav className="justify-content-center" activeKey="/home">
                  <div style={{ cursor: 'pointer', color: '#002333', fontSize: '16px', fontFamily: 'Gordita-bold', paddingLeft: '2rem' }} onClick={() => {this.kliknew(0)}} >All Categories</div>
                    {this.state.categories.map((categories) => {
                      return (
                        <div style={{ cursor: 'pointer', color: '#002333', fontSize: '16px', fontFamily: 'Gordita-bold', paddingLeft: '2rem' }} onClick={() => { this.kliknew(categories.id) }}>{categories.category_name}</div>
                      );
                    })}
                  </Nav>
                </Col>
                <Col lg={2}>
                  <div onClick={() => { this.props.history.push({  pathname: '/NewCourse', data: { state: this.state.categories } }); }} className="btn btn-outline-secondary" >
                    View All
                      </div>
                </Col>
              </Row>

              <Row style={{ width: "80%", margin: "auto" }}>
                {(this.state.newsId) 
                ? this.state.newCourses.map((list) => {
                  return (
                    <Col style={{ paddingBottom: "15%" }} xs={12} sm={6} md={4} lg={3} xl={3} >
                      <NewCourseList id={list.id} key={list.id} list={list} history={(url) => { this.changeURL(url); }} ></NewCourseList>
                    </Col>
                  );
                })
                : this.state.newCourse?.map((list) => {
                  return (
                    <Col style={{ paddingBottom: "15%" }} xs={12} sm={6} md={4} lg={3} xl={3} >
                      <NewCourseList id={list.id} key={list.id} list={list} history={(url) => { this.changeURL(url); }} ></NewCourseList>
                    </Col>
                  );
                })
                }
              </Row>
              {(this.state.nextcorse > 3)
              ?<Row style={{ width: "95%", margin: "auto", marginTop: "-19%" }} >
              <Col>
                <Button variant="light" disabled={this.state.prevscour} style={{ width: "60px", height: "60px", borderRadius: "50px", boxShadow: "4px 4px 20px rgba(0, 0, 0, 0.2)", }} onClick={() => { this.prevcour(4) }}>
                  <i style={{ color: "#3B2881" }} className="fa fa-arrow-left" ></i>
                </Button>
              </Col>
              <Col>
                <Button variant="light" style={{ width: "60px", height: "60px", borderRadius: "50px", boxShadow: "4px 4px 20px rgba(0, 0, 0, 0.2)", marginLeft: "90%", }} onClick={() => { this.nextcour(4) }}>
                  <i style={{ color: "#3B2881" }} className="fa fa-arrow-right" ></i>
                </Button>
              </Col>
            </Row>
            : <Row style={{ width: "95%", margin: "auto", marginTop: "-19%" }} >
            <Col>
              <Button variant="light" disabled={this.state.prevscour} style={{ width: "60px", height: "60px", borderRadius: "50px", boxShadow: "4px 4px 20px rgba(0, 0, 0, 0.2)", }} onClick={() => { this.prevcour(4) }}>
                <i style={{ color: "#3B2881" }} className="fa fa-arrow-left" ></i>
              </Button>
            </Col>
            <Col>
              <Button variant="light" disabled={this.state.nextscour} style={{ width: "60px", height: "60px", borderRadius: "50px", boxShadow: "4px 4px 20px rgba(0, 0, 0, 0.2)", marginLeft: "90%", }} onClick={() => { this.nextcour(4) }}>
                <i style={{ color: "#3B2881" }} className="fa fa-arrow-right" ></i>
              </Button>
            </Col>
          </Row>
            }

              <Row className="justify-content-center" style={{ marginTop: "20%" }} >
                <h3 style={{ fontFamily: "Gordita-bold", color: "#392C7E" }} >
                  Special Offer
                    </h3>
              </Row>

              <Row className="justify-content-center" style={{ marginTop: "2%" }} >
                <Col lg={8}>
                  <Nav className="justify-content-center" activeKey="/home">
                    <div style={{ cursor: 'pointer', color: '#002333', fontSize: '16px', fontFamily: 'Gordita-bold', paddingLeft: '2rem' }} onClick={() => {this.klikspes(0)}} >All Categories</div>
                    {this.state.categories.map((categories) => {
                      return (
                        <div style={{ cursor: 'pointer', color: '#002333', fontSize: '16px', fontFamily: 'Gordita-bold', paddingLeft: '2rem' }} onClick={() => {this.klikspes(categories.id)}}>{categories.category_name}</div>
                      );
                    })}
                  </Nav>
                </Col>
                <Col lg={2}>
                  <div onClick={() => { this.props.history.push({  pathname: '/SpecialOffer', data: { state: this.state.categories } }); }} className="btn btn-outline-secondary" >
                    View All
                      </div>
                </Col>
              </Row>

              <Row style={{ width: "80%", margin: "auto" }}>
                { (this.state.specId)
                 ? this.state.specialOffers?.map((list) => {
                  return (
                    <Col style={{ paddingBottom: "15%" }} xs={12} sm={6} md={3} lg={3} xl={3} >
                      <SpecialOfferList id={list.id} key={list.id} list={list} discount={this.state.discountspe} history={(url) => { this.changeURL(url); }} ></SpecialOfferList>
                    </Col>
                  );
                })
                : this.state.specialOffer?.map((list) => {
                  return (
                    <Col style={{ paddingBottom: "15%" }} xs={12} sm={6} md={3} lg={3} xl={3} >
                      <SpecialOfferList id={list.id} key={list.id} list={list} history={(url) => { this.changeURL(url); }} ></SpecialOfferList>
                    </Col>
                  );
                })
                }
              </Row>

              {(this.state.nextof > 3)
              ?<Row style={{ width: "95%", margin: "auto", marginTop: "-19%" }} >
              <Col>
                <Button variant="light" disabled={this.state.prevsoff} style={{ width: "60px", height: "60px", borderRadius: "50px", boxShadow: "4px 4px 20px rgba(0, 0, 0, 0.2)", }} onClick={() => { this.prevoff(4) }}>
                  <i style={{ color: "#3B2881" }} className="fa fa-arrow-left" ></i>
                </Button>
              </Col>
              <Col>
                <Button variant="light" style={{ width: "60px", height: "60px", borderRadius: "50px", boxShadow: "4px 4px 20px rgba(0, 0, 0, 0.2)", marginLeft: "90%", }} onClick={() => { this.nextoff(4) }}>
                  <i style={{ color: "#3B2881" }} className="fa fa-arrow-right" ></i>
                </Button>
              </Col>
            </Row>
            : <Row style={{ width: "95%", margin: "auto", marginTop: "-19%" }} >
            <Col>
              <Button variant="light" disabled={this.state.prevsoff} style={{ width: "60px", height: "60px", borderRadius: "50px", boxShadow: "4px 4px 20px rgba(0, 0, 0, 0.2)", }} onClick={() => { this.prevoff(4) }}>
                <i style={{ color: "#3B2881" }} className="fa fa-arrow-left" ></i>
              </Button>
            </Col>
            <Col>
              <Button variant="light" disabled={this.state.nextsoff} style={{ width: "60px", height: "60px", borderRadius: "50px", boxShadow: "4px 4px 20px rgba(0, 0, 0, 0.2)", marginLeft: "90%", }} onClick={() => { this.nextoff(4) }}>
                <i style={{ color: "#3B2881" }} className="fa fa-arrow-right" ></i>
              </Button>
            </Col>
          </Row>
            }
            </Fragment>
          </Container>

          <Modal size="lg" show={modalSignIsOpen} onHide={this.onCloseSignModal} aria-labelledby="contained-modal-title-vcenter">
            <Modal.Header closeButton bsPrefix></Modal.Header>
            <Modal.Body className="show-grid">
              <Container>
                <Row className="justify-content-center">
                  <Col xs={9} md={6}>
                    <Row className="justify-content-center">
                      <Col md={12}>
                        <p style={{ fontFamily: "Gordita-Bold", fontSize: "22px", }}>
                          Joining Galilagi For
                            </p>
                        <p style={{ fontFamily: "Gordita-Bold", fontSize: "22px", width: "15%", }} >
                          Free
                            </p>
                        <p style={{ width: "15%", height: "5px", marginTop: "-24px", background: "#3722D3", borderRadius: "8px", }} ></p>
                        <p style={{ fontFamily: "Gordita", fontSize: "18px", }} >
                          Learn and improve skills across business, tech,
                          design, and more. Taught by experts to help you
                          workforce do whatever comes next.
                            </p>
                      </Col>
                    </Row>
                  </Col>
                  <Col xs={9} md={6}>
                    <Form.Label style={{ color: "red" }}>
                      {this.state.message}
                    </Form.Label>
                    <Row className="justify-content-center">
                      <GoogleLogin clientId="944247106925-m1vaiosjtsdsv876ctg54qdpgfcvkpht.apps.googleusercontent.com" buttonText="Login with Google" onSuccess={this.responseGoogle} onFailure={this.responseGoogle} cookiePolicy={"single_host_origin"} />
                    </Row>
                    <p className="text-center">or</p>
                    <Row className="justify-content-center">
                      <Fragment>
                        <Form>
                          <Form.Row className="justify-content-center">
                            <Form.Group as={Col}>
                              <Form.Control placeholder="First Name" onChange={(event) => { this.setState({ first_name: event.target.value, }); }} />
                            </Form.Group>

                            <Form.Group as={Col}>
                              <Form.Control placeholder="Last Name" onChange={(event) => { this.setState({ last_name: event.target.value, }); }} />
                            </Form.Group>
                          </Form.Row>
                          <Form.Group controlId="formGridEmail">
                            <Form.Control type="email" placeholder="Enter email" onChange={(event) => { this.setState({ email: event.target.value, }); }} />
                          </Form.Group>
                          <Form.Row className="justify-content-center">
                            <Form.Group as={Col} controlId="formGridPassword" >
                              <Form.Control type="password" placeholder="Password" onChange={(event) => { this.setState({ password: event.target.value, }); }} />
                            </Form.Group>

                            <Form.Group as={Col} controlId="formGridPassword" >
                              <Form.Control type="password" placeholder="Confirm" onChange={(event) => { this.setState({ compass: event.target.value, }); }} />
                            </Form.Group>
                          </Form.Row>
                          <Button variant="dark" onClick={() => { this.register(); }} block >
                            Sign Up
                              </Button>
                          <p className="text-center">
                            Already Registered?{" "}
                            <a onClick={this.onOpenLoginModal} className="blue-text" style={{ color: "#219ddb" }} >
                              Login
                                </a>
                          </p>
                        </Form>
                      </Fragment>
                    </Row>
                  </Col>
                </Row>
              </Container>
            </Modal.Body>
          </Modal>
          <Footer history={this.props.history} />
        </Router>
      </>
    );
  }
}

export default Landing;
