import React, { Component, Suspense, lazy } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "../src/style/styles.css";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  withRouter,
  Redirect
} from "react-router-dom";
import "./App.css";
const  Landing = lazy (() => import("../src/component/Landing"))
const  User = lazy (() => import("../src/component/User/User"))
const  Instructor = lazy (() => import("../src/component/Instructor/Instructor"))
const  Create = lazy (() => import("../src/component/User/Create-User"))
const  CreateInstructor = lazy (() => import("../src/component/Instructor/CreateInstructor"))
const  UserEdit = lazy (() => import("../src/component/User/Edit"))
const  Dashboard = lazy (() => import("../src/component/Dashboard/Dashboard"))
const  Earnings = lazy (() => import("../src/component/Dashboard/Earnings"))
const  DashboardCat = lazy (() => import("../src/component/Dashboard/Admin-Category-Dashboard"))
const  DashboardCourse = lazy (() => import("../src/component/Dashboard/Admin-Course-Dashboard"))
const  Role = lazy (() => import("../src/component/Role/Role"))
const  CreateRole = lazy (() => import("../src/component/Role/Create-Role"))
const  EditRole = lazy (() => import("../src/component/Role/Edit-Role"))
const  Category = lazy (() => import("./component/Category/Category"))
const  Upload = lazy (() => import("./component/Upload/upload"))
const  TestVideo = lazy (() => import("./component/Upload/testVideo"))
const  Confirm = lazy (() => import("./component/Email/Confirm"))
const  Course = lazy (() => import("./component/Course/Course"))
const  AddCourse = lazy (() => import("./component/Course/Create-Course"))
const  CourseAll = lazy (() => import("./component/Course/CourseAll"))
const  DetailCourse = lazy (() => import("./component/Course/Detail-Course"))
const  MyCourses = lazy (() => import("./component/Courses/MyCourses"))
const  Courses = lazy (() => import("./component/Courses/Courses"))
const  NewCourse = lazy (() => import("./component/Courses/NewCourse"))
const  NewCourseCat = lazy (() => import("./component/Courses/NewCourseCat"))
const  SpecialOffer = lazy (() => import("./component/Courses/SpecialOffer"))
const  SpecialOfferCat = lazy (() => import("./component/Courses/SpecialOfferCat"))
const  BestSelling = lazy (() => import("./component/Courses/BestSelling"))
const  BestSellingCat = lazy (() => import("./component/Courses/BestSellingCat"))
const  UserDetailCourses = lazy (() => import("./component/Courses/UserDetailCourses"))
const  EditCourse = lazy (() => import("./component/Course/Edit-Course"))
const  Detail = lazy (() => import("../src/component/User/Detail-User"))
const  Profile = lazy (() => import('./component/User/Profile'))
const  ForgotPassword = lazy (() => import('../src/component/ForgotPassword/ForgotPassword'))
const  FindAccount = lazy (() => import('../src/component/ForgotPassword/FindAccount'))
const  GetToken = lazy (() => import('../src/component/ForgotPassword/GetToken'))
const  ResetPassword = lazy (() => import('../src/component/ForgotPassword/ResetPassword'))
const  ShoppingChart = lazy (() => import('../src/component/ShoppingChart/ShoppingChart'))
const  UserCourses = lazy (() => import("./component/Courses/User-Courses"))
const  Exam = lazy (() => import('./component/Exam/Exam'))
const  ReviewCourses = lazy (() => import('./component/Reviews/ReviewCourses'))
const  RateCourse = lazy (() => import('./component/Reviews/RateCourse'))
const  Result = lazy (() => import('./component/Exam/Result'))
const  AddPayment = lazy (() => import("./component/Payment/AddPayment"))
const  Payment = lazy (() => import("./component/Payment/Payment"))
const  EditPayment = lazy (() => import("./component/Payment/EditPayment"))
const  Checkout = lazy (() => import("./component/Payment/Checkout"))
const  Confirmation = lazy (() => import("./component/Payment/Confirmation"))
const  Webinar = lazy(() => import("./component/Webinar/Webinar"))
const  AddWebinar = lazy(() => import("./component/Webinar/Create-Webinar"))
// const EditWebinar = lazy(() => import("./component/Webinar/EditWebinar"))
const  Verification = lazy (() => import("./component/Certificate/Verification"))
const  Webinars = lazy (() => import("./component/UserWebinar/WebinarAll"))
const  UserDetailWebinar = lazy (() => import("./component/UserWebinar/UserDetailWebinar"))
// const  Transaction = lazy (() => import("./component/Transaction/Transaction"))
const Invoice = lazy (() => import('./component/Transaction/Invoice'))
const MyWebinar = lazy (() => import('./component/UserWebinar/MyWebinar'))
const WebinarReviews = lazy (() => import('./component/UserWebinar/WebinarReviews'))
const RateWebinar = lazy (() => import('./component/UserWebinar/RateWebinar'))


class App extends Component {
  render() {
    return (
      <>
        <div className="App">
          <Router>
            <div className="test">
              <Suspense fallback={<></>}>
              <Switch>
                <Route exact path="/" component={Landing}></Route>
                <Route path="/User" component={User}></Route>
                <Route path="/Instructor" component={Instructor}></Route>
                <Route path="/Create" component={Create}></Route>
                <Route path="/CreateInstructor" component={CreateInstructor}></Route>
                <Route path="/Detail/:id" component={Detail}></Route>
                <Route path="/Dashboard" component={Dashboard}></Route>
                <Route path="/Earnings" component={Earnings}></Route>
                <Route path="/DashboardCategory" component={DashboardCat}></Route>
                <Route path="/Role/:page" component={withRouter(Role)}></Route>
                <Route path="/Role" component={Role}></Route>
                <Route path="/AddRole" component={CreateRole}></Route>
                <Route path="/Admin/User/:id" component={UserEdit}></Route>
                <Route path="/Roles/Role/:id" component={EditRole}></Route>
                <Route path="/Delete/Role/:id" component={Role}></Route>
                <Route path="/Category" component={Category}></Route>
                <Route path="/upload" component={Upload}></Route>
                <Route path="/testVideo" component={TestVideo}></Route>
                <Route path="/Course/:id" component={Course}></Route>
                <Route path="/DashboardCourse/:id" component={DashboardCourse}></Route>
                <Route path="/Course" component={Course}>
                  <Redirect to="/Course/0" />
                </Route>
                <Route path="/AddCourse" component={CourseAll}></Route>
                <Route path="/Confirm" component={Confirm}></Route>
                <Route path="/DetailCourse/:id" component={DetailCourse}></Route>
                <Route path="/EditCourse/:id" 
                  render={(props) => (
                    <CourseAll {...props} is_edit />
                  )}
                ></Route>
                <Route path="/Course/Delete/:id" component={Course}></Route>
                <Route path="/MyCourses" component={MyCourses}></Route>
                <Route path="/Courses/:id" component={Courses}></Route>
                <Route path="/NewCourse" component={NewCourse}></Route>
                <Route path="/NewCourseCategory/:id" component={NewCourseCat}></Route>
                <Route path="/SpecialOffer" component={SpecialOffer}></Route>
                <Route path="/SpecialOfferCategory/:id" component={SpecialOfferCat}></Route>
                <Route path="/BestSelling" component={BestSelling}></Route>
                <Route path="/BestSellingCategory/:id" component={BestSellingCat}></Route>
                <Route path="/ViewCourses/:id" component={UserDetailCourses}></Route>
                <Route path="/Profile/:id" component={Profile}></Route>
                <Route path="/ForgotPassword" component={ForgotPassword}></Route>
                <Route path="/FindAccount" component={FindAccount}></Route>
                <Route path="/GetToken" component={GetToken}></Route>
                <Route path="/ResetPassword" component={ResetPassword}></Route>
                <Route path="/ShoppingChart" component={ShoppingChart}></Route>
                <Route path="/DetailCourses/:id" component={UserCourses}></Route>
                <Route path="/Exam" component={Exam}></Route>
                <Route path="/Result" component={Result}></Route>
                <Route path="/ReviewCourses" component={ReviewCourses}></Route>
                <Route path="/RateCourse/:id" component={RateCourse}></Route>
                <Route path="/AddPayment" component={AddPayment}></Route>
                <Route path="/Payment" component={Payment}></Route>
                <Route path="/EditPayment/:id" component={EditPayment}></Route>
                <Route path="/Checkout/:id" component={Checkout}></Route>
                <Route path="/Confirmation" component={Confirmation}></Route>
                <Route path="/Webinar" component={Webinar}></Route>
                <Route path="/AddWebinar" component={AddWebinar}></Route>
                  {/* <Route path="/EditWebinar/:id" component={EditWebinar}></Route> */}
                  <Route path="/EditWebinar/:id" 
                  render={(props) => (
                    <AddWebinar {...props} is_edit />
                  )}
                ></Route>
                <Route path="/Verification" component={Verification}></Route>
                <Route path="/Webinars" component={Webinars}></Route>
                <Route path="/UserDetailWebinar/:id" component={UserDetailWebinar}></Route>
                {/* <Route path="/Transaction" component={Transaction}></Route> */}
                <Route path="/Invoice/:id" component={Invoice}></Route>
                <Route path="/MyWebinar" component={MyWebinar}></Route>
                <Route path="/WebinarReviews" component={WebinarReviews}></Route>
                <Route path="/RateWebinar/:id" component={RateWebinar}></Route>
              </Switch>
              </Suspense>
            </div>
          </Router>
        </div>
      </>
    );
  }
}

export default App;

